﻿CREATE TABLE [dbo].[hrActiveEvent] (
  [idEvent] [int] IDENTITY,
  [tpEvent] [char](1) NOT NULL,
  [dtEvent] [datetime] NOT NULL,
  [NumActive] [int] NOT NULL,
  [StatusEvent] [tinyint] NOT NULL DEFAULT (0),
  [StatusEventEDOKO] [tinyint] NOT NULL DEFAULT (0),
  CONSTRAINT [PK_hrActiveEvent] PRIMARY KEY CLUSTERED ([idEvent]),
  CONSTRAINT [C_hrActiveEvent_StatusEvent] CHECK ([StatusEvent]=(1) OR [StatusEvent]=(0))
)
ON [PRIMARY]
GO

CREATE INDEX [I_hrActiveEvent_status_event]
  ON [dbo].[hrActiveEvent] ([StatusEvent], [tpEvent])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrActiveEvent]
  ADD CONSTRAINT [FK_hrActiveEvent_numActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive])
GO

ALTER TABLE [dbo].[hrActiveEvent]
  ADD CONSTRAINT [FK_hrActiveEvent_tpEvent] FOREIGN KEY ([tpEvent]) REFERENCES [dbo].[rfActiveEventType] ([tpEvent])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Изменения состояния активов для взаимодействия АС ППА и АС "Сибирь"', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор события', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent', 'COLUMN', N'idEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип события', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent', 'COLUMN', N'tpEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время события', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent', 'COLUMN', N'dtEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус события  0 - не обработан, 1 - обработан', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveEvent', 'COLUMN', N'StatusEvent'
GO