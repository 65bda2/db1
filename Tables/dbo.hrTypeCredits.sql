﻿CREATE TABLE [dbo].[hrTypeCredits] (
  [idTypeCredits] [int] IDENTITY,
  [idAcc] [int] NOT NULL,
  [dtBegin] [datetime] NOT NULL,
  [tpCredit] [char](1) NOT NULL,
  [dtEnd] [datetime] NULL,
  [cmCredit] [char](250) NULL,
  CONSTRAINT [PK_hrTypeCredits] PRIMARY KEY CLUSTERED ([idAcc], [dtBegin], [tpCredit])
)
ON [PRIMARY]
GO