﻿CREATE TABLE [dbo].[rfActiveState] (
  [idActiveState] [int] IDENTITY,
  [idParentActiveState] [int] NULL,
  [tpSect] [varchar](4) NOT NULL,
  [CommentState] [varchar](100) NOT NULL,
  [isActual] [bit] NOT NULL CONSTRAINT [DF_rfActiveState_isActual] DEFAULT (1),
  [IsGoodForMAPL] [bit] NOT NULL,
  [NonsystemName] [varchar](150) NULL,
  CONSTRAINT [PK_rfActiveState] PRIMARY KEY CLUSTERED ([idActiveState])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [XAK1rfActiveState]
  ON [dbo].[rfActiveState] ([tpSect])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'(Таблица предназначена для определения соответствия типов разделов в АС ППА и соответсвующих им разделов в смежных системах (Н: АС ДКО).', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи соответствия раздела со смежными системами', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'idActiveState'
GO

DECLARE @value SQL_VARIANT = CAST(N'' AS nvarchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'idParentActiveState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код раздела, соответствующий существующей таблице hrStockSect Состояние ББР и залога (Внесистемный учет)', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'tpSect'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание раздела', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'CommentState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Актуальность записи', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'isActual'
GO

DECLARE @value SQL_VARIANT = CAST(N'' AS nvarchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'IsGoodForMAPL'
GO

DECLARE @value SQL_VARIANT = CAST(N'' AS nvarchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'rfActiveState', 'COLUMN', N'NonsystemName'
GO