﻿CREATE TABLE [dbo].[EDOKO_CtrlCode] (
  [CtrlCode] [char](4) NOT NULL,
  [Annotation] [varchar](255) NULL,
  [Description] [varchar](2000) NULL,
  CONSTRAINT [PK_rEDOKO_CtrlCode] PRIMARY KEY CLUSTERED ([CtrlCode])
)
ON [PRIMARY]
GO