﻿CREATE TABLE [dbo].[hrWaitCheck] (
  [NumActive] [int] NOT NULL,
  [dtBegin] [datetime] NULL,
  [dtEnd] [datetime] NULL,
  [idTransh] [int] NULL,
  [IdWaitCheck] [int] IDENTITY,
  [Reason] [varchar](500) NOT NULL DEFAULT (N'Приостановление рассмотрения документа'),
  [ListDocument] [varchar](500) NULL,
  CONSTRAINT [PK_IdWaitCheck] PRIMARY KEY CLUSTERED ([IdWaitCheck])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrWaitCheck]
  ADD CONSTRAINT [FK_hrWaitCheck_jrActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive])
GO

ALTER TABLE [dbo].[hrWaitCheck]
  ADD CONSTRAINT [FK_hrWaitCheck_jrTransh] FOREIGN KEY ([idTransh]) REFERENCES [dbo].[jrtransh] ([IdTransh])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Приостановка проверки по ативам (для МБК)', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный номер актива', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата приостановления проверки', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата возобновления проверки', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'dtEnd'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный номер транша', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'idTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'IdWaitCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Причины для приостановления рассмотрения актива', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'Reason'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список документов, которые должна предоставить кредитная организация для возобновления рассмотрения актива', 'SCHEMA', N'dbo', 'TABLE', N'hrWaitCheck', 'COLUMN', N'ListDocument'
GO