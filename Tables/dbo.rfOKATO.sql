﻿CREATE TABLE [dbo].[rfOKATO] (
  [CODE] [int] NOT NULL,
  [KOD] [char](11) NOT NULL,
  [NAME_RUS] [varchar](250) NOT NULL,
  CONSTRAINT [PK_rfOKATO] PRIMARY KEY CLUSTERED ([CODE])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'rfOKATO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKATO', 'COLUMN', N'CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'rfOKATO', 'COLUMN', N'KOD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'rfOKATO', 'COLUMN', N'NAME_RUS'
GO