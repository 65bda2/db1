﻿CREATE TABLE [dbo].[EDOKO_ListChangeField] (
  [idField] [int] IDENTITY,
  [NmFieldText] [varchar](255) NOT NULL,
  [NMFieldASPPA] [varchar](50) NOT NULL,
  [NmFieldEDOKO] [varchar](50) NOT NULL,
  [isImportant] [tinyint] NOT NULL,
  [FieldType] [char](1) NOT NULL,
  [tpTable] [varchar](30) NOT NULL,
  [isWork] [tinyint] NOT NULL DEFAULT (1),
  [ChangeView] [varchar](20) NULL,
  CONSTRAINT [PK_EDOKO_ListChangeField] PRIMARY KEY CLUSTERED ([idField])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Реквизиты организации', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор реквизита', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'idField'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текстовое имя реквизита', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'NmFieldText'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя реквизита в АС ППА  Для полей типа dopinf указывается порядковый номер пункта в поле dopinf', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'NMFieldASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя реквизита в ЭДО КО', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'NmFieldEDOKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значимость для проверки (1- значимо, 0 - не значимо)', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'isImportant'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип реквизита S -string D- date, C-currency I-integer F - float', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'FieldType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ORG, ACTIVE', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'tpTable'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак актуальности реквизита (1- актуально, 0 - не актуально, 2 - в АБС КО не передаем)', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'isWork'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид изменения', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ListChangeField', 'COLUMN', N'ChangeView'
GO