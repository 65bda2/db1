﻿CREATE TABLE [dbo].[hub_JrBuhRecord] (
  [idRecord] [int] NOT NULL,
  [RValue] [decimal](25, 2) NULL,
  [idOrg] [int] NOT NULL,
  [idperiod] [int] NOT NULL,
  [OperationDate] [datetime] NULL
)
ON [PRIMARY]
GO