﻿CREATE TABLE [dbo].[hub_hrOrg] (
  [OperationDate] [datetime] NOT NULL,
  [HrStatus] [tinyint] NOT NULL DEFAULT (0),
  [DFROM] [datetime] NULL,
  [idOrg] [int] NULL,
  [INN] [varchar](12) NULL,
  [EGRUL] [varchar](20) NULL,
  [NonResidentID] [varchar](30) NULL,
  [CompanyName] [varchar](255) NOT NULL,
  [CodeOKVED] [int] NULL,
  [KodOKVED] [char](8) NULL,
  [CodeOKOPF] [int] NULL,
  [KODOKOPF] [char](5) NULL,
  [CodeOKFS] [int] NULL,
  [KODOKFS] [char](2) NULL,
  [IsSubject] [tinyint] NOT NULL,
  [tpDopInfo] [char](5) NOT NULL DEFAULT ('FFFFF'),
  [IsConforming] [tinyint] NULL,
  [IsInList] [tinyint] NULL,
  [IsResident] [tinyint] NULL,
  [BusinessQuality] [tinyint] NULL,
  [D_lastRegDate] [smalldatetime] NULL,
  [IsBookKeepingOK] [tinyint] NULL,
  [IsFederalLaw] [tinyint] NULL,
  [AlterComment] [varchar](255) NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('F'),
  [ShortNmORG] [varchar](255) NULL,
  [TpDtReg] [tinyint] NULL,
  [dtBookKeeping] [datetime] NULL,
  [AnotherInf] [int] NULL DEFAULT (0),
  [AnotherInfNote] [varchar](255) NULL,
  [TpCheck] [varchar](5) NULL,
  [nmOrg] [varchar](255) NULL
)
ON [PRIMARY]
GO