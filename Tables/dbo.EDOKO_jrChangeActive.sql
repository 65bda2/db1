﻿CREATE TABLE [dbo].[EDOKO_jrChangeActive] (
  [idChangeActive] [int] IDENTITY,
  [ChangeType] [tinyint] NOT NULL,
  [idGroup_hrPG] [int] NULL,
  [NewValue] [varchar](10) NULL,
  [OldValue] [varchar](10) NULL,
  [IsCreateReport] [tinyint] NOT NULL CONSTRAINT [DF_EDOKO_jrChangeActive_IsCreateReport] DEFAULT (0),
  [DtCreateReport] [smalldatetime] NULL,
  [IsInfractionTime] [tinyint] NOT NULL CONSTRAINT [DF_EDOKO_jrChangeActive_IsInfractionTime] DEFAULT (0),
  [ChangeSum] [numeric](22, 7) NULL,
  [idES] [int] NOT NULL,
  CONSTRAINT [PK_EDOKO_jrChangeActive_1] PRIMARY KEY CLUSTERED ([idChangeActive])
)
ON [PRIMARY]
GO

CREATE INDEX [AK_EDOKO_jrChangeActive]
  ON [dbo].[EDOKO_jrChangeActive] ([idES], [ChangeType])
  ON [PRIMARY]
GO

CREATE INDEX [UI_EDOKO_jrChangeActive_IsCreateReport]
  ON [dbo].[EDOKO_jrChangeActive] ([IsCreateReport], [idChangeActive])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EDOKO_jrChangeActive]
  ADD CONSTRAINT [FK_EDOKO_hrES_EDOKO_jrChangeActive] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал изменений по активу в надзор', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип изменений:  1 - график погашения, 2 - категория качества', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'ChangeType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор группы графика погашения в hrPaymentGrafic , в который записано изменение графика', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'idGroup_hrPG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак формирования отчета: 0 - не сформирован  1 - сформирован 2 - не должен попадать в отчет в надзорный блок', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'IsCreateReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата формирования отчета', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'DtCreateReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак обнаружения нарушения сроков: 0 - Срок не нарушен, 1 - Срок нарушен', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'IsInfractionTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сумма измененний (при изменении графика платежей)', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeActive', 'COLUMN', N'ChangeSum'
GO