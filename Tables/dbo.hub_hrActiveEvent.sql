﻿CREATE TABLE [dbo].[hub_hrActiveEvent] (
  [idEvent] [int] NOT NULL,
  [tpEvent] [char](1) NOT NULL,
  [dtEvent] [datetime] NOT NULL,
  [NumActive] [int] NOT NULL,
  [StatusEvent] [tinyint] NOT NULL DEFAULT (0),
  [StatusEventEDOKO] [tinyint] NOT NULL DEFAULT (0)
)
ON [PRIMARY]
GO