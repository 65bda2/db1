﻿CREATE TABLE [dbo].[hrOrg] (
  [OperationDate] [datetime] NOT NULL,
  [HrStatus] [tinyint] NOT NULL DEFAULT (0),
  [DFROM] [datetime] NULL,
  [idOrg] [int] NULL,
  [INN] [varchar](12) NULL,
  [EGRUL] [varchar](20) NULL,
  [NonResidentID] [varchar](30) NULL,
  [CompanyName] [varchar](255) NOT NULL,
  [CodeOKVED] [int] NULL,
  [KodOKVED] [char](8) NULL,
  [CodeOKOPF] [int] NULL,
  [KODOKOPF] [char](5) NULL,
  [CodeOKFS] [int] NULL,
  [KODOKFS] [char](2) NULL,
  [IsSubject] [tinyint] NOT NULL,
  [tpDopInfo] [char](5) NOT NULL DEFAULT ('FFFFF'),
  [IsConforming] [tinyint] NULL,
  [IsInList] [tinyint] NULL,
  [IsResident] [tinyint] NULL,
  [BusinessQuality] [tinyint] NULL,
  [D_lastRegDate] [smalldatetime] NULL,
  [IsBookKeepingOK] [tinyint] NULL,
  [IsFederalLaw] [tinyint] NULL,
  [AlterComment] [varchar](255) NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('F'),
  [ShortNmORG] [varchar](255) NULL,
  [TpDtReg] [tinyint] NULL,
  [dtBookKeeping] [datetime] NULL,
  [AnotherInf] [int] NULL DEFAULT (0),
  [AnotherInfNote] [varchar](255) NULL,
  [TpCheck] [varchar](5) NULL,
  [nmOrg] AS ([CompanyName]),
  [idhrOrg] [int] IDENTITY,
  [OrgStatus] [varchar](1) NOT NULL DEFAULT ('T'),
  [isReprocess] [varchar](500) NULL
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [I_hrORG]
  ON [dbo].[hrOrg] ([DFROM])
  ON [PRIMARY]
GO

CREATE INDEX [I_hrORG_hrstatus]
  ON [dbo].[hrOrg] ([HrStatus])
  ON [PRIMARY]
GO

CREATE INDEX [I_hrORG_numactive]
  ON [dbo].[hrOrg] ([idOrg])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список юридических лиц/субъектов РФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата операционного дня', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'OperationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус записи: 0 - не прошла обработку, 1 - прошла обработку по удалению дублирующих записей', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'HrStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИНН', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'INN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЕГРЮЛ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'EGRUL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентифифкатор для организации-нерезидента', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'NonResidentID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование юр. лица/субъекта РФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'CompanyName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'CodeOKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'текстовый код ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'KodOKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'CodeOKOPF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'текстовый код ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'KODOKOPF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКФС', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'CodeOKFS'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'текстовый код ОКФС', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'KODOKFS'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак организации', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsSubject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак организации, обязанной по ЭКСАР, Инвестпроектам, ЮГ и т.п.', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'tpDopInfo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак соответствия требованиям БР для субъектов РФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsConforming'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак включенности в перечень БР', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsInList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак резидента РФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsResident'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак субъекта РФ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'BusinessQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата регистрации(перерегистрации, реорганизации)', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'D_lastRegDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'бух.отчетность соответствуют требованиям БР', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Организация образована или реорганизована  в соответствии с ФЗ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'IsFederalLaw'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'комментарий', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'AlterComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак соответствия критериям', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'isSootv'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'   ShortNmORG', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'ShortNmORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'-1 нет иной информации 1- есть и она соотв. критерием 0 -есть и она не соотв. критериям', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'AnotherInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'AS CompanyName', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'nmOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить статус новой организации', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'OrgStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак Возможность повторной обработки NULL - возможность повторной обработки не определена 1 - Доступна повторная обработка 0 - Организация отклонена пользователем, повторная обработка не доступна', 'SCHEMA', N'dbo', 'TABLE', N'hrOrg', 'COLUMN', N'isReprocess'
GO