﻿CREATE TABLE [dbo].[hub_jrNoteActive] (
  [Tag] [int] NOT NULL,
  [DateNote] [datetime] NULL,
  [Note] [varchar](500) NULL,
  [NumActive] [int] NOT NULL,
  [idTransh] [int] NULL
)
ON [PRIMARY]
GO