﻿CREATE TABLE [dbo].[EDOKO_hrChangeDoer] (
  [idChangeDoer] [int] IDENTITY,
  [DQuestion] [smalldatetime] NOT NULL,
  [ChangeStatus] [tinyint] NOT NULL,
  [VerificationUser] [varchar](50) NULL,
  [DtVerification] [smalldatetime] NULL,
  [OldDoer] [varchar](50) NULL,
  [NewDoer] [varchar](50) NOT NULL,
  [idES] [int] NOT NULL,
  CONSTRAINT [PK_EDOKO_hrChangeDoer_1] PRIMARY KEY CLUSTERED ([idChangeDoer])
)
ON [PRIMARY]
GO

CREATE INDEX [AK_EDOKO_hrChangeDoer]
  ON [dbo].[EDOKO_hrChangeDoer] ([idES], [DQuestion])
  ON [PRIMARY]
GO

CREATE INDEX [UI_EDOKO__hrChangeDoer_EDFileName]
  ON [dbo].[EDOKO_hrChangeDoer] ([idChangeDoer])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EDOKO_hrChangeDoer]
  ADD CONSTRAINT [FK_EDOKO_hrES_EDOKO_hrChangeDoer] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал изменений ответственных лиц по ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата запроса на смену исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'DQuestion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0-требуется подтверждение смены исполнителя, 1 - смена исполнителя подтверждена, 2 - принят исполнителем на себя, 3 -  смена исполнителя отклонена', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'ChangeStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь подтвердивший изменения', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'VerificationUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата подтверждения изменения', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'DtVerification'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Старый пользователь', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'OldDoer'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Новый пользователь', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrChangeDoer', 'COLUMN', N'NewDoer'
GO