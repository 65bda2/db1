﻿CREATE TABLE [dbo].[jrCurUsersASPPA] (
  [nmRealUser] [char](50) NOT NULL,
  [nmSysUser] [char](50) NOT NULL DEFAULT (suser_sname()),
  [hostName] [char](50) NOT NULL DEFAULT (host_name()),
  [spid] [smallint] NOT NULL DEFAULT (@@spid),
  [dtLogUser] [datetime] NOT NULL DEFAULT (getdate()),
  CONSTRAINT [PK_jrCurUsersASPPA] PRIMARY KEY CLUSTERED ([nmRealUser], [hostName])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата и время входа пользователя', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'реальное имя пользователя базы данных', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA', 'COLUMN', N'nmRealUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'системное имя пользователя базы данных', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA', 'COLUMN', N'nmSysUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'workstation name', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA', 'COLUMN', N'hostName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'SQL Server process ID for sysUser', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA', 'COLUMN', N'spid'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата и время входа пользователя', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsersASPPA', 'COLUMN', N'dtLogUser'
GO