﻿CREATE TABLE [dbo].[hub_jrRequestDocActive] (
  [dtBegin] [datetime] NOT NULL,
  [RequestStatus] [char](1) NOT NULL,
  [dtControl] [datetime] NULL,
  [TpRequest] [char](1) NOT NULL,
  [dtEnd] [datetime] NULL,
  [Numactive] [int] NOT NULL,
  [NumRequest] [int] NOT NULL,
  [Comment] [varchar](500) NULL
)
ON [PRIMARY]
GO