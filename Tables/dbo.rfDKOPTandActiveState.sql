﻿CREATE TABLE [dbo].[rfDKOPTandActiveState] (
  [idRecord] [int] IDENTITY,
  [idActiveStateTo] [int] NOT NULL,
  [IdDKOPartitionTypeTo] [int] NOT NULL,
  [OperType] [tinyint] NOT NULL,
  [ToolCode] [char](1) NULL,
  [RowComment] [varchar](255) NULL,
  [IdActiveStateFrom] [int] NULL,
  [IdDKOPartitionTypeFrom] [int] NULL,
  [InitiativeType] [tinyint] NOT NULL CONSTRAINT [DF_rfDKOPTandActiveState_InitiativeType] DEFAULT (1),
  [Kategory] [char](2) NULL,
  [tpEvent] [char](1) NULL,
  [ActiveStatus] [char](1) NULL,
  [IS_AssetState_IN201] [tinyint] NULL,
  CONSTRAINT [PK_rfDKOPTandActiveState] PRIMARY KEY CLUSTERED ([idRecord]),
  CONSTRAINT [CK_rfDKOPTandActiveState_IS_AssetState_IN201] CHECK ([IS_AssetState_IN201]>=(0) AND [IS_AssetState_IN201]<=(1))
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[rfDKOPTandActiveState]
  ADD CONSTRAINT [FK_rfDKOPTandActiveState_rfActiveEventType] FOREIGN KEY ([tpEvent]) REFERENCES [dbo].[rfActiveEventType] ([tpEvent])
GO

ALTER TABLE [dbo].[rfDKOPTandActiveState]
  ADD CONSTRAINT [FK_rfDKOPTandActiveState_rfActiveState_from] FOREIGN KEY ([IdActiveStateFrom]) REFERENCES [dbo].[rfActiveState] ([idActiveState])
GO

ALTER TABLE [dbo].[rfDKOPTandActiveState]
  ADD CONSTRAINT [FK_rfDKOPTandActiveState_rfActiveStatus] FOREIGN KEY ([ActiveStatus]) REFERENCES [dbo].[rfActiveStatus] ([status])
GO

ALTER TABLE [dbo].[rfDKOPTandActiveState]
  ADD CONSTRAINT [FK_rfDKOPTandActiveState_rfDKOPartitionType_from] FOREIGN KEY ([IdDKOPartitionTypeFrom]) REFERENCES [dbo].[rfDKOPartitionType] ([IdDKOPartitionType])
GO

ALTER TABLE [dbo].[rfDKOPTandActiveState]
  ADD CONSTRAINT [FK_rfDKOPTandActiveState_rfJournalActiveKategory] FOREIGN KEY ([Kategory]) REFERENCES [dbo].[rfJournalActiveKategory] ([Kategory])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица соответствия переводов обеспечения при провоедении операций в АС Сибирь и АС ДКО', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'idRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификтор вида раздела, в который производится перевод. Ссылка на rfActiveState.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'idActiveStateTo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи вида раздела АС ДКО, в который производится перевод.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'IdDKOPartitionTypeTo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид заявления, на основании которого проводится операция', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'OperType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код инструмента кредитования в номере кредита (10-й символ):', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'ToolCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'RowComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификтор вида раздела, из которого производится перевод. Ссылка на rfActiveState.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'IdActiveStateFrom'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи вида раздела АС ДКО, из которого производится перевод.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'IdDKOPartitionTypeFrom'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид инициативы по проведению операции', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'InitiativeType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория события. Ссылка на таблицу rfJournalActiveKategory.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'Kategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код события. Ссылка на таблицу rfActiveEventType.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'tpEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код статуса актива.  Ссылка на таблицу rfActiveStatus.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'ActiveStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак, указывающий требуется ли включать данные по активу в исходящую DKO201 в сущность AssetState, направляемую хабом. 
	Возможные значения «1» - включать движение в DKO201, «0» - не включать движение в DKO201', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPTandActiveState', 'COLUMN', N'IS_AssetState_IN201'
GO