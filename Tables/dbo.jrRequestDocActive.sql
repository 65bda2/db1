﻿CREATE TABLE [dbo].[jrRequestDocActive] (
  [dtBegin] [datetime] NOT NULL,
  [RequestStatus] [char](1) NOT NULL,
  [dtControl] [datetime] NULL,
  [TpRequest] [char](1) NOT NULL,
  [dtEnd] [datetime] NULL,
  [Numactive] [int] NOT NULL,
  [NumRequest] [int] NOT NULL,
  [Comment] [varchar](500) NULL,
  CONSTRAINT [PK_jrRequestDocActive] PRIMARY KEY CLUSTERED ([dtBegin], [NumRequest]),
  CONSTRAINT [C_jrRequestDocActive_RequestStatus] CHECK ([RequestStatus]='L' OR [RequestStatus]='A' OR [RequestStatus]='F' OR [RequestStatus]='T' OR [RequestStatus]='S' OR [RequestStatus]='R'),
  CONSTRAINT [C_jrRequestDocActive_TpRequest] CHECK ([TpRequest]='M' OR [TpRequest]='H' OR [TpRequest]='O')
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[jrRequestDocActive]
  ADD CONSTRAINT [FK_jrRequestDocActive_Numactive] FOREIGN KEY ([Numactive]) REFERENCES [dbo].[jrActive] ([NumActive]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица запроса документов у КО об активе', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала действия запроса', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус запроса: R - сформирован, S - послан в КО,  T - успешно обработан(данные получены), F - отрицательный ответ КО(документы не получены)   , A - аннулирован', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'RequestStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Контрольная дата', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'dtControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип запроса: M - Запрос документов по МСП, H - запрос документов по ста миллиардам, O - Запрос оригиналов КД', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'TpRequest'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания работы с запросом', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'dtEnd'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'Numactive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер в течении дня (при заполнении выбирается максимальный в разрезе dtBegin)', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'NumRequest'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'jrRequestDocActive', 'COLUMN', N'Comment'
GO