﻿CREATE TABLE [dbo].[EDOKO_jrChangeORG] (
  [idChangeOrg] [int] IDENTITY,
  [idField] [int] NOT NULL,
  [idORG] [int] NOT NULL,
  [ChangeStatus] [char](1) NOT NULL,
  [CurrentValue] [varchar](500) NULL,
  [NewValue] [varchar](500) NOT NULL,
  [Annotation] [varchar](255) NULL,
  [idES] [int] NOT NULL,
  [idESProcess] [int] NULL,
  CONSTRAINT [PK_EDOKO_jrChangeORG] PRIMARY KEY CLUSTERED ([idChangeOrg])
)
ON [PRIMARY]
GO

CREATE INDEX [AK_EDOKO_jrChangeORG]
  ON [dbo].[EDOKO_jrChangeORG] ([idES], [idField], [idORG])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EDOKO_jrChangeORG]
  ADD CONSTRAINT [FK_EDOKO_hrES_EDOKO_jrChangeORG] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

ALTER TABLE [dbo].[EDOKO_jrChangeORG]
  ADD CONSTRAINT [FK_EDOKO_hrESidESProcess_EDOKO_jrChangeORGidES] FOREIGN KEY ([idESProcess]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал изменений по организации', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи из таблицы ListFieldjrORG', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'idField'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор организации', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'idORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус изменения: T-принято, F-не принято, R - на рассмотрении, N - значения реквизитов из С101', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'ChangeStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение реквизита в текстовом формате', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'CurrentValue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'новое значение реквизита в текстовом формате', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'NewValue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Основание отказа', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_jrChangeORG', 'COLUMN', N'Annotation'
GO