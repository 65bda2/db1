﻿CREATE TABLE [dbo].[rfDKOPartitionType] (
  [IdDKOPartitionType] [int] IDENTITY,
  [PartitionType] [char](2) NOT NULL,
  [Name] [varchar](20) NOT NULL,
  CONSTRAINT [PK_rfDKOPartitionType] PRIMARY KEY CLUSTERED ([IdDKOPartitionType])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UI_rfDKOPartitionType_PartitionType]
  ON [dbo].[rfDKOPartitionType] ([PartitionType])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'справочником видов разделов используемых в обмене с АС ДКО.', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPartitionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPartitionType', 'COLUMN', N'IdDKOPartitionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код вида раздела в соответствии с альбомом УФЭБС ПЦА – ТУ [2]', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPartitionType', 'COLUMN', N'PartitionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование вида раздела. Возможные значения приведены в Приложении В, раздел B1.1.3, альбома УФЭБС ПЦА - ТУ', 'SCHEMA', N'dbo', 'TABLE', N'rfDKOPartitionType', 'COLUMN', N'Name'
GO