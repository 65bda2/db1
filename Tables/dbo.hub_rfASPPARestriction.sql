﻿CREATE TABLE [dbo].[hub_rfASPPARestriction] (
  [idKod] [int] NOT NULL,
  [DownKod] [char](11) NOT NULL,
  [UpKod] [char](11) NOT NULL,
  [dtBegin] [datetime] NULL,
  [dtend] [datetime] NULL
)
ON [PRIMARY]
GO