﻿CREATE TABLE [dbo].[rfBuhFactorType] (
  [FSymbol] [char](2) NOT NULL,
  [FNumber] [varchar](2) NOT NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список периодов отчетности', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactorType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор периода отчетности', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactorType', 'COLUMN', N'FSymbol'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование периода', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactorType', 'COLUMN', N'FNumber'
GO