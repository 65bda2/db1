﻿CREATE TABLE [dbo].[rfOKFS] (
  [CODE] [int] NOT NULL,
  [KOD] [char](2) NOT NULL,
  [NAME_RUS] [varchar](250) NOT NULL,
  CONSTRAINT [PK_rfOKFS] PRIMARY KEY CLUSTERED ([CODE])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник форм собственности', 'SCHEMA', N'dbo', 'TABLE', N'rfOKFS'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKFS', 'COLUMN', N'CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ОКФС', 'SCHEMA', N'dbo', 'TABLE', N'rfOKFS', 'COLUMN', N'KOD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование ОКФС', 'SCHEMA', N'dbo', 'TABLE', N'rfOKFS', 'COLUMN', N'NAME_RUS'
GO