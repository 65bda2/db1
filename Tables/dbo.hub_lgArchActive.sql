﻿CREATE TABLE [dbo].[hub_lgArchActive] (
  [idArhActive] [int] NOT NULL,
  [Dateit] [datetime] NULL,
  [RekvOrg] [varchar](500) NULL,
  [CheckP36] [char](15) NULL,
  [CheckP35] [char](15) NULL,
  [Cost] [varchar](100) NULL,
  [Kategory] [char](10) NULL,
  [Kurs] [char](20) NULL,
  [Koef] [char](10) NULL,
  [costrub] [varchar](100) NULL,
  [SavePlace] [varchar](200) NULL,
  [status] [varchar](2048) NULL,
  [DSign1] [varchar](150) NULL,
  [DSign2] [varchar](150) NULL,
  [FIo1] [varchar](100) NULL,
  [FIO2] [varchar](100) NULL,
  [NumActive] [int] NOT NULL,
  [Numorder] [int] NULL,
  [Komment] [varchar](150) NULL,
  [happen] [char](1) NULL,
  [NmCurrency] [varchar](20) NULL
)
ON [PRIMARY]
GO