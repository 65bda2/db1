﻿CREATE TABLE [dbo].[jrNoteActive] (
  [Tag] [int] NOT NULL,
  [DateNote] [datetime] NULL,
  [Note] [varchar](1000) NULL,
  [NumActive] [int] NOT NULL,
  [idTransh] [int] NULL,
  [IdNoteActive] [int] IDENTITY,
  [IsCreateTag] [tinyint] NULL,
  CONSTRAINT [PK_jrNoteActive] PRIMARY KEY CLUSTERED ([IdNoteActive])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrNoteActive]
ON [dbo].[jrNoteActive]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(Tag AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), DateNote, 104)+', '+
  RTRIM(Note)+', '+CAST(NumActive AS VARCHAR(10)) FROM deleted

  SELECT @newValues = CAST(Tag AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), DateNote, 104)+', '+
  RTRIM(Note)+', '+CAST(NumActive AS VARCHAR(10)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Замечания по ответам на вопросы', 
   'Tэг, Дата замечания, Текст замечания, идентификатор актива', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Замечания по ответам на вопросы', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Tэг', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'Tag'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата замечания', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'DateNote'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст замечания', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор транша', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'idTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак создания информации о рисках', 'SCHEMA', N'dbo', 'TABLE', N'jrNoteActive', 'COLUMN', N'IsCreateTag'
GO