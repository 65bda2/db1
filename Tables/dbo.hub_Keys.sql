﻿CREATE TABLE [dbo].[hub_Keys] (
  [idTUKey] [int] NULL,
  [idHubKey] [int] NULL,
  [NameTable] [varchar](25) NULL,
  [TU] [varchar](2) NULL,
  [isReplace] [bit] NULL
)
ON [PRIMARY]
GO