﻿CREATE TABLE [dbo].[hub_DKO_hrRepayment] (
  [IdRecord] [bigint] NOT NULL,
  [_IdRecord] [int] NOT NULL,
  [IdRepayment] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [IdAsset] [numeric](16) NOT NULL,
  [D_Payment] [smalldatetime] NOT NULL,
  [Amount] [numeric](22, 7) NOT NULL,
  [AlterComment] [varchar](500) NULL,
  [IdAssetIdRecord] [bigint] NOT NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL
)
ON [PRIMARY]
GO