﻿CREATE TABLE [dbo].[rfDays] (
  [dtDay] [datetime] NOT NULL,
  [tpDay] [char](1) NOT NULL,
  [cmDay] [char](100) NULL,
  CONSTRAINT [PK_rfDays] PRIMARY KEY CLUSTERED ([dtDay]),
  CONSTRAINT [C_rfDays_tpDay] CHECK ([tpDay]='W' OR [tpDay]='F')
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfDays]
ON [dbo].[rfDays]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtDay, 104)+', '+tpDay+', '+RTRIM(cmDay) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtDay, 104)+', '+tpDay+', '+RTRIM(cmDay) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 'Календарь рабочих и выходных дней', 'дата, тип записи, основание', @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'список нестандартных рабочих и выходных дней', 'SCHEMA', N'dbo', 'TABLE', N'rfDays'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата', 'SCHEMA', N'dbo', 'TABLE', N'rfDays', 'COLUMN', N'dtDay'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип записи (F - выходной, W - рабочий)', 'SCHEMA', N'dbo', 'TABLE', N'rfDays', 'COLUMN', N'tpDay'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Основание', 'SCHEMA', N'dbo', 'TABLE', N'rfDays', 'COLUMN', N'cmDay'
GO