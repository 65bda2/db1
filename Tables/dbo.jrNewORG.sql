﻿CREATE TABLE [dbo].[jrNewORG] (
  [idNewOrg] [int] IDENTITY,
  [idOrg] [int] NOT NULL,
  [dtcheck] [datetime] NULL,
  [nmUser] [varchar](500) NULL,
  [idES] [int] NOT NULL,
  CONSTRAINT [PK_jrNewORG_1] PRIMARY KEY CLUSTERED ([idNewOrg])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[jrNewORG]
  ADD CONSTRAINT [FK_EDOKO_hrES_jrNewORG] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

ALTER TABLE [dbo].[jrNewORG]
  ADD CONSTRAINT [FK_jrNewOrg_2_jrOrg] FOREIGN KEY ([idOrg]) REFERENCES [dbo].[jrORG] ([idOrg])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список новых организаций, поступивших  в С101 и С102', 'SCHEMA', N'dbo', 'TABLE', N'jrNewORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор периода отчетности', 'SCHEMA', N'dbo', 'TABLE', N'jrNewORG', 'COLUMN', N'idOrg'
GO