﻿CREATE TABLE [dbo].[hub_jrTransh] (
  [IdTransh] [int] NOT NULL,
  [Cost] [money] NOT NULL,
  [NomerVD] [varchar](100) NULL,
  [CreationDate] [datetime] NOT NULL,
  [NumActive] [int] NOT NULL,
  [PaymentDate] [datetime] NOT NULL,
  [Status] [char](1) NOT NULL,
  [Dopinf] [char](25) NOT NULL,
  [IdLetter] [int] NOT NULL,
  [BeginDate] [datetime] NULL,
  [inclDate] [datetime] NULL,
  [IsOriginal] [char](1) NULL DEFAULT ('F'),
  [AlterComment] [varchar](500) NULL
)
ON [PRIMARY]
GO