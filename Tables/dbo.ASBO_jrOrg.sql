﻿CREATE TABLE [dbo].[ASBO_jrOrg] (
  [INN] [varchar](12) NULL,
  [EGRUL] [varchar](20) NULL,
  [OKPO] [varchar](8) NULL,
  [CompanyName] [varchar](255) NOT NULL,
  [CodeOKVED] [varchar](8) NULL,
  [CodeOKOPF] [varchar](15) NULL,
  [IsSubject] [tinyint] NULL,
  [IsConforming] [tinyint] NULL,
  [IsInList] [tinyint] NULL,
  [IsResident] [tinyint] NULL,
  [BusinessQuality] [tinyint] NULL,
  [D_lastRegDate] [smalldatetime] NULL,
  [IsBookKeepingOK] [tinyint] NULL,
  [belonging] [tinyint] NULL,
  [dtOgrn] [datetime] NULL,
  [CodeOKATO] [char](11) NULL,
  [ShortNmORG] [varchar](255) NULL,
  [TpDtReg] [tinyint] NULL,
  [NmLaw] [varchar](255) NULL,
  [AnotherInf] [int] NULL DEFAULT (0),
  [AnotherInfNote] [varchar](255) NULL,
  [Rating] [varchar](255) NULL,
  [nmAddr] [varchar](255) NULL,
  [CodeTU] [varchar](255) NULL,
  [DtReport] [datetime] NULL,
  [DtBegin] [datetime] NULL,
  [DtEnd] [datetime] NULL,
  [idES] [bigint] NULL,
  [AlterComment] [varchar](255) NULL,
  [nmOKVED] [varchar](4000) NULL,
  [nmOKOPF] [varchar](200) NULL,
  [IsCheck] [int] NULL DEFAULT (0),
  [NoPresentReport] [int] NULL,
  [AddressMassReg] [int] NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_JrOrg_EGRUL]
  ON [dbo].[ASBO_jrOrg] ([EGRUL])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Централизованный справочник организаций', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИНН', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'INN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЕГРЮЛ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'EGRUL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'OKPO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование юр. лица/субъекта РФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'CompanyName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'CodeOKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'CodeOKOPF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак организации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'IsSubject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак соответствия требованиям БР для субъектов РФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'IsConforming'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак включенности в перечень БР', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'IsInList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак резидента РФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'IsResident'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак субъекта РФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'BusinessQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата регистрации(перерегистрации, реорганизации)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'D_lastRegDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'бух.отчетность соответствуют требованиям БР', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'IsBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - государственное 1-муниципальное', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'belonging'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - нет информации о наличии рисков; 1 - информация есть и она не соответствует критериям; 2 - информация есть и она соответствует критериям;', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'AnotherInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'адрес', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_jrOrg', 'COLUMN', N'nmAddr'
GO