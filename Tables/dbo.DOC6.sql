﻿CREATE TABLE [dbo].[DOC6] (
  [CO_Name] [varchar](500) NOT NULL,
  [CO_CREGN] [varchar](50) NOT NULL,
  [ASSET_ID] [varchar](16) NOT NULL,
  [INCL_DATE] [datetime] NOT NULL,
  [ASSET_TYPE] [varchar](100) NOT NULL,
  [BORR_NAME] [varchar](500) NOT NULL,
  [ORG_NAME] [varchar](500) NOT NULL,
  [ORG_LIST] [varchar](50) NOT NULL,
  [ASSET_VALUE] [money] NOT NULL,
  [ASSET_CURR] [varchar](50) NOT NULL,
  [BILL_FVALUE] [money] NULL,
  [ASSET_QUALITY] [char](1) NOT NULL,
  [MATURITY_DATE] [datetime] NOT NULL,
  [MATURITY_SUM] [money] NOT NULL,
  [EX_RATE] [char](10) NULL,
  [DOG_NUM] [char](100) NULL,
  [DOG_DATE] [datetime] NULL,
  [COEF] [decimal](12, 5) NOT NULL,
  [Idcolumn] [int] IDENTITY
)
ON [PRIMARY]
GO