﻿CREATE TABLE [dbo].[ASBO_CheckLog] (
  [idCheckLog] [bigint] IDENTITY,
  [idES] [bigint] NOT NULL,
  [idBlockES] [int] NULL,
  [mess] [varchar](500) NOT NULL,
  [code] [varchar](500) NULL,
  [status] [int] NOT NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'протокол контроля для ИЭС1 и ИЭС2', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'idCheckLog'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор блока данных ЭС для ИЭС2', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'idBlockES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'сообщение', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'mess'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код сообщения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'code'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус  0 -сообщение 1 -предупреждение 2-ошибка', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_CheckLog', 'COLUMN', N'status'
GO