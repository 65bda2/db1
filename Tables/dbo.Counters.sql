﻿CREATE TABLE [dbo].[Counters] (
  [NameCounter] [varchar](48) NULL,
  [ValueCounter] [int] NULL
)
ON [PRIMARY]
GO