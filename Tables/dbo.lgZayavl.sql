﻿CREATE TABLE [dbo].[lgZayavl] (
  [dtPostup] [datetime] NOT NULL,
  [tmPostup] [char](5) NULL,
  [nuReg] [int] NOT NULL,
  [idBlock] [int] NOT NULL,
  [dtZayavl] [datetime] NOT NULL,
  [nuZayavl] [char](20) NOT NULL,
  [tpZayavl] [char](1) NULL DEFAULT ('S'),
  [tpRazdTo] [char](1) NULL,
  [nuRazdTo] [char](17) NULL,
  [cdStock] [int] IDENTITY,
  [idLimit] [int] NULL,
  [isCorrectForm] [char](1) NULL,
  [isCorrectSign] [char](1) NULL,
  [isCorrectTrust] [char](1) NULL,
  [dtSendDocum] [datetime] NULL,
  [dtControl] [datetime] NULL,
  [cmZayavl] [varchar](500) NULL,
  [dtDocument11] [datetime] NULL,
  [nuDocument11] [char](20) NULL,
  [isEDOKO] [char](1) NULL DEFAULT ('F'),
  [vsPorNum] [char](20) NULL,
  [vsPorDate] [datetime] NULL,
  [tpVsPor] [int] NULL,
  [dtIspoln] [datetime] NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL,
  CONSTRAINT [PK_lgZayavl] PRIMARY KEY CLUSTERED ([dtPostup], [nuReg], [idBlock]),
  CONSTRAINT [C_lgZayavl_isCorrectForm] CHECK ([isCorrectForm]='F' OR ([isCorrectForm]='T' OR [isCorrectForm]='')),
  CONSTRAINT [C_lgZayavl_isCorrectSign] CHECK ([isCorrectSign]='F' OR ([isCorrectSign]='T' OR [isCorrectSign]='')),
  CONSTRAINT [C_lgZayavl_isCorrectTrust] CHECK ([isCorrectTrust]='F' OR ([isCorrectTrust]='T' OR [isCorrectTrust]='')),
  CONSTRAINT [C_lgZayavl_isEDOKO] CHECK ([isEDOKO]='F' OR [isEDOKO]='T'),
  CONSTRAINT [C_lgZayavl_tpRazdTo] CHECK ([tpRazdTo]='M' OR [tpRazdTo]='B'),
  CONSTRAINT [C_lgZayavl_tpVsPor] CHECK ([tpVsPor]=(510) OR [tpVsPor]=(100) OR [tpVsPor]=(1)),
  CONSTRAINT [C_lgZayavl_tpZayavl] CHECK ([tpZayavl]='F' OR ([tpZayavl]='T' OR ([tpZayavl]='P' OR [tpZayavl]='S')))
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UI_lgZayavl_cdStock]
  ON [dbo].[lgZayavl] ([cdStock])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLgZayavl] ON [dbo].[lgZayavl] AFTER INSERT
AS
   IF (SELECT vsPorNum FROM inserted) IS NULL
     INSERT INTO DKO_IdUpdate (TypeDKO, IdObject) SELECT 'DKO510', cdStock FROM inserted 
   ELSE
     INSERT INTO DKO_IdUpdate (TypeDKO, IdObject) SELECT 'DKO511', cdStock FROM inserted
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogLgZayavl]
ON [dbo].[lgZayavl]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL  SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtPostup, 104)+', '+tmPostup+', '+CAST(nuReg AS VARCHAR(20))+', '+
   CAST(idBlock AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtZayavl, 104)+', '+RTRIM(nuZayavl)+', '+
   tpZayavl+', '+tpRazdTo+', '+nuRazdTo+', '+CAST(cdStock AS VARCHAR(20)) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtPostup, 104)+', '+tmPostup+', '+CAST(nuReg AS VARCHAR(20))+', '+
   CAST(idBlock AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtZayavl, 104)+', '+RTRIM(nuZayavl)+', '+
   tpZayavl+', '+tpRazdTo+', '+nuRazdTo+', '+CAST(cdStock AS VARCHAR(20)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал регистрации заявлений КО на перевод активов', 
   'дата поступления заявления, время поступления заявления, рег.номер заявления, идентификатор раздела ББР, дата заявления КО, номер заявления КО, тип раздела, из которого переводятся ЦБ, тип раздела, в который переводятся ЦБ, ссылка на список отобранных бумаг', 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[lgZayavl]
  ADD CONSTRAINT [FK_lgZayavl_idLimit] FOREIGN KEY ([idLimit]) REFERENCES [dbo].[lgOrder] ([idOrder])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал регистрации заявлений КО на вывод ценных бумаг/активов/слитков золота', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата поступления заявления КО', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtPostup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'время поступления заявления КО (учитывается только для 236, 312, КД МБК)', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'tmPostup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер заявления КО', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'nuReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор ББР', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'idBlock'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата заявления КО', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtZayavl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер заявления КО', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'nuZayavl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус Заявления/Ходатайства
F - не исполнено/сформирован отказ
P - рассмотрение приостановлено(для 362-П)
S - на исполнении/на рассмотрении
T - исполнено/слитки выведены из пула', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'tpZayavl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип раздела счета депо, в который переводятся ценные бумаги
B - Раздел Блокировано Банком России для 236-П или Раздел 1 "Активы, принятые в целях обеспечения возможности получения банком кредитов для" 312-П и 362-П
M - Основной Раздел для 236П или возврат банку для 312-П и 362-П', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'tpRazdTo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер раздела ББР, если выбран тип раздела B или Основной, если тип раздела M(236-П)', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'nuRazdTo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на список отобранных бумаг', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'cdStock'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на соответствующий лимит после разблокировки (lgOrder)', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'idLimit'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ходатайство составлено по утвержденной форме', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'isCorrectForm'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ходатайство подписано уполномоченным лицом / подпись (печать) соответствует образцу, имеющемуся в ТУ', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'isCorrectSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Доверенность на возврат слитков кредитной организации и подписание акта приема-передачи имеется и оформлена надлежащим образом', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'isCorrectTrust'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата досылки документов', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtSendDocum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Контрольная дата рассмотрения ходатайства', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий. причина неисполнения заявления, в т.ч. из док-та 11 «Отказ в разблокировке (высвобождении) ценных бумаг»', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'cmZayavl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата документа 11 «Отказ в разблокировке (высвобождении) ценных бумаг»', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtDocument11'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер документа 11 «Отказ в разблокировке (высвобождении) ценных бумаг»', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'nuDocument11'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак просьбы КО о выводе активов (ЭДО КО-ТУ)', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'isEDOKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер встречного поручения', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'vsPorNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата встречного поручения', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'vsPorDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип встречного поручения: 1 - Встречное поручение принято к исполнению в СЭД, 100 - ЭС DKO100 отправлено в СЭД (встречное поручение отклонено в XML-обмене или отправлен ответ после обработки в АС "Сибирь"), 510 - ЭС DKO510 отправлено в СЭД
1 - Встречное поручение принято к исполнению в СЭД
100 - ЭС DKO100 отправлено в СЭД (встречное поручение отклонено в XML-обмене или отправлен ответ после обработки в АС "Сибирь")
510 - ЭС DKO510 отправлено в СЭД', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'tpVsPor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата исполнения. Заполняется, если заявление удовлетворено (XML)', 'SCHEMA', N'dbo', 'TABLE', N'lgZayavl', 'COLUMN', N'dtIspoln'
GO