﻿CREATE TABLE [dbo].[ASBO_hrES] (
  [idES] [bigint] IDENTITY,
  [UnIdES] [char](36) NULL,
  [NumES] [int] NULL,
  [status] [char](1) NOT NULL,
  [CodeForm] [char](4) NULL,
  [dtReport] [datetime] NULL,
  [dtCreationES] [datetime] NULL,
  [KPTU] [char](2) NULL,
  [dtSignES] [datetime] NULL,
  [nmfileES] [varchar](255) NULL,
  [PostExecutor] [varchar](255) NULL,
  [NmExecutor] [varchar](255) NULL,
  [PhoneExecutor] [varchar](255) NULL,
  [FaxExecutor] [varchar](255) NULL,
  [EmailExecutor] [varchar](255) NULL,
  [ParentUnIdES] [char](36) NULL,
  [RegDate] [datetime] NULL,
  [ParentIdES] [int] NULL,
  [XMLBody] [varchar](max) NULL,
  [IsArchive] [int] NOT NULL DEFAULT (0),
  [NumChekActive] [int] NULL DEFAULT (NULL),
  [idCheckOrg] [int] NULL DEFAULT (NULL),
  [KPHUB] [char](2) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал электроннных сообщений', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Внутренний уникальный номер ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор для АС БО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'UnIdES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Порядковый номер сообщение(за данный год)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'NumES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус ЭС R - ждем ИЭС1 I - ждеим ИЭС2  T-обмен успешно завершен  F-отказ по ИЭС1 , L-ожидается А202, K - ожидается 401', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код формы А101,  А201, А202, ИЭС1, ИЭС2, А401, А400    А-русская', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'CodeForm'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Отчетная дата', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'dtReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата формирования ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'dtCreationES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ТУ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'KPTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата подписания отчета', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'dtSignES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'nmfileES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'должность исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'PostExecutor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'имя исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'NmExecutor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'телефон исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'PhoneExecutor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'факс исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'FaxExecutor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'EMAIL исполнителя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'EmailExecutor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уник.идентификатор исходного сообщения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'ParentUnIdES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата регистрации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'RegDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор исходного сообщения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'ParentIdES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - запись в архиве, 0 - нет', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для С103', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'NumChekActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для С103', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'idCheckOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Двузначный код Хаба, к которому будет относиться подразделение Банка России (Если KPTU= KPHub, то это Хаб)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_hrES', 'COLUMN', N'KPHUB'
GO