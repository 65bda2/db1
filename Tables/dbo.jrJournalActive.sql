﻿CREATE TABLE [dbo].[jrJournalActive] (
  [DateIt] [datetime] NOT NULL,
  [SysDate] [datetime] NULL,
  [Note] [varchar](2048) NULL,
  [NumActive] [int] NOT NULL,
  [Kategory] [char](2) NULL,
  [idJournal] [int] IDENTITY,
  [NumLetter] [varchar](2048) NULL,
  [Doer] [varchar](255) NULL,
  CONSTRAINT [PK_jrJournalActive] PRIMARY KEY CLUSTERED ([idJournal])
)
ON [PRIMARY]
GO

CREATE INDEX [I_jrJournalActive_Numactive]
  ON [dbo].[jrJournalActive] ([NumActive])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrJournalActive]
ON [dbo].[jrJournalActive]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), DateIt, 104)+', '+CONVERT(VARCHAR(10), SysDate, 104)+', '+
    RTRIM(Note)+', '+CAST(NumActive AS VARCHAR(10))+', '+Kategory+', '+CAST(idJournal AS VARCHAR(10)) FROM deleted

  SELECT @newValues = CONVERT(VARCHAR(10), DateIt, 104)+', '+CONVERT(VARCHAR(10), SysDate, 104)+', '+
    RTRIM(Note)+', '+CAST(NumActive AS VARCHAR(10))+', '+Kategory+', '+CAST(idJournal AS VARCHAR(10)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал актива', 
   'Дата события, Операционная дата, Событие, идентификатор актива, Категория события, идентификатор события', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата события', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'DateIt'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Операционная Дата', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'SysDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Событие', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория События', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'Kategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор события', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'idJournal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дополнительная информация', 'SCHEMA', N'dbo', 'TABLE', N'jrJournalActive', 'COLUMN', N'NumLetter'
GO