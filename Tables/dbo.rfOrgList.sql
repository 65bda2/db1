﻿CREATE TABLE [dbo].[rfOrgList] (
  [idRecord] [int] IDENTITY,
  [nmOrg] [varchar](255) NOT NULL,
  [EGRUL] [varchar](20) NOT NULL,
  [Comment] [varchar](255) NULL,
  [dtBegin] [datetime] NOT NULL,
  [dtEnd] [datetime] NULL,
  [NmdtBeginOrg] [varchar](255) NULL,
  CONSTRAINT [PK_rfOrgList] PRIMARY KEY CLUSTERED ([idRecord])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfOrgList]
ON [dbo].[rfOrgList]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
 SET CONCAT_NULL_YIELDS_NULL OFF
 SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
 IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues =CAST(idRecord AS VARCHAR(10))+', '+RTRIM(NmOrg) +', '+RTRIM(EGRUL) +', '+RTRIM(Comment)+', '+CONVERT(VARCHAR(10), dtBegin, 104)+', '+
    CONVERT(VARCHAR(10), dtEnd, 104) FROM deleted

  SELECT @newValues = CAST(idRecord AS VARCHAR(10))+', '+RTRIM(NmOrg) +', '+RTRIM(EGRUL) +', '+RTRIM(Comment)+', '+CONVERT(VARCHAR(10), dtBegin, 104)+', '+
    CONVERT(VARCHAR(10), dtEnd, 104) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Перечень Банка России', 
   'Идентификатор записи, Наименование организации, ЕГРЮЛ, Комментарий, Дата начала действия записи, Дата окончания действия записи ', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник перечень банка России', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'idRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование организации', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'nmOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЕГРЮЛ', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'EGRUL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'Comment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала действия записи', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания действия записи', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'dtEnd'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата внесения организации в перечень Банка России', 'SCHEMA', N'dbo', 'TABLE', N'rfOrgList', 'COLUMN', N'NmdtBeginOrg'
GO