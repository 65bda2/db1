﻿CREATE TABLE [dbo].[hub_rfReportsASPPA] (
  [idReport] [int] NOT NULL,
  [dtreport] [datetime] NOT NULL,
  [nuReport] [int] NOT NULL,
  [tpreport] [char](1) NOT NULL,
  [nuIshod] [varchar](30) NOT NULL,
  [idKO] [int] NULL,
  [idORG] [int] NULL,
  [Numactive] [int] NULL,
  [nmUser] [varchar](50) NOT NULL,
  [komment] [varchar](255) NULL
)
ON [PRIMARY]
GO