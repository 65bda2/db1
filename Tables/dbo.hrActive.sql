﻿CREATE TABLE [dbo].[hrActive] (
  [OperationDate] [datetime] NOT NULL,
  [HrStatus] [tinyint] NOT NULL,
  [DFROM] [datetime] NOT NULL,
  [TypeActive] [varchar](2) NOT NULL,
  [NumActive] [int] NOT NULL,
  [NmZaemshik] [int] NULL,
  [Nominal] [money] NULL,
  [DatePayment] [datetime] NULL,
  [Cost] [money] NULL,
  [firstCost] [money] NULL,
  [TempCost] [money] NULL,
  [Company] [int] NULL,
  [DopInf] [char](27) NOT NULL,
  [idLetter] [int] NOT NULL,
  [UnNum] [char](16) NULL,
  [status] [char](1) NOT NULL,
  [Currency] [char](3) NULL,
  [firstCurrency] [char](3) NULL,
  [CostCurrency] [char](3) NULL,
  [CostRub] [money] NULL,
  [TempCostRub] [money] NULL,
  [inclDate] [datetime] NULL,
  [Begindate] [datetime] NULL,
  [Decidedate] [datetime] NULL,
  [getdate] [datetime] NULL,
  [dateTUletter] [datetime] NULL,
  [Note] [varchar](500) NULL,
  [NomerVD] [varchar](255) NULL,
  [AssetIssue] [char](20) NULL,
  [TypeVexel] [varchar](50) NULL,
  [CreationDate] [datetime] NULL,
  [AuditDate] [datetime] NULL,
  [Course] [decimal](20, 5) NULL,
  [SavePlace] [varchar](500) NULL,
  [IsActNumber] [char](1) NULL,
  [isHundredBillion] [tinyint] NULL,
  [AssetQuality] [int] NULL,
  [IndosamentDate] [datetime] NULL,
  [FirstCostRub] [decimal](18, 2) NULL,
  [IsOriginal] [int] NULL,
  [idGrouphrPG] [int] NULL,
  [Nucred] [char](20) NULL,
  [OkvedOrgToCheck] [varchar](25) NULL,
  [NmZaem] [varchar](255) NULL,
  [ZaemOKOPF] [varchar](10) NULL,
  [businessQuality] [int] NULL,
  [NmOrgToCheck] [varchar](255) NULL,
  [OGRNOrgtocheck] [varchar](20) NULL,
  [NMCheck] [varchar](5) NULL,
  [isInfoSAR] [bit] NULL CONSTRAINT [DF_hrActive_isInfoSAR] DEFAULT (0),
  [idSubdivisionSave] [int] NULL,
  [idhrActive] [int] IDENTITY,
  [DebtCreditPart] [decimal](25, 5) NULL,
  [MaturityDate] [smalldatetime] NULL,
  [isToExclude] [bit] NULL,
  [TrancheNumber] [varchar](255) NULL,
  [isToExcludeRep] [bit] NULL CONSTRAINT [DF_hrActive_isToExcludeRep] DEFAULT (0),
  CONSTRAINT [PK_hrActive] PRIMARY KEY NONCLUSTERED ([idhrActive])
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [I_hrActive]
  ON [dbo].[hrActive] ([DFROM])
  ON [PRIMARY]
GO

CREATE INDEX [I_hrActive_hrstatus]
  ON [dbo].[hrActive] ([HrStatus])
  ON [PRIMARY]
GO

CREATE INDEX [I_hrActive_numactive]
  ON [dbo].[hrActive] ([NumActive])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrActive]
  ADD CONSTRAINT [FK_hrActive_idSubdivisionSave] FOREIGN KEY ([idSubdivisionSave]) REFERENCES [dbo].[rfSubdivision] ([idSubdivision])
GO

ALTER TABLE [dbo].[hrActive]
  ADD CONSTRAINT [FK_hrActive_rfActiveStatus] FOREIGN KEY ([status]) REFERENCES [dbo].[rfActiveStatus] ([status])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Хронология по активам', 'SCHEMA', N'dbo', 'TABLE', N'hrActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Операционная дата', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'OperationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус записи: 0 - не прошла обработку, 2 - прошла обработку по удалению дублирующих записей', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'HrStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип актива: V-вексель, D-договор, M-МБК, G-золото, E-ЭКСАР, I-Инвестпроекты, YO-ЮГ(Обязанное лицо - организация jrOrg), YK-ЮГ(Обязанное лицо - КО jrKO)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'TypeActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Заемщик (уникальный идентификатор организации)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'NmZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номинал актива', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Nominal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата последнего платежа', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'DatePayment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Покупная стоимость актива', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Cost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Начальная стоимость актива при вводе актива в БД(архив значения)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'firstCost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива на текущую дату в валюте номинала', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'TempCost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Организация обязанная по активу (уникальный идентификатор организации)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Company'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Поле содержит ответы на вопросы по активу', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'DopInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор письма', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'idLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер актива (первые две цифры-регион+вид актива+NumActive)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'UnNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус актива(состояние)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код Валюты из справочника валют для номинала', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Currency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Начальный код валюты при вводе актива в БД(архив значения)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'firstCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код Валюты из справочника валют для стоимости покупки векселя', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'CostCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Покупная стоимость векселя в рублях', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'CostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива на текущую дату в рублях', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'TempCostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения в обеспечение', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'inclDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата отказа или Дата принятия актива в обеспечение', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Begindate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата принятия решения', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Decidedate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата получения решения банком', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'getdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата нового начала рассмотрения письма(при отказе по организации)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'dateTUletter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Замечание по активу', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер векселя/кредитного договора/номер слитка', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'NomerVD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер выпуска векселя', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'AssetIssue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип векселя', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'TypeVexel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания договора/векселя', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'CreationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата проверки', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'AuditDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Фиксированный курс', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'Course'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Место хранения', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'SavePlace'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Есть ли данные акта по отказу 1-есть, другое значение -нет', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'IsActNumber'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'актив принадлежит КО с капиталом >= 100млрд 1 – больше ста миллиардов, 0 - меньше', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'isHundredBillion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последняя дата, в которую передаваемый кредит/транш полностью будет погашен', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'MaturityDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'True - актив был оставлен в пуле обеспечения при истечении срока предоставления БО (только для последнего актуального обязательного периода)', 'SCHEMA', N'dbo', 'TABLE', N'hrActive', 'COLUMN', N'isToExcludeRep'
GO