﻿CREATE TABLE [dbo].[DataTransferVariant] (
  [idDTVariant] [int] IDENTITY,
  [DTVariantName] [varchar](256) NOT NULL,
  [spDataTransfer] [varchar](128) NOT NULL,
  CONSTRAINT [PK_DataTransferVariant] PRIMARY KEY CLUSTERED ([idDTVariant])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Cправочник реализованных стартовых решений', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferVariant'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД попытки переноса данных', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferVariant', 'COLUMN', N'idDTVariant'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Краткое описание решения', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferVariant', 'COLUMN', N'DTVariantName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование хранимой процедуры, для стандартного механизма переноса', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferVariant', 'COLUMN', N'spDataTransfer'
GO