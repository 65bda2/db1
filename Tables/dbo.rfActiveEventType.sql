﻿CREATE TABLE [dbo].[rfActiveEventType] (
  [tpEvent] [char](1) NOT NULL,
  [Name] [varchar](255) NOT NULL,
  CONSTRAINT [PK_rfActiveEventType] PRIMARY KEY CLUSTERED ([tpEvent])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Cправочник типов событий, используемых в истории изменения состояний актива для взаимодействия АС (hrActiveEvent)', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveEventType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код события', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveEventType', 'COLUMN', N'tpEvent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование события', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveEventType', 'COLUMN', N'Name'
GO