﻿CREATE TABLE [dbo].[hub_rfOrgList] (
  [idRecord] [int] NOT NULL,
  [nmOrg] [varchar](255) NOT NULL,
  [EGRUL] [varchar](20) NOT NULL,
  [Comment] [varchar](255) NULL,
  [dtBegin] [datetime] NOT NULL,
  [dtEnd] [datetime] NULL,
  [NmdtBeginOrg] [varchar](255) NULL
)
ON [PRIMARY]
GO