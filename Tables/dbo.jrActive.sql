﻿CREATE TABLE [dbo].[jrActive] (
  [TypeActive] [varchar](2) NOT NULL,
  [NumActive] [int] IDENTITY,
  [NmZaemshik] [int] NULL,
  [Nominal] [money] NULL,
  [DatePayment] [datetime] NULL,
  [Cost] [money] NULL,
  [Company] [int] NULL,
  [DopInf] [char](27) NOT NULL,
  [idLetter] [int] NOT NULL,
  [UnNum] [char](16) NULL,
  [status] [char](1) NOT NULL,
  [Currency] [char](3) NULL,
  [Begindate] [datetime] NULL,
  [Decidedate] [datetime] NULL,
  [getdate] [datetime] NULL,
  [dateTUletter] [datetime] NULL,
  [Note] [varchar](500) NULL,
  [NomerVD] [varchar](255) NULL,
  [TypeVexel] [varchar](50) NULL,
  [CreationDate] [datetime] NULL,
  [AuditDate] [datetime] NULL,
  [Course] [decimal](20, 5) NULL,
  [IsActNumber] [char](1) NULL,
  [CostCurrency] [char](3) NULL,
  [CostRub] [money] NULL,
  [inclDate] [datetime] NULL,
  [AssetIssue] [char](20) NULL,
  [TempCost] [money] NULL,
  [TempCostRub] [money] NULL,
  [firstCost] [money] NULL,
  [firstCurrency] [char](3) NULL,
  [MarkBullion] [varchar](50) NULL,
  [LigatureWeight] [numeric](21, 4) NULL,
  [ChemicalWeight] [numeric](21, 4) NULL,
  [fineness] [numeric](10, 4) NULL,
  [tpStatus] [char](1) NULL,
  [Metaltype] [char](3) NULL,
  [NotInStore] [int] NULL DEFAULT (0),
  [AssetQuality] [int] NULL,
  [InsurancePercent] [decimal](25, 2) NULL,
  [IndosamentDate] [datetime] NULL,
  [FirstCostRub] [decimal](18, 2) NULL,
  [IsBaseRefusal] [tinyint] NULL,
  [isAKG] [int] NULL,
  [IsOriginal] [int] NULL,
  [isPartnerMSP] [char](1) NULL DEFAULT ('F'),
  [dtRequestDocum] [datetime] NULL,
  [dtRequestOriginal] [datetime] NULL,
  [isHundredBillion] [tinyint] NULL,
  [istrigger] [int] NULL DEFAULT (0),
  [IsSimpleCheck] [int] NULL DEFAULT (0),
  [isElectronForm] [tinyint] NULL DEFAULT (0),
  [CreditAcc] [varchar](20) NULL,
  [SecuringReq] [varchar](2000) NULL,
  [IsNoKreditBlockage] [int] NULL DEFAULT (1),
  [DebtCreditPart] [decimal](25, 5) NULL,
  [isInfoSAR] [bit] NULL CONSTRAINT [DF_jrActive_isInfoSAR] DEFAULT (0),
  [idSubdivisionSave] [int] NULL,
  [MaturityDate] [smalldatetime] NULL,
  [isToExclude] [bit] NULL CONSTRAINT [DF_jrActive_isToExclude] DEFAULT (0),
  [TrancheNumber] [varchar](255) NULL,
  [isToExcludeRep] [bit] NULL CONSTRAINT [DF_jrActive_isToExcludeRep] DEFAULT (0),
  [dtExclude] [datetime] NULL,
  CONSTRAINT [PK_jrActive] PRIMARY KEY CLUSTERED ([NumActive])
)
ON [PRIMARY]
GO

CREATE INDEX [ix_Company1]
  ON [dbo].[jrActive] ([Company])
  INCLUDE ([NumActive], [DopInf], [idLetter], [dtExclude])
  ON [PRIMARY]
GO

CREATE INDEX [IX_IsSimpleCheck_dtExclude]
  ON [dbo].[jrActive] ([IsSimpleCheck], [dtExclude])
  ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_jrActive]
  ON [dbo].[jrActive] ([UnNum])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- hrActive
CREATE TRIGGER [trHrActive]
ON [dbo].[jrActive]
AFTER UPDATE,INSERT
AS
declare @dt DateTime, @dtWork DateTime
IF OBJECT_ID(N'tempdb..#TempTrhrActive', N'U') IS NOT NULL DROP TABLE #TempTrhrActive
SELECT 
idStock, 
cdStock, 
tpsect,
/*(case when tpSect in ('21L','3L') 
then (select hrStockPawn.nuCred from hrStockPawn where hrStockPawn.cdStock= hrStockSect.cdStock ) 
else ( select hrStockNight.nuCred from hrStockNight where hrStockNight.cdStock= hrStockSect.cdStock )  
end)*/ NULL as nucred 
INTO #TempTrhrActive  
FROM hrStockSect 
INNER JOIN jrBlock ON (hrStockSect.idBlock = jrBlock.idBlock) 
INNER JOIN jrAccount ON (jrAccount.idAcc = jrBlock.idAcc) 
INNER JOIN jrGKD ON (jrGKD.nuGKD = jrAccount.nuGKD) AND (jrGKD.tpGKD = 'B')
WHERE 
dtSect = (select dtwork from rfStatus)  
and tpsect in ('22N','21L','3L','3N') 
set @dt=GETdate()
set @dtWork=(select dtwork from rfStatus) 
delete from  hrActive where DFrom > DATEADD(second,-3, @dt) and DFrom < (@dt) and NumActive in (select NumActive from inserted) and HrStatus=0
Insert into hrActive
(OperationDate
,HrStatus
,DFROM
,TypeActive
,NumActive
,NmZaemshik
,Nominal
,DatePayment
,Cost
,firstCost
,TempCost
,Company
,DopInf
,idLetter
,UnNum
,status
,Currency
,firstCurrency
,CostCurrency
,CostRub
,TempCostRub
,inclDate
,Begindate
,Decidedate
,getdate
,dateTUletter
,Note
,NomerVD
,AssetIssue
,TypeVexel
,CreationDate
,AuditDate
,Course
--           ,SavePlace
,IsActNumber
,isHundredBillion
,AssetQuality
,IndosamentDate
,FirstCostRub
,IsOriginal
,idGrouphrPG
,Nucred
,OkvedOrgToCheck
,NmZaem
,ZaemOKOPF
,businessQuality
,NmOrgToCheck
,OGRNOrgtocheck
,NMCheck
,isInfoSAR
,idSubdivisionSave
,DebtCreditPart
,MaturityDate
,TrancheNumber    --AHEBURG-2242 65vev3
,isToExclude
,isToExcludeRep 
)
select 
@dtWork,
0,
@dt,
TypeActive,
NumActive, 
NmZaemshik, 
Nominal, 
DatePayment, 
Cost, 
firstCost,
TempCost,
Company,
DopInf,
idLetter,
UnNum,
status,
Currency,
firstCurrency,
CostCurrency,
CostRub,
TempCostRub,
inclDate,
Begindate,
Decidedate,
getdate,
dateTUletter,
Note,
NomerVD,
AssetIssue,
TypeVexel,
CreationDate,
AuditDate,
Course,
--		SavePlace,
IsActNumber,  
isHundredBillion,
AssetQuality,
IndosamentDate,
FirstCostRub,
IsOriginal,
(select max(idgroup) from hrPaymentGrafic a where a.numactive=inserted.numactive),
NULL, --AHEBURG-1334*65baa2 --(select top 1 a.nuCred from #TempTrhrActive a where  a.idStock=inserted.NumActive), 
(select top 1 c.Kod from rfOKVED c where org1.CodeOKVED=c.Code), 
org2.ShortNmOrg,
(select top 1 a.KOD from rfOKOPF a where a.CODE=org2.codeOkopf),  
org1.businessQuality,
org1.ShortNmOrg, 
org1.EGRUL, 
(case org1.issubject when 0 then (CASE org1.IsInList when 1 then 'НП' ELSE 'ФА' END )  ELSE 'РФ' END),
isInfoSAR,  --ПЗ 6.1.5.1
idSubdivisionSave, --ПЗ 6.1.5.1
DebtCreditPart,
MaturityDate,
TrancheNumber,    --AHEBURG-2242 65vev3
isToExclude,
isToExcludeRep 
from 
inserted 
inner join jrorg org1 on org1.idorg=company  
inner join jrorg org2 on org2.idorg=Nmzaemshik
where TypeActive in ('V','D')
IF OBJECT_ID(N'tempdb..#TempTrhrActive', N'U') IS NOT NULL DROP TABLE #TempTrhrActive;
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrActive] ON [dbo].[jrActive] AFTER INSERT,UPDATE,DELETE
AS
DECLARE 
  @MEPL_STATUS INT,
  -- 0: не в состоянии МЭПЛ
  -- 1: ввод в МЭПЛ; 
  -- 2: находится в состоянии МЭПЛ; 
  -- 3: вывод из состояния МЭПЛ и возврат в пул обеспечения
  -- 4: вывод из состояния МЭПЛ, аннулирован
  @dtWork SMALLDATETIME, 
  @StateSED INT, 
  @WasSed INT, 
  @TypeActive VARCHAR(2), 
  @NmZaemshik INT, 
  @Nominal NUMERIC(22, 7), 
  @DatePayment SMALLDATETIME, 
  @Cost NUMERIC(22, 7), 
  @Company INT, 
  @DopInf CHAR(27), 
  @idLetter INT, 
  @UnNum VARCHAR(20), 
  @status CHAR(1), 
  @Currency NUMERIC(3), 
  @Note VARCHAR(500), 
  @NomerVD VARCHAR(255),
  @CreationDate SMALLDATETIME, 
  @Course NUMERIC(21, 7), 
  @CostCurrency NUMERIC(3), 
  @CostRub MONEY, 
  @inclDate SMALLDATETIME, 
  @AssetIssue VARCHAR(20), 
  @IsOriginal INT,
  @DFrom SMALLDATETIME, 
  @idUpdate INT, 
  @CorrectionType TINYINT, 
  @idAsset NUMERIC(16), 
  @NumActive INT, 
  @IsTrigger INT, 
  @LigatureWeight NUMERIC(10, 2), 
  @Chemicalweight NUMERIC(10, 2), 
  @Fineness NUMERIC(7, 4), 
  @MarkBullion VARCHAR(50), 
  @InsurancePercent NUMERIC(21, 7),
  @MyVariable CURSOR, 
  @numGKD VARCHAR(15), 
  @dtGKD SMALLDATETIME, 
  @nuAccount VARCHAR(20), 
  @nuBikRKC VARCHAR(9),
  @dtwork1 datetime,
  @TrancheNumber VarChar(255),  --AHEBURG-2242 65vev3
  @TU Char(2)
  
SELECT @TU = nmSign FROM rfSign WHERE tpSign= 'KodTU'
  
IF ( SELECT COUNT(*) FROM INSERTED ) = 0     ---- Удаление строки
BEGIN
  SET @MyVariable = CURSOR LOCAL FOR 
	SELECT 
	  TypeActive, NmZaemshik, Nominal, DatePayment, Cost, Company, DopInf, idLetter, UnNum, status, Currency, Note, NomerVD, 
	  CreationDate, Course, CostCurrency, CostRub, ISNULL( inclDate, 0 ), AssetIssue, NumActive, IsTrigger, LigatureWeight, 
	  Chemicalweight, Fineness, MarkBullion, InsurancePercent, IsOriginal, TrancheNumber 
	FROM DELETED				
END	
ELSE BEGIN                                   ---- Добавление или изменение строки
  SET @MyVariable = CURSOR LOCAL FOR 
	SELECT 
	  TypeActive, NmZaemshik, Nominal, DatePayment, Cost, Company, DopInf, idLetter, UnNum, status, Currency, Note, NomerVD, 
	  CreationDate, Course, CostCurrency, CostRub, ISNULL( inclDate, 0 ), AssetIssue, NumActive, IsTrigger, LigatureWeight, 
	  Chemicalweight, Fineness, MarkBullion, InsurancePercent, IsOriginal, TrancheNumber 
	FROM INSERTED
END

OPEN @MyVariable
FETCH NEXT FROM @MyVariable INTO 
  @TypeActive, @NmZaemshik, @Nominal, @DatePayment, @Cost, @Company, @DopInf, @idLetter, @UnNum, @status, @Currency, @Note, @NomerVD, 
  @CreationDate, @Course, @CostCurrency, @CostRub, @inclDate, @AssetIssue, @NumActive, @IsTrigger, @LigatureWeight, @Chemicalweight, 
  @Fineness, @MarkBullion, @InsurancePercent, @IsOriginal, @TrancheNumber

IF ( ( @IsTrigger = ( SELECT IsTrigger FROM deleted WHERE NumActive = @NumActive ) ) OR ( ( SELECT COUNT(*) FROM deleted ) = 0 ) ) 
--- deleted.IsTrigger <>  inserted.Istrigger, если изменяются только поля TempCostRub и TempCost в процедуре - [dbo].[SetTempCost] 
BEGIN 
  WHILE @@FETCH_STATUS = 0 
  BEGIN
	SET @WasSed = 0                    --- Не формировались записи для отправления в ДОФР
	SELECT TOP 1 @dtWork = CAST( dtWork AS SMALLDATETIME ) FROM rfStatus

	IF ( SELECT COUNT(*) FROM DKO_hrAsset WHERE _NumActive = @NumActive AND AssetType IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) and StateSED <> -1 ) > 0 
	---- Существует, запись по данному активу в DKO_hrAsset, со статусом отличным от -1 (не отправлять в ДОФР)
	BEGIN
	  SET @CorrectionType = 2          --- Тип корректировки изменение
	  SET @idAsset = ( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @NumActive AND AssetType IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) ORDER BY IdRecord DESC )	
	  SET @WasSed = 1                  --- Формировались записи для отправления в ДОФР
	END	
	---- Не существует, запись по данному активу в DKO_hrAsset, со статусом отличным от -1 (не отправлять в ДОФР)
	ELSE BEGIN
	  SET @CorrectionType = 1         --- Тип корректировки новая
	  SET @idAsset = ( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @NumActive AND AssetType IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) ORDER BY IdRecord DESC )	
	  IF @idAsset IS NULL  
	  BEGIN 
		SET @idAsset = -( SELECT MAX( IdRecord ) + 1 FROM DKO_hrAsset )
	  END
	  IF @idAsset IS NULL 
	  BEGIN 
		SET @idAsset = -1
	  END
	END

	-- МЭПЛ начало ----------------
	SET @MEPL_STATUS = 0;
	IF ( ( SELECT COUNT(*) FROM DELETED WHERE NumActive = @NumActive AND status = 'T' ) > 0 ) AND ( @status = 'M' )         ---- Ввод в МЭПЛ 
	BEGIN
	  SET @MEPL_STATUS = 1;            --- Ввод в МЭПЛ
	  SET @CorrectionType = 4          --- Тип корректировки исключение
	  SET @idAsset = ( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @NumActive AND AssetType IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) ORDER BY IdRecord DESC )
	END 
	ELSE
	  IF ( ( SELECT COUNT(*) FROM DELETED WHERE NumActive = @NumActive AND status = 'M' ) > 0) AND ( @status = 'M' )        ---- Находится в состоянии МЭПЛ
	  BEGIN
		SET @MEPL_STATUS = 2;          --- Находится в состоянии МЭПЛ
	  END 
	  ELSE
		IF ( ( SELECT COUNT(*) FROM DELETED WHERE NumActive = @NumActive AND status = 'M' ) > 0 ) AND ( @status = 'T' )     ---- Вывод из состояния МЭПЛ и возврат в пул обеспечения 
		BEGIN
			SET @MEPL_STATUS = 3;      --- Вывод из состояния МЭПЛ и возврат в пул обеспечения 
			SET @CorrectionType = 1	   --- Тип корректировки новая		
			SET @idAsset = -( SELECT ISNULL( MAX( IdRecord ) + 1, 1 ) FROM DKO_hrAsset )
		END 
		ELSE 
		  IF ( ( SELECT COUNT(*) FROM DELETED WHERE NumActive = @NumActive AND status = 'M' ) > 0 ) AND ( @status <> 'M' )  ---- Вывод из состояния МЭПЛ, аннулирован
		  BEGIN
			SET @MEPL_STATUS = 4;      --- Вывод из состояния МЭПЛ, аннулирован
		  END
	-- МЭПЛ конец -----------------		

	IF ( ( @WasSed =  1  ) AND 
	     ( @Status = 'E' ) AND 
	     ( ( SELECT COUNT(*) FROM DKO_hrAsset WHERE _NumActive = @NumActive AND Assettype IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) AND CorrectionType = 4 ) = 0 ) )
	--- Формировались записи для отправки в ДОФР, актив возвращен КО, нет записей по активу с CorrectionType = 4 (исключение)
	BEGIN
	  IF @TypeActive IN ( 'G', 'V', 'D', 'M' )
	  BEGIN
		SET @CorrectionType = 4	       --- Тип корректировки исключение
		IF ( SELECT COUNT(*) FROM DKO_hrAssetState WHERE IdAsset = @idAsset AND PartitionTypeTo IN ( 'ОР', 'ЗП', 'РО', 'ТП', 'Пб', 'МЗ' ) ) > 0
		--- Есть записи по активу в DKO_hrAssetState  
		BEGIN
		  SET @dtWork = dbo.IncWorkDay( @dtWork, 1 )
		END
	  END
	  SET @WasSed = 2                  --- Формировались записи для отправки в ДОФР, актив надо исключить
	END		
	
	IF ( ( SELECT COUNT(*) FROM INSERTED ) = 0 )   --- Удаление записи
	BEGIN
	  IF ( SELECT COUNT(*) from DKO_hrAssetState WHERE IdAsset = @idAsset AND PartitionTypeTo IN ('ОР', 'ЗП', 'РО', 'ТП', 'Пб', 'МЗ' ) ) > 0
	  --- Есть записи по активу в DKO_hrAssetState
	  BEGIN
		SET @dtWork = dbo.IncWorkDay( @dtWork, 1 )
	  END
	  SET @CorrectionType = 4          --- Тип корректировки исключение	
	END	
	
	Select @StateSED = -3
	--- Если вывод актива совершен по инициативе АС ДКО, то новую корректировку надо сформировать, но не надо отправлять
	IF ( @CorrectionType = 2 ) And ( @status = 'Q' ) And 
	   ( ( SELECT TOP 1 PartitionTypeTo FROM DKO_hrAssetState WHERE idAsset = @idAsset And StateSED = 2 ORDER BY IdRecord DESC ) = 'ОР' )
	   --Not Exists ( Select * From hrActiveSect Where NumActive = @NumActive And DateAssign = @dtWork )
	   Select @StateSED = 2
	
	IF ( @MEPL_STATUS  IN ( 1, 3 ) )   ---- ввод в МЭПЛ, вывод из МЭПЛ в пул обеспечения 
	   OR                              ---- или
       ( ( @MEPL_STATUS = 0 )              ----   не в состоянии МЭПЛ
         AND                               ----   и 
         ( ( @WasSed = 2 )                 ----   Формировались записи для отправки в ДОФР, актив надо исключить 
            OR                                             ----   или  
           ( ( ( @WasSed = 1 ) OR ( @status = 'T' ) )      ----       Формировались записи для отправки в ДОФР или актив в статус T
             AND                                           ----        и
             ( ( @status NOT IN ( 'E', 'M' ) )             ----        Актив не в статусе E или M
               OR                                                     ----     или      
               ( ( @Status = 'E' )                                    ----         Статус E
                 AND ( @TypeActive IN ( 'M','E', 'I', 'YK', 'YO' ) )  ----         и тип актива M-МБК, E-ЭКСАР    I-Инвестпроекты, 
                                                                      ----         YO-ЮГ(Обязанное лицо - организация jrOrg), YK-ЮГ(Обязанное лицо - КО jrKO) 
                                                                      ----         и в последней записи по активу в DKO_hrAsset isActive = 1
                 AND ( ( SELECT TOP 1 isActive FROM DKO_hrAsset WHERE _NumActive = @NumActive AND AssetType IN ( 0, 1, 3, 4, 5, 6, 7, 8, 15, 17 ) ORDER BY IdRecord DESC ) = 1 )
		       ) 
		     )
           ) 
         )
       )	
	BEGIN	
	  IF ( @TypeActive IN (
				'M',  -- МБК
				'E',  -- ЭКСАР
				'I',  -- Инвестпроекты
				'YK', -- ЮГ (Обязанное лицо - КО jrKO)
				'YO', -- ЮГ (Обязанное лицо - организация jrOrg) 
				'V',  -- вексель
				'D'   -- договор 
				)
		 )
	  BEGIN 
		SELECT TOP 1 
		  @numGKD    = jrGKD.numGKD, 
		  @dtGKD     = CAST( jrGKD.dtGKD AS SMALLDATETIME ), 
		  @nuAccount = jrAccount.nuAccount,
		  @nuBikRKC  = (select jrKO.nuRkcBik from jrKO where jrKO.idKO=jrAccount.idKO) -- //+AHEBURG-1334*65baa2
		FROM 
		  jrAccount 
		  --JOIN jrRKC ON jrRKC.idRKC = jrAccount.idRKC-- //-AHEBURG-1334*65baa2
				    JOIN jrGKD ON jrGKD.nuGKD = jrAccount.nuGKD 
				                  AND ( jrGKD.dtGKDEnd IS NULL OR jrGKD.dtGKDEnd > @dtWork ) 
 				                  AND ( ( jrGKD.tpGKD <> 'K' AND ( jrGKD.dtGKDStop IS NULL OR jrGKD.dtGKDStop > @dtWork ) ) OR 
				                        ( jrGKD.tpGKD =  'K' AND jrGKD.dtEKDBegin <= @dtWork ) )
		WHERE 
		  jrAccount.idKO = ( SELECT idKO FROM jrLetter WHERE idLetter = @idLetter ) 
		  AND ( CASE jrGKD.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE jrGKD.tpGKD END ) + @TypeActive IN ( 'DM', 'FE', 'GI', 'HYK', 'HYO', 'BV', 'BD' ) 
		  AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork ) 
	    ORDER BY 
	      jrGKD.nuGKD ASC -- DTR-6373
		IF ( SELECT TOP 1 jrGKD.numGKD FROM 
		       jrAccount 
		       --JOIN jrRKC ON jrRKC.idRKC = jrAccount.idRKC -- //-AHEBURG-1334*65baa2
				         JOIN jrGKD ON jrGKD.nuGKD = jrAccount.nuGKD 
				                       AND ( jrGKD.dtGKDEnd IS NULL OR jrGKD.dtGKDEnd > @dtWork ) 
 				                       AND ( ( jrGKD.tpGKD <> 'K' AND ( jrGKD.dtGKDStop IS NULL OR jrGKD.dtGKDStop > @dtWork ) ) OR 
				                             ( jrGKD.tpGKD =  'K' AND jrGKD.dtEKDBegin <= @dtWork ) )
			 WHERE 
			   jrAccount.idKO = ( SELECT idKO FROM jrLetter WHERE idLetter = @idLetter ) 
			   AND ( CASE jrGKD.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE jrGKD.tpGKD END ) + @TypeActive IN ( 'DM', 'FE', 'GI', 'HYK', 'HYO', 'BV', 'BD' ) 
			   AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork ) ) -- DTR-6373
		   IS NOT NULL
		BEGIN
		  SELECT @dtwork1 = CASE WHEN @dtWork > @inclDate THEN @dtWork ELSE @inclDate END
		  IF ( SELECT IsNull( Max( D_From ), 0 ) FROM DKO_hrAsset WHERE IdAsset = @idAsset ) < = @dtWork1
		  BEGIN
			INSERT INTO DKO_IdUpdate ( TypeDKO ) VALUES ( 'DKO201' ) 
			SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )
			INSERT INTO DKO_hrAsset 
			  ( _NumActive, IdAsset, D_From, IdUpdate, StateSED, IsArchive, CorrectionType, AssetType, AssetCode, AssetIssue, AssetNumber, AssetDate, 
			    D_Repayment, FaceValue, FaceValueCurrency, FvCurrencyRate, PurchaseValue, PurchaseCurrency, IsActive, AssetQuality, IdIssuer, IdOrgToCheck, 
			    GKDNum, GKDDate, MainAccount, PayOrgBIC, TUHolder, DebtorRegNum, AlterComment, isGuaranteedByRF, isMinFinAgreed, isGuaranteedByCrimea, OriginalsState, TrancheNumber )
			SELECT 
			  --- _NumActive - Уникальный номер актива в БД АС Сибирь
			  @Numactive, 
			  --- IdAsset - Идентификатор актива в АС ДКО 
			  @idAsset, 
			  --- D_From - Дата, с которой действует данный набор значений атрибутов актива
			  CASE WHEN @dtWork > @inclDate THEN @dtWork ELSE @inclDate END, 
			  --- IdUpdate - Идентификатор обновления
			  @idUpdate, 
			  --- StateSED - Состояние сообщения
			  @StateSED,
			  --- IsArchive - 0 - Запись активна, 1 - Запись в архиве
			  0, 
			  --- CorrectionType - Тип корректировки 
			  @CorrectionType, 
			  --- AssetType - Вид обеспечения 
			  CASE @TypeActive WHEN 'V' THEN 0 
			                   WHEN 'D' THEN 1 
			                   WHEN 'M' THEN ( CASE ( SELECT IsSubject FROM jrKO WHERE idKO = @NmZaemshik ) WHEN 0 THEN 3 
			                                                                                                WHEN 1 THEN 15 
			                                                                                                WHEN 2 THEN 17 END ) 
			                   WHEN 'E' THEN 5 
			                   WHEN 'I' THEN 8 
			                   WHEN 'YK' THEN 7 
			                   WHEN 'YO' THEN 6 END, 
			  --- AssetCode - Уникальный номер актива                 
			  RTRIM( @UnNum ),
			  --- AssetIssue - Номер выпуска актива. Заполняется для векселей, которые имеют номер выпуска.
			  RTRIM( @AssetIssue ), 
			  --- AssetNumber - Номер актива (Для прав требования по кредитным договорам – номер договора)
			  RTRIM( @NomerVd ), 
			  --- AssetDate - Дата актива (Для прав требования по кредитному договору – дата договора)
			  CAST( @CreationDate AS SMALLDATETIME ), 
			  --- D_Repayment - Дата полного погашения актива
			  CAST( @DatePayment AS SMALLDATETIME ),
			  --- FaceValue - Номинал
			  CASE WHEN @TypeActive IN ( 'V', 'M', 'E', 'I', 'YK', 'YO' ) THEN @Nominal ELSE @Cost END, 
			  --- FaceValueCurrency - Код валюты номинала
			  @Currency, 
			  --- FvCurrencyRate - Курс валюты "номинала". 
			  @Course, 
			  --- PurchaseValue - Для векселей - стоимость покупки актива банком
			  CASE @TypeActive WHEN 'V' THEN @Cost ELSE NULL END, 
			  -- PurchaseCurrency - Для векселей - код валюты покупки актива банком
			  CASE @TypeActive WHEN 'V' THEN @CostCurrency ELSE NULL END,
			  --- IsActive - «Включённость» актива
			  CASE @Status WHEN 'T' THEN 1 ELSE 0 END, 
			  --- AssetQuality - Категория качества актива.
			  CASE @TypeActive WHEN 'YO' THEN NULL WHEN 'YK' THEN NULL WHEN 'V' THEN SUBSTRING( @DopInf, 7, 1 ) ELSE SUBSTRING( @DopInf, 6, 1 ) END,
			  --- IdIssuer - Идентификатор эмитента. В качестве значения используются значения поля IdCompany из таблицы Company.
			  CASE WHEN @TypeActive NOT IN ( 'G', 'YK', 'M' ) THEN ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = @nmZaemshik And TU = @TU ORDER BY IdRecord DESC ) 
			       WHEN @TypeActive = 'M' THEN ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = 200000000 + @nmZaemshik AND SubjectType IN ( 5, 6 ) ORDER BY IdRecord DESC ) 
			       ELSE NULL END,
			  --- IdOrgToCheck -Идентификатор организации, подлежащей проверке. В качестве значения используются значения поля IdCompany из таблицы Company.     
			  CASE WHEN @TypeActive NOT IN ( 'G', 'E', 'YK', 'M' ) THEN ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = @Company And TU = @TU ORDER BY IdRecord DESC ) 
			       WHEN @TypeActive = 'M' THEN ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = 200000000 + @Company AND SubjectType IN ( 5, 6 ) ORDER BY IdRecord DESC ) 
			       ELSE NULL END,
			  --- GKDNum - Номер кредитного договора, к которому относится основной счёт.      
			  @numGKD, 
			  --- GKDDate - Дата кредитного договора, к которому относится основной счёт.
			  @dtGKD,
			  --- MainAccount - Основной счёт. Номер счёта КО, для обеспечения кредитов на который должен использоваться актив
			  @nuAccount,
			  --- PayOrgBIC - БИК подразделения Банка Росси, в котором открыт основной счёт. 
			  @nuBikRKC,
			  --- TUHolder - КП ТУ, на базе которого создан хаб
			  ( SELECT nmSign FROM rfSign WHERE tpSign= 'KodTU' ), 
			  --- DebtorRegNum - Регистрационный номер КО заёмщика по межбанковскому кредиту
			  CASE WHEN @TypeActive IN ( 'M','YK' ) 
			       THEN CASE ( SELECT IsSubject FROM jrKO WHERE idKO = @NmZaemshik ) WHEN 0 
			                                                                         THEN ( SELECT nuReg FROM jrKO WHERE idKO = @Company ) 
			            END 
			  END, 
			  --- AlterComment - Комментарий
			  SUBSTRING( @Note, 1, 500 ),
			  --- isGuaranteedByRF - Предоставлена государственная гарантия Российской Федерации
			  CASE @TypeActive WHEN 'I' THEN CASE SUBSTRING( @dopinf, 1, 1 ) WHEN 'T' THEN 1 ELSE 0 END END, 
			  --- isMinFinAgreed - Имеется согласие Министерства финансов Российской Федерации на уступку прав требования
			  CASE @TypeActive WHEN 'I' THEN CASE SUBSTRING( @dopinf, 7, 1 ) WHEN 'T' THEN 1 ELSE 0 END END,
			  --- isGuaranteedByCrimea - Предоставлена государственная гарантия Республики Крым или города федерального значения Севастополя
			  CASE WHEN @TypeActive IN ( 'YK','YO' ) THEN CASE SUBSTRING( @dopinf, 8, 1 ) WHEN 'T' THEN 1 ELSE 0 END END, 
			  --- OriginalsState - Состояние оригиналов документов 
			  @IsOriginal,
			  --- TrancheNumber номер транша
			  @TrancheNumber 
		  END 	
		END
		IF @TypeActive IN ( 'M','E','I','YK','YO' ) 
		--- Для запуска триггера на JrTransh
		BEGIN
		  UPDATE JrTransh SET NomerVD = NomerVD WHERE NumActive = @NumActive AND status <> 'E'				
		END	
		IF ( ( @status = 'T' ) AND ( @WasSed = 0 ) ) OR ( @MEPL_STATUS = 3 ) 
		--- Для формирования по новому активу записей в DKO_hrRepayment
		BEGIN
		  UPDATE jrPaymentGrafic SET Payment = Payment WHERE NumActive = @NumActive
		END			
	  END
	END
	
	-- Vlad добавление состояния активов в DKO_hrAssetState для принимаемых с Т+1
	--INSERT INTO DKO_hrAssetState
	--  ( _IdRecord, IdAssetState, D_From, IdUpdate, StateSED, CorrectionType, IdAsset, PartitionTypeTo )
	--SELECT 
	--  0, ISNULL( '1' + REPLICATE('0', 15 - LEN( ABS( @IdAsset ) ) ) + CAST( ABS( @IdAsset ) AS VARCHAR ), 0 ) * SIGN( @idAsset ), 
	--  @inclDate, NULL, 2, 2, @IdAsset, 'МЗ' 
	--FROM 
	--  inserted 
	--WHERE 
	--  ( Status = 'T' ) AND ( inclDate >=  @dtWork ) AND NOT EXISTS( SELECT IdAsset FROM DKO_hrAssetState WHERE IdAsset = @IdAsset AND IsArchive = 0 )
	-- Vlad архивировать состояние активов в DKO_hrAssetState для IsActive = 0 
	
	UPDATE DKO_hrAssetState SET IsArchive = 1 WHERE IdAsset = @idAsset AND @Status <> 'T' AND PartitionTypeTo NOT IN ( 'ОР', 'ЗП', 'РО', 'ТП' ) AND IsArchive = 0
	
	-- Vlad доработка по DTR-3120 и DTR-3563
	--INSERT INTO DKO_hrAssetState
	--  ( _IdRecord, IdAssetState, D_From, IdUpdate, StateSED, IsArchive, CorrectionType, IdAsset, PartitionTypeTo, CrdNum, ReqType, ReqNum, ReqDate )
	--SELECT 
	--  _IdRecord, IdAssetState, @dtWork, IdUpdate, StateSED, 1, 4, IdAsset, PartitionTypeTo, CrdNum, ReqType, ReqNum, ReqDate 
	--FROM 
	--  DKO_hrAssetState 
	--WHERE 
	--  IdRecord = ( SELECT MAX( IdRecord ) FROM DKO_hrAssetState WHERE IdAsset = @idAsset AND @Status <> 'T' ) AND PartitionTypeTo NOT IN ( 'ОР', 'ЗП', 'РО', 'ТП' )
	
	FETCH NEXT FROM @MyVariable INTO
	  @TypeActive, @NmZaemshik, @Nominal, @DatePayment, @Cost, @Company, @DopInf, @idLetter, @UnNum, @status, @Currency, @Note, @NomerVD,	
	  @CreationDate, @Course, @CostCurrency, @CostRub, @inclDate, @AssetIssue, @NumActive, @IsTrigger, @LigatureWeight, @Chemicalweight, 
	  @Fineness, @MarkBullion, @InsurancePercent, @IsOriginal, @TrancheNumber
  END
END		
CLOSE @MyVariable
DEALLOCATE @MyVariable
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-----------------------------------------------------------------------------
--ПЗ 6.1.5.1.	Новые поля jrActive и hrActive (isInfoSAR, idSubdivisionSave)
--удалено поле SavePlace
-----------------------------------------------------------------------------

CREATE TRIGGER [trLogJrActive]
ON [dbo].[jrActive]
FOR INSERT, UPDATE, DELETE
AS
	DECLARE @tpAction  CHAR(1)
	DECLARE @userName  CHAR(50)
	DECLARE @oldValues VARCHAR(500)
	DECLARE @newValues VARCHAR(500)

	SET CONCAT_NULL_YIELDS_NULL OFF
	SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
	IF @userName IS NULL SELECT @userName = SYSTEM_USER

	SELECT @oldValues = 
		TypeActive+', '+
		CAST(NumActive AS VARCHAR(10))+', '+
		CAST(NmZaemshik AS VARCHAR(10))+', '+
		CONVERT(VARCHAR(20), Nominal)+', '+
		CONVERT(VARCHAR(10), DatePayment, 104)+', '+
		CONVERT(VARCHAR(20), Cost)+', '+
		CAST(Company AS VARCHAR(10))+', '+
		RTRIM(DopInf)+', '+
		CAST(idLetter AS VARCHAR(10))+', '+
		UnNum+', '+
		status+', '+
		Currency+', '+
		CONVERT(VARCHAR(10), 
		Begindate, 104)+', '+
		CONVERT(VARCHAR(10), 
		Decidedate, 104)+', '+
		CONVERT(VARCHAR(10), getdate, 104)+', '+
		CONVERT(VARCHAR(10), 
		dateTUletter, 104)+', '+
		RTRIM(Note)+', '+
		RTRIM(NomerVD)+', '+
		RTRIM(TypeVexel)+', '+
		CONVERT(VARCHAR(10), CreationDate, 104)+', '+
		CONVERT(VARCHAR(10), AuditDate, 104)+', '+
		CONVERT(VARCHAR(20), Course)+', '+
--		RTRIM(SavePlace)+', '+
		IsActNumber 
	FROM deleted
	
	IF (@oldValues IS NULL) 
		SELECT @newValues = 
			TypeActive+', '+
			CAST(NumActive AS VARCHAR(10))+', '+
			CAST(NmZaemshik AS VARCHAR(10))+', '+
			CONVERT(VARCHAR(20), Nominal)+', '+
			CONVERT(VARCHAR(10), DatePayment, 104)+', '+
			CONVERT(VARCHAR(20), Cost)+', '+
			CAST(Company AS VARCHAR(10))+', '+
			RTRIM(DopInf)+', '+
			CAST(idLetter AS VARCHAR(10))+', '+
			UnNum+', '+
			status+', '+
			Currency+', '+
			CONVERT(VARCHAR(10), Begindate, 104)+', '+
			CONVERT(VARCHAR(10), Decidedate, 104)+', '+
			CONVERT(VARCHAR(10), getdate, 104)+', '+
			CONVERT(VARCHAR(10), dateTUletter, 104)+', '+
			RTRIM(Note)+', '+
			RTRIM(NomerVD)+', '+
			RTRIM(TypeVexel)+', '+
			CONVERT(VARCHAR(10), CreationDate, 104)+', '+
			CONVERT(VARCHAR(10), AuditDate, 104)+', '+
			CONVERT(VARCHAR(20), Course)+', '+
--			RTRIM(SavePlace)+', '+
			IsActNumber 
		FROM inserted 
	ELSE
		SELECT @newValues = 
			a.TypeActive+', '+
			CAST(a.NumActive AS VARCHAR(10))+', '+
			CAST(a.NmZaemshik AS VARCHAR(10))+', '+
			CONVERT(VARCHAR(20), a.Nominal)+', '+
			CONVERT(VARCHAR(10), a.DatePayment, 104)+', '+
			CONVERT(VARCHAR(20), a.Cost)+', '+
			CAST(a.Company AS VARCHAR(10))+', '+
			RTRIM(a.DopInf)+', '+
			CAST(a.idLetter AS VARCHAR(10))+', '+
			a.UnNum+', '+
			a.status+', '+
			a.Currency+', '+
			CONVERT(VARCHAR(10), a.Begindate, 104)+', '+
			CONVERT(VARCHAR(10), a.Decidedate, 104)+', '+
			CONVERT(VARCHAR(10), a.getdate, 104)+', '+
			CONVERT(VARCHAR(10), a.dateTUletter, 104)+', '+
			RTRIM(a.Note)+', '+
			RTRIM(a.NomerVD)+', '+
			RTRIM(a.TypeVexel)+', '+
			CONVERT(VARCHAR(10), a.CreationDate, 104)+', '+
			CONVERT(VARCHAR(10), a.AuditDate, 104)+', '+
			CONVERT(VARCHAR(20), a.Course)+', '+
--			RTRIM(a.SavePlace)+', '+
			a.IsActNumber 
		FROM inserted a 
			inner join deleted c on c.NumActive=a.NumActive 
		where a.istrigger = c.ISTRIGGER

	IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
		SET @tpAction = 'I'
	ELSE  
	IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
		SET @tpAction = 'U'
	ELSE  
	IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
		SET @tpAction = 'D'
	ELSE  
		RETURN

	INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
	VALUES (@tpAction, @userName, 
	'Список Активов', 
	'Тип актива, идентификатор, Заемщик, Номинал, Дата последнего платежа, Стоимость, Обязанная организация, '+
	'Критерии, идентификатор письма, Уникальный номер актива, Статус актива, Код валюты, '+
	'Дата отказа или принятия, Дата принятия решения, Дата получения решения, Дата нового рассмотрения, '+
	'Замечание, Номер, Тип векселя, Дата записи, Дата проверки, Фиксированный курс, '+
--	'Место хранения, '+
	'данные акта по отказу', 
	@oldValues, @newValues)
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
create TRIGGER [trUpdate_dtExclude] ON [dbo].[jrActive] 
AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @dtWork DATETIME
  if UPDATE(IsSimpleCheck)
  begin
    -----БО НЕ ПРЕДОСТАВЛЕНА за актуальный, обязательный период
    SELECT TOP 1 @dtWork = dtWork FROM rfStatus
    UPDATE jrActive 
    set dtExclude=dbo.IncWorkDay(@dtWork,1),
    DopInf=STUFF(a.DopInf, 27, 1,'F')
    from inserted i join deleted d  on i.numactive=d.numactive
    join jrActive a on i.numactive=a.numactive
    join jrLetter l on a.idLetter=l.idLetter
    where d.IsSimpleCheck=1 and i.IsSimpleCheck=0 and d.[status]<>'Q'
    and i.TypeActive = 'D'  -- (активы ЕКД) and 
    --and i.[status] = 'T' --(находится в пуле обеспечения)
    and i.[status] in ('T','M')
    --and substring(d.DopInf,27,1)<>'F' 
    and --[dbo].[fnCheck_dopinfo27](a.numactive)='F'
    Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(a.Company, l.idKo, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = l.idKo And idOrg = a.Company) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null)
    /*
    and exists(SELECT * FROM dbo.fnGetActualBuhPeriod(a.Company, l.idKo, DEFAULT) as periods 
																LEFT JOIN jrOrgBuhReport bu on bu.idPeriod = periods.idPeriod AND bu.idOrg = a.Company AND bu.idKO = l.idKo
																WHERE bu.idOrgBuhReport IS NULL AND periods.obPeriod = 1)
    */
    -----БО ПРЕДОСТАВЛЕНА за актуальный, обязательный период
    UPDATE jrActive 
    set dtExclude=null,
    DopInf=STUFF(a.DopInf, 27, 1,'T')
    from inserted i join deleted d  on i.numactive=d.numactive
    join jrActive a on i.numactive=a.numactive
    join jrLetter l on a.idLetter=l.idLetter
    where d.IsSimpleCheck=1 and i.IsSimpleCheck=0 and d.[status]<>'Q'
    and i.TypeActive = 'D'  -- (активы ЕКД) and 
    --and i.[status] = 'T' --(находится в пуле обеспечения)
    and i.[status] in ('T','M')
    --and substring(d.DopInf,27,1)<>'T' 
    and -- [dbo].[fnCheck_dopinfo27](a.numactive)='T'
    not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(a.Company, l.idKo, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = l.idKo And idOrg = a.Company) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null)
    /*
    and not exists(SELECT * FROM dbo.fnGetActualBuhPeriod(a.Company, l.idKo, DEFAULT) as periods 
																LEFT JOIN jrOrgBuhReport bu on bu.idPeriod = periods.idPeriod AND bu.idOrg = a.Company AND bu.idKO = l.idKo
																WHERE bu.idOrgBuhReport IS NULL AND periods.obPeriod = 1)
    */   
    UPDATE jrActive set dtExclude=null,
    DopInf=STUFF(a.DopInf, 27, 1,'0') 
    from inserted i join deleted d  on i.numactive=d.numactive
    join jrActive a on i.numactive=a.numactive
    where d.IsSimpleCheck=0 and i.IsSimpleCheck=1
    and i.[status] in ('T','M')
    and i.TypeActive = 'D'
    and substring(d.DopInf,27,1)<>'0' 
    -->>>AHEBURG-(4365)4505 65MakarovDA проверка БО за актуальный, обязательный период для активов status in ('R','W','K')

    UPDATE jrActive 
    set 
    DopInf=STUFF(a.DopInf, 26, 1,[dbo].[fnCheck_dopinfo26](a.numactive))
    from inserted i join deleted d  on i.numactive=d.numactive
    join jrActive a on i.numactive=a.numactive
    join jrLetter l on a.idLetter=l.idLetter
    where d.IsSimpleCheck=1 and i.IsSimpleCheck=0 
    and i.TypeActive = 'D'  -- (активы ЕКД) and 
    and i.[status] in ('R','W','K')
    and substring(d.DopInf,26,1)<>[dbo].[fnCheck_dopinfo26](a.numactive)
  
       
    UPDATE jrActive set
    DopInf=STUFF(a.DopInf, 26, 1,'0') 
    from inserted i join deleted d  on i.numactive=d.numactive
    join jrActive a on i.numactive=a.numactive
    where d.IsSimpleCheck=0 and i.IsSimpleCheck=1
    and i.[status] in ('R','W','K')
    and i.TypeActive = 'D'
    and substring(d.DopInf,26,1)<>'0'
    --<<<AHEBURG-(4365)4505 65MakarovDA проверка БО за актуальный, обязательный период для активов status in ('R','W','K')  
  end
END
GO

ALTER TABLE [dbo].[jrActive] WITH NOCHECK
  ADD CONSTRAINT [FK_jrActive_idletter] FOREIGN KEY ([idLetter]) REFERENCES [dbo].[jrLetter] ([idLetter]) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[jrActive]
  ADD CONSTRAINT [FK_jrActive_idSubdivisionSave] FOREIGN KEY ([idSubdivisionSave]) REFERENCES [dbo].[rfSubdivision] ([idSubdivision])
GO

ALTER TABLE [dbo].[jrActive]
  ADD CONSTRAINT [FK_jrActive_rfActiveStatus] FOREIGN KEY ([status]) REFERENCES [dbo].[rfActiveStatus] ([status])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Активы', 'SCHEMA', N'dbo', 'TABLE', N'jrActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип актива V-вексель D-договор M-МБК G-золото E-ЭКСАР, I-Инвестпроекты, YO-ЮГ(Обязанное лицо - организация jrOrg), YK-ЮГ(Обязанное лицо - КО jrKO)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'TypeActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Заемщик (уникальный идентификатор организации)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'NmZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номинал актива', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Nominal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата последнего платежа', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'DatePayment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Покупная стоимость актива', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Cost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Организация обязанная по активу (уникальный идентификатор организации)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Company'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Поле содержит ответы на вопросы по активу: 1-15 символы ответы на вопросы по активу, 16-25 - по организации, Форма ответа: 0-нет ответа, T-да,F-нет', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'DopInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор письма', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'idLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер актива (первые две цифры-регион+вид актива+NumActive)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'UnNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус актива(состояние) R->U->I->T->N->Q->E', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код Валюты из справочника валют для номинала', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Currency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата отказа или Дата принятия актива в обеспечение', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Begindate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата принятия решения', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Decidedate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата получения решения банком', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'getdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата нового начала рассмотрения письма(при отказе по организации)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'dateTUletter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Замечание по активу', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер векселя/кредитного договора/номер слитка', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'NomerVD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип векселя', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'TypeVexel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания договора/векселя', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'CreationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата проверки', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'AuditDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Фиксированный курс', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Course'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Есть ли данные акта по отказу 1-есть, другое значение -нет', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'IsActNumber'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код Валюты из справочника валют для стоимости покупки векселя', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'CostCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Покупная стоимость векселя в рублях', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'CostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения в обеспечение', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'inclDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер выпуска векселя', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'AssetIssue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива на текущую дату в валюте номинала', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'TempCost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива на текущую дату в рублях', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'TempCostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Начальная стоимость актива при вводе актива в БД(архив значения)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'firstCost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Начальный код валюты при вводе актива в БД(архив значения)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'firstCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Марка слитка', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'MarkBullion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Лигатурная масса', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'LigatureWeight'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Химически чистая масса', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'ChemicalWeight'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'проба', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'fineness'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип статуса', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'tpStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип метала (заложено на будущее)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'Metaltype'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'представлялся ли слиток в хранилище БР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'NotInStore'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'AssetQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'застрахованная доля, %', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'InsurancePercent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'IndosamentDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'FirstCostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'IsBaseRefusal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'isAKG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'IsOriginal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'актив принадлежит КО с капиталом >= 100млрд 1 – больше ста миллиардов, 0 - меньше', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'isHundredBillion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последняя дата, в которую передаваемый кредит/транш полностью будет погашен', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'MaturityDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'True - актив был оставлен в пуле обеспечения при истечении срока предоставления БО (только для последнего актуального обязательного периода)', 'SCHEMA', N'dbo', 'TABLE', N'jrActive', 'COLUMN', N'isToExcludeRep'
GO