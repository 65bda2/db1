﻿CREATE TABLE [dbo].[rfFactor] (
  [dtFactor] [datetime] NOT NULL,
  [tpFactor] [char](2) NOT NULL,
  [pcFactor] [decimal](7, 4) NOT NULL,
  [idOrg] [int] NULL,
  [dtEnd] [datetime] NULL,
  CONSTRAINT [C_rfFactor_tpFactor] CHECK ([tpFactor]='Y2' OR [tpFactor]='Y1' OR [tpFactor]='I2' OR [tpFactor]='I1' OR [tpFactor]='E2' OR [tpFactor]='E1' OR [tpFactor]='W2' OR [tpFactor]='W1' OR [tpFactor]='X2' OR [tpFactor]='X1' OR [tpFactor]='Z2' OR [tpFactor]='Z1' OR [tpFactor]='A2' OR [tpFactor]='A1' OR [tpFactor]='M2' OR [tpFactor]='M1' OR [tpFactor]='S2' OR [tpFactor]='S1' OR [tpFactor]='R2' OR [tpFactor]='R1' OR [tpFactor]='F2' OR [tpFactor]='F1' OR [tpFactor]='T2' OR [tpFactor]='T1')
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [I_rfFactor]
  ON [dbo].[rfFactor] ([dtFactor], [tpFactor], [idOrg])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfFactor]
ON [dbo].[rfFactor]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtFactor, 104)+', '+tpFactor+', '+CONVERT(VARCHAR(10), pcFactor) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtFactor, 104)+', '+tpFactor+', '+CONVERT(VARCHAR(10), pcFactor) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Таблица поправочных коэффициентов для нерыночных активов', 
   'дата начала действия, тип, процентная ставка', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Хронология таблицы поправочных коэффициентов для нерыночных активов', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала действия', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor', 'COLUMN', N'dtFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип поправочного коэффициента
A1 - Агенство кредитных гарантий 1 категория качества
A2 - Агенство кредитных гарантий 2 категория качества
E1 - КД на ЭКСАР 1 категория качества
E2 - КД на ЭКСАР 2 категория качества
F1 - Нерейтинг 1 категория качества
F2 - Нерейтинг 2 категория качества
I1 - КД на Инвестпроекты 1 категория качества
I2 - КД на Инвестпроекты 2 категория качества
M1 - Муниципальное образование 1 категория качества
M2 - Муниципальное образование 2 категория качества
R1 - Российская Федерация 1 категория качества
R2 - Российская Федерация 2 категория качества
S1 - Субъект РФ 1 категория качества
S2 - Субъект РФ 2 категория качества
T1 - Рейтинг 1 категория качества
T2 - Рейтинг 2 категория качества
W1 - КД на МСП (обязанное лицо - лизинговая компания) 1 категория качества
W2 - КД на МСП (обязанное лицо - лизинговая компания) 2 категория качества
X1 - КД на МСП (обязанное лицо - МФО) 1 категория качества
X2 - КД на МСП (обязанное лицо - МФО) 2 категория качества
Y1 - КД на ЮГ, обеспеченные госгарантией
Y2 - КД на ЮГ, не обеспеченные госгарантией
Z1 - КД на МСП (обязанное лицо - КО) 1 категория качества
Z2 - КД на МСП (обязанное лицо - КО) 2 категория качества', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor', 'COLUMN', N'tpFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение поправочного коэффициента', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor', 'COLUMN', N'pcFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор организации, входящей в перечень Банка России (для индивидуальнх коэффициентов)', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания действия (для индивидуальнх коэффициентов)', 'SCHEMA', N'dbo', 'TABLE', N'rfFactor', 'COLUMN', N'dtEnd'
GO