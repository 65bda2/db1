﻿CREATE TABLE [dbo].[hub_jrDopLetter] (
  [idDopLetter] [int] NOT NULL,
  [Note] [varchar](500) NULL,
  [NumLetter] [varchar](20) NOT NULL,
  [DateLetter] [datetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [tpLetter] [char](1) NOT NULL
)
ON [PRIMARY]
GO