﻿CREATE TABLE [dbo].[hub_jrAccount] (
  [idAcc] [int] NOT NULL,
  [tpCredit] [char](1) NULL,
  [mnLimit] [money] NULL,
  [mnLiability] [money] NULL,
  [tpRazdel] [char](1) NULL,
  [idKO] [int] NOT NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('T'),
  [nuGKD] [int] NOT NULL,
  [dtIncl] [datetime] NOT NULL,
  [dtExcl] [datetime] NULL,
  [isPayForBKL] [char](1) NOT NULL DEFAULT ('F'),
  [nmKO] [varchar](250) NULL,
  [nnKO] [varchar](128) NULL,
  [nuReg] [varchar](9) NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nuAccount] [varchar](20) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [idRKC] [int] NULL,
  [nuCorrAcc] [varchar](20) NULL,
  [tpInstr] [char](1) NOT NULL DEFAULT ('C'),
  [tpAsset] [char](1) NULL
)
ON [PRIMARY]
GO