﻿CREATE TABLE [dbo].[hub_DKO_hrAssetState] (
  [IdRecord] [bigint] NOT NULL,
  [_IdRecord] [int] NOT NULL,
  [IdAssetState] [varchar](17) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [IdAsset] [numeric](16) NOT NULL,
  [PartitionType] [char](2) NOT NULL,
  [CrdNum] [varchar](10) NULL,
  [ReqType] [tinyint] NULL,
  [ReqNum] [varchar](20) NULL,
  [ReqDate] [smalldatetime] NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL
)
ON [PRIMARY]
GO