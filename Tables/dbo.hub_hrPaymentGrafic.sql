﻿CREATE TABLE [dbo].[hub_hrPaymentGrafic] (
  [Dfrom] [datetime] NOT NULL,
  [idGroup] [int] NOT NULL,
  [NumActive] [int] NOT NULL,
  [tpActive] [char](1) NOT NULL,
  [DatePay] [smalldatetime] NOT NULL,
  [Payment] [money] NOT NULL,
  [Advice] [char](1) NULL,
  [IdTransh] [int] NULL,
  [AlterComment] [char](255) NULL
)
ON [PRIMARY]
GO