﻿CREATE TABLE [dbo].[seSEDReport6] (
  [NumActive] [int] NULL,
  [kodTU] [char](2) NULL,
  [co_name] [varchar](250) NULL,
  [co_cregn] [varchar](9) NULL,
  [asset_id] [varchar](16) NULL,
  [incl_date] [datetime] NULL,
  [asset_type] [varchar](255) NULL,
  [borr_name] [varchar](255) NULL,
  [org_name] [varchar](278) NULL,
  [org_list] [char](3) NULL,
  [asset_value] [decimal](30, 6) NULL,
  [asset_curr] [char](3) NULL,
  [asset_quality] [char](1) NULL,
  [maturity_date] [datetime] NULL,
  [maturity_sum] [decimal](30, 6) NULL,
  [maturity_curr] [char](3) NULL,
  [ex_rate] [varchar](20) NULL,
  [block] [varchar](20) NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код региона по справочнику ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'kodTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'co_name'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер КО', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'co_cregn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификационный номер актива', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'asset_id'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения актива в состав активов, принимаемых в обеспечение кредитов Банка России', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'incl_date'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид актива', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'asset_type'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование векселедателя (заемщика)', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'borr_name'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование организации, в отношении которой проверен актив', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'org_name'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Критерии, на соответствие которым проверена организация, указанная в графе 10: "НП" - организация проверена на соответствие подпунктам 3.6.1 - 3.6.2 Положения; "ФА" - организация проверена на соответствие подпунктам 3.6.2 - 3.6.4 Положения', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'org_list'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость приобретения векселя/остаток суммы основного долга по кредиту', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'asset_value'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код валюты (буквенный), в которой выражена стоимость приобретения векселя/остаток суммы основного долга по кредиту', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'asset_curr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория качества актива', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'asset_quality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата погашения векселя (дата истечения срока предъявления векселя к платежу)/дата погашения суммы (части суммы) основного долга', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'maturity_date'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Для права требования по кредитному договору: сумма, подлежащая погашению, для векселя: минимальная из двух величин - стоимости приобретения векселя и вексельной суммы', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'maturity_sum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код валюты (буквенный), в которой выражена сумма, подлежащая погашению', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'maturity_curr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Курс иностранной валюты к рублю, подлежащий применению для определения суммы платежа по векселю (кредиту) (если подлежит применению курс, установленный Банком России, указывается "CBR", если вексельная сумма (сумма основного долга) и сумма, подлежащая погашению, выражены в рублях, указывается "1" (единица).', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'ex_rate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак актива, находящегося в залоге (указывается уникальный номер кредита, в залоге по которому находится актив, или "0" (ноль), если актив не находится в залоге)', 'SCHEMA', N'dbo', 'TABLE', N'seSEDReport6', 'COLUMN', N'block'
GO