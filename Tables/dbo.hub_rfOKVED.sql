﻿CREATE TABLE [dbo].[hub_rfOKVED] (
  [CODE] [int] NOT NULL,
  [KOD] [char](8) NOT NULL,
  [NAME_RUS] [varchar](1000) NOT NULL,
  [N_RAZDEL] [char](1) NULL,
  [CB_Date] [datetime] NULL,
  [CE_Date] [datetime] NULL,
  [ID] [numeric](12) NULL,
  [CODEFORPPA] [int] NULL
)
ON [PRIMARY]
GO