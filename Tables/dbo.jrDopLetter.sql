﻿CREATE TABLE [dbo].[jrDopLetter] (
  [idDopLetter] [int] IDENTITY,
  [Note] [varchar](500) NULL,
  [NumLetter] [varchar](20) NOT NULL,
  [DateLetter] [datetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [tpLetter] [char](1) NOT NULL,
  CONSTRAINT [PK_jrDopLetter] PRIMARY KEY CLUSTERED ([idDopLetter])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrDopLetter]
ON [dbo].[jrDopLetter]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idDopLetter AS VARCHAR(10))+', '+RTRIM(Note)+', '+RTRIM(NumLetter)+', '+
    CONVERT(VARCHAR(10), DateLetter, 104)+', '+CAST(idKO AS VARCHAR(10)) FROM deleted
  SELECT @newValues = CAST(idDopLetter AS VARCHAR(10))+', '+RTRIM(Note)+', '+RTRIM(NumLetter)+', '+
    CONVERT(VARCHAR(10), DateLetter, 104)+', '+CAST(idKO AS VARCHAR(10)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список дополнительных писем', 
   'идентификатор, краткое содержание, номер доп.письма, дата доп.письма, идентификатор КО', 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrDopLetter]
  ADD CONSTRAINT [FK_jrDopLetter_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дополнительное письмо', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор доп.письма', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'idDopLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'краткое содержание', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер доп. письма', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'NumLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата доп. письма', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'DateLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип письма D - по договору,векселю, МБК G - золото', 'SCHEMA', N'dbo', 'TABLE', N'jrDopLetter', 'COLUMN', N'tpLetter'
GO