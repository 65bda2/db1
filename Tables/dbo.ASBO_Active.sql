﻿CREATE TABLE [dbo].[ASBO_Active] (
  [idMainDataES] [bigint] NOT NULL,
  [NumActive] [int] NULL,
  [Unnum] [char](16) NOT NULL,
  [TypeActive] [char](1) NOT NULL,
  [NomerVD] [varchar](255) NULL,
  [CreationDate] [datetime] NOT NULL,
  [AssetQuality] [char](1) NOT NULL,
  [NmZaemshik] [varchar](255) NULL,
  [NmZaemshikShort] [varchar](255) NULL,
  [OKVEDZaemshik] [char](8) NULL,
  [Nominal] [decimal](18, 2) NULL,
  [TempCost] [decimal](18, 2) NULL,
  [Currency] [char](3) NOT NULL,
  [FirstCostRub] [decimal](18, 2) NOT NULL,
  [Cost] [decimal](18, 2) NULL,
  [CostCurrency] [char](3) NULL,
  [DatePayment] [datetime] NOT NULL,
  [DtSend] [datetime] NULL,
  [DtCheck] [datetime] NULL,
  [DtIndosament] [datetime] NULL,
  [dtFailure] [datetime] NULL,
  [NoteFailure] [varchar](2048) NULL,
  [DtReturn] [datetime] NULL,
  [Nowcost] [decimal](18, 2) NOT NULL,
  [DtNowcost] [datetime] NULL,
  [NowCourse] [decimal](18, 4) NULL,
  [FixCourse] [decimal](18, 4) NULL,
  [DtIncl] [datetime] NULL,
  [DtReincl] [datetime] NULL,
  [NoteReincl] [varchar](2048) NULL,
  [TypeVexel] [varchar](50) NULL,
  [SavePlace] [varchar](500) NULL,
  [AnotherInfVD] [char](1) NULL,
  [AnotherInfVDNote] [varchar](255) NULL,
  [V1] [char](1) NULL,
  [V2] [char](1) NULL,
  [V3] [char](1) NULL,
  [V4] [char](1) NULL,
  [V5] [char](1) NULL,
  [V6] [char](1) NULL,
  [V7] [char](1) NULL,
  [D1] [char](1) NULL,
  [D2] [char](1) NULL,
  [D3] [char](1) NULL,
  [D4] [char](1) NULL,
  [D5] [char](1) NULL,
  [D6] [char](1) NULL,
  [D7] [char](1) NULL,
  [TpZaemshik] [char](1) NULL,
  [IsOpinion] [char](1) NULL,
  [NmOpinion] [varchar](255) NULL,
  [NumLetKO] [varchar](100) NULL,
  [DateLetter] [datetime] NULL,
  [NumSADD] [varchar](50) NULL,
  [DateLetterTU] [datetime] NULL,
  [Kolvo] [int] NULL,
  [nucred] [varchar](20) NULL,
  [CostZalog] [decimal](18, 2) NULL,
  [StatusActive] [char](1) NULL,
  [OGRNZaemshik] [varchar](20) NULL,
  [statusactiveCA] [int] NULL DEFAULT (0),
  [Okopfzaemshik] [varchar](5) NULL,
  [businessQualityzaemshik] [char](1) NULL,
  [dtOgrnzaemshik] [datetime] NULL,
  [D_lastRegDatezaemshik] [datetime] NULL,
  [TpDtRegzaemshik] [char](1) NULL,
  [INNzaemshik] [char](12) NULL,
  [IsResidentzaemshik] [char](1) NULL,
  [IsInListzaemshik] [char](1) NULL,
  [IsSubjectzaemshik] [char](1) NULL,
  [IsConformingzaemshik] [tinyint] NULL,
  [IsSimpleCheck] [int] NULL DEFAULT (0),
  [UnpaidSum] [decimal](18, 2) NULL,
  [CurrentStatusActive] [tinyint] NULL,
  [TrancheNumber] [varchar](255) NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_Active_idMainDataES]
  ON [dbo].[ASBO_Active] ([idMainDataES])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogASBO_Active]
ON [dbo].[ASBO_Active]
FOR INSERT, UPDATE, DELETE
AS
DECLARE @tpAction  CHAR(1)
DECLARE @userName  CHAR(50)
DECLARE @oldValues VARCHAR(500)
DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(20), idMainDataES)+', '+CONVERT(VARCHAR(16), Unnum)+', '+CONVERT(VARCHAR(30), Cost)+', '+CONVERT(VARCHAR(30), TempCost)  FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(20), idMainDataES)+', '+CONVERT(VARCHAR(16), Unnum)+', '+CONVERT(VARCHAR(30), Cost)+', '+CONVERT(VARCHAR(30), TempCost)  FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'ППА_ЦА_АКТИВЫ', 
   'идентификатор блока данных, ун.номер актива, общая задолженность, текущая стоимость', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Данные, по активам', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на  ASBO_MainDataES', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'idMainDataES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вн. номер актива в АС ППА', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Unnum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'TypeActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер векселя/КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NomerVD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания весселя/КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'CreationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Категория качества', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'AssetQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование заемщика', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NmZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование заемщика краткое', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NmZaemshikShort'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКВЭД заемщика', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'OKVEDZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номинал актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Nominal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива на текущий день в валюте номинала', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'TempCost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Валюта номинала', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Currency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость в рублях на момент принятия актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'FirstCostRub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Cost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Валюта стоимости', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'CostCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата погашения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DatePayment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата поступления актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtSend'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата проверки', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата совершения индосамента', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtIndosament'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата отказа', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'dtFailure'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Основание отказа', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NoteFailure'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата возврата актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtReturn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текущая стоимость в рублях', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Nowcost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата расчета стоимости', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtNowcost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Курс валюты', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NowCourse'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Фиксированный курс', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'FixCourse'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtIncl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата исключения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DtReincl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Основание исключения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NoteReincl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'TypeVexel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Место хранения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'SavePlace'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соотв. другой инф. критериям', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'AnotherInfVD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Другая инф.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'AnotherInfVDNote'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'параметры V -векселя D - актива в том же порядке, что и в док. форматов обмена', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V4'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V5'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V6'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры векселя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'V7'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры V -векселя D - актива в том же порядке, что и в док. форматов обмена', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D4'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D5'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D6'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Параметры КД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'D7'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип заемщика', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'TpZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Заключение', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'IsOpinion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст Заключения', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NmOpinion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер письма банка', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NumLetKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата письма банка', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DateLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер письма в САДД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'NumSADD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата получения письма ТУ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'DateLetterTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'количество активов, направленных с письмом', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'Kolvo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный номер кредита Банка России', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'nucred'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость актива в залоге (заполняется если актив в залоге)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'CostZalog'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'статус актива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'StatusActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОГРН заемщика', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'OGRNZaemshik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Непогашенная часть суммы основного долга по кредиту на дату проверки, в рублях', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'UnpaidSum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ТекущийСтатусАктива', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'CurrentStatusActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить номер транша', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_Active', 'COLUMN', N'TrancheNumber'
GO