﻿CREATE TABLE [dbo].[hrSQLScripts] (
  [dtExec] [datetime] NOT NULL CONSTRAINT [DF_hrSQLScripts_dtExec] DEFAULT (getdate()),
  [NameScript] [varchar](50) NOT NULL,
  [VersionAfter] [varchar](10) NOT NULL,
  [Application] [varchar](100) NOT NULL,
  [DBname] [varchar](50) NOT NULL,
  [SQLScript] [varchar](max) NULL,
  [Result] [bit] NOT NULL,
  [ErrorText] [varchar](max) NULL,
  CONSTRAINT [PK_hrSQLScripts] PRIMARY KEY CLUSTERED ([dtExec])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время выполнения скрипта', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'dtExec'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование файла скрипта', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'NameScript'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Версия БД, после выполнения скрипта', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'VersionAfter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Приложение в котором выполняли скрипт', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'Application'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя БД, на которой выполняется скрипт', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'DBname'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Скрипт', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'SQLScript'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Успешно или нет', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'Result'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст ошибки', 'SCHEMA', N'dbo', 'TABLE', N'hrSQLScripts', 'COLUMN', N'ErrorText'
GO