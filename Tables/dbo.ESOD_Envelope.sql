﻿CREATE TABLE [dbo].[ESOD_Envelope] (
  [IdEnvelope] [int] IDENTITY,
  [GUID] [varchar](36) NOT NULL,
  [ID] [varchar](36) NOT NULL,
  [CorrelationID] [varchar](36) NULL,
  [DateTime] [datetime] NULL,
  [Envelope] [varchar](max) NULL,
  [SubscriberID] [int] NULL,
  [Status] [int] NOT NULL CONSTRAINT [DF_ESOD_Envelope_Status] DEFAULT (0),
  [StatusDesc] AS (case [Status] when (0) then 'Неопределен' when (1) then 'Загружен' when (2) then 'Отправлен' when (3) then 'Доставлен' when (4) then 'Ошибка' else '' end),
  [Description] [varchar](max) NULL,
  [CreatedDateTime] [datetime] NULL CONSTRAINT [DF_ESOD_Envelope_CreatedDate] DEFAULT (getdate()),
  [CreatedDate] AS (CONVERT([varchar],[CreatedDateTime],(104))),
  [CreatedTime] AS (CONVERT([time](0),[CreatedDateTime],(0))),
  [CreatedBy] [int] NULL CONSTRAINT [DF_ESOD_Envelope_CreatedBy] DEFAULT (suser_id()),
  [CreatedName] AS (substring(suser_name([CreatedBy]),charindex('\',suser_name([CreatedBy]))+(1),len(suser_name([CreatedBy])))),
  [ModifedDateTime] [datetime] NULL,
  [ModifedDate] AS (CONVERT([varchar],[ModifedDateTime],(104))),
  [ModifedTime] AS (CONVERT([time](0),[ModifedDateTime],(0))),
  [ModifedBy] [int] NULL,
  [ModifedName] AS (substring(suser_name([ModifedBy]),charindex('\',suser_name([ModifedBy]))+(1),len(suser_name([ModifedBy])))),
  CONSTRAINT [PK_ESOD_Envelope] PRIMARY KEY CLUSTERED ([IdEnvelope])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

CREATE UNIQUE INDEX [AK_ESOD_Envelope]
  ON [dbo].[ESOD_Envelope] ([ID])
  ON [PRIMARY]
GO

CREATE INDEX [IE_ESOD_Envelope]
  ON [dbo].[ESOD_Envelope] ([GUID])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trESOD_Envelope_Modify]
	ON  [dbo].[ESOD_Envelope]
	AFTER UPDATE
	AS
BEGIN
	SET NOCOUNT ON;
	UPDATE
		[ESOD_Envelope]
	SET
		[ModifedDateTime]	= GETDATE(),
		[ModifedBy]		= SUSER_ID()
	FROM
		INSERTED, [ESOD_Envelope]
	WHERE
		[ESOD_Envelope].GUID = INSERTED.GUID
	SET NOCOUNT OFF;
END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Представление интеграционного конверта ЕСОД-4Р в базе данных', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла интеграционного конверта без расширения', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'GUID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение элемента DocumentPackID служебного файла RouteInfo.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение элемента DocumentPackCorrelationID служебного файла RouteInfo.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CorrelationID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ДатаВремя создания', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'DateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тело служебного файла envelope.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'Envelope'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор абонента = [jrKO].[idKO]', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'SubscriberID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус конверта', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'Status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание статуса конверта', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'StatusDesc'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Описание ошибки обработки файла ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'Description'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ДатаВремя создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CreatedDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CreatedDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Время создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CreatedTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ID логина автора ', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин автора', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'CreatedName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ДатаВремя изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ModifedDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ModifedDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Время изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ModifedTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ID логина автора изменений', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ModifedBy'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин автора изменений', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Envelope', 'COLUMN', N'ModifedName'
GO