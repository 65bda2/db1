﻿CREATE TABLE [dbo].[DataTransferGo] (
  [idDTGo] [int] IDENTITY,
  [idDTVariant] [int] NOT NULL,
  [dtStart] [datetime] NOT NULL,
  [dtFinish] [datetime] NULL,
  [isSuccess] [bit] NULL,
  [xmlParam] [xml] NULL,
  [xmlLog] [xml] NULL,
  CONSTRAINT [PK_DataTransferGo] PRIMARY KEY CLUSTERED ([idDTGo])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[DataTransferGo]
  ADD CONSTRAINT [FK_DataTransferGo_DataTransferVariant] FOREIGN KEY ([idDTVariant]) REFERENCES [dbo].[DataTransferVariant] ([idDTVariant])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица учета попыток переноса данных.', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'первичный ключ	ИД попытки переноса данных', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'idDTGo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'вторичный ключ к таблице DataTransferVariant полю idDTVariant	ИД варианта переноса, который осуществлялся', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'idDTVariant'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'заполняется по умолчанию датой создания записи	Дата старта переноса данных (выгрузки/загрузки в зависимости от варианта)', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'dtStart'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания попытки переноса данных', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'dtFinish'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'-Результат переноса', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'isSuccess'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'XML-структура для запоминания параметров переноса', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'xmlParam'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'XML-структура для запоминания результатов переноса данных, для каждого idDTVariant своя структура, в зависимости от реализованного решения', 'SCHEMA', N'dbo', 'TABLE', N'DataTransferGo', 'COLUMN', N'xmlLog'
GO