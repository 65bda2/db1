﻿CREATE TABLE [dbo].[hub_rfStatus] (
  [version] [varchar](20) NOT NULL,
  [dtWork] [datetime] NULL,
  [ASPPAversion] [varchar](20) NULL,
  [DataMartVersion] [varchar](20) NULL,
  [dtBeginTechWork] [datetime] NULL,
  [dtEndTechWork] [datetime] NULL
)
ON [PRIMARY]
GO