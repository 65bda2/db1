﻿CREATE TABLE [dbo].[EDOKO_ActiveList] (
  [idActiveList] [int] IDENTITY,
  [idES] [int] NOT NULL,
  [UnNum] [varchar](16) NOT NULL,
  CONSTRAINT [PK_EDOKO_ActiveList] PRIMARY KEY CLUSTERED ([idActiveList])
)
ON [PRIMARY]
GO

CREATE INDEX [IX_EDOKO_ActiveList]
  ON [dbo].[EDOKO_ActiveList] ([idES], [UnNum])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EDOKO_ActiveList]
  ADD CONSTRAINT [FK_EDOKO_hrES_EDOKO_ActiveList] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Первичный ключ таблицы', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ActiveList', 'COLUMN', N'idActiveList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Внешний ключ с EDOKO_hrES', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ActiveList', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер актива, присвоенный Банком России', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_ActiveList', 'COLUMN', N'UnNum'
GO