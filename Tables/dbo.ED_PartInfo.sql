﻿CREATE TABLE [dbo].[ED_PartInfo] (
  [IdRecord] [bigint] IDENTITY,
  [D_Rep] [smalldatetime] NOT NULL,
  [ESType] [varchar](6) NOT NULL,
  [ESNo] [int] NOT NULL,
  [PartAggregateID] [varchar](27) NOT NULL,
  [PartQuantity] [int] NOT NULL,
  [PartNo] [int] NOT NULL,
  CONSTRAINT [PK_ED_PartInfo] PRIMARY KEY CLUSTERED ([PartAggregateID], [PartNo])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество частей', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Отчётная дата', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'D_Rep'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ED255, ED256, ED257', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'ESType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер ЭС в течение опердня', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'ESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор совокупности частей', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'PartAggregateID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество частей. Указывается номер последней части в совокупности', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'PartQuantity'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер части. Указывается порядковый номер части, начиная с 1 (номер первой части в совокупности)', 'SCHEMA', N'dbo', 'TABLE', N'ED_PartInfo', 'COLUMN', N'PartNo'
GO