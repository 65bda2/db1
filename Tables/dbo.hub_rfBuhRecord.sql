﻿CREATE TABLE [dbo].[hub_rfBuhRecord] (
  [idClass] [int] NOT NULL,
  [idRecord] [int] NOT NULL,
  [NmRecord] [varchar](500) NOT NULL,
  [TpClass] [int] NULL,
  [Kod] [varchar](10) NULL,
  [NumKod] [varchar](10) NOT NULL,
  [isrequired] [int] NULL DEFAULT (1),
  [KodFNS] [varchar](50) NULL,
  [IsactiveRecord] [tinyint] NULL DEFAULT (0)
)
ON [PRIMARY]
GO