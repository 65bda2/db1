﻿CREATE TABLE [dbo].[hrOwnFunds] (
  [idKO] [int] NOT NULL,
  [dtOwnFund] [datetime] NOT NULL,
  [mnOwnFund] [money] NULL,
  [cmOwnFund] [varchar](500) NULL,
  CONSTRAINT [PK_hrOwnFunds] PRIMARY KEY CLUSTERED ([idKO], [dtOwnFund])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrOwnFunds]
  ADD CONSTRAINT [FK_hrOwnFunds_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Изменения величины собственных средств (капитала)', 'SCHEMA', N'dbo', 'TABLE', N'hrOwnFunds'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор банка', 'SCHEMA', N'dbo', 'TABLE', N'hrOwnFunds', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата изменения', 'SCHEMA', N'dbo', 'TABLE', N'hrOwnFunds', 'COLUMN', N'dtOwnFund'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'величина капитала (если NULL - не в компетенции)', 'SCHEMA', N'dbo', 'TABLE', N'hrOwnFunds', 'COLUMN', N'mnOwnFund'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'основание об изменении', 'SCHEMA', N'dbo', 'TABLE', N'hrOwnFunds', 'COLUMN', N'cmOwnFund'
GO