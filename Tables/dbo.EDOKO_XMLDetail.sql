﻿CREATE TABLE [dbo].[EDOKO_XMLDetail] (
  [idES] [int] NOT NULL,
  [BillNum] [varchar](255) NULL,
  [BillDate] [datetime] NULL,
  [ObligedRegID] [varchar](20) NULL,
  [ShortNameObliged] [varchar](255) NULL,
  [DebtorRegID] [varchar](20) NULL,
  [ShortNameDebtor] [varchar](255) NULL,
  [AssetStatus] [tinyint] NULL,
  [TypeRepmnt] [tinyint] NULL,
  CONSTRAINT [PK_EDOKO_XMLDetail] PRIMARY KEY CLUSTERED ([idES])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Первичный ключ, внешний ключ на EDOKO_hrES.idES', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер КД', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'BillNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата КД', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'BillDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОГРН обязанного лица', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'ObligedRegID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование обязанного лица', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'ShortNameObliged'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОГРН заемщика', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'DebtorRegID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование заемщика', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'ShortNameDebtor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус актива:
	1-включен в состав активов (свободен от залога);
	2-включен в состав активов (в залоге);
	3-подлежит исключению (в залоге);
	4-исключен из пула обеспечения;
	5-на рассмотрении;
	6-рассмотрение приостановлено.', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_XMLDetail', 'COLUMN', N'AssetStatus'
GO