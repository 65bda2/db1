﻿CREATE TABLE [dbo].[hub_jrJournalActive] (
  [DateIt] [datetime] NOT NULL,
  [SysDate] [datetime] NULL,
  [Note] [varchar](2048) NULL,
  [NumActive] [int] NOT NULL,
  [Kategory] [char](2) NULL,
  [idJournal] [int] NOT NULL,
  [NumLetter] [varchar](2048) NULL,
  [Doer] [varchar](255) NULL
)
ON [PRIMARY]
GO