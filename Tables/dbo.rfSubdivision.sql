﻿CREATE TABLE [dbo].[rfSubdivision] (
  [idSubdivision] [int] NOT NULL,
  [KPTU] [varchar](2) NOT NULL,
  [KPHub] [varchar](2) NOT NULL,
  [EDAuthor] [varchar](10) NULL,
  [isLoad] [bit] NULL,
  [isNotErr] [bit] NULL,
  CONSTRAINT [PK_rfSubdivision] PRIMARY KEY CLUSTERED ([idSubdivision])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник распределения ТУ по Хабам', 'SCHEMA', N'dbo', 'TABLE', N'rfSubdivision'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД подразделения БР', 'SCHEMA', N'dbo', 'TABLE', N'rfSubdivision', 'COLUMN', N'idSubdivision'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Двузначный код подразделения, соответствующий полю KP таблицы rfDKKP', 'SCHEMA', N'dbo', 'TABLE', N'rfSubdivision', 'COLUMN', N'KPTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Двузначный код Хаба, к которому будет относиться подразделение БР, соответствующий полю KP таблицы rfDKKP (подгружается из справочника НСИ на основании файла DKKP.DBF)', 'SCHEMA', N'dbo', 'TABLE', N'rfSubdivision', 'COLUMN', N'KPHub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код составителя ЭС, предназначен для определения корректности маршрутизации ЭС от КО, пришедшей в Хаб, а также для отправки ответных ЭС в КО.', 'SCHEMA', N'dbo', 'TABLE', N'rfSubdivision', 'COLUMN', N'EDAuthor'
GO