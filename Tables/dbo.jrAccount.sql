﻿CREATE TABLE [dbo].[jrAccount] (
  [idAcc] [int] IDENTITY,
  [tpCredit] [char](1) NULL,
  [mnLimit] [money] NULL,
  [tpRazdel] [char](1) NULL,
  [idKO] [int] NOT NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('T'),
  [nuGKD] [int] NOT NULL,
  [mnLiability] [money] NULL,
  [dtIncl] [datetime] NOT NULL,
  [dtExcl] [datetime] NULL,
  [isPayForBKL] [char](1) NOT NULL DEFAULT ('F'),
  [nmKO] [varchar](250) NULL,
  [nnKO] [varchar](128) NULL,
  [nuReg] [varchar](9) NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nuAccount] [varchar](20) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [idRKC] [int] NULL,
  [nuCorrAcc] AS ([nuAccount]),
  [tpInstr] [char](1) NOT NULL DEFAULT ('C'),
  [tpAsset] [char](1) NULL,
  CONSTRAINT [PK_jrAccount] PRIMARY KEY CLUSTERED ([idAcc]),
  CONSTRAINT [C_jrAccount_isSootv] CHECK ([isSootv]='N' OR ([isSootv]='F' OR [isSootv]='T')),
  CONSTRAINT [C_jrAccount_tpAsset] CHECK ([tpAsset]='C' OR [tpAsset]='B' OR [tpAsset]='A'),
  CONSTRAINT [C_jrAccount_tpCredit] CHECK ([tpCredit]='C' OR ([tpCredit]='B' OR [tpCredit]='A')),
  CONSTRAINT [C_jrAccount_tpInstr] CHECK ([tpInstr]='C' OR [tpInstr]='B' OR [tpInstr]='A'),
  CONSTRAINT [C_jrAccount_tpRazdel] CHECK ([tpRazdel]='M' OR ([tpRazdel]='B' OR [tpRazdel]=''))
)
ON [PRIMARY]
GO

CREATE INDEX [I_jrAccount_idKO]
  ON [dbo].[jrAccount] ([idKO])
  ON [PRIMARY]
GO

CREATE INDEX [I_jrAccount_nuGKD]
  ON [dbo].[jrAccount] ([nuGKD])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrAccount] ON [dbo].[jrAccount] AFTER INSERT, UPDATE, DELETE
AS
DECLARE @idAcc INTEGER, 
		@idKO INTEGER, 
		@idRKC INTEGER,
		@nuAccount VARCHAR(20),
		@tpGKD CHAR(1),
		@MyVariable CURSOR
  IF EXISTS(SELECT idAcc FROM deleted)
  BEGIN
-- ЕКД	SET @MyVariable = CURSOR FOR SELECT deleted.idAcc, deleted.idKO, deleted.idRKC, deleted.nuAccount, jrGKD.tpGKD FROM deleted JOIN jrGKD ON jrGKD.nuGKD=deleted.nuGKD WHERE jrGKD.tpGKD IN ('B', 'D', 'E', 'F', 'G', 'H')
	SET @MyVariable = CURSOR FOR SELECT deleted.idAcc, deleted.idKO, deleted.idRKC, deleted.nuAccount, (CASE jrGKD.tpGKD WHEN 'K' THEN deleted.tpAsset ELSE jrGKD.tpGKD END) FROM deleted JOIN jrGKD ON jrGKD.nuGKD=deleted.nuGKD WHERE (CASE jrGKD.tpGKD WHEN 'K' THEN deleted.tpAsset ELSE jrGKD.tpGKD END) IN ('B', 'D', 'E', 'F', 'G', 'H') -- ЕКД
    OPEN @MyVariable
    FETCH NEXT FROM @MyVariable INTO @idAcc, @idKO, @idRKC, @nuAccount, @tpGKD
    WHILE @@FETCH_STATUS = 0 
    BEGIN
  	  IF (@idRKC <> (SELECT idRKC FROM jrAccount WHERE idAcc = @idAcc)) or (@nuAccount <> (SELECT nuAccount FROM jrAccount WHERE idAcc = @idAcc)) 
	    UPDATE jrActive set status=status where (jractive.idletter in (select idLetter from jrLetter where jrLetter.idKO = @idKO)) AND (@tpGKD + jractive.TypeActive in ('DM','FE','GI','HYK','HYO','BV','BD', 'EG')) -- tpGKD уже в SELECT переделан в ЕКД
	  FETCH NEXT FROM @MyVariable INTO @idAcc, @idKO, @idRKC, @nuAccount, @tpGKD
    END	
    CLOSE @MyVariable	
    DEALLOCATE @MyVariable
  END
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
--------
CREATE TRIGGER [trLogJrAccount]
ON [dbo].[jrAccount]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idAcc AS VARCHAR(20))+', '+nmKO+', '+nnKO+', '+nuReg+', '+nuBIK+', '+nuINN+', '+nuAccount+', '+nmJurAddr+', '+nmFactAddr+', '+nmNadzTU+', '+
    CAST(idRKC AS VARCHAR(20))+', '+tpCredit+', '+CONVERT(VARCHAR(20), mnLimit)+', '+tpRazdel+', '+
    CAST(idKO AS VARCHAR(20))+', '+isSootv+', '+ CAST(nuGKD AS VARCHAR(20)) FROM deleted
  SELECT @newValues = CAST(idAcc AS VARCHAR(20))+', '+nmKO+', '+nnKO+', '+nuReg+', '+nuBIK+', '+nuINN+', '+nuAccount+', '+nmJurAddr+', '+nmFactAddr+', '+nmNadzTU+', '+
    CAST(idRKC AS VARCHAR(20))+', '+tpCredit+', '+CONVERT(VARCHAR(20), mnLimit)+', '+tpRazdel+', '+
    CAST(idKO AS VARCHAR(20))+', '+isSootv+', '+ CAST(nuGKD AS VARCHAR(20)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список основных счетов', 
   'идентификатор, наименование (полное/краткое), рег.номер, БИК, ИНН, номер основного счета, юр.адрес, факт.адрес, ТУ, идентификатор РКЦ, виды кредитов, лимит кредитования, тип раздела счета депо, идентификатор КО, соответсвие критериям, идентификатор ГКД', 
   @oldValues, @newValues)
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trUpdAccount]
ON [dbo].[jrAccount]
FOR INSERT, UPDATE
AS
  IF UPDATE(isSootv)
    UPDATE jrGKD SET isSootv = CASE WHEN EXISTS(SELECT idAcc FROM jrAccount WHERE nuGKD IN (SELECT nuGKD FROM inserted) AND isSootv = 'F') THEN 'F' ELSE 'T' END
    WHERE nuGKD IN (SELECT nuGKD FROM inserted) AND (tpGKD <> 'K') AND (jrGKD.dtGKDEnd IS NULL OR jrGKD.dtGKDEnd > (SELECT TOP 1 dtWork FROM rfStatus))
GO

ALTER TABLE [dbo].[jrAccount]
  ADD CONSTRAINT [FK_jrAccount_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO])
GO

ALTER TABLE [dbo].[jrAccount]
  ADD CONSTRAINT [FK_jrAccount_nuGKD] FOREIGN KEY ([nuGKD]) REFERENCES [dbo].[jrGKD] ([nuGKD]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список основных счетов', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'idAcc'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Виды предоставляемых кредитов (ВДК и ОВН, Ломбардные, Иные)', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'tpCredit'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'установленный лимит кредитования', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'mnLimit'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип раздела счета депо, в который следует перевести ЦБ после погашения кредита овернайт', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'tpRazdel'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'соответсвие критериям Банка России', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'isSootv'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальная величина обязательств кредитной организации - заемщика', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'mnLiability'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата начала кредитования по основному счету', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'dtIncl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата исключения основного счета из ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'dtExcl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Кредитная организация внесла плату за пользование БКЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'isPayForBKL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'полное наименование КО/филиала', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nmKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'краткое наименование КО/филиала', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nnKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'регистрационный номер КО/филиала', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК кредитной организации/филиала', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuBIK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ИНН', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuINN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'корреспондентский счет КО/филиала (основной счет)/Коррсчет, открытый в уполномоченной РНКО', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuAccount'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'юридический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nmJurAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'фактический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nmFactAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ТУ, осуществляющее банковский надзор', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nmNadzTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор РКЦ/РНКО (в зависимости от вида ГКД)', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'idRKC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'(deprecated) для восстановления работоспособности Блока Витрина 1.6', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'nuCorrAcc'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип инструмента (A - только Аукцион, B - только Фикс, C - все)', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'tpInstr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип обеспечения для ЕКД (A-ценные бумаги корсчет(аналог 236-П), B-активы(аналог 312-П), C-ценные бумаги счет в НРД(аналог КД на СЭТ ММВБ))', 'SCHEMA', N'dbo', 'TABLE', N'jrAccount', 'COLUMN', N'tpAsset'
GO