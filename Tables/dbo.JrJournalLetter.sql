﻿CREATE TABLE [dbo].[JrJournalLetter] (
  [dateIt] [datetime] NOT NULL,
  [SysDate] [datetime] NOT NULL,
  [Note] [varchar](500) NOT NULL,
  [idletter] [int] NOT NULL,
  [Kategory] [char](2) NOT NULL,
  [idJournal] [int] IDENTITY,
  [NumLetter] [varchar](500) NULL,
  CONSTRAINT [PK_JrJournalLetter] PRIMARY KEY CLUSTERED ([idJournal])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogjrJournalLetter]
ON [dbo].[JrJournalLetter]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), DateIt, 104)+', '+CONVERT(VARCHAR(10), SysDate, 104)+', '+
    RTRIM(Note)+', '+CAST(idletter AS VARCHAR(10))+', '+RTRIM(Kategory)+', '+CAST(idJournal AS VARCHAR(10))+ ', '+
    RTRIM(NumLetter) FROM deleted

  SELECT @newValues = CONVERT(VARCHAR(10), DateIt, 104)+', '+CONVERT(VARCHAR(10), SysDate, 104)+', '+
    RTRIM(Note)+', '+CAST(idletter AS VARCHAR(10))+', '+RTRIM(Kategory)+', '+CAST(idJournal AS VARCHAR(10))+ ', '+
    RTRIM(NumLetter) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал ходатайства', 
   'Дата события, Операционная дата, Событие, идентификатор письма, Категория события, идентификатор события, дополнительная информация', 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[JrJournalLetter]
  ADD CONSTRAINT [FK_JrJournalLetter_idLetter] FOREIGN KEY ([idletter]) REFERENCES [dbo].[jrLetter] ([idLetter]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата события', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'dateIt'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Системная дата события', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'SysDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Событие', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'Note'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор письма', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'idletter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид события', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'Kategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор события', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'idJournal'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дополнительная информация', 'SCHEMA', N'dbo', 'TABLE', N'JrJournalLetter', 'COLUMN', N'NumLetter'
GO