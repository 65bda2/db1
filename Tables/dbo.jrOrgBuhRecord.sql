﻿CREATE TABLE [dbo].[jrOrgBuhRecord] (
  [idOrgBuhRecord] [int] IDENTITY,
  [idOrgBuhReport] [int] NOT NULL,
  [idBuhRecord] [int] NOT NULL,
  [ShowingAccSum] [decimal](25, 5) NULL,
  CONSTRAINT [PK_jrOrgBuhRecord] PRIMARY KEY CLUSTERED ([idOrgBuhRecord])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 26.04.2019
-- Description:	Триггер для сохранения истории в таблице hrOrgBuhRecord
-- =============================================
CREATE TRIGGER [trhrOrgBuhRecord]
   ON  [dbo].[jrOrgBuhRecord]
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM inserted)
		INSERT INTO [dbo].[hrOrgBuhRecord] ([OperationDate],[idOrgBuhRecord],[idOrgBuhReport],[idBuhRecord],[ShowingAccSum])
		SELECT (SELECT [dtWork] FROM [dbo].[rfStatus]), [idOrgBuhRecord],[idOrgBuhReport],[idBuhRecord],[ShowingAccSum] FROM inserted

END
GO

ALTER TABLE [dbo].[jrOrgBuhRecord]
  ADD CONSTRAINT [FK_jrOrgBuhRecord_idOrgBuhReport] FOREIGN KEY ([idOrgBuhReport]) REFERENCES [dbo].[jrOrgBuhReport] ([idOrgBuhReport])
GO

ALTER TABLE [dbo].[jrOrgBuhRecord]
  ADD CONSTRAINT [FK_jrOrgBuhRecord_rfBuhRecord] FOREIGN KEY ([idBuhRecord]) REFERENCES [dbo].[rfBuhRecord] ([idBuhRecord])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Хранение данных о финансовых показателях отчетности', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhRecord', 'COLUMN', N'idOrgBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N' ИД записи входящего ЭС С105', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhRecord', 'COLUMN', N'idOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N' Код показателя отчетности idBuhRecord из справочника rfBuhRecord(вторичный ключ к таблице rfBuhRecord полю idBuhRecord)', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhRecord', 'COLUMN', N'idBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сумма соответствующая показателю отчетности ShowingAccCode', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhRecord', 'COLUMN', N'ShowingAccSum'
GO