﻿CREATE TABLE [dbo].[hub_hrTypeCredits] (
  [idAcc] [int] NOT NULL,
  [dtBegin] [datetime] NOT NULL,
  [tpCredit] [char](1) NOT NULL,
  [dtEnd] [datetime] NULL,
  [cmCredit] [char](250) NULL
)
ON [PRIMARY]
GO