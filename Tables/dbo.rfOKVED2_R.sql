﻿CREATE TABLE [dbo].[rfOKVED2_R] (
  [ID] [numeric](12) NOT NULL,
  [CODE] [numeric](10) NOT NULL,
  [N_Razdel] [char](1) NOT NULL,
  [NAME_RUS] [varchar](1000) NULL,
  [CB_Date] [datetime] NULL,
  [CE_Date] [datetime] NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Новый ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'rfOKVED2_R'
GO