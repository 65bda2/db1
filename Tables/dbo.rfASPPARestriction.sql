﻿CREATE TABLE [dbo].[rfASPPARestriction] (
  [idKod] [int] IDENTITY,
  [DownKod] [char](11) NOT NULL,
  [UpKod] [char](11) NOT NULL,
  [dtBegin] [datetime] NULL,
  [dtend] [datetime] NULL
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfASPPARestriction]
ON [dbo].[rfASPPARestriction]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
 SET CONCAT_NULL_YIELDS_NULL OFF
 SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
 IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues =CAST(idKod AS VARCHAR(10))+', '+RTRIM(DownKod) +', '+RTRIM(UpKod) FROM deleted

  SELECT @newValues = CAST(idKod AS VARCHAR(10))+', '+RTRIM(DownKod) +', '+RTRIM(UpKod) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Количественные ограничения и настройки', 
   'Идентификатор записи, Нижняя граница, Верхняя граница', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник "Количественные ограничения и настройки" Допустимые значения ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'rfASPPARestriction'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfASPPARestriction', 'COLUMN', N'idKod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'нижняя граница', 'SCHEMA', N'dbo', 'TABLE', N'rfASPPARestriction', 'COLUMN', N'DownKod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'верхняя граница', 'SCHEMA', N'dbo', 'TABLE', N'rfASPPARestriction', 'COLUMN', N'UpKod'
GO