﻿CREATE TABLE [dbo].[hrBIC] (
  [BIC] [varchar](9) NOT NULL,
  [RegN] [varchar](9) NULL,
  [NameP] [varchar](250) NOT NULL,
  [PtType] [varchar](2) NOT NULL,
  [Nnp] [varchar](25) NULL,
  [Adr] [varchar](160) NULL,
  [DateIn] [datetime] NOT NULL,
  [DateOut] [datetime] NULL,
  [Account] [varchar](20) NULL,
  [AccountCBRBIC] [varchar](9) NULL,
  [Account_DateIn] [datetime] NULL,
  [Account_DateOut] [datetime] NULL,
  [BusinessDay] [datetime] NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК (Bnkseek.NewNum)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'BIC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный порядковый номер (Bnkseek.RegN)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'RegN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование (Bnkseek.NameP)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'NameP'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип участника перевода (Bnkseek.Pzn)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'PtType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование населенного пункта (Bnkseek.Nnp)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'Nnp'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Адрес (Bnkseek.Adr)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'Adr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения в состав (Bnkseek.Date_In)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'DateIn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата исключения информации (Bnkseek.Date_Out)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'DateOut'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер счета (Bnkseek.Ksnp)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'Account'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК ПБР, обслуживающего счет участника перевода. (Bnkseek.Rkc)', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'AccountCBRBIC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата открытия счета', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'Account_DateIn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата исключения информации о счете', 'SCHEMA', N'dbo', 'TABLE', N'hrBIC', 'COLUMN', N'Account_DateOut'
GO