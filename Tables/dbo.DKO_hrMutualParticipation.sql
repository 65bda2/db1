﻿CREATE TABLE [dbo].[DKO_hrMutualParticipation] (
  [IdRecord] [bigint] IDENTITY,
  [_IdRecord] [int] NOT NULL,
  [IdMutualParticipation] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [IdCompany] [numeric](16) NOT NULL,
  [CORegNum] [varchar](20) NOT NULL,
  [IsCompanyOwner] [tinyint] NOT NULL,
  [AlterComment] [varchar](500) NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL,
  CONSTRAINT [PK_DKO_hrMutualParticipation] PRIMARY KEY CLUSTERED ([IdRecord]),
  CONSTRAINT [CK_DKO_hrMutualParticipation_CorrectionType] CHECK ([CorrectionType]>=(1) AND [CorrectionType]<=(4)),
  CONSTRAINT [CK_DKO_hrMutualParticipation_IsArchive] CHECK ([IsArchive]>=(0) AND [IsArchive]<=(1)),
  CONSTRAINT [CK_DKO_hrMutualParticipation_StateSED] CHECK ([StateSED]>=(-3) AND [StateSED]<=(3))
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор В АС "Сибирь"', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'_IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный начальный отрицательный идентификатор В АСДКО', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'IdMutualParticipation'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, с которой действует данный набор значений', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'D_From'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор обновления', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'IdUpdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Состояния: -3 - Для проверки контролёром, -2 - Стартовое решение, -1 - Не отправлять, 0 - Неотправленное, 1 - Отправленное, 2 - Подтверждённое, 3 - Отклонённое', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'StateSED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - Запись активна, 1 - Запись в архиве', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - Создание, 2 - Изменение, 3 - Удаление ошибочной, 4 - Удаление по актуальности', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'CorrectionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'ESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrMutualParticipation', 'COLUMN', N'ESNo'
GO