﻿CREATE TABLE [dbo].[DKO_rfFields] (
  [IdField] [int] IDENTITY,
  [IdTable] [int] NOT NULL,
  [nmField] [varchar](50) NOT NULL,
  CONSTRAINT [XPKDKO_rfFields] PRIMARY KEY CLUSTERED ([IdField])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [XAK1DKO_rfFields]
  ON [dbo].[DKO_rfFields] ([IdTable], [nmField])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[DKO_rfFields]
  ADD CONSTRAINT [FK_DKO_rfFields_DKO_rfTable] FOREIGN KEY ([IdTable]) REFERENCES [dbo].[DKO_rfTable] ([idTable])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор таблицы: 0 - Company, 1 - MutualParticipation, 2 - Asset, 3 - Repayment, 4 - BankInTU, 5 - AssetState', 'SCHEMA', N'dbo', 'TABLE', N'DKO_rfFields', 'COLUMN', N'IdTable'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя поля', 'SCHEMA', N'dbo', 'TABLE', N'DKO_rfFields', 'COLUMN', N'nmField'
GO