﻿CREATE TABLE [dbo].[rfSignRekvizitASPPA] (
  [tpSign] [varchar](50) NOT NULL,
  [nmSign] [varchar](400) NULL,
  [tpGroup] [char](1) NOT NULL DEFAULT ('A'),
  CONSTRAINT [PK_rfSignRekvizitASPPA] PRIMARY KEY CLUSTERED ([tpSign])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfSignRekvizitASPPA]
ON [dbo].[rfSignRekvizitASPPA]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
 SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  SELECT @oldValues =  RTRIM(tpSign)+', '+RTRIM(nmSign)+', '+ RTRIM(tpGroup) FROM deleted
  SELECT @newValues =  RTRIM(tpSign)+', '+RTRIM(nmSign)+', '+ RTRIM(tpGroup) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Реквизиты подписей АСППА', 
   'Тип реквизита, Значение, Группа ', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Общие реквизиты подписей (настройки)', 'SCHEMA', N'dbo', 'TABLE', N'rfSignRekvizitASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование показателя', 'SCHEMA', N'dbo', 'TABLE', N'rfSignRekvizitASPPA', 'COLUMN', N'tpSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'значение показателя', 'SCHEMA', N'dbo', 'TABLE', N'rfSignRekvizitASPPA', 'COLUMN', N'nmSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип показателя A-базовый', 'SCHEMA', N'dbo', 'TABLE', N'rfSignRekvizitASPPA', 'COLUMN', N'tpGroup'
GO