﻿CREATE TABLE [dbo].[hub_hrWaitCheck] (
  [NumActive] [int] NULL,
  [dtBegin] [datetime] NOT NULL,
  [dtEnd] [datetime] NULL,
  [idTransh] [int] NULL
)
ON [PRIMARY]
GO