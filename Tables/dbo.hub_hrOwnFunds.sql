﻿CREATE TABLE [dbo].[hub_hrOwnFunds] (
  [idKO] [int] NOT NULL,
  [dtOwnFund] [datetime] NOT NULL,
  [mnOwnFund] [money] NULL,
  [cmOwnFund] [varchar](500) NULL
)
ON [PRIMARY]
GO