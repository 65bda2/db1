﻿CREATE TABLE [dbo].[ASBO_AFL] (
  [idMainDataES] [bigint] NOT NULL,
  [dtbegin] [datetime] NOT NULL,
  [dtend] [datetime] NULL,
  [IsCompanyOwner] [int] NOT NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_AFL_idMainDataES]
  ON [dbo].[ASBO_AFL] ([idMainDataES])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Информация об аффилированности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_AFL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на родительский элемент (ASBO_MainDataEs)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_AFL', 'COLUMN', N'idMainDataES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'начало аффилированности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_AFL', 'COLUMN', N'dtbegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'окончание аффилированности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_AFL', 'COLUMN', N'dtend'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак аффилированности организации: 0 - организация участвует в капитале КО, 1 - КО участвует в капитале организации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_AFL', 'COLUMN', N'IsCompanyOwner'
GO