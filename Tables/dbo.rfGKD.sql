﻿CREATE TABLE [dbo].[rfGKD] (
  [idDopSogl] [int] IDENTITY,
  [nuGKD] [int] NOT NULL,
  [nuDopSogl] [char](30) NOT NULL,
  [dtDopSogl] [datetime] NOT NULL,
  [cmDopSogl] [varchar](500) NOT NULL,
  CONSTRAINT [PK_rfGKD] PRIMARY KEY CLUSTERED ([idDopSogl])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfGKD]
ON [dbo].[rfGKD]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(nuGKD AS VARCHAR(20))+', '+RTRIM(nuDopSogl)+', '+
    CONVERT(VARCHAR(10), dtDopSogl, 104)+', '+RTRIM(cmDopSogl) FROM deleted
  SELECT CAST(nuGKD AS VARCHAR(20))+', '+RTRIM(nuDopSogl)+', '+
    CONVERT(VARCHAR(10), dtDopSogl, 104)+', '+RTRIM(cmDopSogl) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
  'Список Дополнительных соглашений', 
  'идентификатор ГКД, номер доп.соглашения, дата доп.соглашения, содержание', @oldValues, @newValues)
GO

ALTER TABLE [dbo].[rfGKD]
  ADD CONSTRAINT [FK_rfGKD_nuGKD] FOREIGN KEY ([nuGKD]) REFERENCES [dbo].[jrGKD] ([nuGKD]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дополнительные соглашения', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор доп. соглашения', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD', 'COLUMN', N'idDopSogl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор ГКД', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD', 'COLUMN', N'nuGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер доп. соглашения', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD', 'COLUMN', N'nuDopSogl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата доп. соглашения', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD', 'COLUMN', N'dtDopSogl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'содержание доп.соглашения', 'SCHEMA', N'dbo', 'TABLE', N'rfGKD', 'COLUMN', N'cmDopSogl'
GO