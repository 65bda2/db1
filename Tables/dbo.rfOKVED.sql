﻿CREATE TABLE [dbo].[rfOKVED] (
  [CODE] [int] NOT NULL,
  [KOD] [char](8) NOT NULL,
  [NAME_RUS] [varchar](1000) NOT NULL,
  [N_RAZDEL] [char](1) NULL,
  [CB_Date] [datetime] NULL,
  [CE_Date] [datetime] NULL,
  [ID] [numeric](12) NULL,
  [CODEFORPPA] [int] NULL,
  CONSTRAINT [PK_rfOKVED] PRIMARY KEY CLUSTERED ([CODE])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник видов экономической деятельности', 'SCHEMA', N'dbo', 'TABLE', N'rfOKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKVED', 'COLUMN', N'CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'rfOKVED', 'COLUMN', N'KOD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'rfOKVED', 'COLUMN', N'NAME_RUS'
GO