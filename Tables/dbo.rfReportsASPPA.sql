﻿CREATE TABLE [dbo].[rfReportsASPPA] (
  [idReport] [int] IDENTITY,
  [dtreport] [datetime] NOT NULL,
  [nuReport] [int] NOT NULL,
  [tpreport] [char](1) NOT NULL,
  [nuIshod] [varchar](30) NOT NULL,
  [idKO] [int] NULL,
  [idORG] [int] NULL,
  [Numactive] [int] NULL,
  [nmUser] [varchar](50) NOT NULL,
  [komment] [varchar](255) NULL,
  CONSTRAINT [C_rfReportsASPPA_tpreport] CHECK ([tpreport]='N' OR [tpreport]='O')
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfReportsASPPA]
ON [dbo].[rfReportsASPPA]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  SELECT @oldValues = CAST(idReport AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), dtreport, 104)+', '+ CAST(nuReport AS VARCHAR(10))+', '
  + RTRIM(tpreport)+', '+RTRIM(nuIshod)+', '+CAST(idKO AS VARCHAR(10))+', '+CAST(idORG AS VARCHAR(10))+', '+CAST(Numactive AS VARCHAR(10))+
  ', '+ RTRIM(nmUser)+', '+RTRIM(komment) FROM deleted

  SELECT @newValues =  CAST(idReport AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), dtreport, 104)+', '+ CAST(nuReport AS VARCHAR(10))+', '
  + RTRIM(tpreport)+', '+RTRIM(nuIshod)+', '+CAST(idKO AS VARCHAR(10))+', '+CAST(idORG AS VARCHAR(10))+', '+CAST(Numactive AS VARCHAR(10))+
  ', '+ RTRIM(nmUser)+', '+RTRIM(komment) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал отчетов АСППА', 
   'Идентификатор отчета, Дата отчета, порядковый номер, Тип , Исходящий номер, КО, Организация, '+
   'Актив, Пользователь, Комментарий' , 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал  отчётов', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'idReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата отчёта', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'dtreport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'порядковый номер отчёта', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'nuReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип отчета', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'tpreport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'исходящий номер', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'nuIshod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор организации', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'idORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на активы', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'Numactive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'имя пользователя', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'nmUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'комментарий', 'SCHEMA', N'dbo', 'TABLE', N'rfReportsASPPA', 'COLUMN', N'komment'
GO