﻿CREATE TABLE [dbo].[lgOrder] (
  [idOrder] [int] IDENTITY,
  [dtOrder] [datetime] NOT NULL,
  [tmOrder] [datetime] NULL,
  [nuOrder] [int] NULL,
  [idBlock] [int] NULL,
  [tpOrder] [char](1) NOT NULL,
  [idRKC] [int] NULL,
  [nuIshod] [char](30) NULL,
  [nuCred] [char](20) NULL,
  [mnUst] [money] NULL,
  [mnCalc] [money] NULL,
  [tmUstan] [char](5) NULL,
  [isUstan] [char](1) NULL DEFAULT ('S'),
  [isCurr] [char](1) NULL DEFAULT ('F'),
  [nmUser] [char](50) NOT NULL,
  [dtUstan] [datetime] NULL,
  CONSTRAINT [PK_lgOrder] PRIMARY KEY CLUSTERED ([idOrder]),
  CONSTRAINT [C_lgOrder_isCurr] CHECK ([isCurr]='F' OR [isCurr]='T'),
  CONSTRAINT [C_lgOrder_isUstan] CHECK ([isUstan]='Z' OR ([isUstan]='F' OR ([isUstan]='T' OR [isUstan]='S'))),
  CONSTRAINT [C_lgOrder_tpOrder] CHECK ([tpOrder]='K' OR [tpOrder]='J' OR [tpOrder]='A' OR [tpOrder]='Y' OR [tpOrder]='X' OR [tpOrder]='W' OR [tpOrder]='V' OR [tpOrder]='O' OR [tpOrder]='T' OR [tpOrder]='B' OR [tpOrder]='U' OR [tpOrder]='F' OR [tpOrder]='I' OR [tpOrder]='Z' OR [tpOrder]='M' OR [tpOrder]='P' OR [tpOrder]='G' OR [tpOrder]='S' OR [tpOrder]='L' OR [tpOrder]='N' OR [tpOrder]='D')
)
ON [PRIMARY]
GO

CREATE INDEX [I_lgOrder_dtOrder_idBlock_tpOrder]
  ON [dbo].[lgOrder] ([dtOrder], [idBlock], [tpOrder])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogLgOrder]
ON [dbo].[lgOrder]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idOrder AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtOrder, 104)+', '+CONVERT(VARCHAR(10), tmOrder, 108)+', '+
   CAST(nuOrder AS VARCHAR(20))+', '+CAST(idBlock AS VARCHAR(20))+', '+tpOrder+', '+CAST(idRKC AS VARCHAR(20))+', '+RTRIM(nuIshod)+', '+
   RTRIM(nuCred)+', '+CONVERT(VARCHAR(20), mnUst)+', '+CONVERT(VARCHAR(20), mnCalc)+', '+tmUstan+', '+isUstan+', '+isCurr+', '+nmUser FROM deleted
  SELECT @newValues = CAST(idOrder AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtOrder, 104)+', '+CONVERT(VARCHAR(10), tmOrder, 108)+', '+
   CAST(nuOrder AS VARCHAR(20))+', '+CAST(idBlock AS VARCHAR(20))+', '+tpOrder+', '+CAST(idRKC AS VARCHAR(20))+', '+RTRIM(nuIshod)+', '+
   RTRIM(nuCred)+', '+CONVERT(VARCHAR(20), mnUst)+', '+CONVERT(VARCHAR(20), mnCalc)+', '+tmUstan+', '+isUstan+', '+isCurr+', '+nmUser FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал распоряжений', 
   'идентификатор, дата распоряжения, время распоряжения, рег.номер, идентификатор ББР, тип распоряжения, идентификатор РКЦ, исходящий номер, рег.номер кредита, установленный лимит или сумма кредита, '+
   'расчетный лимит или сумма залога или сумма начисленных процентов, время исполнения распоряжения, признак установления лимита, признак "текущий лимит", уполномоченный сотрудник', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'журнал распоряжений РКЦ по датам', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'idOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата формирования распоряжения/уведомления', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'dtOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Время формирования распоряжения/уведомления', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'tmOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер распоряжения/уведомления', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'nuOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор ББР', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'idBlock'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип распоряжения/уведомления
A - Распоряжение на списание с внебалансового учета пакетов с активами, выданных ранее под отчет
B - Письмо о направлении копии Уведомления об изменении процентных ставок в адрес КО-поручителя (если кредит под поручительства)
D - распоряжение на установку лимита внутридневного кредита
F - информация для подготовки извещения/уведомления
G - распоряжение на погашение кредита
I - извещение о предоставлении кредитов/уведомление об изменении состава залога по кредиту обеспеченному залогом золота
J - Письмо в СЭД о выводе активов (более не используется, только для предыдущей хронологии)
K - Регистрация писем в КО о рассчитанном лимите БКЛ
L - распоряжение на выдачу ломбардного/иного кредита
M - распоряжение на перенос процентов при классификации/переклассификации
N - распоряжение на выдачу кредита овернайт
O - распоряжение на отражение просроченных процентов
P - распоряжение на отражение процентов
S - распоряжение на списание обеспечения
T - Распоряжение об отражении новой величины поручительств(при изменении процентных ставок)
U - уведомление об нзменении процентных ставок по выданным кредитам Банка России в адрес КО - заемщика
V - распоряжение на передачу активов на хранение
W - распоряжение на передачу активов, ранее выданных подотчет, на хранение
X - распоряжение на возврат активов
Y - распоряжение на возврат активов подотчет
Z - распоряжение на замену обеспечения по кредиту', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'tpOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор РКЦ(В каком РКЦ) (для tpOrder = B в этом поле хранится idKO банка - поручителя)', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'idRKC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Исходящий номер', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'nuIshod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер кредита или для лимита два символа - признаки распоряжения, третий - признак ролл-овера: nuCred[1](только для tpOrder = D), A - Вид имущества: ценные бумаги, B - Вид имущества: имущество, кроме ценных бумаг, C - Вид имущества: ценные бумаги и(или) иное имущество, D - Нет имущества (лимит = 0), E - Вид имущества: слитки золота, nuCred[2](только для tpOrder = D), T - Существует неопределенность в получении дохода по кредитной операции, F - НЕ существует неопределенности в получении дохода по кредитной операции, nuCred[3](только для tpOrder = D), T - лимит ВДК с учетом ролл-овер ОВН, F - лимит ВДК БЕЗ учета ролл-овер ОВН
(только для tpOrder = D)
A - Вид имущества: ценные бумаги.
B - Вид имущества: имущество, кроме ценных бумаг.
C - Вид имущества: ценные бумаги и(или) иное имущество.
D - Нет имущества (лимит = 0)
E - Вид имущества: слитки золота.
F - НЕ существует неопределенности в получении дохода по кредитной операции
F - лимит ВДК БЕЗ учета ролл-овер ОВН
T - лимит ВДК с учетом ролл-овер ОВН
T - Существует неопределенность в получении дохода по кредитной операции', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'nuCred'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сумма установленного лимита (для tpOrder = D, K) или сумма кредита или процентная ставка (для tpOrder = U, B)', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'mnUst'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Стоимость активов с учетом попр.коэф.(для tpOrder = D) или сумма залога или сумма начисленных процентов(для tpOrder = P,M,G)', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'mnCalc'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'время выполнения последней операции с распоряжением на лимит', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'tmUstan'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак исполнения распоряжения для установления лимита ВДК
F - лимит не установлен
S - направлено на исполнение в расчетное подразделение
T - лимит установлен
Z - распоряжение аннулировано', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'isUstan'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак действующего в расчетной системе лимита ВДК', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'isCurr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уполномоченный сотрудник', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'nmUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата, на которую действует распоряжение', 'SCHEMA', N'dbo', 'TABLE', N'lgOrder', 'COLUMN', N'dtUstan'
GO