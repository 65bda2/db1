CREATE TABLE [dbo].[test2] (
  [id] [int] IDENTITY,
  [name2] [varchar](100) NULL,
  [isP] [bit] NULL,
  CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак', 'SCHEMA', N'dbo', 'TABLE', N'test2', 'COLUMN', N'isP'
GO