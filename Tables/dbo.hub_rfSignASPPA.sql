﻿CREATE TABLE [dbo].[hub_rfSignASPPA] (
  [idsign] [int] NOT NULL,
  [tpSign] [char](1) NOT NULL,
  [nmSign] [varchar](400) NOT NULL,
  [nmPost] [varchar](400) NOT NULL,
  [Dopinf] [char](15) NOT NULL DEFAULT ('000000000000000'),
  [NumOrder] [int] NOT NULL,
  [tpGroup] [char](2) NULL,
  [foundation] [varchar](255) NULL
)
ON [PRIMARY]
GO