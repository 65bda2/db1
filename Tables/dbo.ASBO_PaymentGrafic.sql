﻿CREATE TABLE [dbo].[ASBO_PaymentGrafic] (
  [DatePay] [datetime] NOT NULL,
  [Payment] [decimal](18, 2) NOT NULL,
  [unnum] [char](16) NULL,
  [idMainDataES] [bigint] NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_PaymentGrafic_idMainDataES]
  ON [dbo].[ASBO_PaymentGrafic] ([idMainDataES], [unnum])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Информация о графике платежей', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_PaymentGrafic'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата платежа', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_PaymentGrafic', 'COLUMN', N'DatePay'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Платеж', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_PaymentGrafic', 'COLUMN', N'Payment'
GO