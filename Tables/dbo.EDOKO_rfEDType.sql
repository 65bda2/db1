﻿CREATE TABLE [dbo].[EDOKO_rfEDType] (
  [EDType] [varchar](6) NOT NULL,
  [EDTypeAnswer] [varchar](6) NULL,
  [Description] [varchar](255) NULL,
  [Direction] [tinyint] NOT NULL,
  [DayControl] [tinyint] NULL,
  [isControlES] [bit] NOT NULL,
  CONSTRAINT [PK_EDOKO_rfEDType] PRIMARY KEY CLUSTERED ([EDType])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_EDOKO_rfEDType]
  ON [dbo].[EDOKO_rfEDType] ([EDType], [EDTypeAnswer], [Direction], [DayControl])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[EDOKO_rfEDType]
  ADD CONSTRAINT [FK_EDOKO_rfEDTypeEDType_EDOKO_rfEDTypeEDTypeAnswer] FOREIGN KEY ([EDTypeAnswer]) REFERENCES [dbo].[EDOKO_rfEDType] ([EDType])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Типы ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование типа', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'EDType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответное сообщение', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'EDTypeAnswer'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Назначение ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'Description'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Напрвление: 1 - Входящее в АС ППА; 2 - Исходящее из АС ППА', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'Direction'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество рабочих дней для контроля срока ответа', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'DayControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак того, что ЭС попадает в форму контроля', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_rfEDType', 'COLUMN', N'isControlES'
GO