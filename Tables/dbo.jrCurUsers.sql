﻿CREATE TABLE [dbo].[jrCurUsers] (
  [nmRealUser] [char](50) NOT NULL,
  [nmSysUser] [char](50) NOT NULL DEFAULT (suser_sname()),
  [hostName] [char](50) NOT NULL DEFAULT (host_name()),
  [spid] [smallint] NOT NULL DEFAULT (@@spid),
  [dtLogUser] [datetime] NOT NULL DEFAULT (getdate()),
  CONSTRAINT [PK_jrCurUsers] PRIMARY KEY CLUSTERED ([nmRealUser], [hostName])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogEnterUsers]
ON [dbo].[jrCurUsers]
FOR INSERT, UPDATE
AS
 DECLARE @userName  CHAR(50)
  SELECT @userName = nmRealUser FROM inserted
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  INSERT INTO lgControl (tpAction, nmUser) VALUES ('E', @userName)
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogQuitUsers]
ON [dbo].[jrCurUsers]
FOR DELETE
AS
 DECLARE @userName  CHAR(50)
  SELECT @userName = nmRealUser FROM deleted
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  INSERT INTO lgControl (tpAction, nmUser) VALUES ('Q', @userName)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список активных пользователей', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'реальное имя пользователя базы данных', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers', 'COLUMN', N'nmRealUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'системное имя пользователя базы данных', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers', 'COLUMN', N'nmSysUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'workstation name', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers', 'COLUMN', N'hostName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'SQL Server process ID for sysUser', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers', 'COLUMN', N'spid'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата и время входа пользователя', 'SCHEMA', N'dbo', 'TABLE', N'jrCurUsers', 'COLUMN', N'dtLogUser'
GO