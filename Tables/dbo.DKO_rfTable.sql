﻿CREATE TABLE [dbo].[DKO_rfTable] (
  [idTable] [int] NOT NULL,
  [nmTable] [varchar](255) NOT NULL,
  [IsIn] [int] NOT NULL,
  [IsOut] [int] NULL,
  CONSTRAINT [PK_DKO_rfTable] PRIMARY KEY CLUSTERED ([idTable])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование сущности', 'SCHEMA', N'dbo', 'TABLE', N'DKO_rfTable', 'COLUMN', N'nmTable'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сущность может быть во входящих АС Сибирь/АС ППА 0 - нет, 1 - да', 'SCHEMA', N'dbo', 'TABLE', N'DKO_rfTable', 'COLUMN', N'IsIn'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сущность может быть во исходящих АС Сибирь/АС ППА 0 - нет, 1 - да', 'SCHEMA', N'dbo', 'TABLE', N'DKO_rfTable', 'COLUMN', N'IsOut'
GO