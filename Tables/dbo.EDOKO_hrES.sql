﻿CREATE TABLE [dbo].[EDOKO_hrES] (
  [EDDate] [smalldatetime] NOT NULL,
  [EDNo] [varchar](9) NOT NULL,
  [idKO] [int] NULL,
  [idORG] [int] NULL,
  [EDAuthor] [char](10) NOT NULL,
  [EDRecipient] [char](10) NOT NULL,
  [FileVersion] [tinyint] NOT NULL,
  [EDFileName] [varchar](33) NOT NULL,
  [CtrlCode] [varchar](4) NULL,
  [CtrlDateTime] [datetime] NULL,
  [EDType] [varchar](6) NOT NULL,
  [Annotation] [varchar](2048) NULL,
  [RefEDDate] [smalldatetime] NULL,
  [RefEDNo] [varchar](9) NULL,
  [RefEDAuthor] [char](10) NULL,
  [XMLBody] [xml] NULL,
  [CtrlCodeComment] [text] NULL,
  [ParseComment] [text] NULL,
  [IsSent] [int] NOT NULL DEFAULT (0),
  [EDStatus] [char](1) NOT NULL,
  [UnNum] [varchar](16) NULL,
  [IsArchive] [tinyint] NULL DEFAULT (0),
  [NuRegKO] [varchar](9) NULL,
  [ChangeDate] [smalldatetime] NULL,
  [Doer] [varchar](100) NULL,
  [DocFileName] [varchar](255) NULL,
  [INNORG] [varchar](12) NULL,
  [PathTOFile] [varchar](2000) NULL,
  [ResultCode] [tinyint] NULL,
  [EDRegNo] AS (case left([EDType],(2)) when 'C1' then CONVERT([varchar](21),((left([EDRecipient],(2))+[EDType])+replace(CONVERT([varchar],[EDDate],(4)),'.',''))+[EDNo],(0))  end),
  [EDCreateDateTime] [datetime] NULL DEFAULT (getdate()),
  [EDDescription] [varchar](2000) NULL,
  [idES] [int] IDENTITY,
  [IKFileName] [varchar](255) NULL,
  [ChangeView] [varchar](70) NULL,
  [BankShortName] [varchar](255) NULL,
  [idHeadES] [int] NULL,
  CONSTRAINT [PK_EDOKO_idES] PRIMARY KEY CLUSTERED ([idES])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

CREATE UNIQUE INDEX [IX_EDOKO_hrES_3]
  ON [dbo].[EDOKO_hrES] ([idES], [EDType], [idHeadES])
  ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UI_EDOKO_hrES_EDDate_EDNo_EDAuthor]
  ON [dbo].[EDOKO_hrES] ([EDDate], [EDNo], [EDAuthor])
  ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UI_EDOKO_hrES_ESFileName]
  ON [dbo].[EDOKO_hrES] ([EDFileName])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 31.07.2019
-- Description:	Заполнение таблицы EDOKO_XMLDetail из поля EDOKO_hrES.XMLBody
-- =============================================
CREATE TRIGGER [trEDOKO_hrES_XMLDetail]
   ON  [dbo].[EDOKO_hrES]
   AFTER INSERT,DELETE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 idES FROM deleted)
		DELETE FROM EDOKO_XMLDetail WHERE idES in (SELECT idES FROM deleted)

	IF EXISTS(SELECT TOP 1 idES FROM inserted)
	BEGIN
		DECLARE @idES int

		DECLARE cr CURSOR FOR 
		SELECT [idES] FROM inserted

		OPEN cr

		FETCH NEXT FROM cr
		INTO @idES

		WHILE @@FETCH_STATUS = 0
		BEGIN
	
			EXEC [dbo].[spFill_EDOKO_XMLDetail] @idES

			FETCH NEXT FROM cr
			INTO @idES
		END

		CLOSE cr
		DEALLOCATE cr

	END
END
GO

ALTER TABLE [dbo].[EDOKO_hrES]
  ADD CONSTRAINT [FK_EDOKO_hrES_EDOKO_rfEDType] FOREIGN KEY ([EDType]) REFERENCES [dbo].[EDOKO_rfEDType] ([EDType])
GO

ALTER TABLE [dbo].[EDOKO_hrES]
  ADD CONSTRAINT [FK_EDOKO_hrESidES_EDOKO_hrESidHeadES] FOREIGN KEY ([idHeadES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Электронные сообщения , принимаемые и формируемые в рамках электронного обмена с КО', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата операционного дня', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер ЭС в течении опердня', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор составителя ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDAuthor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор получателя ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDRecipient'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Версия ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'FileVersion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла, содержащего ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDFileName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код результата обработки ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'CtrlCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Календарные дата и время проведения обработки исходного ЭС (реальное у нас)', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'CtrlDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'KOxxxx', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'Annotation'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Дата операционного дня', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'RefEDDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Номер ЭС в течение опердня', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'RefEDNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Уникальный идентификатор составителя ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'RefEDAuthor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Содержимое ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'XMLBody'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст пояснения или детальная диагностика ошибки из ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'CtrlCodeComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Диагностика ошибки при обработке ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'ParseComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЭС отправлено/не отправлено по почте или через СДС: 0 - не отправлено/не требует отправки, 1 - отправлено и ожидает ответ из КО, 2 - от ко получен положительный ответ, 3 - от ко получен отрицательный ответ, 4 - отправлено и от ко ответ не требуется, 5 - ЭС успешно принято ЕСОД, 6 - ЕСОД забраковал ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'IsSent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус ЭС: R - ожидает отправки, Т- по сообщению принято положительное решение, S - отправлено, F - сообщение не принято, L - ожидается принятие решения по сообщению, P - по сообщению сформирован отказ', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'EDStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер актива', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'UnNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - Сообщение перемещено в Архив', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер КО', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'NuRegKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата фактического изменения параметров для С102, С104, С202, С204.', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'ChangeDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Исполнитель', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'Doer'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла в приложении', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'DocFileName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИНН организации', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'INNORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Путь к файлу с нестуктуированной информацией', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'PathTOFile'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Кодовый результат, передающийся в ответных ЭС', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'ResultCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид изменения', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'ChangeView'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Краткое наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'BankShortName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Фишхук на поле idES', 'SCHEMA', N'dbo', 'TABLE', N'EDOKO_hrES', 'COLUMN', N'idHeadES'
GO