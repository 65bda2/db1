﻿CREATE TABLE [dbo].[hrOrgBuhRecord] (
  [idRecord] [int] IDENTITY,
  [OperationDate] [datetime] NOT NULL,
  [DFrom] [datetime] NOT NULL CONSTRAINT [DF_hrOrgBuhRecord_DFrom] DEFAULT (getdate()),
  [UserName] [varchar](128) NOT NULL CONSTRAINT [DF_hrOrgBuhRecord_UserName] DEFAULT (suser_sname()),
  [idOrgBuhRecord] [int] NOT NULL,
  [idOrgBuhReport] [int] NOT NULL,
  [idBuhRecord] [int] NOT NULL,
  [ShowingAccSum] [decimal](25, 5) NULL,
  CONSTRAINT [PK_hrOrgBuhRecord] PRIMARY KEY CLUSTERED ([idRecord])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'История хранения данных о финансовых показателях отчетности', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'idRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Операционый день, в который произошли изменения', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'OperationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время создания записи', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'DFrom'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин пользователя, который создал запись', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'UserName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи jrOrgBuhRecord.idOrgBuhRecord', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'idOrgBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи входящего ЭС С105', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'idOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код показателя отчетности idBuhRecord из справочника rfBuhRecord(вторичный ключ к таблице rfBuhRecord полю idBuhRecord)', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'idBuhRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сумма соответствующая показателю отчетности ShowingAccCode', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhRecord', 'COLUMN', N'ShowingAccSum'
GO