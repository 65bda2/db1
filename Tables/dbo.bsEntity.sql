﻿CREATE TABLE [dbo].[bsEntity] (
  [idEntity] [int] IDENTITY,
  [TypeEntity] [varchar](2) NOT NULL,
  [NameEntity] [varchar](128) NULL,
  [TableEntity] [varchar](24) NOT NULL,
  [CaptionEntity] [varchar](128) NULL,
  [DateCrt] [datetime] NOT NULL CONSTRAINT [DF_bsEntity_DateCrt] DEFAULT (getdate()),
  CONSTRAINT [PK_bsEntity] PRIMARY KEY CLUSTERED ([idEntity])
)
ON [PRIMARY]
GO