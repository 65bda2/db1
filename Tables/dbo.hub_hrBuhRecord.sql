﻿CREATE TABLE [dbo].[hub_hrBuhRecord] (
  [OperationDate] [datetime] NOT NULL,
  [DFrom] [datetime] NOT NULL,
  [idRecord] [int] NOT NULL,
  [RValue] [decimal](25, 2) NULL,
  [idOrg] [int] NOT NULL,
  [idperiod] [int] NOT NULL
)
ON [PRIMARY]
GO