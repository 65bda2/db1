﻿CREATE TABLE [dbo].[rfActiveGroup] (
  [IdActiveGroup] [int] IDENTITY,
  [idParentGroup] [int] NULL,
  [GroupName] [varchar](255) NOT NULL,
  [GroupCode] [varchar](10) NOT NULL,
  CONSTRAINT [PK_rfActiveGroup] PRIMARY KEY CLUSTERED ([IdActiveGroup])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[rfActiveGroup]
  ADD CONSTRAINT [FK_rfActiveGroup_idParentGroup] FOREIGN KEY ([idParentGroup]) REFERENCES [dbo].[rfActiveGroup] ([IdActiveGroup])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица является справочником групп (видов разделов) активов, используемых в АС ППА в окне «АС ППА – Приём и учёт активов»', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор группы', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveGroup', 'COLUMN', N'IdActiveGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор вшестояей руппы. Ссылка на таблицу rfActiveGroup', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveGroup', 'COLUMN', N'idParentGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование группы', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveGroup', 'COLUMN', N'GroupName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код группы, например 1., 1.1, 1.2, 2. ...', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveGroup', 'COLUMN', N'GroupCode'
GO