﻿CREATE TABLE [dbo].[jrGKD] (
  [nuGKD] [int] IDENTITY,
  [dtGKD] [datetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('T'),
  [mnLimitAgg] [money] NULL,
  [dtGKDend] [datetime] NULL,
  [numGKD] [varchar](15) NOT NULL,
  [tpGKD] [char](1) NOT NULL,
  [isTreb] [char](1) NOT NULL DEFAULT ('F'),
  [dtGKDUved] [datetime] NULL,
  [isPartner] [char](1) NOT NULL DEFAULT ('F'),
  [nuGKD236] [int] NULL,
  [nuGKD312] [int] NULL,
  [nuGKD362] [int] NULL,
  [nmKO] [varchar](250) NULL,
  [nnKO] [varchar](128) NULL,
  [nuReg] [varchar](9) NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [nuEKD] [int] NULL,
  [dtEKDBegin] AS ([dbo].[GetDtEKDBegin]([dtGKD],[tpGKD])),
  [dtGKDStop] AS ([dbo].[GetDtGKDStop]([tpGKD],[dtGKDEnd])),
  CONSTRAINT [PK_jrGKD] PRIMARY KEY CLUSTERED ([nuGKD]),
  CONSTRAINT [C_jrGKD_isSootv] CHECK ([isSootv]='N' OR ([isSootv]='F' OR [isSootv]='T')),
  CONSTRAINT [C_jrGKD_isTreb] CHECK ([isTreb]='F' OR [isTreb]='T'),
  CONSTRAINT [C_jrGKD_tpGKD] CHECK ([tpGKD]='K' OR [tpGKD]='J' OR [tpGKD]='I' OR [tpGKD]='H' OR [tpGKD]='G' OR [tpGKD]='F' OR [tpGKD]='E' OR [tpGKD]='D' OR [tpGKD]='C' OR [tpGKD]='B' OR [tpGKD]='A')
)
ON [PRIMARY]
GO

CREATE INDEX [I_jrGKD_idKO]
  ON [dbo].[jrGKD] ([idKO])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrGkd] ON [dbo].[jrGKD] AFTER INSERT, UPDATE, DELETE
AS
DECLARE @MyVariable CURSOR,
	    @CorrectionType TINYINT,
		@idKO INTEGER, 
		@nuReg CHAR(9), 
		@isPartner CHAR(1),
		@dtWork DATETIME

SELECT TOP 1 @dtWork = dtWork FROM rfStatus

IF ( ( SELECT isPartner FROM deleted WHERE 
         EXISTS( SELECT idAcc FROM jrAccount WHERE 
                   jrAccount.nuGKD = deleted.nuGKD AND tpAsset = 'B' AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork ) ) 
         AND ( dtGKDEnd IS NULL OR dtGKDEnd > @dtWork ) ) <> 
	 ( SELECT isPartner FROM inserted WHERE 
	     EXISTS( SELECT idAcc FROM jrAccount WHERE 
	               jrAccount.nuGKD = inserted.nuGKD AND tpAsset = 'B' AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork ) ) 
	     AND ( dtGKDEnd IS NULL OR dtGKDEnd > @dtWork ) ) )
   OR ( UPDATE( isPartner ) AND NOT UPDATE( numGKD ) ) -- затычка для Петровича повторной отправки инф-ции, отклоненной в ДОФР
--- Условие выполняется, если вставляем новую строку или меняем isPartner в существующей строке   
BEGIN 
  IF EXISTS( SELECT isPartner FROM inserted )    ---- Добавление или изменение строки
  BEGIN
	SET @MyVariable = CURSOR FOR 
	  SELECT 
	    inserted.idKO, inserted.nuReg, isPartner 
	  FROM 
		/*inserted JOIN jrAccount ON inserted.nuGKD = jrAccount.nuGKD AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork )
	  WHERE 
	    ( ( CASE inserted.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE inserted.tpGKD END ) = 'B' ) AND ( dtGKDEnd IS NULL OR dtGKDEnd > @dtWork ) -- ЕКД Aheburg-1518*/
	    inserted 
	  WHERE     EXISTS( SELECT idAcc FROM jrAccount WHERE 
	               jrAccount.nuGKD = inserted.nuGKD AND (jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork)	   
	     AND ( ( CASE inserted.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE inserted.tpGKD END ) = 'B' ) ) AND ( dtGKDEnd IS NULL OR dtGKDEnd > @dtWork ) -- ЕКД

  END
  ELSE BEGIN                                     ---- Удаление строки
    SET @CorrectionType = 4                      ---- Тип корректровки новая
	SET @MyVariable = CURSOR FOR 
	  SELECT 
	    deleted.idKO, deleted.nuReg, isPartner 
	  FROM 
	    deleted JOIN jrAccount ON deleted.nuGKD = jrAccount.nuGKD AND ( jrAccount.dtExcl IS NULL OR jrAccount.dtExcl > @dtWork )
	  WHERE 
	    ( ( CASE deleted.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE deleted.tpGKD END ) = 'B' ) AND ( dtGKDEnd IS NULL OR dtGKDEnd > @dtWork ) -- ЕКД
  END

  OPEN @MyVariable
  FETCH NEXT FROM @MyVariable INTO @idKO, @nuReg, @isPartner

  IF EXISTS( SELECT isPartner FROM inserted )    ---- Добавление или изменение строки
  BEGIN
	SELECT TOP 1 @CorrectionType = CorrectionType FROM DKO_hrBankInTU WHERE _IdKO = @idKO ORDER BY idRecord DESC
	IF ISNULL( @CorrectionType, 0 ) = 0 OR @CorrectionType = 4 
	  SET @CorrectionType = 1                    ---- Тип корректровки новая
	ELSE 
	  SET @CorrectionType = 2                    ---- Тип корректровки мзменение
  END

  WHILE @@FETCH_STATUS = 0 
  BEGIN 
	INSERT INTO DKO_IdUpdate( TypeDKO ) Values ( 'DKO201' ) 

	INSERT INTO DKO_hrBankInTU
	  ( _IdKO, IdBankInTU, D_From, IdUpdate, StateSED, CorrectionType, BankRegNum, IsPartnerOfMSP, TU )
 	SELECT 
 	  --- _IdKO - Уникальный идентифтикатор КО в АС Сибирь
 	  @idKO, 
 	  --- IdBankInTU - Уникальный идентифтикатор КО в АС ДКО
 	  ISNULL( ( SELECT TOP 1 IdBankInTU FROM DKO_hrBankInTU WHERE _IdKO = @idKO ), 0 ), 
 	  --- D_From - Дата, с которой действует данный набор значений атрибутов КО. 
 	  ( SELECT CAST( dtWork AS SMALLDATETIME ) FROM rfStatus ),
 	  --- IdUpdate - Идентификатор обновления
 	  ( SELECT IDENT_CURRENT( 'DKO_IdUpdate' ) ),
 	  --- StateSED - Состояние сообщения 
 	  0, 
 	  --- CorrectionType - Тип корректировки
 	  @CorrectionType, 
 	  --- BankRegNum - Регистрационный номер КО.
	  @nuReg, 
	  --- IsPartnerOfMSP - Атрибут определяет, является ли КО партнёром МСП Банка
	  CASE @isPartner WHEN 'T' THEN 1 ELSE 0 END,
	  --- КП ТУ, на базе которого создан хаб
	  ( SELECT nmSign FROM rfSign WHERE tpSign= 'KodTU' )
	  
	FETCH NEXT FROM @MyVariable	INTO @idKO, @nuReg, @isPartner
  END

  CLOSE @MyVariable
  DEALLOCATE @MyVariable
END

UPDATE DKO_hrBankInTU SET IdBankInTU = -IdRecord WHERE IdBankInTU = 0
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-------
CREATE TRIGGER [trLogJrGKD]
ON [dbo].[jrGKD]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL   SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(nuGKD AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtGKD, 104)+', '+
   CAST(idKO AS VARCHAR(20))+', '+nmKO+', '+nnKO+', '+nuReg+', '+nuBIK+', '+nuINN+', '+nmJurAddr+', '+nmFactAddr+', '+nmNadzTU+', '+
   isSootv+', '+CONVERT(VARCHAR(20), mnLimitAgg)+', '+
   CONVERT(VARCHAR(10), dtGKDEnd, 104)+', '+numGKD+tpGKD FROM deleted
  SELECT @newValues = CAST(nuGKD AS VARCHAR(20))+', '+CONVERT(VARCHAR(10), dtGKD, 104)+', '+
   CAST(idKO AS VARCHAR(20))+', '+nmKO+', '+nnKO+', '+nuReg+', '+nuBIK+', '+nuINN+', '+nmJurAddr+', '+nmFactAddr+', '+nmNadzTU+', '+
   isSootv+', '+CONVERT(VARCHAR(20), mnLimitAgg)+', '+
   CONVERT(VARCHAR(10), dtGKDEnd, 104)+', '+numGKD+tpGKD FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список ГКД', 
   'идентификатор, дата, идентификатор КО, наименование (полное/краткое), рег.номер, БИК, ИНН, юр.адрес, факт.адрес, ТУ, критерии, совокупный лимит, дата расторжения ГКД, номер ГКД, тип ГКД', 
    @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrGKD]
  ADD CONSTRAINT [FK_jrGKD_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'dtGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'соответсвие критериям Банка России T - соответсвует, F - нет
F - нет
N - не соответствует только отдельный филиал (не все КО по этому ГКД)
T - соответсвует', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'isSootv'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Совокупный лимит/лимит задолженности', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'mnLimitAgg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата расторжения генерального кредитного договора', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'dtGKDend'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'numGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип ГКД
A - ГКД по 236-П (рыночные активы)
B - ГКД по 312-П (нерыночные активы)
C - КД на СЭТ ММВБ
D - КД на МБК
E - ГКД по 362-П (золото)
F - КД на ЭКСАР
G - КД на ИНВЕСТПРОЕКТЫ
H - КД на ЮГ
I - КД до 3-х лет
J - КД БКЛ
K - ЕКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'tpGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'DEPRECATED!!! Более не заполняется!!! соответствие требованиям Банка России (бухгалтерская отчетность и другая информация об организациях, обязанных по активам, представленным данной КО, не проверяется)', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'isTreb'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата направления/получения уведомления о расторжении ГКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'dtGKDUved'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'является партнером АО «МСП Банк» (для данной кредитной организации процесс рассмотрения и принятия активов, обязанное лицо по которым имеет статус субъекта малого предпринимательства, осуществляется в «упрощенном» порядке)', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'isPartner'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор ГКД по 236-П для КД БКЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuGKD236'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор ГКД по 312-П для КД БКЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuGKD312'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор ГКД по 362-П для КД БКЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuGKD362'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'полное наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nmKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'краткое наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nnKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'регистрационный номер КО', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК кредитной организации', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuBIK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ИНН', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuINN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'юридический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nmJurAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'фактический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nmFactAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ТУ, осуществляющее банковский надзор', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nmNadzTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор ЕКД для КД БКЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'nuEKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата начала действия ЕКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'dtEKDBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата приостановления ГКД 236-П, 312-П, КД на СЭТ ММВБ с даты вступления в силу Регламента ЕКД', 'SCHEMA', N'dbo', 'TABLE', N'jrGKD', 'COLUMN', N'dtGKDStop'
GO