﻿CREATE TABLE [dbo].[DKO_hrBankInTU] (
  [IdRecord] [bigint] IDENTITY,
  [_IdKO] [int] NOT NULL,
  [IdBankInTU] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [BankRegNum] [varchar](20) NULL,
  [IsPartnerOfMSP] [tinyint] NULL,
  [AlterComment] [varchar](500) NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL,
  [TU] [char](2) NULL,
  CONSTRAINT [PK_DKO_hrBankInTU] PRIMARY KEY CLUSTERED ([IdRecord]),
  CONSTRAINT [CK_DKO_hrBankInTU_CorrectionType] CHECK ([CorrectionType]>=(1) AND [CorrectionType]<=(4)),
  CONSTRAINT [CK_DKO_hrBankInTU_IsArchive] CHECK ([IsArchive]>=(0) AND [IsArchive]<=(1)),
  CONSTRAINT [CK_DKO_hrBankInTU_StateSED] CHECK ([StateSED]>=(-3) AND [StateSED]<=(3))
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор В АС "Сибирь" ', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'_IdKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный начальный отрицательный идентификатор В АСДКО', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'IdBankInTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, с которой действует данный набор значений', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'D_From'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор обновления', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'IdUpdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Состояния: -3 - Для проверки контролёром, -2 - Стартовое решение, -1 - Не отправлять, 0 - Неотправленное, 1 - Отправленное, 2 - Подтверждённое, 3 - Отклонённое', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'StateSED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - Запись активна, 1 - Запись в архиве', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - Создание, 2 - Изменение, 3 - Удаление ошибочной, 4 - Удаление по актуальности', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'CorrectionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'ESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'ESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код КП ТУ, в котором находится Хаб', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrBankInTU', 'COLUMN', N'TU'
GO