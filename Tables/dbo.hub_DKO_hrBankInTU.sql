﻿CREATE TABLE [dbo].[hub_DKO_hrBankInTU] (
  [IdRecord] [bigint] NOT NULL,
  [_IdKO] [int] NOT NULL,
  [IdBankInTU] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [BankRegNum] [varchar](20) NULL,
  [IsPartnerOfMSP] [tinyint] NULL,
  [AlterComment] [varchar](500) NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL
)
ON [PRIMARY]
GO