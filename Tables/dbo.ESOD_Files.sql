﻿CREATE TABLE [dbo].[ESOD_Files] (
  [idFile] [int] IDENTITY,
  [GUID] [varchar](36) NOT NULL,
  [idEnvelope] [int] NOT NULL,
  [FileName] [varchar](255) NULL,
  [FileType] [varchar](255) NULL,
  [Option] [varchar](255) NULL,
  [XML] [varchar](max) NULL,
  [CreatedDateTime] [datetime] NULL CONSTRAINT [DF_ESOD_Files_CreatedDate] DEFAULT (getdate()),
  [CreatedDate] AS (CONVERT([varchar],[CreatedDateTime],(104))),
  [CreatedTime] AS (CONVERT([time](0),[CreatedDateTime],(0))),
  [CreatedBy] [int] NULL CONSTRAINT [DF_ESOD_Files_CreatedBy] DEFAULT (suser_id()),
  [CreatedName] AS (substring(suser_name([CreatedBy]),charindex('\',suser_name([CreatedBy]))+(1),len(suser_name([CreatedBy])))),
  [ModifedDateTime] [datetime] NULL,
  [ModifedDate] AS (CONVERT([varchar],[ModifedDateTime],(104))),
  [ModifedTime] AS (CONVERT([time](0),[ModifedDateTime],(0))),
  [ModifedBy] [int] NULL,
  [ModifedName] AS (substring(suser_name([ModifedBy]),charindex('\',suser_name([ModifedBy]))+(1),len(suser_name([ModifedBy])))),
  [idES] [int] NULL,
  CONSTRAINT [PK_ESOD_Files] PRIMARY KEY CLUSTERED ([idFile])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

CREATE INDEX [IE_ESOD_Files]
  ON [dbo].[ESOD_Files] ([idEnvelope], [GUID])
  ON [PRIMARY]
GO

CREATE INDEX [IX_ESOD_Files_2]
  ON [dbo].[ESOD_Files] ([idES], [FileName])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER  [trESOD_Files_Modify]
	ON  [dbo].[ESOD_Files]
	AFTER UPDATE
	AS
BEGIN
	SET NOCOUNT ON;
	UPDATE
		[ESOD_Files]
	SET
		[ModifedDateTime]	= GETDATE(),
		[ModifedBy]			= SUSER_ID()
	FROM
		INSERTED, [ESOD_Files]
	WHERE
		[ESOD_Files].GUID = INSERTED.GUID
	SET NOCOUNT OFF;
END
GO

ALTER TABLE [dbo].[ESOD_Files]
  ADD CONSTRAINT [FK_EDOKO_hrESidES_ESOD_FilesidES] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

ALTER TABLE [dbo].[ESOD_Files]
  ADD CONSTRAINT [FK_ESOD_Envelope_ESOD_Files] FOREIGN KEY ([idEnvelope]) REFERENCES [dbo].[ESOD_Envelope] ([IdEnvelope])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Файлы интеграционного конверта', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор файла = имя файла в ИК = значение igr:fileIdentity описания файла в envelope.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'GUID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла (в файловой системе) = значение igr:fileName описания файла в envelope.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'FileName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип файла = igr:FileProperty описания файла в envelope.xml', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'FileType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип сообщения (С101, С102 и т.д.) для файлов ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'Option'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тело файлов *.xml (служебных и ЭС)', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'XML'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ДатаВремя создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'CreatedDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'CreatedDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Время создания записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'CreatedTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ID логина автора ', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'CreatedBy'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин автора', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'CreatedName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ДатаВремя изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'ModifedDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'ModifedDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Время изменения записи', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'ModifedTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ID логина автора изменений', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'ModifedBy'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин автора изменений
)', 'SCHEMA', N'dbo', 'TABLE', N'ESOD_Files', 'COLUMN', N'ModifedName'
GO