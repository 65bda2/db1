﻿CREATE TABLE [dbo].[hrOrgBuhReport] (
  [idRecord] [int] IDENTITY,
  [OperationDate] [datetime] NOT NULL,
  [DFrom] [datetime] NOT NULL CONSTRAINT [DF_hrOrgBuhReport_DFrom] DEFAULT (getdate()),
  [UserName] [varchar](128) NOT NULL CONSTRAINT [DF_hrOrgBuhReport_UserName] DEFAULT (suser_sname()),
  [idOrgBuhReport] [int] NOT NULL,
  [idES] [int] NULL,
  [idPeriod] [int] NOT NULL,
  [DateReport] [smalldatetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [idOrg] [int] NOT NULL,
  [OKVED] [int] NOT NULL,
  [isRuleControl] [bit] NULL,
  [isCorrect] [bit] NULL,
  [isReferense] [bit] NOT NULL,
  [isBookKeepingOK] [bit] NULL,
  [isArchive] [bit] NOT NULL,
  [OKVEDCalc] [char](8) NULL,
  CONSTRAINT [PK_hrOrgBuhReport] PRIMARY KEY CLUSTERED ([idRecord])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'История хранения данных о вводе финансовых показателей отчетности «вручную» оператором', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Операционый день, в который произошли изменения', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'OperationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата и время создания записи', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'DFrom'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Логин пользователя, который создал запись', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'UserName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи jrOrgBuhReport.idOrgBuhReport', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД ЭС С105 с финансовой отчетностью', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД периода отчетности, за который заполняются данные', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата ввода данных о финансовой отчетности', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'DateReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД КО, от которой пришла финансовая отчетность', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД организации, по которой заполняется финансовая отчетность', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД кода ОКВЭД на момент заполнения информации по финансовой отчетности ', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'OKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Проверка правил контроля', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'isRuleControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Корректность не определялась', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'isCorrect'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Не эталон', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'isReferense'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Проверка на соответствие требованиям БР не проводилась', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'isBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Новая/не архив, требует проведения проверки', 'SCHEMA', N'dbo', 'TABLE', N'hrOrgBuhReport', 'COLUMN', N'isArchive'
GO