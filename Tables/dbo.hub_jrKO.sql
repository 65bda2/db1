﻿CREATE TABLE [dbo].[hub_jrKO] (
  [idKO] [int] NOT NULL,
  [nmKO] [varchar](250) NOT NULL,
  [nuReg] [varchar](9) NOT NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [nuAccount] [varchar](20) NULL,
  [nuRkcBik] [varchar](9) NULL,
  [nnKO] [varchar](128) NULL,
  [idRKC] [int] NULL,
  [OKATO] [varchar](11) NULL,
  [OKPO] [varchar](8) NULL,
  [OGRN] [varchar](13) NULL,
  [nuAccRNKO] [varchar](20) NULL,
  [idRNKO] [int] NULL,
  [isInList] [tinyint] NULL,
  [IsSubject] [tinyint] NOT NULL DEFAULT (0),
  [isElectronFormKO] [tinyint] NULL DEFAULT (0),
  [ESODVersion] [int] NULL
)
ON [PRIMARY]
GO