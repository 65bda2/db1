﻿CREATE TABLE [dbo].[rfDKKP] (
  [CODE] [int] NOT NULL,
  [KP] [varchar](2) NOT NULL,
  [NAME_RUS] [varchar](250) NULL,
  CONSTRAINT [PK_rfDKKP] PRIMARY KEY CLUSTERED ([CODE])
)
ON [PRIMARY]
GO

CREATE INDEX [UI_rfDKKP_KP]
  ON [dbo].[rfDKKP] ([KP])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Подразделение', 'SCHEMA', N'dbo', 'TABLE', N'rfDKKP'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'внутренний код', 'SCHEMA', N'dbo', 'TABLE', N'rfDKKP', 'COLUMN', N'CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код подразделения', 'SCHEMA', N'dbo', 'TABLE', N'rfDKKP', 'COLUMN', N'KP'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование подразделения', 'SCHEMA', N'dbo', 'TABLE', N'rfDKKP', 'COLUMN', N'NAME_RUS'
GO