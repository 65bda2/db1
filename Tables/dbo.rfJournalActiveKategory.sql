﻿CREATE TABLE [dbo].[rfJournalActiveKategory] (
  [Kategory] [char](2) NOT NULL,
  [Name] [varchar](255) NOT NULL,
  CONSTRAINT [PK_rfJournalActiveKategory] PRIMARY KEY CLUSTERED ([Kategory])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица является справочником видов событий для журнала операций с активом jrJournalActive.', 'SCHEMA', N'dbo', 'TABLE', N'rfJournalActiveKategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код категории. Первичный ключ.', 'SCHEMA', N'dbo', 'TABLE', N'rfJournalActiveKategory', 'COLUMN', N'Kategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование категории', 'SCHEMA', N'dbo', 'TABLE', N'rfJournalActiveKategory', 'COLUMN', N'Name'
GO