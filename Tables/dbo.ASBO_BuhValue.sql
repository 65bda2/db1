﻿CREATE TABLE [dbo].[ASBO_BuhValue] (
  [idRecord] [bigint] IDENTITY,
  [idMainDataES] [bigint] NOT NULL,
  [RValue] [decimal](18, 2) NOT NULL,
  [Code] [varchar](10) NOT NULL,
  [ReportDate] [datetime] NOT NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_BuhValue_idMainDataES]
  ON [dbo].[ASBO_BuhValue] ([idMainDataES])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Информация по бухгалтерской отчетности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue', 'COLUMN', N'idRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ссылка на родительский элемент (ASBO_MainDataEs)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue', 'COLUMN', N'idMainDataES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Значение показателя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue', 'COLUMN', N'RValue'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код показателя', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue', 'COLUMN', N'Code'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Отчетная дата', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_BuhValue', 'COLUMN', N'ReportDate'
GO