﻿CREATE TABLE [dbo].[hrReports] (
  [dtReport] [datetime] NOT NULL,
  [nmReport] [char](100) NOT NULL,
  [idBlock] [int] NOT NULL,
  [imReport] [image] NOT NULL,
  [idReport] [int] IDENTITY,
  CONSTRAINT [PK_hrReports] PRIMARY KEY CLUSTERED ([dtReport], [nmReport])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogHrReports]
ON [dbo].[hrReports]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtReport, 104)+', '+RTRIM(nmReport)+', '+CAST(idBlock AS VARCHAR(20)) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtReport, 104)+', '+RTRIM(nmReport)+', '+CAST(idBlock AS VARCHAR(20)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Сформированные отчеты', 
   'дата записи, наименование отчета, уникальный идентификатор ББР', @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'хронология сформированных отчетов', 'SCHEMA', N'dbo', 'TABLE', N'hrReports'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата', 'SCHEMA', N'dbo', 'TABLE', N'hrReports', 'COLUMN', N'dtReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование отчета', 'SCHEMA', N'dbo', 'TABLE', N'hrReports', 'COLUMN', N'nmReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор ББР', 'SCHEMA', N'dbo', 'TABLE', N'hrReports', 'COLUMN', N'idBlock'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'отчет', 'SCHEMA', N'dbo', 'TABLE', N'hrReports', 'COLUMN', N'imReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор отчета ', 'SCHEMA', N'dbo', 'TABLE', N'hrReports', 'COLUMN', N'idReport'
GO