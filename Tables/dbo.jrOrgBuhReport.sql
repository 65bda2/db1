﻿CREATE TABLE [dbo].[jrOrgBuhReport] (
  [idOrgBuhReport] [int] IDENTITY,
  [idES] [int] NULL,
  [idPeriod] [int] NOT NULL,
  [DateReport] [smalldatetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [idOrg] [int] NOT NULL,
  [OKVED] [int] NOT NULL,
  [isRuleControl] [bit] NULL,
  [isCorrect] [bit] NULL,
  [isReferense] [bit] NOT NULL CONSTRAINT [DF_jrOrgBuhReport_isReferense] DEFAULT (0),
  [isBookKeepingOK] [bit] NULL,
  [isArchive] [bit] NOT NULL CONSTRAINT [DF_jrOrgBuhReport_isArchive] DEFAULT (0),
  [OKVEDCalc] [char](8) NULL,
  CONSTRAINT [PK_jrOrgBuhReport] PRIMARY KEY CLUSTERED ([idOrgBuhReport])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 26.04.2019
-- Description:	Триггер для сохранения истории в таблице hrOrgBuhReport
-- =============================================
CREATE TRIGGER [trhrOrgBuhReport]
   ON  [dbo].[jrOrgBuhReport]
   AFTER INSERT,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM inserted)
	INSERT INTO [dbo].[hrOrgBuhReport] ([OperationDate], [idOrgBuhReport], [idES], [idPeriod], [DateReport], [idKO], [idOrg], [OKVED], [isRuleControl], [isCorrect], [isReferense], [isBookKeepingOK], [isArchive], [OKVEDCalc])
	SELECT (SELECT [dtWork] FROM [dbo].[rfStatus]), [idOrgBuhReport], [idES], [idPeriod], [DateReport], [idKO], [idOrg], [OKVED], [isRuleControl], [isCorrect], [isReferense], [isBookKeepingOK], [isArchive], [OKVEDCalc] FROM inserted

END
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 21.01.2020
-- Description:	Если имеется БО за все обязательные периоды, то для активов (status=R) удаляем dtEclude
-- =============================================
CREATE TRIGGER [trOrgBuhReport]
   ON  [dbo].[jrOrgBuhReport]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	DECLARE @idOrg INT, @idKO INT
	DECLARE @NA INT, @isEF BIT, @UN varchar(16)
	DECLARE @dt DateTime
	
	SELECT @idOrg = idOrg, @idKO = idKO FROM inserted

	IF @idKO = NULL
		SELECT @idOrg = idOrg, @idKO = idKO FROM deleted

	DECLARE cr CURSOR
	FOR SELECT a.NumActive, a.IsElectronForm, a.UnNum
		FROM jrActive a INNER JOIN jrLetter b ON a.idLetter = b.idLetter
		WHERE a.status = 'R'
		AND a.dtExclude IS NOT NULL
		AND b.idKo = @idKO
		AND a.Company = @idOrg

	OPEN cr

	FETCH NEXT FROM cr
	INTO @NA, @isEF, @UN

	WHILE @@FETCH_STATUS = 0
	BEGIN

	IF @isEF = 1
		SELECT @dt = EDDate FROM EDOKO_hrES with (nolock) WHERE EDType = 'C208' and idHeadES in (SELECT TOP 1 idES FROM EDOKO_hrES WHERE EDType ='C101' AND UnNum = @UN)  
	ELSE	
		SELECT @dt = DateIt FROM jrJournalActive with (nolock) WHERE Kategory = 'R' and NumActive = @NA

	IF NOT EXISTS(SELECT * FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) as periods 
				  LEFT JOIN jrOrgBuhReport bu on bu.idPeriod = periods.idPeriod AND bu.idOrg = @idOrg AND bu.idKO = @idKO AND bu.DateReport <= @dt
				  WHERE bu.idOrgBuhReport IS NULL AND periods.obPeriod = 1)
		UPDATE jrActive
		SET dtExclude = NULL
		WHERE NumActive = @NA

		FETCH NEXT FROM cr
		INTO @NA, @isEF, @UN
	END
	CLOSE cr
	DEALLOCATE cr

END
GO

ALTER TABLE [dbo].[jrOrgBuhReport]
  ADD CONSTRAINT [FK_jrOrgBuhReport_idES] FOREIGN KEY ([idES]) REFERENCES [dbo].[EDOKO_hrES] ([idES])
GO

ALTER TABLE [dbo].[jrOrgBuhReport]
  ADD CONSTRAINT [FK_jrOrgBuhReport_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO])
GO

ALTER TABLE [dbo].[jrOrgBuhReport]
  ADD CONSTRAINT [FK_jrOrgBuhReport_idOrg] FOREIGN KEY ([idOrg]) REFERENCES [dbo].[jrORG] ([idOrg])
GO

ALTER TABLE [dbo].[jrOrgBuhReport]
  ADD CONSTRAINT [FK_jrOrgBuhReport_idperiod] FOREIGN KEY ([idPeriod]) REFERENCES [dbo].[rfBuhPeriod] ([idPeriod])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Хранение данных о вводе финансовых показателей отчетности «вручную» оператором', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'idOrgBuhReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД ЭС С105 с финансовой отчетностью', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N' ИД периода отчетности, за который заполняются данные', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'idPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата ввода данных о финансовой отчетности', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'DateReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД КО, от которой пришла финансовая отчетность', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД организации, по которой заполняется финансовая отчетность', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД кода ОКВЭД на момент заполнения информации по финансовой отчетности ', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'OKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Проверка правил контроля', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'isRuleControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Корректность не определялась', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'isCorrect'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Не эталон', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'isReferense'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Проверка на соответствие требованиям БР не проводилась', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'isBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Новая/не архив, требует проведения проверки', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'isArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКВЭД, на основании которого выполнен расчет показателей БО', 'SCHEMA', N'dbo', 'TABLE', N'jrOrgBuhReport', 'COLUMN', N'OKVEDCalc'
GO