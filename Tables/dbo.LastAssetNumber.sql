﻿CREATE TABLE [dbo].[LastAssetNumber] (
  [nuReg] [varchar](4) NOT NULL,
  [KPTU] [char](2) NOT NULL,
  [AssetNumNA] [int] NULL,
  [AssetNumMSP] [int] NULL,
  [AssetNumEcsar] [int] NULL,
  [AssetNumInvest] [int] NULL,
  CONSTRAINT [PK_LastAssetNumber] PRIMARY KEY CLUSTERED ([nuReg], [KPTU])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Максимальный номер актива в разрезе ТУ, КО и типа актива - нерыночного, по специнструментам – МСП, ЭКСАР, Инвестпроекты.', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер головной КО', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'nuReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ТУ, из которого загружались данные', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'KPTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последний номер нерыночного актива (НА) в рамках ТУ', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'AssetNumNA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последний номер актива по специнструментам по КД на МСП в рамках ТУ', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'AssetNumMSP'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последний номер актива по специнструментам по КД на ЭКСАР в рамках ТУ.', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'AssetNumEcsar'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последний номер актива по специнструментам по КД на Инвестпроекты в рамках ТУ.', 'SCHEMA', N'dbo', 'TABLE', N'LastAssetNumber', 'COLUMN', N'AssetNumInvest'
GO