﻿CREATE TABLE [dbo].[jrtransh] (
  [IdTransh] [int] IDENTITY,
  [Cost] [money] NOT NULL,
  [NomerVD] [varchar](255) NULL,
  [CreationDate] [datetime] NOT NULL,
  [NumActive] [int] NOT NULL,
  [PaymentDate] [datetime] NOT NULL,
  [Status] [char](1) NOT NULL,
  [Dopinf] [char](27) NOT NULL,
  [IdLetter] [int] NOT NULL,
  [BeginDate] [datetime] NULL,
  [inclDate] [datetime] NULL,
  [IsOriginal] [char](1) NULL DEFAULT ('F'),
  [AlterComment] [varchar](500) NULL,
  CONSTRAINT [PK_jrTransh] PRIMARY KEY CLUSTERED ([IdTransh])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrTransh] ON [dbo].[jrtransh] AFTER INSERT,UPDATE,DELETE
AS

DECLARE @dtWork SMALLDATETIME, @IdTransh INT, @WasSED INT, @statesed INT, @Cost NUMERIC(22,7), @NomerVD VARCHAR(100),
        @CreationDate SMALLDATETIME, @NumActive INT, @PaymentDate SMALLDATETIME, @Status CHAR(1), @Dopinf CHAR(25), @IdLetter INT,
        @BeginDate SMALLDATETIME, @inclDate SMALLDATETIME, @IsOriginal CHAR(1), @idUpdate INT, @CorrectionType TINYINT, @idAsset NUMERIC(16), 
        @TypeActive VARCHAR(2), @CountDELCorrection INT --dtr3586

DECLARE @MyVariable CURSOR

SET @CountDELCorrection = ( SELECT Count(*) FROM DKO_hrAsset WHERE _NumActive = @IdTransh And Assettype in (10,11,12,13,14,16,18) And @CorrectionType = 4 ) --dtr3586
SELECT @dtWork = CAST( dtWork AS SMALLDATETIME ) FROM rfStatus

IF ( SELECT Count(*) FROM INSERTED ) = 0      ---- Удаление записи
BEGIN
  SET @MyVariable = CURSOR FOR 
    SELECT 
      IdTransh, Cost, NomerVD, CreationDate, NumActive, PaymentDate, Status, Dopinf, IdLetter, BeginDate, 
      ISNULL( ( SELECT CAST( inclDate AS SMALLDATETIME ) FROM jrActive WHERE jrActive.NumActive = DELETED.NumActive ), 0 ),
      IsOriginal 
    FROM DELETED	
END
ELSE BEGIN                                    ---- Добавление или изменение записи
  SET @MyVariable = CURSOR FOR 
    SELECT 
      IdTransh, Cost, NomerVD, CreationDate, NumActive, PaymentDate, Status, Dopinf, IdLetter, BeginDate,
      ISNULL( ( SELECT CAST( inclDate AS SMALLDATETIME ) FROM jrActive WHERE jrActive.NumActive = INSERTED.NumActive ), 0 ),
      IsOriginal 
    FROM 
      INSERTED
END

OPEN @MyVariable
FETCH NEXT FROM @MyVariable INTO @IdTransh, @Cost, @NomerVD, @CreationDate, @NumActive, @PaymentDate, @Status, @Dopinf, @IdLetter, @BeginDate, @inclDate, @IsOriginal

WHILE @@FETCH_STATUS = 0 
BEGIN
  IF ( SELECT typeLetter FROM jrLetter where idLetter=@IdLetter) <> 'D'  --не обрабатывать доп. транши
  BEGIN
    SET @WasSED = 0
	IF ( SELECT Count(*) FROM DKO_hrAsset WHERE _NumActive = @NumActive And AssetType in (0,1,3,5,6,7,8,15,17) And StateSED <> -1 ) > 0 
	BEGIN
	  SET @WasSed = 1
    END		
    
	IF ( ( SELECT Count(*) MyCount FROM DKO_hrAsset WHERE _NumActive = @IdTransh And Assettype in (10,11,12,13,14,16,18) ) > 0 )
	BEGIN
	  SET @CorrectionType = 2
	  SET @idAsset = ( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @IdTransh And AssetType in (10,11,12,13,14,16,18) Order By IdRecord Desc )	
	END	
	ELSE BEGIN
	  SET @CorrectionType = 1	
	  SET @idAsset = ( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @IdTransh And Assettype in (10,11,12,13,14,16,18) Order by IdRecord Desc )	
	  
	  IF @idAsset Is NULL
	  BEGIN
		SET @idAsset = -( SELECT MAX( IdRecord ) + 1 FROM DKO_hrAsset )
	  END
	  
	  IF @idAsset Is NULL  
	  BEGIN 
	    SET @idAsset = -1 
	  END
    END
	
	IF ( SELECT Count(*) FROM INSERTED ) = 0 
	BEGIN
	  SET @CorrectionType = 4
	  IF ( Select Count(*) FROM DKO_hrAssetState WHERE IdAsset = @idAsset AND PartitionTypeTo IN ( 'ОР', 'ЗП', 'РО', 'ТП','Пб', 'МЗ' ) ) > 0
	  BEGIN
		Set @dtWork = dbo.IncWorkDay( @dtWork, 1 )
	  END
	END
	
	--dtr3586
	--If ((@wassed=1) and (@Status='E') and (SELECT count(*) FROM DKO_hrAsset WHERE _numactive=@IdTransh and Assettype in (10,11,12,13,14,16,18) and @CorrectionType=4 )=0 ) 
	If ( ( @WasSED = 1 ) And ( @Status = 'E' ) And @CountDELCorrection = 0 ) 
	BEGIN
	  SET @CorrectionType = 4
	  SET @WasSED = 2
	  IF ( SELECT Count(*) FROM DKO_hrAssetState WHERE IdAsset = @idAsset AND PartitionTypeTo IN ( 'ОР', 'ЗП', 'РО', 'ТП','Пб','МЗ' ) ) > 0
	  BEGIN
		SET  @dtWork = dbo.IncWorkDay( @dtWork, 1 )
	  END
    END

	--IF	(((SELECT Status FROM jractive where numactive=@numactive)<>'E' and @wassed=1) or (@WasSED=2)) 
	IF ( ( ( SELECT Status FROM jractive WHERE NumActive = @NumActive ) <> 'E' And @WasSED = 1 ) 
	     OR 
	     ( @WasSED = 2 ) 
	   )  
	   AND 
	   @CountDELCorrection = 0 
    BEGIN	
	  INSERT INTO DKO_IdUpdate ( TypeDKO ) Values ( 'DKO201' ) 
	  SELECT @IdUpdate = IDENT_CURRENT('DKO_IdUpdate')	
	  
	  SET @TypeActive = ( Select TypeActive From jrActive Where NumActive = @NumActive )
	  
 	  INSERT INTO DKO_hrAsset
 	    ( _NumActive, IdAsset, D_From, IdUpdate, StateSED, IsArchive, CorrectionType, AssetType, AssetNumber, AssetDate, D_Repayment, FaceValue,
		  IsActive, IdParentAsset, OriginalsState )
	  SELECT 
	    @IdTransh, 
	    @idAsset, 
	    CASE WHEN @dtWork > @inclDate THEN @dtWork ELSE @inclDate END, 
	    @IdUpdate, 
	    -3,
	    0, 
	    @CorrectionType, 
	    Case @TypeActive  When 'M'  Then ( ( Case ( Select IsSubject From jrKO Where idKO = ( Select jrActive.NmZaemshik From jrActive Where jrActive.NumActive = @NumActive ) ) 
	                                           When 0 Then 10 
	                                           When 1 Then 16 
	                                           When 2 Then 18 
	                                         End ) ) 
	                      When 'E'  Then 11 
	                      When 'I'  Then 14  
	                      When 'YK' Then 13 
	                      When 'YO' Then 12 
	    End,
		RTRIM( @NomerVd ), 
		CAST( @CreationDate AS SMALLDATETIME ), 
		CAST( @PaymentDate AS SMALLDATETIME ),	
		@Cost,
		CASE @Status WHEN 'T' THEN 1 ELSE 0 END, 
		( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE _NumActive = @NumActive And AssetType in (3,5,6,7,8,15,17) ORDER BY IdRecord DESC ), 
		CASE @IsOriginal WHEN 'T' THEN 1  WHEN 'E' THEN 2 ELSE 0 END
	END
  END		
  
  FETCH NEXT FROM @MyVariable
	INTO @IdTransh, @Cost, @NomerVD, @CreationDate, @NumActive, @PaymentDate, @Status, @Dopinf, @IdLetter, @BeginDate, @inclDate, @IsOriginal
END

CLOSE @MyVariable
DEALLOCATE @MyVariable
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrTransh]
ON [dbo].[jrtransh]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  SELECT @oldValues = CAST(NumActive AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), PaymentDate, 104)+', '+CONVERT(VARCHAR(20), Cost)+', '+
   RTRIM(DopInf)+', '+CAST(idLetter AS VARCHAR(10))+', '+status+', '+ CONVERT(VARCHAR(10), Begindate, 104)+', '+CONVERT(VARCHAR(10), inclDate, 104)+', '+
    CONVERT(VARCHAR(10), CreationDate, 104)+', '+RTRIM(IsOriginal)+', '+RTRIM(NomerVD)+', '+ CAST(IdTransh AS VARCHAR(10))
   FROM deleted

  SELECT @newValues = CAST(NumActive AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), PaymentDate, 104)+', '+CONVERT(VARCHAR(20), Cost)+', '+
   RTRIM(DopInf)+', '+CAST(idLetter AS VARCHAR(10))+', '+status+', '+ CONVERT(VARCHAR(10), Begindate, 104)+', '+CONVERT(VARCHAR(10), inclDate, 104)+', '+
    CONVERT(VARCHAR(10), CreationDate, 104)+', '+RTRIM(IsOriginal)+', '+RTRIM(NomerVD)+', '+ CAST(IdTransh AS VARCHAR(10)) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список Траншей', 
   'Ссылка на главный актив, Дата погашения, Стоимость,Доп.информация , Идентификатор письма, Статус, BeginDate, '+
   'Дата включения, Дата КД, Признак оригинала документов, Номер КД, Идентификатор транша' , 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrtransh]
  ADD CONSTRAINT [FK_jrTransh_idletter] FOREIGN KEY ([IdLetter]) REFERENCES [dbo].[jrLetter] ([idLetter]) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[jrtransh]
  ADD CONSTRAINT [FK_jrTransh_NumActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Транши для МБК', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный номер транша', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'IdTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'стоимость транша', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'Cost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер транша', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'NomerVD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата транша', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'CreationDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата платежа', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'PaymentDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'Status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'инф об ответах на вопросы', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'Dopinf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор письма', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'IdLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата какого либо решения', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'BeginDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата принятия в актив', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'inclDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'представлены оригиналы документов', 'SCHEMA', N'dbo', 'TABLE', N'jrtransh', 'COLUMN', N'IsOriginal'
GO