﻿CREATE TABLE [dbo].[jrKO] (
  [idKO] [int] IDENTITY,
  [nmKO] [varchar](255) NOT NULL,
  [nuReg] [varchar](9) NOT NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [nuAccount] [varchar](20) NULL,
  [nuRkcBik] [varchar](9) NULL,
  [nnKO] [varchar](255) NULL,
  [idRKC] [int] NULL,
  [OKATO] [varchar](11) NULL,
  [OKPO] [varchar](8) NULL,
  [OGRN] [varchar](13) NULL,
  [nuAccRNKO] [varchar](20) NULL,
  [idRNKO] [int] NULL,
  [isInList] [tinyint] NULL,
  [IsSubject] [tinyint] NOT NULL DEFAULT (0),
  [isElectronFormKO] [tinyint] NULL DEFAULT (0),
  [ESODVersion] [int] NULL CONSTRAINT [DF_jrKO_VersionESOD] DEFAULT (0),
  [idSubdivision] [int] NULL,
  CONSTRAINT [PK_jrKO] PRIMARY KEY CLUSTERED ([idKO])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trjrKO] ON [dbo].[jrKO] AFTER INSERT, UPDATE, DELETE
AS

  DECLARE @idKO INT, @nuAccount CHAR(20), @idRKC INT
  DECLARE @MyVariable CURSOR, 
          @CorrectionType TINYINT,
          @INN VARCHAR(12), @EGRUL VARCHAR(20), @CompanyName VARCHAR(255),
	      @IsSubject TINYINT,  @IsInList TINYINT

  IF EXISTS( SELECT idKO FROM inserted )           ---- Добавление или изменение КО
  BEGIN
    IF EXISTS ( SELECT idKO FROM deleted )         ---- Изменение КО  
      SET @CorrectionType = 2 
    ELSE                                           ---- Новая КО  
      SET @CorrectionType = 1
      
    SET @MyVariable = CURSOR FOR 
      SELECT idKO, nuINN, OGRN, nmKO, IsSubject, IsInList FROM inserted
  END
  ELSE BEGIN                                       ---- Удаление КО 
    SET @CorrectionType = 4
    SET @MyVariable = CURSOR FOR 
      SELECT idKO, nuINN, OGRN, nmKO, IsSubject, IsInList FROM deleted
  END

  OPEN @MyVariable
  FETCH NEXT FROM @MyVariable INTO @idKO, @INN, @EGRUL, @CompanyName, @IsSubject, @IsInList
  
  ------------- Занесение МФО/Лизинговой компании в JrORG -------------
  WHILE @@FETCH_STATUS = 0 
  BEGIN 
	IF @IsSubject <> 0   ---- 1 - МФО, 2 - лизинговая компания
	BEGIN 
	  INSERT INTO DKO_IdUpdate ( TypeDKO ) Values ( 'DKO201' )  ---- Номер обновления
	  
	  INSERT INTO DKO_hrCompany ( _IdOrg, IdCompany, D_From, IdUpdate, 
	                              StateSED, CorrectionType, INN, EGRUL, CompanyName,  
	                              SubjectType, IsInMFOList, IsLOConforming, IsResident, TU ) 
	  SELECT 
	    --- Уникальный идентификатор организации в АС Сибирь. 
	    @idKO + 200000000, 
	    --- Уникальный идентификатор организации в АС ДКО. 	    
	    ISNULL( ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = @idKO+200000000 ), 0 ), 
	    --- D_From - Дата, с которой действует данный набор значений атрибутов КО. 
	    ( SELECT CAST( dtWork AS SMALLDATETIME) FROM rfStatus ), 
	    --- IdUpdate - Идентификатор обновления
	    ( SELECT IDENT_CURRENT( 'DKO_IdUpdate' ) ), 
	    --- StateSED - Состояние сообщения
	    0, 
	    --- CorrectionType - Тип корректировки
	    @CorrectionType, 
	    --- INN - ИНН организации.
	    @INN, 
	    --- EGRUL - Регистрационный номер организации в ЕГРЮЛ (ОГРН).
	    @EGRUL, 
	    --- CompanyName - Наименование организации. 	    
	    @CompanyName, 
	    --- SubjectType - Тип субъекта.
	    CASE @IsSubject WHEN 1 THEN 5 WHEN 2 THEN 6 END, 
	    --- IsInMFOList - Включённость МФО в Список МФО, имеющих задолженность по кредитам целевого характера, связанного с кредитованием субъектов МСП
		CASE @IsSubject WHEN 1 THEN @IsInList END, 
		--- IsLOConforming - Соответствие требованиям, установленным Банком России, для лизинговых организаций.
		CASE @IsSubject WHEN 2 THEN @IsInList END, 
		--- IsResident - Является ли организация Резидентом РФ 
		1,
		--- КП ТУ, на базе которого создан хаб
	    ( SELECT nmSign FROM rfSign WHERE tpSign= 'KodTU' )

	END
	
    FETCH NEXT FROM @MyVariable
	INTO @idKO, @INN, @EGRUL, @CompanyName, @IsSubject, @IsInList
  END
  
  CLOSE @MyVariable
  DEALLOCATE @MyVariable  

  UPDATE DKO_hrCompany SET IdCompany = -IdRecord WHERE IdCompany = 0
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrKO]
ON [dbo].[jrKO]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL  SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idKO AS VARCHAR(20))+', '+RTRIM(nmKO)+', '+RTRIM(nuReg)+', '+RTRIM(nuBIK)+', '+RTRIM(nuINN)+', '+ 
    RTRIM(nmJurAddr)+', '+RTRIM(nmFactAddr)+', '+RTRIM(nmNadzTU) FROM deleted
  SELECT @newValues = CAST(idKO AS VARCHAR(20))+', '+RTRIM(nmKO)+', '+RTRIM(nuReg)+', '+RTRIM(nuBIK)+', '+RTRIM(nuINN)+', '+ 
    RTRIM(nmJurAddr)+', '+RTRIM(nmFactAddr)+', '+RTRIM(nmNadzTU) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Спписок кредитных организаций', 
   'идентификатор, наименование, регистрационный номер, БИК, ИНН, юр.адрес, факт.адрес, надзорное ТУ', 
    @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrKO]
  ADD CONSTRAINT [FK_jrKO_idSubdivision] FOREIGN KEY ([idSubdivision]) REFERENCES [dbo].[rfSubdivision] ([idSubdivision])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список КО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'полное наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nmKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'регистрационный номер КО (для МФО - 0000)', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК кредитной организации', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuBIK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ИНН', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuINN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'юридический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nmJurAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'фактический адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nmFactAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ТУ, осуществляющее банковский надзор', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nmNadzTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'корреспондентский счет КО (основной счет)', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuAccount'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК РКЦ где обслуживается(deprecated. for solonec)', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuRkcBik'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'краткое наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nnKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор РКЦ', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'idRKC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'OKATO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'OKPO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОГРН', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'OGRN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Коррсчет, открытый в уполномоченной РНКО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'nuAccRNKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор уполномоченной РНКО', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'idRNKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'отметка о наличии в списке Банка России (для МФО) / является партнером Банка по программе финансовой поддержки МСП (для лизинговых компаний)', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'isInList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак 0 - банк, 1 - МФО, 2 - лизинговая компания
0 - банк
1 - МФО
2 - лизинговая компания', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'IsSubject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'отметка о включении ЭДО КО-ТУ (активы) (Используется в АС ППА)', 'SCHEMA', N'dbo', 'TABLE', N'jrKO', 'COLUMN', N'isElectronFormKO'
GO