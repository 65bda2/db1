﻿CREATE TABLE [dbo].[jrORG] (
  [idOrg] [int] IDENTITY,
  [nmAddr] [varchar](255) NULL,
  [businessQuality] [int] NULL,
  [OKPO] [varchar](8) NULL,
  [INN] [varchar](12) NULL,
  [EGRUL] [varchar](20) NULL,
  [CompanyName] [varchar](255) NULL,
  [CodeOKVED] [int] NULL,
  [CodeOKOPF] [int] NULL,
  [CodeOKFS] [int] NULL,
  [IsSubject] [tinyint] NULL,
  [IsConforming] [tinyint] NULL,
  [IsInList] [tinyint] NULL,
  [IsResident] [tinyint] NULL,
  [D_lastRegDate] [smalldatetime] NULL,
  [IsBookKeepingOK] [tinyint] NULL,
  [IsFederalLaw] [tinyint] NULL,
  [AlterComment] [varchar](255) NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('F'),
  [nmOrg] AS ([CompanyName]),
  [belonging] [tinyint] NULL,
  [dtOgrn] [datetime] NULL,
  [CodeOKATO] [char](11) NULL,
  [ShortNmORG] [varchar](255) NULL,
  [TpDtReg] [tinyint] NULL,
  [NmLaw] [varchar](255) NULL,
  [dtBookKeeping] [datetime] NULL,
  [AnotherInf] [int] NULL DEFAULT (0),
  [AnotherInfNote] [varchar](255) NULL,
  [Rating] [varchar](255) NULL,
  [NonResidentID] [varchar](30) NULL,
  [tpDopInfo] [char](5) NOT NULL DEFAULT ('FFFFF'),
  [OrgStatus] [varchar](1) NOT NULL DEFAULT ('T'),
  [isReprocess] [bit] NULL,
  CONSTRAINT [PK_jrOrg] PRIMARY KEY CLUSTERED ([idOrg]),
  CONSTRAINT [C_jrOrg_businessQuality] CHECK ([businessQuality]>=(0) AND [businessQuality]<=(2)),
  CONSTRAINT [C_jrOrg_isSootv] CHECK ([isSootv]='F' OR ([isSootv]='L' OR ([isSootv]='P' OR [isSootv]='T')))
)
ON [PRIMARY]
GO

CREATE INDEX [I_jrOrg_CompanyName]
  ON [dbo].[jrORG] ([CompanyName])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trhrORG]
ON [dbo].[jrORG]
AFTER UPDATE,INSERT
AS
	declare @dt DateTime, @dtWork DateTime	
	set @dt=GETdate()
	set @dtWork=(select dtwork from rfStatus)
	delete from  hrORG where DFrom > DATEADD(second,-1, @dt) and DFrom < (@dt) and idorg in (select idorg from inserted) and HrStatus=0		
	Insert into hrORG select @dtWork,0,@dt  as DFROM,
	idOrg, INN,EGRUL,NonResidentID,CompanyName,CodeOKVED,(select top 1 c.Kod from rfOKVED c where inserted.CodeOKVED=c.Code),CodeOKOPF,(select top 1 a.KOD from rfOKOPF a where a.CODE=inserted.codeOkopf),CodeOKFS, (select top 1 b.KOD from rfOKFS b where b.CODE=inserted.codeOKFS),IsSubject,tpDopInfo,IsConforming,IsInList,IsResident,BusinessQuality, 
	D_lastRegDate,IsBookKeepingOK,IsFederalLaw,AlterComment,isSootv,ShortNmORG,TpDtReg,dtBookKeeping,AnotherInf, AnotherInfNote,(case issubject when 0 then (CASE IsInList when 1 then 'НП' ELSE 'ФА' END )  ELSE 'РФ' END), OrgStatus, isReprocess
	from inserted 
	update jrActive set istrigger=istrigger+1  where TypeActive in ('V','D') and (company in (select idorg from inserted) or Nmzaemshik in (select idorg from inserted) ) and status<>'E'

SET ANSI_WARNINGS ON
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrOrg] ON [dbo].[jrORG] AFTER INSERT, UPDATE, DELETE
AS
  DECLARE @Cursor CURSOR, 
    @IdOrg INTEGER, @CorrectionType TINYINT,
    @INN VARCHAR(12), @EGRUL VARCHAR(20), @CompanyName VARCHAR(255), @CodeOKVED INTEGER, @CodeOKOPF INTEGER, @CodeOKFS INTEGER, 
    @IsSubject TINYINT, @IsConforming TINYINT, @IsInList TINYINT, @IsResident TINYINT, @BusinessQuality TINYINT, 
    @D_LastRegDate SMALLDATETIME, @IsBookKeepingOK TINYINT, @AlterComment VARCHAR(500)
  IF EXISTS( SELECT idOrg FROM inserted )       ---- Добавление или изменение организации
  BEGIN
    IF EXISTS( SELECT idOrg FROM deleted )      ---- Изменение по организации
      SET @CorrectionType = 2 
    ELSE                                        ---- Новая организация  
      SET @CorrectionType = 1
    SET @Cursor = CURSOR FOR 
      SELECT 
        idOrg, INN, EGRUL, CompanyName, CodeOKVED, CodeOKOPF, CodeOKFS, 
        CASE WHEN IsSubject NOT IN ( 1, 2, 3, 4 ) THEN 0 ELSE IsSubject END, 
        IsConforming, IsInList, IsResident, BusinessQuality, CAST( D_LastRegDate AS SMALLDATETIME ) AS D_lastRegDate, IsBookKeepingOK, AlterComment 
      FROM inserted
      where OrgStatus='T' --AHEBURG-2774 65MakarovDA
  END
  ELSE BEGIN                                    ---- Удаление организации 
    SET @CorrectionType = 4
    SET @Cursor = CURSOR FOR 
      SELECT 
        idOrg, INN, EGRUL, CompanyName, CodeOKVED, CodeOKOPF, CodeOKFS, 
        CASE WHEN IsSubject NOT IN ( 1, 2, 3, 4 ) THEN 0 ELSE IsSubject END, 
        IsConforming, IsInList, IsResident, BusinessQuality, CAST( D_LastRegDate AS SMALLDATETIME ) AS D_lastRegDate, IsBookKeepingOK, AlterComment 
      FROM deleted
      where OrgStatus='T' --AHEBURG-2774 65MakarovDA
  END
  OPEN @Cursor
  FETCH NEXT FROM @Cursor INTO @IdOrg, @INN, @EGRUL, @CompanyName, @CodeOKVED, @CodeOKOPF, @CodeOKFS, @IsSubject, @IsConforming, @IsInList, @IsResident, 
                               @BusinessQuality, @D_LastRegDate, @IsBookKeepingOK, @AlterComment
  WHILE @@FETCH_STATUS = 0 
  BEGIN
    INSERT INTO DKO_IdUpdate ( TypeDKO ) Values ( 'DKO201' )   ---- Номер обновления 
    INSERT INTO DKO_hrCompany ( _IdOrg, IdCompany, D_From, IdUpdate, StateSED, CorrectionType, INN, EGRUL, CompanyName, CodeOKVED, CodeOKOPF, CodeOKFS, 
                                SubjectType, IsConforming, IsInList, IsResident, BusinessQuality, D_lastRegDate, IsBookKeepingOK, AlterComment, TU ) 
    SELECT 
      --- _IdOrg - Уникальный идентификатор организации в АС ППА    
      @IdOrg,        
      ---  IdCompany - Уникальный идентификатор организации в АС ДКО                                                                            
      ISNULL( ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = @idOrg ), 0 ),    
      --- D_From - Дата, с которой действует данный набор значений атрибутов КО.
      ( SELECT CAST( dtWork AS SMALLDATETIME ) FROM rfStatus ), 
      --- IdUpdate - Идентификатор обновления
      ( SELECT IDENT_CURRENT( 'DKO_IdUpdate' ) ), 
      --- StateSED - Состояние сообщения
      -3, 
      --- CorrectionType - Тип корректировки
      @CorrectionType, 
      --- INN - ИНН организации.
      @INN, 
      --- EGRUL - Регистрационный номер организации в ЕГРЮЛ (ОГРН).
      @EGRUL, 
      --- CompanyName - Наименование организации. 
      @CompanyName, 
      --- CodeOKVED - Код основного вида экономической деятельности
      @CodeOKVED, 
      --- CodeOKOPF - Код организационно-правовой формы организации
      @CodeOKOPF, 
      --- CodeOKFS - Код формы собственности. 
      @CodeOKFS, 
      --- SubjectType - Тип субъекта.
      @IsSubject, 
      --- IsConforming - Соответствует "требованиям, установленным Банком России" для субъектов РФ
      @IsConforming, 
      --- IsInList - Включённость организации в перечень, установленный Банком России
      @IsInList, 
      --- IsResident - Является ли организация Резидентом РФ
      @IsResident, 
      --- BusinessQuality - Является ли организация субъектом, малого, среднего или крупного  предпринимательства
      @BusinessQuality, 
      --- D_lastRegDate - Дата регистрации, или последней перерегистрации, или последней реорганизации
      @D_LastRegDate, 
      --- IsBookKeepingOK - Бухгалтерская отчетность и др. информация об организации соответствуют требованиям
      @IsBookKeepingOK,
      --- AlterComment - Комментарий.
      @AlterComment,
      --- КП ТУ, на базе которого создан хаб
	  ( SELECT nmSign FROM rfSign WHERE tpSign= 'KodTU' ) 
    FETCH NEXT FROM @Cursor INTO @IdOrg, @INN, @EGRUL, @CompanyName, @CodeOKVED, @CodeOKOPF, @CodeOKFS, @IsSubject, @IsConforming, @IsInList, @IsResident, 
                                 @BusinessQuality, @D_LastRegDate, @IsBookKeepingOK, @AlterComment
  END
  CLOSE @Cursor
  DEALLOCATE @Cursor
  ---- Если уникальный идентификатор организации в АС ДКО не найден, делаем его отрицательныи
  UPDATE DKO_hrCompany SET IdCompany = -IdRecord WHERE IdCompany = 0
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-------------------------------------------------------------------------
CREATE TRIGGER [trLogJrOrg]
ON [dbo].[jrORG]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idOrg AS VARCHAR(10))+', '+RTRIM(CompanyName)+', '+RTRIM(OKPO)+', '+RTRIM(INN)+', '+RTRIM(EGRUL)+', '+
  CAST(CodeOKVED AS VARCHAR(10))+', '+CAST(CodeOKOPF AS VARCHAR(10))+', '+CAST(CodeOKFS AS VARCHAR(10))+', '+
  CAST(IsSubject AS CHAR(1))+', '+CAST(IsConforming AS CHAR(1))+', '+CAST(IsInList AS CHAR(1))+', '+CAST(IsResident AS CHAR(1))+', '+CAST(BusinessQuality AS CHAR(1))+', '+
	CONVERT(VARCHAR(10), D_lastRegDate, 104)+', '+CAST(IsBookKeepingOK AS CHAR(1))+', '+/*CAST(IsOtherOK AS CHAR(1))+', '+*/RTRIM(nmAddr) FROM deleted
  SELECT @newValues = CAST(idOrg AS VARCHAR(10))+', '+RTRIM(CompanyName)+', '+RTRIM(OKPO)+', '+RTRIM(INN)+', '+RTRIM(EGRUL)+', '+
  CAST(CodeOKVED AS VARCHAR(10))+', '+CAST(CodeOKOPF AS VARCHAR(10))+', '+CAST(CodeOKFS AS VARCHAR(10))+', '+
  CAST(IsSubject AS CHAR(1))+', '+CAST(IsConforming AS CHAR(1))+', '+CAST(IsInList AS CHAR(1))+', '+CAST(IsResident AS CHAR(1))+', '+CAST(BusinessQuality AS CHAR(1))+', '+
	CONVERT(VARCHAR(10), D_lastRegDate, 104)+', '+CAST(IsBookKeepingOK AS CHAR(1))+', '+/*CAST(IsOtherOK AS CHAR(1))+', '+*/RTRIM(nmAddr) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список юридических лиц', 
   'идентификатор, наименование, OKPO, ИНН, ЕГРЮЛ, код ОКВЭД, код ОКОПФ, код ОКФС, признак субъекта, соответствие, перечень БР, резидент, тип субъекта, дата регистрации, бух.отчетность, прочее, адрес', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список юридических лиц/субъектов РФ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'адрес', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'nmAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак субъекта РФ
1 - субъект малого предпринимательства
2 - субъект крупного или среднего предпринимательства', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'businessQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПО', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'OKPO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИНН', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'INN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЕГРЮЛ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'EGRUL'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование юр. лица/субъекта РФ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'CompanyName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКВЭД', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'CodeOKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'CodeOKOPF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'код ОКФС', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'CodeOKFS'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак организации
0 - организация (312-П)
1 - РФ (312-П)
2 - субъект РФ (312-П)
3 - Муниципальное образование (312-П)
4 - НДКО "Агенство кредитных гарантий"', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsSubject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак соответствия требованиям БР для субъектов РФ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsConforming'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак включенности в перечень БР', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsInList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак резидента РФ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsResident'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата регистрации(перерегистрации, реорганизации)', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'D_lastRegDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'бух.отчетность соответствуют требованиям БР', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Организация образована или реорганизована  в соответствии с ФЗ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'IsFederalLaw'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'комментарий', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'AlterComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак соответствия критериям
F - не соответствует
L - соответствует для 1 и 2 категории
P - соответствует для 1 категории,
T - полностью соответствует,', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'isSootv'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование юр. лица/субъекта РФ (для совместимости)', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'nmOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - государственное 1-муниципальное', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'belonging'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'-1 нет иной информации 1- есть и она соотв. критерием 0 -есть и она не соотв. критериям', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'AnotherInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'   AnotherInf integer  NULL default 0, -- -1 нет иной информации 1- есть и она соотв. критерием 0 -есть и она не соотв. критериям', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'AnotherInfNote'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентифифкатор для организации-нерезидента', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'NonResidentID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'признак организации, обязанной по ЭКСАР, Инвестпроектам, ЮГ и т.п.
1 - организация КД на ЭКСАР
2 - организация КД на Инвестпроекты
3 - организация КД на ЮГ', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'tpDopInfo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить статус новой организации', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'OrgStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак Возможность повторной обработки; NULL - возможность повторной обработки не определена; 1 - Доступна повторная обработка; 0 - Организация отклонена пользователем, повторная обработка не доступна', 'SCHEMA', N'dbo', 'TABLE', N'jrORG', 'COLUMN', N'isReprocess'
GO