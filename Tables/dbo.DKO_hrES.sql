﻿CREATE TABLE [dbo].[DKO_hrES] (
  [ESDate] [smalldatetime] NOT NULL,
  [ESNo] [int] NOT NULL,
  [ESAuthor] [char](10) NOT NULL,
  [FileVersion] [tinyint] NOT NULL,
  [ESDateTime] [datetime] NOT NULL,
  [ESFileName] [varchar](26) NOT NULL,
  [CtrlCode] [varchar](4) NULL,
  [CtrlDateTime] [datetime] NULL,
  [ParseDateTime] [datetime] NOT NULL,
  [ESType] [varchar](6) NOT NULL,
  [AlterComment] [varchar](255) NULL,
  [CorrectionNumber] [bigint] NULL,
  [RequestCode] [tinyint] NULL,
  [RepNum] [int] NULL,
  [RefESDate] [smalldatetime] NULL,
  [RefESNo] [int] NULL,
  [RefESAuthor] [char](10) NULL,
  [WaitESType] [varchar](6) NULL,
  [XMLBody] [text] NULL,
  [CtrlCodeComment] [text] NULL,
  [ParseComment] [text] NULL,
  [IsSent] [int] NOT NULL DEFAULT (0),
  [DataType] [tinyint] NULL,
  [PrevESNo] [int] NULL,
  [IsSaed] [tinyint] NULL,
  [IsEmptyDKO502] [tinyint] NULL,
  [UseDate] [datetime] NULL,
  [IsCheckBeforeLimit] [int] NULL,
  [Status_otpravki] [varchar](30) NULL,
  [idDKOhrES] [int] IDENTITY,
  [CorrectionStatus] [int] NULL,
  PRIMARY KEY CLUSTERED ([idDKOhrES])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [tr_DKO_hrES] 
ON [dbo].[DKO_hrES]
FOR INSERT
AS
	DECLARE @so VARCHAR(30)
    DECLARE @ESNo int,@ESAuthor CHAR(10),@ESDate SMALLDATETIME,@FileVersion tinyint,@EsFileName as varchar(26),@ESType VARCHAR(6) --,@isSaed TINYINT,@RefESDate SMALLDATETIME,@RefESNo INT,@RefESAuthor CHAR(10)
	
	DECLARE @CURSOR  CURSOR SET @CURSOR=CURSOR STATIC  SCROLL FOR 
	SELECT ESDate, ESNo, ESAuthor, FileVersion, EsFileName, ESType
	FROM inserted 

	OPEN @CURSOR 
		FETCH FIRST 
		FROM @CURSOR 
		INTO @ESDate, @ESNo, @ESAuthor, @FileVersion, @EsFileName, @ESType
		WHILE @@FETCH_STATUS=0
		BEGIN
			UPDATE t2 SET Status_otpravki = '70-прочитан' 
			FROM DKO_hrES t1 INNER JOIN DKO_hrES t2 ON t2.ESNO=t1.RefESNo and t2.ESDate=t1.RefESDate and t2.ESAuthor=t1.RefESAuthor	                
			WHERE t1.ESType='DKO100' and SUBSTRING(t1.EsFileName,1,2) = '00'
			AND t1.ESDate=@ESDate AND t1.ESNo=@ESNo AND t1.ESAuthor=@ESAuthor AND t1.FileVersion=@FileVersion 
			
			UPDATE t1 SET Status_otpravki = (CASE @ESType WHEN 'DKO100' THEN '-' ELSE (CASE SUBSTRING(@EsFileName,1,2) WHEN '00' THEN '-' ELSE '50-отправлен' END) END)
			FROM DKO_hrES t1
			WHERE t1.ESDate=@ESDate AND t1.ESNo=@ESNo AND t1.ESAuthor=@ESAuthor AND t1.FileVersion=@FileVersion 
		
			FETCH NEXT 
			FROM @CURSOR
			INTO @ESDate, @ESNo, @ESAuthor, @FileVersion, @EsFileName, @ESType  
		END
	CLOSE @CURSOR
	DEALLOCATE @CURSOR
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'XML - section', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата операционного дня', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер ЭС в течении опердня', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор составителя ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESAuthor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Версия ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'FileVersion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Календарные дата и время составления ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имя файла, содержащего ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESFileName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код результата обработки ЭС.', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'CtrlCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Календарные дата и время проведения обработки исходного ЭС (взятое из ЭС)', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'CtrlDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Календарные дата и время проведения обработки исходного ЭС (Реальное у нас)', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ParseDateTime'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'EDxxx, DKOxxx', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ESType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'AlterComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер корректировки', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'CorrectionNumber'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код запроса', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'RequestCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер отчёта (номер сводной таблицы)', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'RepNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Дата операционного дня', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'RefESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Номер ЭС в течение опердня', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'RefESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификаторы исходного ЭС, Уникальный идентификатор составителя ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'RefESAuthor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ожидаемый тип ЭС, DKOxxx', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'WaitESType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Содержимое ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'XMLBody'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст пояснения или детальная диагностика ошибки из ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'CtrlCodeComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Диагностика ошибки при обработке ЭС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'ParseComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЭС отправлено/не отправлено по почте или через СДС', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'IsSent'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид дананных для DKO106', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'DataType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Предыдущее ЭС для DKO106', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'PrevESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Создан ли файд на диске?', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'IsSaed'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Пустой DKO502', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'IsEmptyDKO502'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, по состоянию на которую запрашиваются данные, для DKO202, DKO203, DKO204, DKO205, DKO206', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'UseDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак сверки перед установкой лимита для DKO206', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrES', 'COLUMN', N'IsCheckBeforeLimit'
GO