﻿CREATE TABLE [dbo].[hub_rfAffPers] (
  [idKO] [int] NOT NULL,
  [idOrg] [int] NOT NULL,
  [pcShare] [decimal](7, 4) NULL,
  [IsCompanyOwner] [tinyint] NOT NULL DEFAULT (0),
  [dtBegin] [datetime] NULL,
  [dtEnd] [datetime] NULL,
  [idRecord] [int] NOT NULL,
  [AlterComment] [varchar](500) NULL
)
ON [PRIMARY]
GO