﻿CREATE TABLE [dbo].[hrClassification] (
  [dtBegin] [datetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [dtEnd] [datetime] NULL,
  [cmClass] [varchar](1000) NULL,
  [dtInput] [datetime] NOT NULL,
  [tpClass] [char](1) NOT NULL,
  [cdClass] [int] IDENTITY,
  CONSTRAINT [PK_hrClassification] PRIMARY KEY CLUSTERED ([cdClass]),
  CONSTRAINT [C_hrClassification_tpClass] CHECK ([tpClass]>='A' AND [tpClass]<='O')
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrClassification]
  ADD CONSTRAINT [FK_hrClassification_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Хронология классификации КО', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата принятия решения о классификации(начало действия)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор КО (ссылка на jrGKD)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата принятия решения о снятии классификации(окончание действия)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'dtEnd'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'основание (комментарий)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'cmClass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата внесения записи о классификации', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'dtInput'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип применимости (по КО, по ГКД, по кредиту)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'tpClass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор (используется в hrStockPawn)', 'SCHEMA', N'dbo', 'TABLE', N'hrClassification', 'COLUMN', N'cdClass'
GO