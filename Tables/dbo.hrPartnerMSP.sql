﻿CREATE TABLE [dbo].[hrPartnerMSP] (
  [nuGKD] [int] NOT NULL,
  [dtBegin] [datetime] NOT NULL,
  [dtEnd] [datetime] NULL,
  CONSTRAINT [PK_hrPartnerMSP] PRIMARY KEY CLUSTERED ([nuGKD], [dtBegin])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrPartnerMSP]
  ADD CONSTRAINT [FK_hrPartnerMSP_nuGKD] FOREIGN KEY ([nuGKD]) REFERENCES [dbo].[jrGKD] ([nuGKD]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Изменения признака партнерства МСП', 'SCHEMA', N'dbo', 'TABLE', N'hrPartnerMSP'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор ГКД', 'SCHEMA', N'dbo', 'TABLE', N'hrPartnerMSP', 'COLUMN', N'nuGKD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата начала партнерства МСП', 'SCHEMA', N'dbo', 'TABLE', N'hrPartnerMSP', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата окончания партнерства МСП', 'SCHEMA', N'dbo', 'TABLE', N'hrPartnerMSP', 'COLUMN', N'dtEnd'
GO