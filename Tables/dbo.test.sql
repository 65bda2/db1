CREATE TABLE [dbo].[test] (
  [id] [int] IDENTITY,
  [name1] [varchar](500) NULL,
  [isP] [bit] NULL,
  CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак', 'SCHEMA', N'dbo', 'TABLE', N'test', 'COLUMN', N'isP'
GO