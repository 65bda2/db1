﻿CREATE TABLE [dbo].[hrPaymentGrafic] (
  [Dfrom] [datetime] NOT NULL,
  [idGroup] [int] NOT NULL,
  [NumActive] [int] NOT NULL,
  [tpActive] [char](1) NOT NULL,
  [DatePay] [smalldatetime] NOT NULL,
  [Payment] [money] NOT NULL,
  [Advice] [char](1) NULL,
  [Currency] [char](3) NULL,
  [IdTransh] [int] NULL,
  [AlterComment] [char](255) NULL,
  CONSTRAINT [C_hrPaymentGrafic_Payment] CHECK ([Payment]>(0))
)
ON [PRIMARY]
GO

CREATE INDEX [I_hrPaymentGrafic_dFrom]
  ON [dbo].[hrPaymentGrafic] ([Dfrom])
  ON [PRIMARY]
GO

CREATE INDEX [I_hrPaymentGrafic_idGroup]
  ON [dbo].[hrPaymentGrafic] ([idGroup])
  ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IC_hrPaymentGrafic_NumActive]
  ON [dbo].[hrPaymentGrafic] ([NumActive])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrPaymentGrafic]
  ADD CONSTRAINT [FK_hrPaymentGrafic_NumActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'хронология графика платежей', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата с которой действует график', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'Dfrom'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор группы графика', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'idGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип актива', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'tpActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата платежа', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'DatePay'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'платеж', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'Payment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'подтверждение платежа', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'Advice'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор транша', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'IdTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарии', 'SCHEMA', N'dbo', 'TABLE', N'hrPaymentGrafic', 'COLUMN', N'AlterComment'
GO