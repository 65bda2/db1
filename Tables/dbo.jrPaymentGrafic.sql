﻿CREATE TABLE [dbo].[jrPaymentGrafic] (
  [NumActive] [int] NULL,
  [Payment] [money] NOT NULL,
  [DatePay] [datetime] NOT NULL,
  [Advice] [char](1) NOT NULL DEFAULT ('F'),
  [IdTransh] [int] NULL,
  [TpActive] [char](1) NOT NULL,
  [AlterComment] [char](255) NULL,
  [Currency] [char](3) NULL,
  [IdRecord] [bigint] IDENTITY,
  CONSTRAINT [C_jrPaymentGrafic_Advice] CHECK ([Advice]='F' OR [Advice]='T'),
  CONSTRAINT [C_jrPaymentGrafic_Payment] CHECK ([Payment]>(0)),
  CONSTRAINT [C_jrPaymentGrafic_tpActive] CHECK ([tpActive]='D' OR [tpActive]='B' OR [tpActive]='A')
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [IC_jrPaymentGrafic_NumActive]
  ON [dbo].[jrPaymentGrafic] ([NumActive])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trJrPaymentGrafic] ON [dbo].[jrPaymentGrafic] AFTER INSERT, UPDATE
AS

DECLARE @dtWork SMALLDATETIME, @inclDate SMALLDATETIME, @NumActive INT, @WasSed INT, @Payment NUMERIC(22,7), 
        @DatePay SMALLDATETIME, @IdTransh INT, @idUpdate INT, @CorrectionType TINYINT, @IdRepayment NUMERIC(16), 
        @IdRecord INT, @IdAssetIdRecord INT, @StateSed INT, @AlterComment VARCHAR(255)

DECLARE @MyVariable CURSOR

SELECT @dtWork = CAST( dtWork AS SMALLDATETIME ) FROM rfStatus

IF ( SELECT COUNT(*) FROM INSERTED ) = 0    ---- Удаление записи
BEGIN
  SET @MyVariable = CURSOR FOR SELECT NumActive, Payment, DatePay, IdTransh, IdRecord, AlterComment FROM DELETED	
END
ELSE BEGIN                                  ---- Добавление или изменение  
  SET @MyVariable = CURSOR FOR SELECT NumActive, Payment, DatePay, IdTransh, IdRecord, AlterComment FROM INSERTED
END

OPEN @MyVariable
FETCH NEXT FROM @MyVariable INTO @NumActive, @Payment, @DatePay, @IdTransh, @IdRecord, @AlterComment

WHILE @@FETCH_STATUS = 0 
BEGIN
  IF ( NOT EXISTS( SELECT * FROM jrActive WHERE NumActive = @NumActive AND status = 'M' ) ) OR -- не обрабатывать МЭПЛ
	 ( @NumActive IS NOT NULL ) -- не обрабатывать доп транши
  BEGIN
	IF ( SELECT COUNT(*) FROM DKO_hrRepayment WHERE _idRecord = @IdRecord and StateSED <> -1 ) > 0 
	BEGIN
	  SET @CorrectionType = 2
	  SET @IdRepayment = ( SELECT TOP 1 IdRepayment FROM DKO_hrRepayment WHERE _IdRecord = @IdRecord )	
	END	
	ELSE BEGIN
	  SET @CorrectionType = 1	
	  SET @IdRepayment = -( SELECT MAX( IdRecord ) + 1 FROM DKO_hrRepayment )
	  
	  IF @IdRepayment IS NULL 
	  BEGIN 
	    SET @IdRepayment = -1 
	  END
	END
			
	IF ( SELECT COUNT(*) FROM INSERTED ) = 0 
	BEGIN
	  SET @CorrectionType = 4	
	END	
			
	SET @idAssetIdRecord = ( SELECT MAX( IdRecord ) FROM DKO_hrAsset WHERE
	                           ( _NumActive = @NumActive And AssetType NOT IN ( 10,11,12,13,14,16,18 ) ) OR 
	                           ( @IdTransh IS NOT NULL AND _NumActive = @IdTransh AND AssetType IN ( 10,11,12,13,14,16,18 ) ) )
	SET @WasSed = 0
			
	IF ( SELECT COUNT(*) FROM DKO_hrAsset WHERE _NumActive = @NumActive AND Assettype IN (0,1,3,5,6,7,8,15,17) AND StateSED <> -1 ) > 0 
	BEGIN
	  SET @WasSed = 1
	END	
			
	IF @WasSed = 1 AND @CorrectionType <> 2
	BEGIN
	  INSERT INTO DKO_IdUpdate ( TypeDKO ) Values ( 'DKO201' )  
	  SELECT @IdUpdate = IDENT_CURRENT('DKO_IdUpdate')	
	  
	  SELECT @inclDate = ISNULL( CAST( inclDate AS SMALLDATETIME ), 0 ) FROM jrActive WHERE NumActive = @NumActive -- vlad 09.06.2016
	  
	  INSERT INTO DKO_hrRepayment
	    ( _IdRecord, IdRepayment, D_From, IdUpdate, StateSed, IsArchive, CorrectionType, IdAsset, D_Payment, Amount, IdAssetIdRecord, AlterComment )
	  SELECT 
	    --- _IdRecord - Уникальный идентификатор В АС "Сибирь" 
	    @IdRecord, 
	    --- IdRepayment - Идентификатор погашения в АС ДКО
	    @IdRepayment, 
	    --- D_From - Дата, с которой действует данный график погашения
	    CASE WHEN @dtWork > @inclDate THEN @dtWork ELSE @inclDate END,
	    --- IdUpdate - Идентификатор обновления 
	    @IdUpdate, 
	    --- StateSED - Состояние сообщения
	    -3, 
	    --- IsArchive - 0 - Запись активна, 1 - Запись в архиве
	    0, 
	    --- CorrectionType - Тип корректировки 
	    @CorrectionType,
	    --- IdAsset - Идентификатор актива. 
		( SELECT TOP 1 IdAsset FROM DKO_hrAsset WHERE ( @IdTransh IS NULL AND _NumActive = @NumActive AND AssetType NOT IN (10,11,12,13,14,16,18) ) OR 
			                                          ( @IdTransh IS NOT NULL AND _NumActive = @IdTransh AND AssetType IN (10,11,12,13,14,16,18) ) ORDER BY IdRecord DESC ),
		---	D_Payment - Дата платежа                                          
		CAST( @DatePay AS SMALLDATETIME ), 
		--- Amount - Сумма платежа 
		@Payment, 
		--- IdAssetIdRecord - Идентификатор обновления актива
		@idAssetIdRecord, 
		--- AlterComment - Комментарий
		@AlterComment
	END
  END

  FETCH NEXT FROM @MyVariable	INTO  @NumActive, @Payment, @DatePay, @IdTransh, @IdRecord, @AlterComment
END

CLOSE @MyVariable
DEALLOCATE @MyVariable
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrPaymentGrafic]
ON [dbo].[jrPaymentGrafic]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(NumActive AS VARCHAR(10))+', '+CONVERT(VARCHAR(20), Payment)+', '+
    CONVERT(VARCHAR(10), DatePay, 104)+', '+Advice FROM deleted
  SELECT @newValues = CAST(NumActive AS VARCHAR(10))+', '+CONVERT(VARCHAR(20), Payment)+', '+
    CONVERT(VARCHAR(10), DatePay, 104)+', '+Advice FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'График платежей (право требования по КД)', 
   'идентификатор актива, сумма платежа, дата платежа, потдверждение платежа', 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrPaymentGrafic] WITH NOCHECK
  ADD CONSTRAINT [FK_jrPaymentGrafic_NumActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'График платежей (для кредитных договоров)', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Платеж', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'Payment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата платежа', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'DatePay'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'потдверждение платежа (для док-та 6)', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'Advice'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор транша', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'IdTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип актива, для которого этот график', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'TpActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарии', 'SCHEMA', N'dbo', 'TABLE', N'jrPaymentGrafic', 'COLUMN', N'AlterComment'
GO