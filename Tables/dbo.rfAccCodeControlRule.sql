﻿CREATE TABLE [dbo].[rfAccCodeControlRule] (
  [idAccCodeControlRule] [int] IDENTITY,
  [NameControlRule] [varchar](150) NOT NULL,
  [isActualRule] [bit] NOT NULL CONSTRAINT [DF_rfAccCodeControlRule_isActualRule] DEFAULT (1),
  [CtrlCode] [char](4) NOT NULL,
  [CtrlCodeComment] [text] NULL,
  [SumControlRule] [decimal](25, 5) NULL,
  [CodeCompare] [smallint] NULL,
  [Formula] [varchar](256) NULL,
  CONSTRAINT [PK_rfAccCodeControlRule] PRIMARY KEY CLUSTERED ([idAccCodeControlRule])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[rfAccCodeControlRule]
  ADD CONSTRAINT [FK_rfAccCodeControlRule_EDOKO_CtrlCode] FOREIGN KEY ([CtrlCode]) REFERENCES [dbo].[EDOKO_CtrlCode] ([CtrlCode])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Правила контроля показателей бух. отчетности, пришедшей в С105', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование правила из альбома УФЭБС', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'NameControlRule'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак необходимости проверки правила', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'isActualRule'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Четырехзначный код результата контроля для отбраковки', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'CtrlCode'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Контрольная сумма (в тыс. руб.), с которой сравниваются значения показателей входящих в правило контроля', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'SumControlRule'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код сравнения', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'CodeCompare'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Формула для расчета левой части правила контроля по значениям ShowingAccSum (или ShowingAccSumPrev ) ', 'SCHEMA', N'dbo', 'TABLE', N'rfAccCodeControlRule', 'COLUMN', N'Formula'
GO