﻿CREATE TABLE [dbo].[hrFactor] (
  [dtBegin] [datetime] NOT NULL,
  [idAsset] [int] NOT NULL,
  [tpAsset] [char](1) NOT NULL,
  [pcFactor] [decimal](5, 4) NOT NULL,
  CONSTRAINT [PK_hrFactor] PRIMARY KEY CLUSTERED ([dtBegin], [idAsset], [tpAsset]),
  CONSTRAINT [C_hrFactor_pcFactor] CHECK ([pcFactor]>=(0) AND [pcFactor]<=(1)),
  CONSTRAINT [C_hrFactor_tpAsset] CHECK ([tpAsset]='D' OR ([tpAsset]='C' OR ([tpAsset]='B' OR [tpAsset]='A')))
)
ON [PRIMARY]
GO

CREATE INDEX [I_hrFactor_idAsset]
  ON [dbo].[hrFactor] ([idAsset])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogHrFactor]
ON [dbo].[hrFactor]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtBegin, 104)+', '+CAST(idAsset AS VARCHAR(10))+', '+
    tpAsset+', '+CONVERT(VARCHAR(10), pcFactor) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtBegin, 104)+', '+CAST(idAsset AS VARCHAR(10))+', '+
    tpAsset+', '+CONVERT(VARCHAR(10), pcFactor) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Справочник поправочных коэффициентов', 
   'дата начала действия, идентификатор актива, тип актива, тип коэффициента, значение', 
    @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'хронология поправочных коэффициентов', 'SCHEMA', N'dbo', 'TABLE', N'hrFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, с которой действуют значения поправочных коэффициентов', 'SCHEMA', N'dbo', 'TABLE', N'hrFactor', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уникальный идентификатор актива (рыночный - jrStock, нерыночный/золото - jrActive)', 'SCHEMA', N'dbo', 'TABLE', N'hrFactor', 'COLUMN', N'idAsset'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип актива', 'SCHEMA', N'dbo', 'TABLE', N'hrFactor', 'COLUMN', N'tpAsset'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'поправочный коэффициент', 'SCHEMA', N'dbo', 'TABLE', N'hrFactor', 'COLUMN', N'pcFactor'
GO