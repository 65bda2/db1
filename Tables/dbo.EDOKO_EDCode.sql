﻿CREATE TABLE [dbo].[EDOKO_EDCode] (
  [id] [int] IDENTITY,
  [EDType] [varchar](6) NOT NULL,
  [Code] [smallint] NOT NULL,
  [Description] [varchar](max) NOT NULL,
  [Status] [varchar](10) NOT NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO