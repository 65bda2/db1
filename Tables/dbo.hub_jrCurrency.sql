﻿CREATE TABLE [dbo].[hub_jrCurrency] (
  [nuISO] [char](3) NOT NULL,
  [nmISO] [char](3) NOT NULL,
  [nmCurr] [char](100) NOT NULL
)
ON [PRIMARY]
GO