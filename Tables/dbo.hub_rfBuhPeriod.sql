﻿CREATE TABLE [dbo].[hub_rfBuhPeriod] (
  [idPeriod] [int] NOT NULL,
  [NmPeriod] [varchar](255) NOT NULL,
  [idclass] [int] NULL,
  [NumPeriod] [int] NOT NULL,
  [YearPeriod] [int] NOT NULL,
  [dtShow] [datetime] NOT NULL,
  [isBuhcheck] [int] NULL DEFAULT (0),
  [ReportDate] [datetime] NOT NULL
)
ON [PRIMARY]
GO