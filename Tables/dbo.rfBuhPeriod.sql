﻿CREATE TABLE [dbo].[rfBuhPeriod] (
  [idPeriod] [int] NOT NULL,
  [NmPeriod] [varchar](255) NOT NULL,
  [idclass] [int] NULL,
  [NumPeriod] [int] NOT NULL,
  [YearPeriod] [int] NOT NULL,
  [dtShow] [datetime] NOT NULL,
  [isBuhcheck] [int] NULL DEFAULT (0),
  [ReportDate] [datetime] NOT NULL,
  [StartDate] [datetime] NOT NULL,
  CONSTRAINT [PK_rfBuhPeriod] PRIMARY KEY CLUSTERED ([idPeriod]),
  CONSTRAINT [C_rfBuhPeriod_isBuhcheck] CHECK ([isBuhcheck]=(1) OR [isBuhcheck]=(0))
)
ON [PRIMARY]
GO

CREATE INDEX [I_rfBuhPeriod_idperiod]
  ON [dbo].[rfBuhPeriod] ([idPeriod])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список периодов отчетности', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор периода отчетности', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'idPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование периода', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'NmPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор класса записей для этого периода', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'idclass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер периода (для квартала - номер квартала , для года - 5 )', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'NumPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Год , за который предоставлена отчетность', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'YearPeriod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата с которой период становится доступным пользователю', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'dtShow'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'параметр для автоматического проведения бух отчености  0-проверка не проведена , 1 - проведена', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'isBuhcheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'отчетная дата', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'ReportDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала отчетного периода', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhPeriod', 'COLUMN', N'StartDate'
GO