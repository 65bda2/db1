﻿CREATE TABLE [dbo].[rfDopLetActive] (
  [NumActive] [int] NOT NULL,
  [idDopLetter] [int] NOT NULL,
  CONSTRAINT [PK_rfDopLetActive] PRIMARY KEY CLUSTERED ([NumActive], [idDopLetter])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[rfDopLetActive]
  ADD CONSTRAINT [FK_rfDopLetActive_idDopLetter] FOREIGN KEY ([idDopLetter]) REFERENCES [dbo].[jrDopLetter] ([idDopLetter]) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[rfDopLetActive] WITH NOCHECK
  ADD CONSTRAINT [FK_rfDopLetActive_NumActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соответствие дополнительных сопроводительных писем активам', 'SCHEMA', N'dbo', 'TABLE', N'rfDopLetActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер актива', 'SCHEMA', N'dbo', 'TABLE', N'rfDopLetActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номер доп письма', 'SCHEMA', N'dbo', 'TABLE', N'rfDopLetActive', 'COLUMN', N'idDopLetter'
GO