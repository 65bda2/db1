﻿CREATE TABLE [dbo].[rfSign] (
  [tpSign] [varchar](60) NOT NULL,
  [nmSign] [varchar](500) NULL,
  CONSTRAINT [PK_rfSign] PRIMARY KEY CLUSTERED ([tpSign])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfSign]
ON [dbo].[rfSign]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = RTRIM(tpSign)+', '+RTRIM(nmSign) FROM deleted
  SELECT @newValues = RTRIM(tpSign)+', '+RTRIM(nmSign) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
  'Реквизиты подписей', 
  'тип реквизита, значение реквизита', @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'реквизиты подписей', 'SCHEMA', N'dbo', 'TABLE', N'rfSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип реквизита', 'SCHEMA', N'dbo', 'TABLE', N'rfSign', 'COLUMN', N'tpSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'значение реквизита', 'SCHEMA', N'dbo', 'TABLE', N'rfSign', 'COLUMN', N'nmSign'
GO