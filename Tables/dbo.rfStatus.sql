﻿CREATE TABLE [dbo].[rfStatus] (
  [version] [varchar](20) NOT NULL,
  [dtWork] [datetime] NULL,
  [XMLZip] [bit] NULL,
  [ASPPAversion] [varchar](20) NULL,
  [DataMartVersion] [varchar](20) NULL,
  [dtBeginTechWork] [datetime] NULL,
  [dtEndTechWork] [datetime] NULL
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfStatus]
ON [dbo].[rfStatus]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtWork, 104)+', '+RTRIM(version) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtWork, 104)+', '+RTRIM(version) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 'Служебная таблица', 'дата проведения операций, версия БД', @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Служебная таблица', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'версия структуры базы данных АС "Сибирь"', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'version'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'текущий ОД', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'dtWork'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'версия структуры базы данных АС ППА', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'ASPPAversion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'версия структуры базы данных Блока "Витрина данных"', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'DataMartVersion'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата и время начала проведения технических работ с ПО', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'dtBeginTechWork'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата и время окончания проведения технических работ с ПО', 'SCHEMA', N'dbo', 'TABLE', N'rfStatus', 'COLUMN', N'dtEndTechWork'
GO