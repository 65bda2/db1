﻿CREATE TABLE [dbo].[rfBuhRecord] (
  [idClass] [int] NOT NULL,
  [idBuhRecord] [int] IDENTITY,
  [NmRecord] [varchar](500) NOT NULL,
  [TpClass] [int] NULL,
  [Kod] [varchar](10) NULL,
  [NumKod] [varchar](10) NOT NULL,
  [isrequired] [int] NOT NULL DEFAULT (1),
  [KodFNS] [varchar](50) NULL,
  [IsactiveRecord] [tinyint] NULL DEFAULT (0),
  [NumOrder] [int] NULL,
  [IsASBO] [bit] NOT NULL,
  CONSTRAINT [PK_rfBuhRecord] PRIMARY KEY CLUSTERED ([idBuhRecord]),
  CONSTRAINT [C_rfBuhRecord_isrequired] CHECK ([isrequired]=(1) OR [isrequired]=(0))
)
ON [PRIMARY]
GO