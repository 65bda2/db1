﻿CREATE TABLE [dbo].[hub_DKO_hrMutualParticipation] (
  [IdRecord] [bigint] NOT NULL,
  [_IdRecord] [int] NOT NULL,
  [IdMutualParticipation] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [IdCompany] [numeric](16) NOT NULL,
  [CORegNum] [varchar](20) NOT NULL,
  [IsCompanyOwner] [tinyint] NOT NULL,
  [AlterComment] [varchar](500) NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL
)
ON [PRIMARY]
GO