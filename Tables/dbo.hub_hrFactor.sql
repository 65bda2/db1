﻿CREATE TABLE [dbo].[hub_hrFactor] (
  [dtBegin] [datetime] NOT NULL,
  [idAsset] [int] NOT NULL,
  [tpAsset] [char](1) NOT NULL,
  [tpFactor] [char](1) NOT NULL,
  [pcFactor] [decimal](5, 4) NOT NULL
)
ON [PRIMARY]
GO