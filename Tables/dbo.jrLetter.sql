﻿CREATE TABLE [dbo].[jrLetter] (
  [idLetter] [int] IDENTITY,
  [NumSADD] [varchar](50) NULL,
  [DateLetter] [datetime] NOT NULL,
  [DateLetterTU] [datetime] NOT NULL,
  [idKo] [int] NOT NULL,
  [Kolvo] [int] NULL,
  [Status] [char](1) NOT NULL,
  [P6] [char](1) NULL,
  [TrueKolvo] [char](1) NULL,
  [VestnikBR] [char](1) NULL,
  [GrLetter] [char](1) NULL,
  [Komment] [varchar](300) NULL,
  [typeLetter] [char](1) NOT NULL DEFAULT ('L'),
  [Q1] [char](1) NULL,
  [Q2] [char](1) NULL,
  [Q3] [char](1) NULL,
  [Q4] [char](1) NULL,
  [NumLetter] [varchar](100) NULL,
  [WarrantDate] [datetime] NULL,
  [LBeginDate] [datetime] NULL,
  [NumLetKo] [varchar](100) NULL,
  [DtLetKo] [datetime] NULL,
  [NumLetStore] [varchar](100) NULL,
  [dtLetstore] [datetime] NULL,
  [NumLetIncl] [varchar](100) NULL,
  [DtLetIncl] [datetime] NULL,
  [DtIncl] [datetime] NULL,
  [idStore] [int] NULL,
  [dtRemoval] [datetime] NULL,
  [countTransh] [int] NULL,
  [Qdopinf] [char](10) NULL DEFAULT ('0000000000'),
  [LoanTrancheApplication] [varchar](2000) NULL,
  CONSTRAINT [PK_jrLetter] PRIMARY KEY CLUSTERED ([idLetter])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrLetter]
ON [dbo].[jrLetter]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idLetter AS VARCHAR(10))+', '+RTRIM(NumSADD)+', '+CONVERT(VARCHAR(10), DateLetter, 104)+', '+
    CONVERT(VARCHAR(10), DateLetterTU, 104)+', '+CAST(idKO AS VARCHAR(10))+', '+CAST(Kolvo AS VARCHAR(10))+', '+
    Status+', '+P6+', '+TrueKolvo+', '+VestnikBR+', '+GrLetter+', '+RTRIM(Komment) FROM deleted
  SELECT @newValues = CAST(idLetter AS VARCHAR(10))+', '+RTRIM(NumSADD)+', '+CONVERT(VARCHAR(10), DateLetter, 104)+', '+
    CONVERT(VARCHAR(10), DateLetterTU, 104)+', '+CAST(idKO AS VARCHAR(10))+', '+CAST(Kolvo AS VARCHAR(10))+', '+
    Status+', '+P6+', '+TrueKolvo+', '+VestnikBR+', '+GrLetter+', '+RTRIM(Komment) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список сопроводительных писем', 
   'идентификатор, Номер в САДД, Дата письма, Дата получения письма ТУ, Уникальный идентификатор КО, Количество активов, Статус письма, '+
   'Ответ по пункту 6, Совпадение кол-ва, КО отвечает требованиям, Наличие Гарантийного письма, Комментарий', 
   @oldValues, @newValues)
GO

ALTER TABLE [dbo].[jrLetter]
  ADD CONSTRAINT [FK_jrletter_idko] FOREIGN KEY ([idKo]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Сопроводительное письмо', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор письма', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'idLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер в САДД', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'NumSADD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата письма', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'DateLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата получения письма ТУ', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'DateLetterTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор КО', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'idKo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Количество активов', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Kolvo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Статус письма F- по письму отказ T-письмо принято  + для золота R - рассмотрения', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ по пункту 6 T-да F-нет', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'P6'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос: кол-во совпадает?', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'TrueKolvo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос: критерий "КО отвечает требованиям"', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'VestnikBR'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос: Наличие Гарантийного письма КО', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'GrLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Komment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип письма L - письмо по активам H - письмо по МБК G-золото', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'typeLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос №1 по ходатайству', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Q1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос №2 по ходатайству', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Q2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ответ на вопрос №3 по ходатайству', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Q3'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ответ на 4 вопрос по письму', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Q4'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер ходатайства Банка', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'NumLetter'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата гарантийного письма (если оно не было предоставлено сразу)', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'WarrantDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата принятия решения по ходатайству', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'LBeginDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер письма в КО о аннулировании согласия по ход-ву', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'NumLetKo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата письма в КО о аннулировании согласия по ход-ву', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'DtLetKo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер письма в хранилище аннулировании согласия по ход-ву', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'NumLetStore'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата письма в Хранилище о аннулировании согласия по ход-ву', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'dtLetstore'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер письма в КО о включении слитков в пул', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'NumLetIncl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата письма в КО о включении слитков в пул', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'DtLetIncl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата включения слитков в пул', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'DtIncl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор хранилища', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'idStore'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'dtRemoval'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'countTransh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'для ЭКСАР', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'Qdopinf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Заявление о предоставлении кредита/номере транша', 'SCHEMA', N'dbo', 'TABLE', N'jrLetter', 'COLUMN', N'LoanTrancheApplication'
GO