﻿CREATE TABLE [dbo].[hub_jrRKC] (
  [idRKC] [int] NOT NULL,
  [nmRKC] [varchar](250) NOT NULL,
  [nmPostRKC] [varchar](200) NULL,
  [nmBossRKC] [varchar](100) NULL,
  [nuBIKRKC] [char](9) NOT NULL,
  [izFormIzvesh] [char](1) NULL,
  [tpRKC] [char](1) NOT NULL DEFAULT ('A'),
  [nuRNKOinRKC] [char](20) NULL
)
ON [PRIMARY]
GO