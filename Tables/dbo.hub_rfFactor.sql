﻿CREATE TABLE [dbo].[hub_rfFactor] (
  [dtFactor] [datetime] NOT NULL,
  [tpFactor] [char](2) NOT NULL,
  [pcFactor] [decimal](7, 4) NOT NULL,
  [idOrg] [int] NULL,
  [dtEnd] [datetime] NULL
)
ON [PRIMARY]
GO