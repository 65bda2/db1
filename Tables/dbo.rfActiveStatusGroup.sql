﻿CREATE TABLE [dbo].[rfActiveStatusGroup] (
  [IdActiveStatusGroup] [int] IDENTITY,
  [IdActiveGroup] [int] NOT NULL,
  [status] [char](1) NOT NULL,
  [idActiveState] [int] NULL,
  [RowComment] [varchar](255) NULL,
  CONSTRAINT [PK_rfActiveStatusGroup] PRIMARY KEY CLUSTERED ([IdActiveStatusGroup])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[rfActiveStatusGroup]
  ADD CONSTRAINT [FK_rfActiveStatusGroup_IdActiveGroup] FOREIGN KEY ([IdActiveGroup]) REFERENCES [dbo].[rfActiveGroup] ([IdActiveGroup])
GO

ALTER TABLE [dbo].[rfActiveStatusGroup]
  ADD CONSTRAINT [FK_rfActiveStatusGroup_idActiveState] FOREIGN KEY ([idActiveState]) REFERENCES [dbo].[rfActiveState] ([idActiveState])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Таблица предназначена для группировки статусов активов по группам (видам разделов).', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup', 'COLUMN', N'IdActiveStatusGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор группы (вида раздела АС ППА). Ссылка на таблицу rfActiveGroup.', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup', 'COLUMN', N'IdActiveGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код статуса актива', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор вида раздела АС "Сибирь". Ссылка на таблицу rfActiveState.', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup', 'COLUMN', N'idActiveState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatusGroup', 'COLUMN', N'RowComment'
GO