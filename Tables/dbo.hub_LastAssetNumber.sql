﻿CREATE TABLE [dbo].[hub_LastAssetNumber] (
  [nuReg] [varchar](4) NOT NULL,
  [KPTU] [char](2) NOT NULL,
  [AssetNumNA] [int] NULL,
  [AssetNumMSP] [int] NULL,
  [AssetNumEcsar] [int] NULL,
  [AssetNumInvest] [int] NULL
)
ON [PRIMARY]
GO