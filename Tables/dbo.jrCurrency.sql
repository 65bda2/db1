﻿CREATE TABLE [dbo].[jrCurrency] (
  [nuISO] [char](3) NOT NULL,
  [nmISO] [char](3) NOT NULL,
  [nmCurr] [char](100) NOT NULL,
  CONSTRAINT [PK_jrCurrency] PRIMARY KEY CLUSTERED ([nuISO])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogJrCurrency]
ON [dbo].[jrCurrency]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = nuISO+', '+nmISO+', '+RTRIM(nmCurr) FROM deleted
  SELECT @newValues = nuISO+', '+nmISO+', '+RTRIM(nmCurr) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список иностранных валют, установленный Банком России', 
   'численное обозначение валюты, символьное обозначение валюты, наименование', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'перечень иностранных валют, установленный Банком России', 'SCHEMA', N'dbo', 'TABLE', N'jrCurrency'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'численное обозначение валюты', 'SCHEMA', N'dbo', 'TABLE', N'jrCurrency', 'COLUMN', N'nuISO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'символьное обозначение валюты', 'SCHEMA', N'dbo', 'TABLE', N'jrCurrency', 'COLUMN', N'nmISO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наименование валюты', 'SCHEMA', N'dbo', 'TABLE', N'jrCurrency', 'COLUMN', N'nmCurr'
GO