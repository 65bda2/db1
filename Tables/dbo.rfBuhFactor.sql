﻿CREATE TABLE [dbo].[rfBuhFactor] (
  [idrecord] [int] IDENTITY,
  [OKVED] [char](8) NOT NULL,
  [REP_Date] [datetime] NOT NULL,
  [TpFactor] [char](2) NOT NULL,
  [PRIZ] [int] NOT NULL,
  [SRED] [decimal](22, 10) NOT NULL,
  [DOPUST] [decimal](22, 10) NOT NULL,
  CONSTRAINT [PK_rfBuhFactor] PRIMARY KEY CLUSTERED ([OKVED], [REP_Date], [TpFactor], [PRIZ])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfBuhFactor]
ON [dbo].[rfBuhFactor]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
 
 SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
 SELECT @oldValues = CAST(idrecord AS VARCHAR(10))+', '+ RTRIM(OKVED)+', '+ CONVERT(VARCHAR(10), REP_Date, 104)+', '+ RTRIM(TpFactor)+', '+
  CONVERT(VARCHAR(20), PRIZ)+', '+CONVERT(VARCHAR(25), SRED)+', '+CONVERT(VARCHAR(25), DOPUST) FROM deleted

  SELECT @newValues = CAST(idrecord AS VARCHAR(10))+', '+ RTRIM(OKVED)+', '+ CONVERT(VARCHAR(10), REP_Date, 104)+', '+ RTRIM(TpFactor)+', '+
  CONVERT(VARCHAR(20), PRIZ)+', '+CONVERT(VARCHAR(25), SRED)+', '+CONVERT(VARCHAR(25), DOPUST)  FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Среднии показатели бух отчетности', 
   'Идентификатор записи, ОКВЕД, Дата показателя ,Тип показателя, Вид предприятия, Среднее значение, Допустимое значение ', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник СРЕДНИЕ И ДОПУСТИМЫЕ ЗНАЧЕНИЯ ПОКАЗАТЕЛЕЙ БУХГАЛТЕРСКОЙ ОТЧЕТНОСТИ ОРГАНИЗАЦИЙ ПО ВИДАМ ЭКОНОМИЧЕСКОЙ ДЕЯТЕЛЬНОСТИ', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'idrecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Оквед', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'OKVED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Отчетная дата', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'REP_Date'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'П2 -     П3 -', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'TpFactor'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1- крупное/среднее , 2 - мелкое', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'PRIZ'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'среднее значение', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'SRED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'допустимое значение', 'SCHEMA', N'dbo', 'TABLE', N'rfBuhFactor', 'COLUMN', N'DOPUST'
GO