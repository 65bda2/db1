﻿CREATE TABLE [dbo].[hrRateExch] (
  [dtRate] [datetime] NOT NULL,
  [nuISO] [char](3) NOT NULL,
  [nuScale] [int] NOT NULL,
  [nuRate] [decimal](20, 5) NOT NULL,
  CONSTRAINT [PK_hrRateExch] PRIMARY KEY CLUSTERED ([dtRate], [nuISO])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogHrRateExch]
ON [dbo].[hrRateExch]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtRate, 104)+', '+nuISO+', '+
    CAST(nuScale AS VARCHAR(20))+', '+CONVERT(VARCHAR(20), nuRate) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtRate, 104)+', '+nuISO+', '+
    CAST(nuScale AS VARCHAR(20))+', '+CONVERT(VARCHAR(20), nuRate) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
  'Изменение курсов иностранных валют', 
  'дата установления курса, численное обозначение валюты, кол-во денежных единиц, курс', @oldValues, @newValues)
GO

ALTER TABLE [dbo].[hrRateExch]
  ADD CONSTRAINT [FK_hrRateExch_nuISO] FOREIGN KEY ([nuISO]) REFERENCES [dbo].[jrCurrency] ([nuISO]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'хронология курсов иностранных валют', 'SCHEMA', N'dbo', 'TABLE', N'hrRateExch'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата', 'SCHEMA', N'dbo', 'TABLE', N'hrRateExch', 'COLUMN', N'dtRate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'численное обозначение валюты', 'SCHEMA', N'dbo', 'TABLE', N'hrRateExch', 'COLUMN', N'nuISO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'кол-во денежных единиц', 'SCHEMA', N'dbo', 'TABLE', N'hrRateExch', 'COLUMN', N'nuScale'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'курс', 'SCHEMA', N'dbo', 'TABLE', N'hrRateExch', 'COLUMN', N'nuRate'
GO