﻿CREATE TABLE [dbo].[hrActiveSect] (
  [idActiveSect] [int] IDENTITY,
  [DateAssign] [datetime] NOT NULL,
  [NumActive] [int] NOT NULL,
  [idAcc] [int] NOT NULL,
  [idActiveState] [int] NOT NULL,
  CONSTRAINT [PK_hrActiveSect] PRIMARY KEY CLUSTERED ([idActiveSect])
)
ON [PRIMARY]
GO

CREATE INDEX [IX_hrActiveSect]
  ON [dbo].[hrActiveSect] ([idAcc])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[hrActiveSect]
  ADD CONSTRAINT [FK_hrActiveSect_idAcc] FOREIGN KEY ([idAcc]) REFERENCES [dbo].[jrAccount] ([idAcc])
GO

ALTER TABLE [dbo].[hrActiveSect]
  ADD CONSTRAINT [FK_hrActiveSect_idActiveState] FOREIGN KEY ([idActiveState]) REFERENCES [dbo].[rfActiveState] ([idActiveState])
GO

ALTER TABLE [dbo].[hrActiveSect]
  ADD CONSTRAINT [FK_hrActiveSect_NumActive] FOREIGN KEY ([NumActive]) REFERENCES [dbo].[jrActive] ([NumActive])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'(Таблица предназначена для учета перемещений актива по разделам', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИД записи в таблице состояний актива', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect', 'COLUMN', N'idActiveSect'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата присвоения нового состояния активу', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect', 'COLUMN', N'DateAssign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код актива (ссылка на актив) ( вторичный ключ к таблице jrActive полю NumActive)', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на основной счет КД, к которому относится актив (вторичный ключ к таблице jrAccount полю idAcc)', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect', 'COLUMN', N'idAcc'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'(вторичный ключ к таблице rfActiveState полю idActiveState)', 'SCHEMA', N'dbo', 'TABLE', N'hrActiveSect', 'COLUMN', N'idActiveState'
GO