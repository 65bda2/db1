﻿CREATE TABLE [dbo].[hub_rfSignRekvizitASPPA] (
  [tpSign] [varchar](50) NOT NULL,
  [nmSign] [varchar](400) NULL,
  [tpGroup] [char](1) NOT NULL DEFAULT ('A')
)
ON [PRIMARY]
GO