﻿CREATE TABLE [dbo].[DKO_hrAssetState] (
  [IdRecord] [bigint] IDENTITY,
  [_IdRecord] [int] NOT NULL,
  [IdAssetState] [varchar](17) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [IdAsset] [numeric](16) NOT NULL,
  [PartitionTypeTo] [char](2) NOT NULL,
  [CrdNum] [varchar](10) NULL,
  [ReqType] [tinyint] NULL,
  [ReqNum] [varchar](20) NULL,
  [ReqDate] [smalldatetime] NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL,
  [ToolCode] [char](1) NULL,
  [OperType] [tinyint] NULL,
  [PartitionTypeFrom] [char](2) NULL,
  [IS_IN] [tinyint] NULL,
  [idDKOPTandActiveState] [int] NULL,
  [IdDKOhrES] [int] NULL,
  [ErrorMessage] [varchar](255) NULL,
  CONSTRAINT [PK_DKO_hrAssetState] PRIMARY KEY CLUSTERED ([IdRecord]),
  CONSTRAINT [CK_DKO_hrAssetState_CorrectionType] CHECK ([CorrectionType]>=(1) AND [CorrectionType]<=(4)),
  CONSTRAINT [CK_DKO_hrAssetState_IS_IN] CHECK ([IS_IN]>=(0) AND [IS_IN]<=(1)),
  CONSTRAINT [CK_DKO_hrAssetState_IsArchive] CHECK ([IsArchive]>=(0) AND [IsArchive]<=(1)),
  CONSTRAINT [CK_DKO_hrAssetState_StateSED] CHECK ([StateSED]>=(-4) AND [StateSED]<=(3))
)
ON [PRIMARY]
GO

CREATE INDEX [I_DKO_hrAssetState]
  ON [dbo].[DKO_hrAssetState] ([IdAsset])
  ON [PRIMARY]
GO

ALTER TABLE [dbo].[DKO_hrAssetState]
  ADD CONSTRAINT [FK_DKO_hrAssetState_rfDKOPTandActiveState] FOREIGN KEY ([idDKOPTandActiveState]) REFERENCES [dbo].[rfDKOPTandActiveState] ([idRecord])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор В АС "Сибирь" ', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'_IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный начальный отрицательный идентификатор В АСДКО', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IdAssetState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, с которой действует данный набор значений', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'D_From'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор обновления', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IdUpdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Cостояния: -3 - Для проверки контролёром, -2 - Стартовое решение, -1 - Не отправлять, 0 - Неотправленное, 1 - Отправленное, 2 - Подтверждённое, 3 - Отклонённое', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'StateSED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - Запись активна, 1 - Запись в архиве', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - Создание, 2 - Изменение, 3 - Удаление ошибочной, 4 - Удаление по актуальности', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'CorrectionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор актива', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IdAsset'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер кредита Банка России. Заполняется если для Вида раздела (PartitionType) указано значение ЗП или ТП', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'CrdNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид заявки/заявления. Заполняется если для Вида раздела (PartitionType) указано значение Пб: 1 Заявление КО на получение кредита по фиксированной процентной ставке или заявка КО на участие в кредитном аукционе, 2 Заявление КО на вывод или замену обеспечения, 3 Прочие операции', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ReqType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер заявки/заявления, присвоенный КО. Заполняется если для Вида заявки/заявления (ReqType) указано значение 1 или 2', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ReqNum'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата заявки/заявления присвоенная КО. Заполняется если для Вида заявки/заявления (ReqType) указано значение 1 или 2', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ReqDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак 0-исходящее 1-входящее', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IS_IN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на rfDKOPTandActiveState', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'idDKOPTandActiveState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить идентификатор ЭС, в котором пришли измененния', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'IdDKOhrES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить сообщение об ошибке, в случае отбраковки измененичя', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAssetState', 'COLUMN', N'ErrorMessage'
GO