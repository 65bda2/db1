﻿CREATE TABLE [dbo].[ASBO_MainDataES] (
  [idES] [bigint] NULL,
  [idMainDataES] [bigint] IDENTITY,
  [idBlockES] [int] NULL,
  [IsArchive] [int] NULL CONSTRAINT [col_IsArchive_def] DEFAULT (0),
  [IsAnswer] [int] NULL,
  [RegNumKO] [char](9) NULL,
  [Status] [char](1) NOT NULL,
  [EGRULOrg] [char](13) NULL,
  [NMKO] [varchar](255) NULL,
  [OKATOKO] [char](11) NULL,
  [OKPOKO] [char](8) NULL,
  [EGRULKO] [char](13) NULL,
  [BIK] [char](9) NULL,
  [isTrebKO] [char](1) NULL,
  [isAffKO] [char](1) NULL,
  [nmOrg] [varchar](255) NULL,
  [ShortNmOrg] [varchar](255) NULL,
  [DtOGRN] [datetime] NULL,
  [D_LastRegDate] [datetime] NULL,
  [TpDtReg] [char](1) NULL,
  [NmLaw] [varchar](255) NULL,
  [INNOrg] [char](12) NULL,
  [OKPOOrg] [char](8) NULL,
  [CodeOKOPFOrg] [varchar](5) NULL,
  [NmOkopfOrg] [varchar](255) NULL,
  [TpCrit] [char](1) NULL,
  [Belonging] [char](1) NULL,
  [CodeOKVEdOrg] [char](8) NULL,
  [businessQuality] [char](1) NULL,
  [OKATOOrg] [char](11) NULL,
  [nmAddr] [varchar](255) NULL,
  [IsResident] [char](1) NULL,
  [isSubject] [char](1) NULL,
  [IsinList] [char](1) NULL,
  [AnotherInf] [char](1) NULL,
  [AnotherInfNote] [varchar](255) NULL,
  [isBookKeepingOK] [char](1) NULL,
  [dtBookKepping] [datetime] NULL,
  [isSootv] [char](1) NULL,
  [Rating] [varchar](255) NULL,
  [AlterComment] [varchar](255) NULL,
  [isAffOrg] [char](1) NULL,
  [KPTU] [char](2) NULL,
  [NmResultCheck] [varchar](255) NULL,
  [ResultCheck] [int] NULL,
  [dtCheck] [datetime] NULL,
  [LastReportDate] [datetime] NULL,
  [idKO] [int] NULL,
  [idOrg] [int] NULL,
  [KPHub] [char](2) NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_ASBO_MainDataES_idMainDataES]
  ON [dbo].[ASBO_MainDataES] ([idMainDataES])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Данные, содержащиеся в электроннном сообщении', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор  ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'idES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'idMainDataES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Порядковый номер блока данных в ЭС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'idBlockES'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - запись в архиве', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - на запись не приходил ответ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'IsAnswer'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Регистрационный номер банка-заемщика', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'RegNumKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'R - данные готовы к отправке, S -данные отправлены, F- данные не приняты ЦИТ по ИЭС1, P- данные не приняты ЦИТ по ИЭС2 , L - ожидаем 202 , T - Данные приняты ЦИТ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'Status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ЕГРЮЛ организации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'EGRULOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'NMKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКАТО КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'OKATOKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПО КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'OKPOKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОГРН КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'EGRULKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'БИК КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'BIK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соотв. требованиям КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isTrebKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Аффилированность КО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isAffKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование орг.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'nmOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Краткое наим. орг.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'ShortNmOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата ОГРН', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'DtOGRN'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата для период. деятельности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'D_LastRegDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак даты', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'TpDtReg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование закона', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'NmLaw'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ИНН орг.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'INNOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПО орг.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'OKPOOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКПФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'CodeOKOPFOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'наим. ОКПФ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'NmOkopfOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Критерии, на соответствие которым проверена организация', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'TpCrit'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'принадлежность', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'Belonging'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКВЕД', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'CodeOKVEdOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип субъекта предпринимательства', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'businessQuality'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ОКАТО', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'OKATOOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'АДРЕС', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'nmAddr'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Резидент', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'IsResident'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип орг.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isSubject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Включена в перечень', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'IsinList'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соотв. другой инф. критериям', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'AnotherInf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Другая инф.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'AnotherInfNote'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соотв. критериям бух.отч.', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isBookKeepingOK'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата бух. отчетности', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'dtBookKepping'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Соотв. организации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isSootv'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Рейтинг для Муниц. и субъектов', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'Rating'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'AlterComment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Аффилированность организации', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'isAffOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ТУ', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'KPTU'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Результат контроля(текст)', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'NmResultCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Результат контроля  код', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'ResultCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата проверки для ИЭС1 и ИЭС2, для А202 - отчетная дата по блоку', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'dtCheck'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Последняя отчётная дата', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'LastReportDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'КодХаба', 'SCHEMA', N'dbo', 'TABLE', N'ASBO_MainDataES', 'COLUMN', N'KPHub'
GO