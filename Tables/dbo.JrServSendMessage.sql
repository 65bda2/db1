﻿CREATE TABLE [dbo].[JrServSendMessage] (
  [MessText] [varchar](1000) NOT NULL,
  [DtMess] [datetime] NOT NULL,
  [idMess] [varchar](17) NOT NULL,
  [TpMess] [char](1) NOT NULL,
  [dtCheck] [datetime] NULL
)
ON [PRIMARY]
GO

CREATE INDEX [I_JrServSendMessage_DtMess]
  ON [dbo].[JrServSendMessage] ([DtMess])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'сервис отправки сообщений в ППА ЦА', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Текст сообщения', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage', 'COLUMN', N'MessText'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата сообщения', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage', 'COLUMN', N'DtMess'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор сообщения -  дата, часы, минуты, секунды, милисекунды', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage', 'COLUMN', N'idMess'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип источника сообщений (1 - сервис отправки сообщений в ППА ЦА) заложено на будущее', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage', 'COLUMN', N'TpMess'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата обработки сообщения', 'SCHEMA', N'dbo', 'TABLE', N'JrServSendMessage', 'COLUMN', N'dtCheck'
GO