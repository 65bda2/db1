﻿CREATE TABLE [dbo].[rfActiveStatus] (
  [status] [char](1) NOT NULL,
  [StatusName] [varchar](255) NOT NULL,
  [isGoodForCredits] [bit] NOT NULL CONSTRAINT [DF_rfActiveStatus_isGoodForCredits] DEFAULT (0),
  CONSTRAINT [PK_rfActiveStatus] PRIMARY KEY CLUSTERED ([status])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'справочником статусов активов.', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatus'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код статуса актива', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatus', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование статуса', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatus', 'COLUMN', N'StatusName'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Признак возможности использования активов имеющих данныхй статус для кредитов БР по Условиям (указание № 4801-У от 22.05.2018)', 'SCHEMA', N'dbo', 'TABLE', N'rfActiveStatus', 'COLUMN', N'isGoodForCredits'
GO