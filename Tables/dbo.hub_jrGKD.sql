﻿CREATE TABLE [dbo].[hub_jrGKD] (
  [nuGKD] [int] NOT NULL,
  [dtGKD] [datetime] NOT NULL,
  [idKO] [int] NOT NULL,
  [isSootv] [char](1) NOT NULL DEFAULT ('T'),
  [mnLimitAgg] [money] NULL,
  [dtGKDend] [datetime] NULL,
  [numGKD] [varchar](15) NOT NULL,
  [tpGKD] [char](1) NOT NULL,
  [isTreb] [char](1) NOT NULL DEFAULT ('F'),
  [dtGKDUved] [datetime] NULL,
  [isPartner] [char](1) NOT NULL DEFAULT ('F'),
  [nuGKD236] [int] NULL,
  [nuGKD312] [int] NULL,
  [nuGKD362] [int] NULL,
  [nmKO] [varchar](250) NULL,
  [nnKO] [varchar](128) NULL,
  [nuReg] [varchar](9) NULL,
  [nuBIK] [varchar](9) NULL,
  [nuINN] [varchar](10) NULL,
  [nmJurAddr] [varchar](250) NULL,
  [nmFactAddr] [varchar](250) NULL,
  [nmNadzTU] [varchar](200) NULL,
  [nuEKD] [int] NULL,
  [dtEKDBegin] [datetime] NULL,
  [dtGKDStop] [datetime] NULL
)
ON [PRIMARY]
GO