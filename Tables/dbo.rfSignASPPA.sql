﻿CREATE TABLE [dbo].[rfSignASPPA] (
  [idsign] [int] IDENTITY,
  [tpSign] [char](1) NOT NULL,
  [nmSign] [varchar](400) NOT NULL,
  [nmPost] [varchar](400) NOT NULL,
  [Dopinf] [char](15) NOT NULL DEFAULT ('000000000000000'),
  [NumOrder] [int] NOT NULL,
  [tpGroup] [char](2) NULL,
  [foundation] [varchar](255) NULL
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogrfSignASPPA]
ON [dbo].[rfSignASPPA]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
 SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER
  SELECT @oldValues = CAST(idsign AS VARCHAR(10))+', '+ CAST(NumOrder AS VARCHAR(10))+', '+ RTRIM(tpSign)+', '+RTRIM(nmSign)+
  ', '+ RTRIM(nmPost)+', '+RTRIM(Dopinf) +', '+RTRIM(tpGroup) FROM deleted
  SELECT @newValues =  CAST(idsign AS VARCHAR(10))+', '+ CAST(NumOrder AS VARCHAR(10))+', '+ RTRIM(tpSign)+', '+RTRIM(nmSign)+
  ', '+ RTRIM(nmPost)+', '+RTRIM(Dopinf) +', '+RTRIM(tpGroup) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Реквизиты подписей(сотрудники)АСППА', 
   'Идентификатор записи, Порядок сортировки, тип подписи, ФИО , Должность, Заполнение отчетов, Группа ', 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Реквизиты подписей (только подписи)', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор подписи', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'idsign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип подписи S - подпись V - виза K - подписи по кладовой U-уполномоченный сотрудник I -исполнитель', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'tpSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'ФИО', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'nmSign'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'должность', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'nmPost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'каждый символ - информация по определенному отчету. если 0 - не выводить, 1 - выводить', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'Dopinf'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'порядок сортировки', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'NumOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип группы. для  tpSign=K:', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'tpGroup'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Действующий на основании', 'SCHEMA', N'dbo', 'TABLE', N'rfSignASPPA', 'COLUMN', N'foundation'
GO