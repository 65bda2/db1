﻿CREATE TABLE [dbo].[DKO_hrAsset] (
  [IdRecord] [bigint] IDENTITY,
  [_NumActive] [int] NOT NULL,
  [IdAsset] [numeric](16) NOT NULL,
  [D_From] [smalldatetime] NOT NULL,
  [IdUpdate] [bigint] NOT NULL,
  [StateSED] [int] NOT NULL DEFAULT (0),
  [IsArchive] [tinyint] NOT NULL DEFAULT (0),
  [CorrectionType] [tinyint] NOT NULL,
  [AssetType] [numeric](5) NOT NULL,
  [AssetCode] [varchar](20) NULL,
  [AssetIssue] [varchar](20) NULL,
  [AssetNumber] [varchar](255) NULL,
  [AssetDate] [smalldatetime] NULL,
  [D_Repayment] [smalldatetime] NULL,
  [FaceValue] [numeric](22, 7) NULL,
  [FaceValueCurrency] [numeric](3) NULL,
  [FvCurrencyRate] [numeric](21, 7) NULL,
  [PurchaseValue] [numeric](22, 7) NULL,
  [PurchaseCurrency] [numeric](3) NULL,
  [PureMass] [numeric](10, 2) NULL,
  [NetMass] [numeric](10, 2) NULL,
  [Standard] [numeric](7, 4) NULL,
  [IsActive] [tinyint] NOT NULL,
  [AssetQuality] [numeric](1) NULL,
  [IdIssuer] [numeric](16) NULL,
  [IdOrgToCheck] [numeric](16) NULL,
  [MainAccount] [varchar](20) NULL,
  [PayOrgBIC] [varchar](9) NULL,
  [TUHolder] [varchar](2) NULL,
  [DebtorRegNum] [varchar](20) NULL,
  [IdParentAsset] [numeric](16) NULL,
  [OriginalsState] [tinyint] NULL,
  [isGuaranteedByCrimea] [tinyint] NULL,
  [isGuaranteedByRF] [tinyint] NULL,
  [isMinFinAgreed] [tinyint] NULL,
  [AlterComment] [varchar](500) NULL,
  [ESDate] [smalldatetime] NULL,
  [ESNo] [int] NULL,
  [GKDNum] [varchar](15) NULL,
  [GKDDate] [datetime] NULL,
  [IdDKOPTandActiveState] [int] NULL,
  [TrancheNumber] [varchar](255) NULL,
  CONSTRAINT [PK_DKO_hrAsset] PRIMARY KEY CLUSTERED ([IdRecord]),
  CONSTRAINT [CK_DKO_hrAsset_CorrectionType] CHECK ([CorrectionType]>=(1) AND [CorrectionType]<=(4)),
  CONSTRAINT [CK_DKO_hrAsset_IsArchive] CHECK ([IsArchive]>=(0) AND [IsArchive]<=(1)),
  CONSTRAINT [CK_DKO_hrAsset_StateSED] CHECK ([StateSED]>=(-4) AND [StateSED]<=(3))
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[DKO_hrAsset]
  ADD CONSTRAINT [FK_DKOhrAsset_idDKOPTandActiveState] FOREIGN KEY ([IdDKOPTandActiveState]) REFERENCES [dbo].[rfDKOPTandActiveState] ([idRecord])
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'IdRecord'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор В АС "Сибирь"', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'_NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный начальный отрицательный идентификатор В АСДКО', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'IdAsset'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата, с которой действует данный набор значений', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'D_From'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор обновления', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'IdUpdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Состояния: -3 - Для проверки контролёром, -2 - Стартовое решение, -1 - Не отправлять, 0 - Неотправленное, 1 - Отправленное, 2 - Подтверждённое, 3 - Отклонённое', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'StateSED'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'0 - Запись активна, 1 - Запись в архиве', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'IsArchive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'1 - Создание, 2 - Изменение, 3 - Удаление ошибочной, 4 - Удаление по актуальности', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'CorrectionType'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Золото', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'PureMass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Золото', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'NetMass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Золото', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'Standard'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Как NumActive, но связь с IdAsset', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'IdParentAsset'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Представила ли КО оригиналы документов', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'OriginalsState'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Предоставлена государственная гарантия Республики Крым или города федерального значения Севастополя', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'isGuaranteedByCrimea'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Предоставлена государственная гарантия Российской Федерации', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'isGuaranteedByRF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Имеется согласие Министерства финансов Российской Федерации на уступку прав требования', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'isMinFinAgreed'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'ESDate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер 201 сообщения', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'ESNo'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'В поле требуется хранить номер транша', 'SCHEMA', N'dbo', 'TABLE', N'DKO_hrAsset', 'COLUMN', N'TrancheNumber'
GO