﻿CREATE TABLE [dbo].[lgArchActive] (
  [Dateit] [datetime] NULL,
  [RekvOrg] [varchar](500) NULL,
  [CheckP36] [char](15) NULL,
  [CheckP35] [char](15) NULL,
  [idArhActive] [int] IDENTITY,
  [Cost] [varchar](100) NULL,
  [Kategory] [char](10) NULL,
  [Kurs] [char](20) NULL,
  [Koef] [char](10) NULL,
  [costrub] [varchar](100) NULL,
  [status] [varchar](2048) NULL,
  [DSign1] [varchar](150) NULL,
  [DSign2] [varchar](150) NULL,
  [FIo1] [varchar](100) NULL,
  [FIO2] [varchar](100) NULL,
  [NumActive] [int] NOT NULL,
  [Numorder] [int] NULL,
  [Komment] [varchar](150) NULL,
  [happen] [char](1) NULL,
  [NmCurrency] [varchar](20) NULL,
  CONSTRAINT [PK_lgArchActive] PRIMARY KEY CLUSTERED ([idArhActive])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Для отчета по карточке актива', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата события', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Dateit'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'реквизиты организации(Наименование,Огрн и )', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'RekvOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'выполняется ли пункт 3.6', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'CheckP36'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'выполняется ли пункт 3.5/3.4', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'CheckP35'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор события', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'idArhActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'стоимость', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Cost'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'категория качества', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Kategory'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'курс', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Kurs'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'поправочный коэфициент', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Koef'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'стоимость в рублях* попр коэф*курс', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'costrub'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'событие', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'status'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Подпись 1-ая не используется', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'DSign1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Подпись 2-ая не используется', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'DSign2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Пользователь выполнивший операцию', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'FIo1'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'не используется', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'FIO2'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер актива', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'NumActive'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Номер сортировки', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Numorder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'Komment'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код события', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'happen'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'символьное обозначение валюты в коотрой выражена стоимость', 'SCHEMA', N'dbo', 'TABLE', N'lgArchActive', 'COLUMN', N'NmCurrency'
GO