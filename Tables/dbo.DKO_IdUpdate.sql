﻿CREATE TABLE [dbo].[DKO_IdUpdate] (
  [IdUpdate] [bigint] IDENTITY,
  [TypeDKO] [varchar](10) NULL,
  [IdObject] [bigint] NULL,
  [TypeCred] [char](1) NULL
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор обновления', 'SCHEMA', N'dbo', 'TABLE', N'DKO_IdUpdate', 'COLUMN', N'IdUpdate'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид DKO, например: DKO510, DKO201  jrOrg, rfAffPers, jrActive, jrTransh, jrPaymentGrafic, DKO206  hrStockSect, DKO502  hrStockPawn, DKO507  hrStockPawn hrStockNight, DKO508  hrPogashCred, DKO509  hrStockPawn hrStockNight, DKO510  lgZayavl, DKO511  lgZayavl, DKO520  hrAdvancedRepayment', 'SCHEMA', N'dbo', 'TABLE', N'DKO_IdUpdate', 'COLUMN', N'TypeDKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Ссылка на объект', 'SCHEMA', N'dbo', 'TABLE', N'DKO_IdUpdate', 'COLUMN', N'IdObject'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Вид кредита: N - кредиты овернайт, L - кредиты, кроме овернайт', 'SCHEMA', N'dbo', 'TABLE', N'DKO_IdUpdate', 'COLUMN', N'TypeCred'
GO