﻿CREATE TABLE [dbo].[LgOrderASPPA] (
  [idOrder] [int] IDENTITY,
  [dtOrder] [datetime] NOT NULL,
  [nuOrder] [int] NULL,
  [idBlock] [int] NULL,
  [tpOrder] [char](1) NOT NULL,
  [idRKC] [int] NULL,
  [nuIshod] [varchar](30) NULL,
  [nmUser] [varchar](50) NOT NULL,
  [tmOrder] [datetime] NULL,
  CONSTRAINT [C_LgOrderASPPA_tpOrder] CHECK ([tpOrder]='B' OR [tpOrder]='A' OR [tpOrder]='Y' OR [tpOrder]='X' OR [tpOrder]='W' OR [tpOrder]='V')
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogLgOrderASPPA]
ON [dbo].[LgOrderASPPA]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)
  SET CONCAT_NULL_YIELDS_NULL OFF
  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CAST(idOrder AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), dtOrder, 104)+', '+ CAST(nuOrder AS VARCHAR(10))+', '+ CAST(idBlock AS VARCHAR(10))+', '
  + RTRIM(tpOrder)+', '+RTRIM(nuIshod)+', '+CAST(idRKC AS VARCHAR(10))+', '+ RTRIM(nmUser)+', '+CONVERT(VARCHAR(10), tmOrder, 108) FROM deleted

  SELECT @newValues =  CAST(idOrder AS VARCHAR(10))+', '+CONVERT(VARCHAR(10), dtOrder, 104)+', '+ CAST(nuOrder AS VARCHAR(10))+', '+ CAST(idBlock AS VARCHAR(10))+', '
  + RTRIM(tpOrder)+', '+RTRIM(nuIshod)+', '+CAST(idRKC AS VARCHAR(10))+', '+ RTRIM(nmUser)+', '+CONVERT(VARCHAR(10), tmOrder, 108) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN
 INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Журнал распоряжений АСППА', 
   'Идентификатор , Дата распоряжения, Порядковый номер, Идентификатор ББР,Тип , Исходящий номер, идентификатор РКЦ, КО,  '+
   ' Пользователь, Комментарий, Время' , 
   @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Журнал рапоряжений', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор распоряжения', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'idOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата распоряжения', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'dtOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'порядковый номер', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'nuOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор ББР', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'idBlock'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип распоряжения', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'tpOrder'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'идентификатор РКЦ', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'idRKC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'исходящийй номер', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'nuIshod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'имя пользователя', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'nmUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'время', 'SCHEMA', N'dbo', 'TABLE', N'LgOrderASPPA', 'COLUMN', N'tmOrder'
GO