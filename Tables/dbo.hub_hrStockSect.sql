﻿CREATE TABLE [dbo].[hub_hrStockSect] (
  [dtSect] [datetime] NOT NULL,
  [idBlock] [int] NOT NULL,
  [tpSect] [char](3) NOT NULL,
  [idStock] [int] NOT NULL,
  [cdStock] [int] NOT NULL DEFAULT (0),
  [qnStock] [int] NOT NULL,
  [dtPrice] [datetime] NOT NULL,
  [mnCost] [money] NULL,
  [mnCostCoef] [money] NULL,
  [dtCoeff] [datetime] NOT NULL,
  [dtCoeffNew] [datetime] NULL,
  [tpActive] [smallint] NOT NULL DEFAULT (0),
  [IdRecord] [bigint] NOT NULL
)
ON [PRIMARY]
GO