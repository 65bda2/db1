﻿CREATE TABLE [dbo].[rfAffPers] (
  [idKO] [int] NOT NULL,
  [idOrg] [int] NOT NULL,
  [pcShare] [decimal](7, 4) NULL,
  [IsCompanyOwner] [tinyint] NOT NULL DEFAULT (0),
  [dtBegin] [datetime] NULL,
  [dtEnd] [datetime] NULL,
  [idRecord] [int] IDENTITY,
  [AlterComment] [varchar](500) NULL,
  CONSTRAINT [PK_rfAffPers] PRIMARY KEY CLUSTERED ([idRecord]),
  CONSTRAINT [C_rfAffPers_IsCompanyOwner] CHECK ([IsCompanyOwner]=(1) OR [IsCompanyOwner]=(0))
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfAffPers]
ON [dbo].[rfAffPers]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(20), idKO)+', '+CONVERT(VARCHAR(20), idOrg)+', '+CONVERT(VARCHAR(10), pcShare) FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(20), idKO)+', '+CONVERT(VARCHAR(20), idOrg)+', '+CONVERT(VARCHAR(10), pcShare) FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список взаимного участия', 
   'идентификатор КО, идентификатор юр.лица, доля взаимного участия', 
   @oldValues, @newValues)
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trRfAffPers] ON [dbo].[rfAffPers] AFTER INSERT, UPDATE, DELETE
AS
  
  DECLARE @Cursor CURSOR, 
    @IdRecord INTEGER, @CorrectionType TINYINT,
    @IdCompany NUMERIC(16), @BankRegNum VARCHAR(20), @IsCompanyOwner TINYINT, @AlterComment VARCHAR(500)

  IF EXISTS( SELECT idOrg FROM inserted )   ---- Добавление или изменение строки
  BEGIN
    IF ( SELECT dtEnd FROM inserted ) IS NOT NULL   ---- Окончание аффилированности
      SET @CorrectionType = 4                   
    ELSE
      IF EXISTS( SELECT idOrg FROM deleted )        ---- Внесение изменений в запись об аффилированности
        SET @CorrectionType = 2 
      ELSE 
        SET @CorrectionType = 1                     ---- Новая запись об аффилированности
        
    SET @Cursor = CURSOR FOR 
      SELECT i.IdRecord,                                                              ---- Уникальный идентификатор записи в АС ППА
             ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = i.idOrg ),    ---- Уникальный идентификатор организации в АС ДКО
             ( SELECT nuReg FROM jrKO WHERE idKO = i.idKO ),                          ---- регистрационный номер КО 
             i.IsCompanyOwner, i.AlterComment 
      FROM inserted i INNER JOIN jrORG o ON i.idOrg = o.idOrg
      WHERE o.OrgStatus = 'T'
  END
  ELSE BEGIN                                ---- Удаление строки
    SET @CorrectionType = 4
    
    SET @Cursor = CURSOR FOR 
      SELECT d.IdRecord,                                                               ---- Уникальный идентификатор записи в АС ППА
             ( SELECT TOP 1 IdCompany FROM DKO_hrCompany WHERE _IdOrg = d.idOrg ),     ---- Уникальный идентификатор организации в АС ДКО 
             ( SELECT nuReg FROM jrKO WHERE idKO = d.idKO ),                           ---- регистрационный номер КО 
             d.IsCompanyOwner, d.AlterComment 
      FROM deleted d INNER JOIN jrORG o ON d.idOrg = o.idOrg
      WHERE o.OrgStatus = 'T'
  END

  OPEN @Cursor
  FETCH NEXT FROM @Cursor
  INTO @IdRecord, @IdCompany, @BankRegNum, @IsCompanyOwner, @AlterComment

  WHILE @@FETCH_STATUS = 0 
  BEGIN
    INSERT INTO DKO_IdUpdate ( TypeDKO ) Values ( 'DKO201' )    --- Номер обновления
  
    INSERT INTO DKO_hrMutualParticipation
      ( _IdRecord, IdMutualParticipation, D_From, IdUpdate, StateSED, CorrectionType, IdCompany, CORegNum, IsCompanyOwner, AlterComment )
    SELECT 
      --- Уникальный номер записи в АС Сибирь
      @IdRecord, 
      --- Уникальный номер записи в АС ДКО 
      ISNULL( ( SELECT TOP 1 IdMutualParticipation FROM DKO_hrMutualParticipation WHERE _IdRecord = @IdRecord ), 0 ),  
      --- D_From - Дата, с которой действует данный набор значений атрибутов участия 
      ( SELECT CAST( dtWork AS SMALLDATETIME ) FROM rfStatus ), 
      --- dUpdate - Идентификатор обновления
      ( SELECT IDENT_CURRENT( 'DKO_IdUpdate' ) ), 
      --- StateSED - Состояние сообщения
      -3, 
      --- CorrectionType - Тип корректировки
      @CorrectionType, 
      --- IdCompany - Идентификатор компании
      @IdCompany, 
      --- CORegNum - Регистрационный номер КО 
      @BankRegNum, 
      ---  IsCompanyOwner - Атрибут определяет направление аффилированности
      @IsCompanyOwner, 
      --- --- AlterComment - Комментарий
      @AlterComment

    FETCH NEXT FROM @Cursor
    INTO @IdRecord, @IdCompany, @BankRegNum, @IsCompanyOwner, @AlterComment
  END

  CLOSE @Cursor
  DEALLOCATE @Cursor

  --- Если уникальный номер записи в АС ДКО не найден, присваеваем ему отрицательное значение 
  UPDATE DKO_hrMutualParticipation SET IdMutualParticipation = -IdRecord WHERE IdMutualParticipation = 0
GO

ALTER TABLE [dbo].[rfAffPers]
  ADD CONSTRAINT [FK_rfAffPers_idKO] FOREIGN KEY ([idKO]) REFERENCES [dbo].[jrKO] ([idKO]) ON DELETE CASCADE
GO

ALTER TABLE [dbo].[rfAffPers]
  ADD CONSTRAINT [FK_rfAffPers_idOrg] FOREIGN KEY ([idOrg]) REFERENCES [dbo].[jrORG] ([idOrg]) ON DELETE CASCADE
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Список взаимного участия', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор кредитной организации', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'idKO'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный идентификатор юридического лица', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'idOrg'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Доля взаимного участия', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'pcShare'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'направление взаимного участия
0 - организация участвует в капитале КО
1 - КО участвует в капитале организации', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'IsCompanyOwner'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата начала действия ', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'dtBegin'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата окончания действия', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'dtEnd'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор записи', 'SCHEMA', N'dbo', 'TABLE', N'rfAffPers', 'COLUMN', N'idRecord'
GO