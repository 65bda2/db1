﻿CREATE TABLE [dbo].[rfTpClassBuh] (
  [NmClass] [varchar](500) NULL,
  [TpClass] [int] NOT NULL,
  [idClass] [int] NOT NULL,
  CONSTRAINT [PK_rfTpClassBuh] PRIMARY KEY CLUSTERED ([idClass], [TpClass])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Перечень подгруп для периода отчетности', 'SCHEMA', N'dbo', 'TABLE', N'rfTpClassBuh'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование подгруппы', 'SCHEMA', N'dbo', 'TABLE', N'rfTpClassBuh', 'COLUMN', N'NmClass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор подгруппы', 'SCHEMA', N'dbo', 'TABLE', N'rfTpClassBuh', 'COLUMN', N'TpClass'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор класса', 'SCHEMA', N'dbo', 'TABLE', N'rfTpClassBuh', 'COLUMN', N'idClass'
GO