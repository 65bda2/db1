﻿CREATE TABLE [dbo].[bsEntityAttribute] (
  [idEntityAttribute] [int] IDENTITY,
  [idLookupEntity] [int] NULL,
  [idLookupAttribute] [int] NULL,
  [idLookupField] [int] NULL,
  [idEntity] [int] NOT NULL,
  [NameField] [varchar](24) NOT NULL,
  [Description] [varchar](255) NULL,
  [isVisible] [bit] NOT NULL CONSTRAINT [DF_bsEntityAttribute_isVisible] DEFAULT (1),
  [Width] [int] NOT NULL CONSTRAINT [DF_bsEntityAttribute_Width] DEFAULT (20),
  [isEdit] [bit] NOT NULL CONSTRAINT [DF_bsEntityAttribute_isEdit] DEFAULT (0),
  [GridOrder] [int] NULL,
  [WidthExcel] [int] NULL,
  [OrderExcel] [int] NULL,
  [isVisibleExcel] [bit] NULL,
  CONSTRAINT [PK_bsEntityAttribute] PRIMARY KEY CLUSTERED ([idEntityAttribute])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[bsEntityAttribute]
  ADD CONSTRAINT [FK_bsEntityAttribute_bsEntity] FOREIGN KEY ([idEntity]) REFERENCES [dbo].[bsEntity] ([idEntity])
GO

ALTER TABLE [dbo].[bsEntityAttribute]
  ADD CONSTRAINT [FK_bsEntityAttributeidLookupAttribute_bsEntityAttribute] FOREIGN KEY ([idLookupAttribute]) REFERENCES [dbo].[bsEntityAttribute] ([idEntityAttribute])
GO

ALTER TABLE [dbo].[bsEntityAttribute]
  ADD CONSTRAINT [FK_bsEntityAttributeidLookupEntity_bsEntity] FOREIGN KEY ([idLookupEntity]) REFERENCES [dbo].[bsEntity] ([idEntity])
GO

ALTER TABLE [dbo].[bsEntityAttribute]
  ADD CONSTRAINT [FK_bsEntityAttributeidLookupField_bsEntityAttribute] FOREIGN KEY ([idLookupField]) REFERENCES [dbo].[bsEntityAttribute] ([idEntityAttribute])
GO