﻿CREATE TABLE [dbo].[hub_jrBlock] (
  [idBlock] [int] NOT NULL,
  [nuBlock] [char](17) NULL,
  [nuMain] [char](17) NULL,
  [nuDepo] [char](20) NULL,
  [nmDepo] [char](250) NULL,
  [cdDepo] [char](12) NULL,
  [dtOut] [datetime] NULL,
  [dtLast] [datetime] NULL,
  [idAcc] [int] NOT NULL
)
ON [PRIMARY]
GO