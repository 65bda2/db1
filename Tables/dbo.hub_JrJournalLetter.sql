﻿CREATE TABLE [dbo].[hub_JrJournalLetter] (
  [dateIt] [datetime] NOT NULL,
  [SysDate] [datetime] NOT NULL,
  [Note] [varchar](500) NOT NULL,
  [idletter] [int] NOT NULL,
  [Kategory] [char](2) NOT NULL,
  [idJournal] [int] NOT NULL,
  [NumLetter] [varchar](500) NULL
)
ON [PRIMARY]
GO