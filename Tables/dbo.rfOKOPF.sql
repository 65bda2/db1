﻿CREATE TABLE [dbo].[rfOKOPF] (
  [CODE] [int] NOT NULL,
  [KOD] [char](5) NOT NULL,
  [NAME_RUS] [varchar](250) NOT NULL,
  CONSTRAINT [PK_rfOKOPF] PRIMARY KEY CLUSTERED ([CODE])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Справочник организационно-правовых форм', 'SCHEMA', N'dbo', 'TABLE', N'rfOKOPF'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Идентификатор ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKOPF', 'COLUMN', N'CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Код ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKOPF', 'COLUMN', N'KOD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Наименование ОКОПФ', 'SCHEMA', N'dbo', 'TABLE', N'rfOKOPF', 'COLUMN', N'NAME_RUS'
GO