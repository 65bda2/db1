﻿CREATE TABLE [dbo].[rfReports] (
  [dtReport] [datetime] NOT NULL,
  [nuReport] [int] NOT NULL,
  [nuIshod] [char](30) NULL,
  [tpReport] [char](1) NOT NULL,
  [tpGKD] [char](1) NULL
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [trLogRfReports]
ON [dbo].[rfReports]
FOR INSERT, UPDATE, DELETE
AS
 DECLARE @tpAction  CHAR(1)
 DECLARE @userName  CHAR(50)
 DECLARE @oldValues VARCHAR(500)
 DECLARE @newValues VARCHAR(500)

  SET CONCAT_NULL_YIELDS_NULL OFF

  SELECT @userName = nmRealUser FROM jrCurUsers WHERE nmSysUser = SYSTEM_USER AND spid = @@SPID
  IF @userName IS NULL SELECT @userName = SYSTEM_USER

  SELECT @oldValues = CONVERT(VARCHAR(10), dtReport, 104)+', '+CAST(nuReport AS VARCHAR(20))+', '+RTRIM(nuIshod)+', '+tpReport FROM deleted
  SELECT @newValues = CONVERT(VARCHAR(10), dtReport, 104)+', '+CAST(nuReport AS VARCHAR(20))+', '+RTRIM(nuIshod)+', '+tpReport FROM inserted

  IF (@oldValues IS NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'I'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NOT NULL) 
    SET @tpAction = 'U'
  ELSE  
  IF (@oldValues IS NOT NULL) AND (@newValues IS NULL) 
    SET @tpAction = 'D'
  ELSE  
    RETURN

  INSERT INTO lgControl (tpAction, nmUser, nmTable, nmFields, oldValues, newValues) 
   VALUES (@tpAction, @userName, 
   'Список отчетов в СЭД', 
   'дата отчета, уникальный номер, исходящий номер, тип отчета', @oldValues, @newValues)
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'номера отчетов в СЭД', 'SCHEMA', N'dbo', 'TABLE', N'rfReports'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Дата отчета', 'SCHEMA', N'dbo', 'TABLE', N'rfReports', 'COLUMN', N'dtReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Уникальный номер отчета', 'SCHEMA', N'dbo', 'TABLE', N'rfReports', 'COLUMN', N'nuReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Исходящий номер', 'SCHEMA', N'dbo', 'TABLE', N'rfReports', 'COLUMN', N'nuIshod'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип отчета', 'SCHEMA', N'dbo', 'TABLE', N'rfReports', 'COLUMN', N'tpReport'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тип ГКД/КД', 'SCHEMA', N'dbo', 'TABLE', N'rfReports', 'COLUMN', N'tpGKD'
GO