﻿CREATE TABLE [dbo].[hub_jrPaymentGrafic] (
  [NumActive] [int] NULL,
  [Payment] [money] NOT NULL,
  [DatePay] [datetime] NOT NULL,
  [Advice] [char](1) NOT NULL DEFAULT ('F'),
  [IdTransh] [int] NULL,
  [TpActive] [char](1) NOT NULL,
  [AlterComment] [char](255) NULL,
  [IdRecord] [bigint] NOT NULL
)
ON [PRIMARY]
GO