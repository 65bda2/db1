﻿CREATE TABLE [dbo].[hub_rfBuhFactor] (
  [idrecord] [int] NOT NULL,
  [OKVED] [char](8) NOT NULL,
  [REP_Date] [datetime] NOT NULL,
  [TpFactor] [char](2) NOT NULL,
  [PRIZ] [int] NOT NULL,
  [SRED] [decimal](22, 10) NOT NULL,
  [DOPUST] [decimal](22, 10) NOT NULL
)
ON [PRIMARY]
GO