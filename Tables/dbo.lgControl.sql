﻿CREATE TABLE [dbo].[lgControl] (
  [dtAction] [datetime] NOT NULL DEFAULT (getdate()),
  [tpAction] [char](1) NOT NULL,
  [nmUser] [varchar](50) NOT NULL,
  [nmTable] [varchar](100) NULL,
  [nmFields] [varchar](500) NULL,
  [oldValues] [varchar](500) NULL,
  [newValues] [varchar](500) NULL,
  CONSTRAINT [C_lgControl_tpAction] CHECK ([tpAction]='D' OR ([tpAction]='U' OR ([tpAction]='I' OR ([tpAction]='Q' OR ([tpAction]='E' OR [tpAction]='A')))))
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [CI_lgControl_dtAction]
  ON [dbo].[lgControl] ([dtAction])
  ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'протокол работы пользователя', 'SCHEMA', N'dbo', 'TABLE', N'lgControl'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'дата действия юзера', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'dtAction'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тип действия
A - архивация протокола
D - удаление
E - вход пользователя в программу
I - добавление,
Q - выход пользователя из программы
U - модификация,', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'tpAction'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'уполномоченный сотрудник', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'nmUser'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'имя таблицы', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'nmTable'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'имена изменяемых полей', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'nmFields'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'старые значения полей', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'oldValues'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'новые значения полей', 'SCHEMA', N'dbo', 'TABLE', N'lgControl', 'COLUMN', N'newValues'
GO