﻿CREATE TABLE [dbo].[hub_hrRateExch] (
  [dtRate] [datetime] NOT NULL,
  [nuISO] [char](3) NOT NULL,
  [nuScale] [int] NOT NULL,
  [nuRate] [decimal](20, 5) NOT NULL
)
ON [PRIMARY]
GO