﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE View [dbo].[vwListBuhReportSatisfiesTerms] AS
select 
	OBR.idOrgBuhReport
	,Rec777.ShowingAccSum
	,OBR.OKVED
	,rfOKVED.KOD
	,jrOrg.businessQuality
	,rfBuhRecord.idClass
	,jrOrg.idOrg
	,OBR.idPeriod
from jrOrgBuhReport OBR
	left outer join jrOrgBuhRecord Rec777 ON Rec777.idOrgBuhReport=obr.idOrgBuhReport and idBuhRecord=38
	left outer join rfOKVED ON rfOKVED.CODE=OBR.OKVED
	left outer join jrOrg ON jrOrg.idOrg = OBR.idOrg
	left outer join rfBuhRecord ON rfBuhRecord.idBuhRecord = Rec777.idBuhRecord
where 
	Rec777.ShowingAccSum=-1 --6
	AND OBR.isReferense = 1 --1
	AND OBR.IsBookKeepingOK IS NULL --2
	AND OBR.idPeriod in (SELECT idPeriod FROM fnGetActualBuhPeriod (DEFAULT, DEFAULT, DEFAULT))--3
-- OkvedInBuhFactor
	AND	( dbo.fnOkvedInBuhFactor(jrOrg.businessQuality, rfOKVED.KOD )= 1)--4
	and
	((select ISNULL(jrOrgBuhRecord.ShowingAccSum,0)
		from jrOrgBuhRecord 
			left join rfBuhRecord ON jrOrgBuhRecord.idBuhRecord=rfBuhRecord.idBuhRecord
		where Kod=2110
			and jrOrgBuhRecord.idOrgBuhReport=OBR.idOrgBuhReport
	)<>0
	and
	(select SUM(ISNULL(jrOrgBuhRecord.ShowingAccSum,0))
		from jrOrgBuhRecord 
			left join rfBuhRecord ON jrOrgBuhRecord.idBuhRecord=rfBuhRecord.idBuhRecord
		where Kod in (1410,1510,1520)
			and jrOrgBuhRecord.idOrgBuhReport=OBR.idOrgBuhReport
	)<>0
	and
	(select SUM(ISNULL(jrOrgBuhRecord.ShowingAccSum,0))
		from jrOrgBuhRecord 
			left join rfBuhRecord ON jrOrgBuhRecord.idBuhRecord=rfBuhRecord.idBuhRecord
		where Kod in (1510,1520)
			and jrOrgBuhRecord.idOrgBuhReport=OBR.idOrgBuhReport
	)<>0
	) --5
	
GO