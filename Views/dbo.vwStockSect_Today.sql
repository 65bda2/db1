﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwStockSect_Today]
AS
SELECT     hrStockSect.*, dbo.jrGKD.tpGKD
FROM         hrStockSect INNER JOIN
                      jrBlock ON hrStockSect.idBlock = jrBlock.idBlock INNER JOIN
                      jrAccount ON jrAccount.idAcc = jrBlock.idAcc INNER JOIN
                      jrGKD ON jrGKD.nuGKD = jrAccount.nuGKD
WHERE     (hrStockSect.dtSect =
                          (SELECT     dtWork
                            FROM          rfStatus))

GO