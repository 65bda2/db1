﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwExcludedActive]
AS
	SELECT  
		d.EDDate, --Дата формирования ЭС 
		e.CreatedDateTime, --Дата и время отправки ЭС
		d.EDNo, --Номер ЭС
		c.nuReg, --Рег. № КО
		c.nmKO, --Наименование КО
		d.UnNum, --Уникальный номер актива
		f.AssetStatus AS ActiveStatus, --Статус актива
		CASE f.AssetStatus
			WHEN 3 THEN 'Подлежит исключению (в залоге)'
			WHEN 4 THEN 'Исключен из пула обеспечения'
			ELSE NULL 
		END AS ActiveStatusText, --Текстовое обозначение значения реквизита AssetStatus
		c.idKo, --AHEBURG-449 (211) * 65baa2 01.08.2019
		d.Annotation
	FROM 
		EDOKO_hrES d 
		INNER JOIN jrKO c ON d.idKO=c.idko 
		INNER JOIN [ESOD_Files] e ON d.EDFileName=e.[FileName]
		INNER JOIN [EDOKO_XMLDetail] f ON d.idES = f.idES
	WHERE 
		d.EDType = 'C202'
		AND f.AssetStatus IN (3, 4)
GO