﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE 
view [dbo].[vwReportOKVED]
as
select jrOrg.idOrg, isnull(jrOrg.Rating,'нет') as Rating, jrOrg.CompanyName, isnull(rfOKVED.KOD,0) as OKVED
from jrORG
 left join rfOKVED ON rfOKVED.code = jrOrg.CodeOKVED
 --order by jrORG.CompanyName
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Отчет в разрезе ОКВЭД организации', 'SCHEMA', N'dbo', 'VIEW', N'vwReportOKVED'
GO