﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE View [dbo].[hrStockSect] as
SELECT hractiveSect.idAcc idBlock 
       ,dateassign dtSect
       ,rfActiveState.tpSect
       ,hractiveSect.NumActive idStock
       ,cast(null as int)cdStock
       ,cast(null as int)qnStock 
       ,cast(null as datetime)dtPrice 
       ,cast(null as datetime)dtCoeff 
       ,cast(null as datetime)dtCoeffNew
       ,cast(null as money) mnCost 
       ,cast(null as money) mnCostCoef
       ,cast(0 as smallint)tpActive
       ,ROW_NUMBER() over (partition by hractiveSect.idAcc  order by hractiveSect.idAcc) as idRecord
FROM hractiveSect inner join rfActiveState 
     on hractiveSect.idActiveState=rfActiveState.idActiveState

GO