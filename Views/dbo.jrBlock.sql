﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW [dbo].[jrBlock] as
SELECT DISTINCT has.idacc as idBlock
                ,cast(Null as char(17)) nuBlock
                ,cast(null as char(17)) nuMain 
                ,cast(null as char(20)) nuDepo
                ,cast(null as char(250)) nmDepo
                ,cast(null as char(12)) cdDepo
                ,cast(null as datetime) dtOut  
                ,ja.idAcc
                ,cast(CASE  
	                    WHEN ja.dtExcl IS NULL THEN ja.dtIncl ELSE ja.dtExcl 
                        END as datetime)AS dtLast
FROM jrAccount ja inner join hrActiveSect has on has.idAcc=ja.idAcc
GO