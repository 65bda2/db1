﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vwNonstructInf]
AS
	SELECT hrES.EDDate, hrES.EDNo, ko.nuReg, ko.nmKO, xd.ObligedRegID as EgrulORG, dbo.fnGetEDOKOActiveListUnNum(hrES.idES) AS UnNum, hrES.EDDescription, hrES.EDFileName, hrES.DocFileName, hrES.EDType, hrES.idES, hrES.PathTOFile
	,ko.idKO --AHEBURG-449 (211) * 65baa2 01.08.2019
	FROM dbo.EDOKO_hrES AS hrES INNER JOIN dbo.jrKO AS ko ON hrES.idKO = ko.idKO LEFT JOIN dbo.EDOKO_XMLDetail xd ON xd.idES = hrES.idES
	WHERE (hrES.EDType = 'C106' and isNull((SELECT idES from EDOKO_hrES as Answer WHERE hrES.idES = Answer.idHeadES AND Answer.EDType = 'C208' AND Answer.ResultCode = 1), 0) <> 0) 
	   OR (hrES.EDType = 'C206')
GO