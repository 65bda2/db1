﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
  
CREATE view [dbo].[vwEDOOrganizationHistory]
as
select o.DFrom, o.OperationDate
,o.CompanyName, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.CompanyName, pr.CompanyName) end as Change_CompanyName
,o.INN, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.INN, pr.INN) end as Change_INN
,o.EGRUL, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.EGRUL, pr.EGRUL) end as Change_EGRUL
,o.IsResident, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsResident, pr.IsResident) end as Change_nmIsResident
,case o.IsResident when 1 then 'Является' when 0 then 'Не является' end as nmIsResident
,o.KodOKVED, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.KodOKVED, pr.KodOKVED) end as Change_KodOKVED
,o.KODOKOPF, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.KODOKOPF, pr.KODOKOPF) end as Change_KODOKOPF
,o.KODOKFS, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.KODOKFS, pr.KODOKFS) end as Change_KODOKFS
,o.IsSubject, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsSubject, pr.IsSubject) end as Change_nmSubject
,case o.isSubject
   when 0 then 'обычная организация'
   when 1 then 'РФ'
   when 2 then 'субъект РФ'
   when 3 then 'муниципальное образование'
   when 4 then 'Агенство кредитных гарантий'
 end as NmSubject
,o.tpDopInfo, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.tpDopInfo, pr.tpDopInfo) end as Change_tpDopInfo
,o.IsConforming, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsConforming, pr.IsConforming) end as Change_nmConforming
,case o.IsConforming when 0 then 'не соответствует' when 1 then 'соответствует' end as nmConforming
,o.IsInList, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsInList, pr.IsInList) end as Change_nmIsInList
,case o.IsInList when 0 then 'Не включена' when 1 then 'Включена' end as NMIsInList
,o.TpDtReg, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.TpDtReg, pr.TpDtReg) end as Change_nmTpDtReg
,case o.TpDtReg
   when 1 then 'Дата государственной регистрации'
   when 2 then 'Дата реорганизации (в форме, отличной от преобразования или присоединения)'
   when 3 then 'Дата государственной регистрации в соответствии с федеральным законом, и (или) нормативным правовым актом Президента РФ, и (или) Правительства РФ'
   when 4 then 'Дата реорганизации (в форме, отличной от преобразования или присоединения) в соответствии с федеральным законом, и (или) нормативным правовым актом Президента РФ , и (или) Правительства РФ'
 end as NmTpDtReg
,o.BusinessQuality, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.BusinessQuality, pr.BusinessQuality) end as Change_nmBusinessQuality
,case o.businessQuality
   when 1 then 'субъект малого предпринимательства'
   when 2 then 'субъект крупного или среднего предпринимательства'
 end as NmbusinessQuality
,o.D_lastRegDate, case when pr.idOrg is not null then dbo.fnIsDistinctDateTime(o.D_lastRegDate, pr.D_lastRegDate) end as Change_D_lastRegDate
,o.IsBookKeepingOK, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsBookKeepingOK, pr.IsBookKeepingOK) end as Change_nmBookKeepingOK
,case o.IsBookKeepingOK when 0 then 'Не соответствует' when 1 then 'Соответствует' else '' end as nmBookKeepingOK
,o.IsFederalLaw, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.IsFederalLaw, pr.IsFederalLaw) end as Change_nmIsFederalLaw
,o.AlterComment, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.AlterComment, pr.AlterComment) end as Change_AlterComment
,o.isSootv, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.isSootv, pr.isSootv) end as Change_nmisSootv
,o.ShortNmORG, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.ShortNmORG, pr.ShortNmORG) end as Change_ShortNmORG
,o.dtBookKeeping, case when pr.idOrg is not null then dbo.fnIsDistinctDateTime(o.dtBookKeeping, pr.dtBookKeeping) end as Change_dtBookKeeping
,o.AnotherInfNote, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.AnotherInfNote, pr.AnotherInfNote) end as Change_AnotherInfNote
,o.TpCheck, case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.TpCheck, pr.TpCheck) end as Change_TpCheck
,o.AnotherInf, case when pr.idOrg is not null then dbo.fnIsDistinctInteger(o.AnotherInf, pr.AnotherInf) end as Change_nmAnotherInf
,case o.AnotherInf
   when 0 then 'нет информации о наличии рисков'
   when 1 then 'информация есть и она не соответствует критериям'
   when 2 then 'информация есть и она соответствует критериям'
 end as NMAnotherInf
,o.idOrg, o.idhrOrg,
case o.OrgStatus
   when 'R' then 'находится на рассмотрении'
   when 'T' then 'принято положительное решение'
   when 'F' then 'принято отрицательное решение'
end as OrgStatus,
case when pr.idOrg is not null then dbo.fnIsDistinctVarchar(o.OrgStatus, pr.OrgStatus) end as Change_OrgStatus
from hrOrg o
  left join hrOrg pr on pr.idhrOrg = (select top 1 idhrOrg from hrOrg where idOrg = o.idOrg and idhrOrg < o.idhrOrg order by idhrOrg desc)

GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'История изменения параметров организации', 'SCHEMA', N'dbo', 'VIEW', N'vwEDOOrganizationHistory'
GO