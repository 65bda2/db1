﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE 
view [dbo].[vwSubdivision]
as
select s.idSubdivision, s.KPTU, d.NAME_RUS, s.KPHub, (select NAME_RUS from rfDKKP dk where dk.KP=s.KPHub) as NAME_HUB, s.EDAuthor
from rfSubdivision s
left join rfDKKP d ON s.KPTU = d.KP

GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Перечень подразделений Банка России', 'SCHEMA', N'dbo', 'VIEW', N'vwSubdivision'
GO