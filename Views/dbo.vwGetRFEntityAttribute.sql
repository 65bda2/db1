﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vwGetRFEntityAttribute]
AS
SELECT     dbo.bsEntity.[idEntity], 
		   dbo.bsEntity.TypeEntity, 
		   dbo.bsEntity.NameEntity, 
		   dbo.bsEntity.TableEntity, 
		   dbo.bsEntityAttribute.idEntityAttribute, 
		   dbo.bsEntityAttribute.idLookupEntity, 
		   dbo.bsEntityAttribute.idLookupAttribute, 
		   dbo.bsEntityAttribute.idLookupField, 
		   dbo.bsEntityAttribute.NameField,
		   dbo.bsEntityAttribute.Description, 
		   dbo.bsEntityAttribute.isVisible, 
		   dbo.bsEntityAttribute.Width, 
		   dbo.bsEntityAttribute.isEdit, 
		   dbo.bsEntityAttribute.GridOrder, 
		   dbo.bsEntityAttribute.WidthExcel, 
		   dbo.bsEntityAttribute.OrderExcel, 
		   dbo.bsEntityAttribute.isVisibleExcel
FROM   dbo.bsEntity INNER JOIN
	   dbo.bsEntityAttribute 
	   ON dbo.bsEntity.[idEntity] = dbo.bsEntityAttribute.[idEntity];
GO