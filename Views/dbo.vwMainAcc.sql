﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE View [dbo].[vwMainAcc] AS
SELECT 
	typeGKD = CASE GKD.tpGKD 
        WHEN 'B' THEN 'ЕКД (активы)' 
        WHEN 'D' THEN 'КД на МСП' 
        WHEN 'F' THEN 'КД на ЭКСАР' 
        WHEN 'G' THEN 'КД на Инвестпроекты'
        WHEN 'K' THEN CASE acc.tpAsset WHEN 'B' THEN 'ЕКД (активы)' ELSE 'ЕКД (ценные бумаги)' END  
    END 
	,ISNULL((SELECT top 1 jrA.status 
			FROM jrActive jrA
			WHERE jrA.status IN ('T', 'N') AND jrA.NumActive in (SELECT hrAs.NumActive 
																FROM hrActiveSect hrAs
																WHERE hrAs.idAcc = acc.idAcc AND hrAs.DateAssign = (SELECT dtWork 
																								FROM rfStatus) )
			), '0') as ActiveStatus
	,tpGKDReal = GKD.tpGKD 
	,tpGKD = CASE GKD.tpGKD WHEN 'K' THEN acc.tpAsset ELSE GKD.tpGKD END
	,acc.tpAsset
	,acc.tpInstr
	,GKD.nuGKD
	,numGKD = RTRIM(GKD.numGKD)
	,GKD.dtGKD
	,idKOGKD = gkd.idKO
	,nmKOGKD = GKD.nmKO
	,isSootvGKD = GKD.isSootv
	,GKD.dtGKDStop
	,GKD.dtGKDUved
	,GKD.dtGKDEnd
	,acc.idKO
	,acc.idAcc
	,acc.nuAccount
	,acc.tpCredit
	,acc.mnLimit
	,acc.tpRazdel
	,acc.isSootv
	,acc.dtIncl
	,acc.dtExcl
	,acc.nmKO
	,nuReg = RTRIM(acc.nuReg)
	,nuRegGKD = RTRIM(GKD.nuReg)
	,acc.nuBIK
	,acc.nuINN
	,isDtOut = 0
	,ustanLimit = CAST(NULL AS VARCHAR(250))
	,statusLimit = '-'
	,isStockIn1_3or1_4 = 0
	,leftNuReg = dbo.LeftNuRegInt(acc.nuReg)
	,rightNuReg = dbo.RightNuRegInt(acc.nuReg)
FROM jrAccount acc 
	INNER JOIN jrGKD GKD ON (acc.nuGKD = GKD.nuGKD) 
		AND GKD.tpGKD IN ('B','D','F','G','K')											-- b.1
		AND (GKD.dtGKDEnd IS NULL OR GKD.dtGKDEnd >= (SELECT dtWork FROM rfStatus))		-- b.2
		AND GKD.dtGKD<=(SELECT dtWork FROM rfStatus)									-- b.2
WHERE acc.dtIncl <= (SELECT dtWork FROM rfStatus)										-- a.1
       AND (acc.dtExcl IS NULL OR acc.dtExcl >= (SELECT dtWork FROM rfStatus))			-- a.2


GO