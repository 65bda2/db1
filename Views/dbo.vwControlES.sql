﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- 20200323_AHEBURG_3331.sql
-- 65fev
CREATE VIEW [dbo].[vwControlES]
AS
/*
2020-03-26 65fev
  AHEBURG-3331
  1. убрал лишние поля из AHEBURG-2900
  2. ResultCode и ProcessResult для v.EDType in ('C101','C204','C205','C201','C204','C205')
     чтобы фильтр соответствовал колонке
ResultCode	ProcessResult
+	        Принято
-	        Отказано
0	        "  "     
NULL	    NULL

2020-03-03 65eav2
  AHEBURG-3248 (20200218_AHEBURG_2900.sql)
  TrancheNumber только для C101 

2020-02-18 65ShitovAV
  AHEBURG-2900
  Добавил некоторые поля, используемые в контроле ЭС.

2019-12-19 65MakarovDA
  AHEBURG-2306  
  добавлен TrancheNumber

2019-12-15 65fev
  AHEBURG-2420
  потенциально возможная выборка нескольких записей в подселекте при вычислении TehControlES

2019-12-12 65fev
  AHEBURG-2203:
    1. c204.DateControl = EDDate
    2. c105.ResultCode = 1 | 2
  AHEBURG-2438
    3. c104.IsDoerGreen = NULL
*/
-- 65fev aheburg-3331 start
select v.*
,case v.ResultCode
   when '+' then 'Принято'
   when '-' then 'Отказано'
   when '0' then '"  "'
 end as ProcessResult
from (
-- 65fev aheburg-3331 finito
SELECT  hrES.idES,hrES.idHeadES, hrES.EDDate, rfEDT.Direction,
	(CASE rfEDT.Direction WHEN 1 THEN (SELECT convert(datetime,convert(datetime2(0),max(ef.CreatedDateTime),0),104) -- AHEBURG-1856 65fev CONVERT(DATETIME,MAX(ef.[CreatedDateTime]),104)
									   FROM [EDOKO_hrES] eh INNER JOIN [ESOD_Files] ef ON eh.idES=ef.[idES] AND eh.EDType='C208'
									   WHERE eh.idHeadES = hrES.idES)
						  WHEN 2 THEN (SELECT convert(datetime,convert(datetime2(0),max(ef.CreatedDateTime),0),104) -- AHEBURG-1856 65fev CONVERT(DATETIME,MAX(ef.[CreatedDateTime]),104)
									   FROM [ESOD_Files] ef
									   WHERE ef.[idES] = hrES.idES)
					      END) AS DateTimeES,
	hrES.EDType, hrES.EDFileName,
	(CASE rfEDT.Direction WHEN 1 THEN hrES.NuRegKO
						  WHEN 2 THEN jk.nuReg END) AS nuRegKO,
	(CASE rfEDT.Direction WHEN 1 THEN hrES.BankShortName
						  WHEN 2 THEN jk.nnKO END) AS nnKO,
	(CASE hrES.EDType WHEN 'C101' THEN hrES.UnNum
					  WHEN 'C102' THEN hrES.UnNum
					  WHEN 'C106' THEN dbo.fnGetEDOKOActiveListUnNum(hrES.idES)
					  WHEN 'C200' THEN dbo.fnGetEDOKOActiveListUnNum(hrES.idES)
					  WHEN 'C201' THEN hrES.UnNum
					  WHEN 'C202' THEN hrES.UnNum
					  WHEN 'C206' THEN dbo.fnGetEDOKOActiveListUnNum(hrES.idES)
					  ELSE NULL END) AS unNum,
	(CASE hrES.EDType WHEN 'C101' THEN ISNULL(ex.ShortNameDebtor+ISNULL(' ('+ex.DebtorRegID+')',''),ISNULL(ex.DebtorRegID,'')) -- AHEBURG-1332*65baa2  
					  ELSE NULL END) AS naimDebtor,
	(CASE hrES.EDType WHEN 'C101' THEN ISNULL(ex.ShortNameObliged+ISNULL(' ('+ex.ObligedRegID+')',''),ISNULL(ex.ObligedRegID,'')) -- AHEBURG-1332*65baa2   
					  WHEN 'C104' THEN ISNULL(ex.ShortNameObliged+ISNULL(' ('+ex.ObligedRegID+')',''),ISNULL(ex.ObligedRegID,'')) -- AHEBURG-1332*65baa2 
					  /*(CASE ex.ShortNameObliged WHEN NULL THEN ex.ObligedRegID
																		   ELSE (CASE ex.ObligedRegID WHEN NULL THEN ex.ShortNameObliged
																												ELSE ex.ShortNameObliged+' ('+ex.ObligedRegID+')' END) END)*/
					  WHEN 'C105' THEN ex.ObligedRegID
					  WHEN 'C106' THEN ex.ObligedRegID
					  WHEN 'C204' THEN ISNULL(ex.ShortNameObliged+ISNULL(' ('+ex.ObligedRegID+')',''),ISNULL(ex.ObligedRegID,'')) --AHEBURG-1746, 65ShitovAV, Comment string below and replace it
					  /*(CASE ex.ShortNameObliged WHEN NULL THEN ex.ObligedRegID
																		   ELSE (CASE ex.ObligedRegID WHEN NULL THEN ex.ShortNameObliged
																												ELSE ex.ShortNameObliged+' ('+ex.ObligedRegID+')' END) END)*/
					  WHEN 'C205' THEN ex.ObligedRegID
					  WHEN 'C206' THEN ex.ObligedRegID
					  ELSE NULL END) AS naimObliged,
	(CASE hrES.EDType WHEN 'C101' THEN ex.BillNum
					  WHEN 'C202' THEN ex.BillNum
					  ELSE NULL END) AS numKD,
	(CASE hrES.EDType WHEN 'C101' THEN CONVERT(varchar(10),ex.BillDate,104)
					  WHEN 'C202' THEN CONVERT(varchar(10),ex.BillDate,104)
					  ELSE NULL END) AS DateKD, hrES.ChangeView,
	(CASE hrES.EDType WHEN 'C101' THEN (CASE WHEN (SELECT COUNT(*) FROM [EDOKO_jrChangeORG] jco WHERE jco.idES=hrES.idES) > 0 THEN 'Карантин'
											 WHEN (SELECT COUNT(*) FROM [jrNewORG] jno WHERE jno.idES=hrES.idES) > 0 THEN 'Новая организация'
											 WHEN (SELECT COUNT(*) FROM [EDOKO_jrChangeORG] jco WHERE jco.idES=hrES.idES) > 0 AND
											      (SELECT COUNT(*) FROM [jrNewORG] jno WHERE jno.idES=hrES.idES) > 0 THEN 'Карантин; Новая организация' END) ELSE NULL END) AS Kar_NewOrg,
/* 65fev aheburg-3331
	(CASE hrES.EDType WHEN 'C201' THEN (CASE hrES.ResultCode WHEN 1 THEN 'Принято' WHEN 2 THEN 'Отказано' END)
					  WHEN 'C204' THEN (CASE hrES.ResultCode WHEN 1 THEN 'Принято' WHEN 2 THEN 'Отказано' END)
					  WHEN 'C205' THEN (CASE WHEN hrES.ResultCode=1 THEN 'Принято' WHEN hrES.ResultCode in (2,3) THEN 'Отказано' END) --Aheburg-1479					  
					  ELSE NULL END) AS ProcessResult,
*/					  
	(CASE rfEDT.Direction WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM EDOKO_hrES h1 WHERE h1.EDType = 'C208' AND h1.idHeadES = hrES.idES AND h1.ResultCode=1)>0 THEN  --afi ResultCode берем из  С208
									  (CASE WHEN hrES.EDType in ('C101','C102','C104') THEN hrES.Doer END) ELSE NULL END)
						  WHEN 2 THEN hrES.Doer END) AS Doer,
	(CASE rfEDT.Direction WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM EDOKO_hrES h1 WHERE h1.EDType = 'C208' AND h1.idHeadES = hrES.idES AND h1.ResultCode=1)>0 THEN  --afi ResultCode берем из  С208
-- 65fev AHEBURG-2438									  (CASE WHEN hrES.EDType in ('C101','C104') THEN (CASE ISNULL(hrES.Doer,'') WHEN '' THEN 1 ELSE 0 END)
									  (CASE WHEN hrES.EDType in ('C101') THEN (CASE ISNULL(hrES.Doer,'') WHEN '' THEN 1 ELSE 0 END)
											WHEN hrES.EDType in ('C100','C102','C105','C106') THEN 0 END)
											ELSE 0 END)
						  WHEN 2 THEN 0 END) AS IsDoerGreen,
	(SELECT MAX(t1.EDDate) FROM [EDOKO_hrES] t1 WHERE t1.EDType=rfEDT.EDTypeAnswer AND t1.idHeadES=hrES.idES) AS DateAnswer,
	
	(CASE rfEDT.Direction WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM EDOKO_hrES h1 WHERE h1.EDType = 'C208' AND h1.idHeadES = hrES.idES AND h1.ResultCode=1)>0 THEN --afi ResultCode берем из  С208
		(CASE hrES.EDType WHEN 'C101' THEN (CASE (SELECT [Status] FROM [jrActive] WHERE UnNum = hrES.UnNum) WHEN 'W'
										THEN dbo.IncWorkDay((SELECT [Begindate] FROM [jrActive] WHERE UnNum = hrES.UnNum), 7)
										ELSE (CASE WHEN (SELECT COUNT(*) FROM [hrWaitCheck] WHERE dtBegin IS NOT NULL AND NumActive = (SELECT NumActive FROM [jrActive] WHERE UnNum = hrES.UnNum))>0
----- 65fev 1.begin 20190920-AHEBURG-1295 CONVERT(DATE, -> CONVERT(DATETIME,
											  THEN dbo.IncWorkDay((SELECT CONVERT(DATE,MAX(dtEnd),104) FROM [hrWaitCheck] WHERE dtBegin IS NOT NULL AND NumActive = (SELECT NumActive FROM [jrActive] WHERE UnNum = hrES.UnNum)),rfEDT.DayControl)
											  ELSE dbo.IncWorkDay((SELECT l.DateLetterTU FROM jrActive a join jrLetter l ON a.UnNum = hrES.UnNum and a.idLetter = l.idLetter), rfEDT.DayControl) END) END)
----- 65fev 1.end 20190920-AHEBURG-1295 
					  WHEN 'C102' THEN dbo.IncWorkDay(hrES.ChangeDate,rfEDT.DayControl)
----- 65fev 2.begin 20190920-AHEBURG-1295				  WHEN 'C104' THEN dbo.IncWorkDay((SELECT CONVERT(DATE,MAX(ef.[CreatedDateTime]),104) FROM [EDOKO_hrES] eh INNER JOIN [ESOD_Files] ef ON eh.idES=ef.[idES] AND eh.EDType='C208' WHERE eh.idHeadES = hrES.idES),rfEDT.DayControl)
					  WHEN 'C104' THEN 
/* 65fev AHEBURG-2203
					    CASE 
							WHEN (SELECT COUNT(*) FROM EDOKO_hrES h1 WHERE h1.EDType = 'C208' AND h1.idHeadES = hrES.idES AND h1.ResultCode=2)>0 THEN NULL
							ELSE (SELECT CONVERT(DATE,MAX(ef.[CreatedDateTime]),104) FROM [EDOKO_hrES] eh INNER JOIN [ESOD_Files] ef ON eh.idES=ef.[idES] AND eh.EDType='C208' WHERE eh.idHeadES = hrES.idES)
							ELSE (SELECT EDDate FROM [EDOKO_hrES] eh WHERE eh.EDType='C208' WHERE eh.idHeadES = hrES.idES)
						END

*/						(SELECT TOP 1 EDDate FROM EDOKO_hrES c208 WHERE c208.EDType = 'C208' and c208.idHeadES = hrES.idES and c208.ResultCode = 1) --- 65fev AHEBURG-2203
----- 65fev 2.end 20190920-AHEBURG-1295 
					  ELSE NULL END) END) END) AS DateControl,
	(CASE (SELECT max(ResultCode) FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES AND EDCreateDateTime = -- 65fev AHEBURG-2420 (CASE (SELECT ResultCode FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES AND EDCreateDateTime =
		  (SELECT MAX(EDCreateDateTime) FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES)) WHEN 1 THEN '+'
																										 WHEN 2 THEN '-' END) AS TehControlES,
	ef.FileName as  headEDFileName, rfEDT.EDTypeAnswer,
	(CASE hrES.EDType WHEN 'C101' THEN (CASE (SELECT COUNT(*)
											  FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
											  WHERE h2.EDType = 'C201' AND h1.idES = hrES.idES)
										WHEN 1 THEN (CASE (SELECT h2.ResultCode
														   FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 on h1.idES = h2.idHeadES
														   WHERE h1.EDType = 'C101' AND h2.EDType='C201' AND h1.idES = hrES.idES) WHEN 1 THEN '+' WHEN 2 THEN '-' END)
										ELSE (CASE (SELECT COUNT(*)
													FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
													WHERE h2.EDType = 'C208' AND h1.idES = hrES.idES)
											  WHEN 1 THEN (CASE (SELECT ResultCode FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES) WHEN 1 THEN '0' ELSE NULL END) END) END)
					  WHEN 'C104' THEN (CASE (SELECT COUNT(*)
											  FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
											  WHERE h2.EDType = 'C204' AND h1.idES = hrES.idES)
										WHEN 1 THEN (CASE (SELECT h2.ResultCode
														   FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 on h1.idES = h2.idHeadES
														   WHERE h1.EDType = 'C104' AND h2.EDType='C204' AND h1.idES = hrES.idES) WHEN 1 THEN '+' WHEN 2 THEN '-' END)
										ELSE (CASE (SELECT COUNT(*)
													FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
													WHERE h2.EDType = 'C208' AND h1.idES = hrES.idES)
											  WHEN 1 THEN  (CASE (SELECT ResultCode FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES) WHEN 1 THEN '0' ELSE NULL END) END) END)
					  WHEN 'C105' THEN (CASE (SELECT COUNT(*)
											  FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
											  WHERE h2.EDType = 'C205' AND h1.idES = hrES.idES)
										WHEN 1 THEN (CASE (SELECT h2.ResultCode
														   FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 on h1.idES = h2.idHeadES
														   WHERE h1.EDType = 'C105' AND h2.EDType='C205' AND h1.idES = hrES.idES) WHEN 1 THEN '+' WHEN 2 THEN '-' END) -- 65fev AHEBURG-2203 WHEN 3 THEN '+' WHEN 4 THEN '-' END)
										ELSE (CASE (SELECT COUNT(*)
													FROM EDOKO_hrES h1 INNER JOIN EDOKO_hrES h2 ON h1.idES = h2.idHeadES AND h1.idES <> h2.idES
													WHERE h2.EDType = 'C208' AND h1.idES = hrES.idES)
											  WHEN 1 THEN (CASE (SELECT ResultCode FROM [EDOKO_hrES] WHERE EDType='C208' AND idHeadES = hrES.idES) WHEN 1 THEN '0' ELSE NULL END) END) END)
-- 65fev aheburg-3331 start
	                  WHEN 'C201' THEN (CASE hrES.ResultCode WHEN 1 THEN '+' WHEN 2 THEN '-' END)
					  WHEN 'C204' THEN (CASE hrES.ResultCode WHEN 1 THEN '+' WHEN 2 THEN '-' END)
					  WHEN 'C205' THEN (CASE hrES.ResultCode WHEN 1 THEN '+' WHEN 2 THEN '-' WHEN 3 THEN '-' END)					  
-- 65fev aheburg-3331 finito
					  ELSE NULL END) AS ResultCode,
					  --AHEBURG-2306 65MakarovDA 
					--AHEBURG-3248 65eav2 begin
					--(SELECT TrancheNumber FROM [jrActive] WHERE UnNum = hrES.UnNum)as TrancheNumber 
					(SELECT TrancheNumber FROM [jrActive] WHERE UnNum = hrES.UnNum AND hrES.EDType = 'C101' )as TrancheNumber /* 65fev aheburg-3331,
					--AHEBURG-3248 65eav2 end 
					-- AHEBURG-2900; 65ShitovAV
					(SELECT Status FROM [jrActive] WHERE UnNum = hrES.UnNum)as ActiveStatus,
					(SELECT NumActive FROM [jrActive] WHERE UnNum = hrES.UnNum)as NumActive,
					(SELECT idLetter FROM [jrActive] WHERE UnNum = hrES.UnNum)as idLetter,
					(SELECT ResultCode from EDOKO_hrES edhr where edhr.idHeadES = hrES.idES and edhr.EDType = 'C208') as c208_ResultCode,
					(SELECT edhr.EDDate FROM EDOKO_hrES edhr where edhr.idHeadES = hrES.idES and edhr.EDType = 'C208') as c208_EDDate,
					hres.ChangeDate as esChangeDate,
					(SELECT MAX(jrpg.DatePay) FROM jrPaymentGrafic jrpg where jrpg.NumActive = NumActive and Advice = 'T') as MaxPaydDate,
					(SELECT jra.MaturityDate FROM jrActive jra WHERE UnNum = hrES.UnNum) as ActiveMaturityDate */
					 

FROM [EDOKO_hrES] hrES INNER JOIN
	 [EDOKO_rfEDType] rfEDT ON hrES.EDType=rfEDT.EDType INNER JOIN
	 [jrKO] jk ON hrES.idKO=jk.idKO LEFT OUTER JOIN
	 [ESOD_FILES] ef ON hrES.idHeadES=ef.idES LEFT OUTER JOIN
	 [EDOKO_XMLDetail] ex ON hrES.idES=ex.idES 

WHERE
	hrES.EDType NOT IN ('C199', 'C299', 'C208')
) v -- 65fev aheburg-3331
GO