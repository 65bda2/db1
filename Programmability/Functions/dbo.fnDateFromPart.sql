﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnDateFromPart]
--********************
-- Функция fnDateFromPart()
-- =============================================
-- Author:<65baa2>
-- Create date: <31.05.2019>
-- Description:	<Функция возвращает дату построенную из частей @D-день, @M-месяц, @Y-год.>
-- =============================================
(
	@D int, -- день
	@M int, -- мес¤ц
	@Y int  -- год:
)
RETURNS datetime
AS
BEGIN
   RETURN convert(datetime,rtrim(str(@D))+'.'+rtrim(str(@M))+'.'+rtrim(str(@Y)),104); 
END
GO