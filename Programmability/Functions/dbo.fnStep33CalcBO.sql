﻿SET QUOTED_IDENTIFIER OFF

SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 24.08.2020
-- Description:	Функция для определения соответствия организации критериям БР
/* http://confluence.dev.ppod.cbr.ru/pages/viewpage.action?pageId=32324839&focusedCommentId=32833758#comment-32833758
33.1 Определить значение признака jrOrgBuhReport.IsBookKeepingOK для БО из последнего (максимального) актуального периода (период с максимальным jrOrgBuhReport.idPeriod) и значения показателя 771 (Рентабельность продаж) для каждого актуального периода:
Если jrOrgBuhReport.IsBookKeepingOK = 1 для БО из последнего (максимального) актуального периода и показатель 771 (Рентабельность продаж) >= 0 для каждого актуального периода , тогда требуется перейти к пункту 33.1.1.
Если jrOrgBuhReport.IsBookKeepingOK = 0 для БО из последнего (максимального) актуального периода и показатель 771 (Рентабельность продаж) < 0 для каждого актуального периода (или хотя бы для одного) , тогда требуется перейти к пункту 33.2.
Если jrOrgBuhReport.IsBookKeepingOK = 0 для БО из последнего (максимального) актуального периода и показатель 771 (Рентабельность продаж) >= 0 , тогда требуется перейти к пункту 33.2.
Если jrOrgBuhReport.IsBookKeepingOK = 1 для БО из последнего (максимального) актуального периода и показатель 771 (Рентабельность продаж) < 0 для каждого актуального периода (или хотя бы для одного), тогда требуется перейти к пункту 33.2.
(RETURN = True) => к пункту 33.1.1, (RETURN = False) => к пункту 33.2 */
-- =============================================
CREATE FUNCTION [dbo].[fnStep33CalcBO](@idOrg int, @idKO int)
RETURNS BIT
AS
BEGIN
	DECLARE @R bit = 0
	DECLARE @idBuhRecord int = (SELECT idBuhRecord FROM rfBuhRecord WHERE NumKod = '771')
	DECLARE @B bit = 1
	IF EXISTS(SELECT 1 FROM rfStatus WHERE DATEPART(m,dtWork) = 4 AND DATEPART(d,dtWork) <= 3) AND
	   NOT EXISTS(SELECT idPeriod FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod=1)
		SET @B = 0

	SET @R = CASE WHEN EXISTS(SELECT idOrgBuhReport 
							  FROM jrOrgBuhReport WITH (NOLOCK) 
							  WHERE idOrgBuhReport = (SELECT max(idOrgBuhReport) 
													  FROM jrOrgBuhReport WITH (NOLOCK) 
													  WHERE idPeriod IN (SELECT idPeriod FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod=@B) 
													  AND isArchive=0 AND idOrg=@idOrg) 
							  AND isBookKeepingOK=1)
			 THEN CAST(1 as bit) ELSE CAST(0 as bit) END 

	IF @R = 1 
	SET @R = CASE WHEN (SELECT count(*) FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod =@B) =
					   (SELECT count(*) FROM jrOrgBuhRecord WITH (NOLOCK) 
						WHERE idOrgBuhReport in (SELECT idOrgBuhReport 
												 FROM jrOrgBuhReport WITH (NOLOCK) 
												 WHERE idPeriod IN (SELECT idPeriod FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod =@B)
												 AND isArchive=0 AND idOrg=@idOrg) 
						AND idBuhRecord = @idBuhRecord AND ShowingAccSum >= 0)
			 THEN CAST(1 as bit) ELSE CAST(0 as bit) END 

	RETURN @R	
END
GO