﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Editor:		Бряков Д.А.
-- Edit date:   08.10.2019, 11.11.2019
-- Description:	Возвращает список актуальных периодов (обязательных, необязательных, дополнительных) на дату, для КО по Орг
--              Варианты запуска:
--              SELECT * FROM [BO].[dbo].[fnGetActualBuhPeriod] (562, 5, DEFAULT) - Список актуальных периодов для КО по организации на ОД
--				SELECT * FROM [BO].[dbo].[fnGetActualBuhPeriod] (DEFAULT, DEFAULT, DEFAULT) - Список акуальных периодов на ОД
--				SELECT * FROM [BO].[dbo].[fnGetActualBuhPeriod] (DEFAULT, DEFAULT, '20191108') - Список акуальных периодов на дату 08.11.2019
-- Editor:		Макаров Д.А.
-- Edit date:   03.09.2020
-- Description:	Добавлено поле StartDate - Дата начала отчетного периода (AHEBURG-4806)
-- =============================================
CREATE FUNCTION [dbo].[fnGetActualBuhPeriod](@idOrg int = -1, @idKO int = -1, @dt datetime = NULL)
RETURNS @tab TABLE(
	[idPeriod] [int] NOT NULL,
	[NmPeriod] [varchar](255) NOT NULL,
	[idclass] [int] NULL,
	[NumPeriod] [int] NOT NULL,
	[YearPeriod] [int] NOT NULL,
	[dtShow] [datetime] NOT NULL,
	[isBuhcheck] [int] NULL,
	[ReportDate] [datetime] NOT NULL,
	[StartDate] [datetime] NOT NULL, -- Дата начала отчетного периода AHEBURG-4806
	[obPeriod] [bit] NULL,  -- обязательный период
	[dopPeriod] [bit] NULL) -- дополнительный период (1,2,3 апреля)
AS
BEGIN
	DECLARE @wd DATETIME
	SET @wd = ISNULL(@dt,(SELECT TOP 1 dtWork FROM rfstatus))
	INSERT @tab
	SELECT p.*
		 , CASE WHEN DATEPART(q,@wd) = 1 THEN CASE WHEN (DATEADD(qq,-1, @wd) > p.ReportDate) THEN 1 ELSE 0 END
				WHEN DATEPART(m,@wd) = 4 AND DATEPART(d,@wd) <= 3 THEN CASE WHEN (DATEADD(qq,-2, @wd) > p.ReportDate) THEN 1 ELSE 0 END
				WHEN (DATEPART(m,@wd) = 4 AND DATEPART(d,@wd) > 3) OR (DATEPART(m,@wd) = 5 AND DATEPART(d,@wd) < 3) THEN CASE WHEN (DATEADD(qq,-1, @wd) > p.ReportDate) THEN 1 ELSE 0 END
				WHEN (DATEPART(m,@wd) = 5 AND DATEPART(d,@wd) > 2) OR (DATEPART(m,@wd) = 6) THEN 1
				WHEN (DATEPART(m,@wd) = 7) OR (DATEPART(m,@wd) = 8 AND DATEPART(d,@wd) = 1) THEN CASE WHEN (DATEADD(qq,-1, @wd) > p.ReportDate) THEN 1 ELSE 0 END
				WHEN (DATEPART(m,@wd) = 9) OR (DATEPART(m,@wd) = 8 AND DATEPART(d,@wd) > 1) THEN 1
				WHEN (DATEPART(m,@wd) = 10) OR (DATEPART(m,@wd) = 11 AND DATEPART(d,@wd) = 1) THEN CASE WHEN (DATEADD(qq,-1, @wd) > p.ReportDate) THEN 1 ELSE 0 END
				WHEN (DATEPART(m,@wd) = 12) OR (DATEPART(m,@wd) = 11 AND DATEPART(d,@wd) > 1) THEN 1
				ELSE NULL END as obPeriod
		 , CASE WHEN DATEPART(m,@wd) = 4 AND DATEPART(d,@wd) <= 3 AND DATEADD(qq,-2, @wd) > p.ReportDate THEN 1 ELSE NULL END as dopPeriod
	FROM rfBuhPeriod p
	WHERE reportdate<@wd
	AND reportdate>=DATEADD(q, CASE DATEPART(q,@wd)
								WHEN 1 THEN -5
								WHEN 2 THEN CASE DATEPART(m,@wd)
											WHEN 4 THEN CASE WHEN DATEPART(d,@wd) < 4 THEN CASE WHEN (@idKO = -1) AND (@idOrg = -1) THEN  -6
																								WHEN (@idKO = -1) AND (@idOrg > 0) THEN CASE (SELECT COUNT(*) cn
																																			  FROM (SELECT DISTINCT idPeriod
																																					FROM jrOrgBuhReport
																																					WHERE idOrg = @idOrg
																																					AND idPeriod IN (SELECT idPeriod
																																									 FROM rfBuhPeriod p
																																									 WHERE reportdate<@wd AND reportdate >= DATEADD(q, CASE WHEN DATEPART(q,@wd) = 2 THEN -2 END, @wd))
																																					) dd)
																																		WHEN 2 THEN -2 ELSE -6 END
																								WHEN (@idKO > 0) AND (@idOrg > 0) THEN CASE (SELECT COUNT(*) cn
																																			  FROM (SELECT DISTINCT idPeriod
																																					FROM jrOrgBuhReport
																																					WHERE idOrg = @idOrg and idKO = @idKO
																																					AND idPeriod IN (SELECT idPeriod
																																									 FROM rfBuhPeriod p
																																									 WHERE reportdate<@wd AND reportdate >= DATEADD(q, CASE WHEN DATEPART(q,@wd) = 2 THEN -2 END, @wd))
																																					) dd)
																																		WHEN 2 THEN -2 ELSE -6 END
																							ELSE NULL END
															 ELSE -2
															 END
											ELSE -2
											END
								WHEN 3 THEN -3
								WHEN 4 THEN -4
								END
							,@wd)
	RETURN
END
GO