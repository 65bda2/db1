﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[CheckActiveInCredit](@numActive INTEGER, @dtCheck DATETIME = 0)
RETURNS TINYINT
BEGIN
  DECLARE @Result TINYINT
  SET @Result = 0
  IF @dtCheck = 0 SELECT @dtCheck = dtWork FROM rfStatus
  IF EXISTS(SELECT idStock FROM hrStockSect 
    JOIN jrBlock ON (hrStockSect.idBlock = jrBlock.idBlock) 
    JOIN jrAccount ON (jrAccount.idAcc = jrBlock.idAcc) 
    JOIN jrGKD ON (jrGKD.nuGKD = jrAccount.nuGKD) AND ((CASE jrGKD.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE jrGKD.tpGKD END) IN ('B', 'D', 'F', 'G', 'H')) 
    WHERE dtSect = @dtCheck AND idStock = @numActive AND tpSect IN ('21L', '22N')) SET @Result = 1
  RETURN @Result
END
GO