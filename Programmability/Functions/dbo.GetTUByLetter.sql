﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetTUByLetter]
(	
	@idL int
) RETURNS Integer
AS
BEGIN
DECLARE @outTU int
DECLARE @V1 int
DECLARE @V2 int=NULL
DECLARE @V2temp int
DECLARE @V3 int

-- idLetter=>idKO=>idSubdivision=>CKPSubdivision Если пусто, то смотрим в ОКАТО
SET @V1=(select s.kptu as nmsign from rfSubdivision s, jrKO k, jrLetter l 
where k.idSubdivision=s.idSubdivision and l.idKO=k.idKO and l.idLetter=@idL)

--idLetter=>idKO=>Left(OKATO,2) - ОКАТО может быть и нулевым и NILL
SET @V2temp=(select Rtrim(Left(k.OKATO,2)) as nmsign from jrKO k, jrLetter l where l.idKO=k.idKO and l.idLetter=@idL)
SET @V2 = case when @V2temp<>0 then @V2temp end; --если ОКАТО=0000000, то смотрим в текущий хаб

SET @V3=(select nmsign from rfSignRekvizitASPPA where tpsign='kodtu')-- если первый и второй варианты NULL, то берем номер из хаба

SET @outTU=ISNULL(@V1,ISNULL(@V2,@V3))
RETURN @outTU
END
GO