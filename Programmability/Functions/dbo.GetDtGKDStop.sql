﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetDtGKDStop](@tpGKD CHAR(1), @dtGKDEnd DATETIME) RETURNS DATETIME -- Функция расчета даты приостановления ГКД 236-П, 312-П, КД на СЭТ ММВБ с даты вступления в силу Регламента ЕКД 
AS
BEGIN
  DECLARE @dtReglamentEKD DATETIME
  SELECT TOP 1 @dtReglamentEKD = CASE WHEN ISDATE(nmSign)=1 THEN CONVERT(DATETIME, nmSign, 104) ELSE NULL END FROM rfSign WHERE tpSign = 'dtReglamentEKD'
  RETURN(SELECT CASE WHEN @tpGKD IN ('A', 'B', 'C') AND @dtGKDEnd IS NULL AND @dtReglamentEKD IS NOT NULL THEN @dtReglamentEKD ELSE NULL END)
END
GO