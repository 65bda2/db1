﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------- 
--Функция: Определение наименования Организации обязанной по активу (Юр.лицо)
--!!! Используется в Таб. 3.4, 3.4_ОВН, 4.4, 4.4_ОВН, Таб.5
----------------------------------------------------------------------------------  
CREATE FUNCTION [dbo].[Opred_nmCompany_vTab5] ( 
	@Comp int, 
	@tpAct char(1) 
)
RETURNS nvarchar(250)
BEGIN
DECLARE @nmCompany nvarchar(250)
IF @tpAct = 'M'
	SET @nmCompany = (SELECT CONVERT(nvarchar(250), k.nmKO) FROM jrKO k
					   WHERE k.idKO = @Comp)
ELSE IF @tpAct IN ('V', 'D', 'E')
	SET @nmCompany = (SELECT CONVERT(nvarchar(250), org.nmOrg) FROM jrOrg org
					   WHERE org.idOrg = @Comp)
RETURN(@nmCompany)
END
GO