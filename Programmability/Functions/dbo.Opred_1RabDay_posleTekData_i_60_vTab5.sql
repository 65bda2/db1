﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------- 
--Функция Определение ДАТЫ следующего рабочего дня 
--(с учётом праздничных и выходных дней (см. переносы по Таб. rfDay)
--Плюс 60 дней (вне вне зависимости куда падает итоговая дата)
--Используется в качестве параметра @dtPayment 
--при вызове функции CalcActiveCost в АС "Сибирь" 
----------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[Opred_1RabDay_posleTekData_i_60_vTab5] (
		@dat date
)
RETURNS date
BEGIN
DECLARE @dned nvarchar(11) --день недели
DECLARE @dat_New date -- результат (расчётная дата для Стоимости актива)
DECLARE @dVrem date --временная новая дата для проверки
DECLARE @tpDay char(1)
DECLARE @inTab_rf_Day int --определяем StatusDay
DECLARE @statusProv int  -- для цикла WHILE
SET @statusProv = 0 
SET @dVrem = DATEADD(day, 1, @dat) --Текущая дата + 61 дней
WHILE @statusProv = 0
-----
BEGIN 
--Определяем наличие (@dat +1) в таблице rfDays
IF EXISTS (SELECT dtDay FROM rfDays WHERE CONVERT(date, dtDay) = @dVrem) 
	  SET @inTab_rf_Day = (SELECT CASE WHEN dtDay IS NOT NULL AND tpDay = 'W' THEN 1 --Рабочий день
									   WHEN dtDay IS NOT NULL AND tpDay = 'F' THEN 2 --Выходной день
								   END AS StatusDay
						     FROM rfDays
						    WHERE CONVERT(date, dtDay) = @dVrem)
ELSE SET @inTab_rf_Day = 0
--Если Рабочий день, то сразу определяем эту дату + 60 дней как расчётную для Стоимости актива
IF @inTab_rf_Day = 1
    BEGIN
	SET @dat_New = DATEADD(day, (60), @dVrem)
	SET @statusProv = 1  --меняем StatusDay
	BREAK --выход из цикла WHILE 
	END
--Если Выходной день, то шаг вперёд на 1н день, для последующей проверки получившейся даты
IF @inTab_rf_Day = 2
    BEGIN
    SET @dVrem = DATEADD(day, (1), @dVrem)
    CONTINUE
    END
--Если даты нет в таблице rfDays, то: 
IF @inTab_rf_Day = 0
    BEGIN
    --Определяем какой день недели у этой даты
    SET @dned = (SELECT DATENAME(weekday, @dVrem) AS WeekDay)
    --Если выходной то делаем шаг вперёд на 1 или 2 дня, о получаем новую дату для проверки 
    SET @dVrem = (SELECT CASE WHEN @dned = 'Суббота' THEN DATEADD(day, (2), @dVrem)
							WHEN @dned = 'Воскресенье' THEN DATEADD(day, (1), @dVrem)
							ELSE DATEADD(day, (0), @dVrem)
					    END
			     )
	--Определяем наличие (@dVrem) в таблице rfDays		     
	IF EXISTS (SELECT dtDay FROM rfDays WHERE CONVERT(date, dtDay) = @dVrem) 
	  SET @inTab_rf_Day = (SELECT CASE WHEN dtDay IS NOT NULL AND tpDay = 'W' THEN 1 --Рабочий день
									   WHEN dtDay IS NOT NULL AND tpDay = 'F' THEN 2 --Выходной день
								   END AS StatusDay
						     FROM rfDays
						    WHERE CONVERT(date, dtDay) = @dVrem)
    ELSE SET @inTab_rf_Day = 0
    --Если Рабочий день или даты нет в таблице rfDays, 
    --то определяем эту дату как расчётную для Стоимости актива
    IF @inTab_rf_Day = 0 OR @inTab_rf_Day = 1
        BEGIN
		SET @dat_New = DATEADD(day, (60), @dVrem)
		SET @statusProv = 1  --меняем StatusDay
		BREAK --выход из цикла WHILE 
		END
	--Если Выходной день, то шаг вперёд на 1н день, для последующей проверки получившейся даты
	IF @inTab_rf_Day = 2
		BEGIN
		SET @dVrem = DATEADD(day, (1), @dVrem)
		CONTINUE
		END
	END
CONTINUE
-----    
END
RETURN (@dat_New)
END
GO