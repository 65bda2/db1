﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetMaxCredMoney](@idKO INTEGER, @dtCurr DATETIME)
 RETURNS MONEY -- возвращает остаток, возможный для новых кредитов
BEGIN
  DECLARE @Result MONEY,
          @SumCred MONEY
          
  SELECT TOP 1 @Result = mnOwnFund FROM hrOwnFunds 
  WHERE hrOwnFunds.dtOwnFund <= @dtCurr AND hrOwnFunds.idKO = @idKO
  ORDER BY hrOwnFunds.dtOwnFund DESC
    
  IF @Result > 0 
  BEGIN
    SELECT @SumCred = ISNULL(SUM(mnCred), 0) FROM hrStockPawn 
    WHERE idBlock IN (
       SELECT idBlock FROM jrBlock
       JOIN jrAccount ON jrAccount.idAcc = jrBlock.idAcc AND jrAccount.idKO = @idKO
       JOIN jrGKD ON jrGKD.nuGKD = jrAccount.nuGKD AND (CASE jrGKD.tpGKD WHEN 'K' THEN jrAccount.tpAsset ELSE jrGKD.tpGKD END) = 'B'
    ) AND isGuarant <> 'T' AND tpSatisf IN ('K', 'A', 'N', 'P', 'C', 'M', 'J', 'O') AND DATEADD(DAY, qnCred, dtCred) > @dtCurr AND SUBSTRING(nuCred, 10, 1) <> '9'

    SET @Result = @Result - @SumCred
  END
  RETURN (@Result)
END
GO