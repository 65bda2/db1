﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnIsDistinctDateTime]
-- =============================================
-- Author:      <65FedorovEV>
-- Create date: <2019-12-21>
-- Description:	<Функция выполняет аналог @A is distinct from @B>
-- =============================================
/*
17.01.2020 Исключено поле OrgStatus для патча 32 (31.1.0.0)
*/
(@A DateTime, @B DateTime)
RETURNS bit
AS
BEGIN
  RETURN case when (@A <> @B or @A is null or @B is null) and not (@A is null and @B is null) then 1 else 0 end;
END
GO