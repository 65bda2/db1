﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------- 
--Функция: Подсчёт количества Дат для КО по типу ГКД
---------------------------------------------------- 
CREATE FUNCTION [dbo].[CountTpGKD_Dat] (
	   @ko int, @tp char(1)
)
RETURNS int
BEGIN
	RETURN (SELECT COUNT(dtGKD) 
	          FROM jrGKD 
	        WHERE idKO = @ko AND tpGKD = @tp)
END
GO