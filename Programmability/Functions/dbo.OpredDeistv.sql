﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
------------------------------------------------------- 
--Функция: Определение наличия для КО действующего
--         договора (соглашения) указанного типа
-- (Обработка строки собранных дат ГКД/ ГДС/ГС, 
--  действующих и расторгнутых))
-- (Таб.1. ==> Доля КО, закл. указанные дог.(согл.),
-- Используется в Функции dbo.Fn_KolDogSogl_KO_Tab1_Dei
------------------------------------------------------- 
CREATE FUNCTION [dbo].[OpredDeistv] ( 
	@st char(100)
)
RETURNS int
BEGIN
DECLARE @n int -- длина строки с Датами ("*")
SET @n = LEN(RTRIM(@st)) 
--строка с Датами конкретного типа ГКД/ ГДС/ ГС
DECLARE @tekSt char(100) 
SET @tekSt = @st 
DECLARE @tekSt2 char(100) -- Дубль строка ("*")
--Итог (Имеет ли данная КО действующий ГКД/ ГДС/ ГС)
DECLARE @zn int --(для поиска "*" )
DECLARE @i int --Счётчик количества звёздочек "*"
SET @i = 0
DECLARE @ras int --Количество расторгнутых
DECLARE @t int --Счётчик количества точек "."
SET @t = 0
--строка с Датами конкретного типа ГКД/ ГДС/ ГС
DECLARE @tSt char(100) --(для поиска ".")
SET @tSt = @st 
DECLARE @tSt2 char(100) --Дубль строка (для ".")
DECLARE @kd int --Количество дат в строке 
DECLARE @tn int -- длина строки с Датами (".")
SET @tn = LEN(RTRIM(@tSt))
-----
WHILE PATINDEX('%*%', @tekSt) <> 0 
BEGIN
SET @tekSt2 = RIGHT(RTRIM(@tekSt), 
             (@n - PATINDEX('%*%', RTRIM(@tekSt)))) 
SET @i = @i + 1 --Количество "*" 
SET @n = LEN(RTRIM(@tekSt2)) 
SET @tekSt = @tekSt2
END
SET @ras = @i / 2 --Количество расторгнутых
-----
WHILE PATINDEX('%.%', @tSt) <> 0 
BEGIN
SET @tSt2 = RIGHT(RTRIM(@tSt), 
           (@tn - PATINDEX('%.%', RTRIM(@tSt))))
SET @t = @t + 1 --Количество "."
SET @tn = LEN(RTRIM(@tSt2)) 
SET @tSt = @tSt2
END
SET @kd = @t / 2 --Количество дат в строке
----------------
IF @ras < @kd
   SET @zn = 1
ELSE IF @ras = 0 AND @kd > 0
   SET @zn = 1
ELSE IF @ras = @kd 
   SET @zn = 0
RETURN (@zn)
END
GO