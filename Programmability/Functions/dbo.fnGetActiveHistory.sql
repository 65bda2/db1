﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnGetActiveHistory](@dtFrom DateTime,@dtTo datetime, @UnNumIn varchar(16)='')
--@UnNumIn добавлен для возможности отследить историю по одному/нескольким активам. По всем активам можно посмотреть историю только за несколько месяцев
-- AHEBURG-2872 65bda2 04/02/2020 +TrancheNumber

-- Состав параметров для АС ППА
RETURNS @tab TABLE(
	UnNum char(16) 
	,NmZaem varchar(255)
	,NmOrgToCheck varchar(255) 
	,OGRNOrgtocheck varchar(20) 
	,NMCheck varchar(5) 
	,Creationdate datetime 
	,NomerVD varchar(100) 
	,DatePayment datetime 
	,AssetQuality int 
	,ZaemOKOPF varchar(10) 
	,businessQuality  int 
	,OkvedOrgToCheck varchar(25) 
	,status char(1) 
	,DFROM datetime
	,nmISO char(3)
    ,OperationDate datetime		
    ,nuCred char(20)				
    ,TempCost	money		
    ,NmZaemshik int			
    ,Company int			
    ,numactive int			
    ,typeactive varchar(2)
    ,Nureg varchar(9) 
    ,NMKO varchar(250)
    ,KPTU varchar(2)
    ,NmStatus varchar(50)
	,nmbusinessQuality varchar(20)
	,TrancheNumber varchar(255)
	,Change_NmZaem tinyint
	,Change_NmOrgToCheck tinyint
	,Change_OGRNOrgtocheck tinyint
	,Change_NMCheck tinyint
	,Change_CreationDate tinyint
	,Change_NomerVD tinyint
	,Change_DatePayment tinyint
	,Change_nmISO tinyint
	,Change_AssetQuality tinyint
	,Change_ZaemOKOPF tinyint
	,Change_BusinessQuality tinyint
	,Change_OkvedOrgToCheck tinyint
	,Change_NmStatus tinyint
	,Change_TrancheNumber tinyint)
AS 
BEGIN

DECLARE @t TABLE (idRecord int IDENTITY(1,1)
				,UnNum char(16) 
				,NmZaem varchar(255)
				,NmOrgToCheck varchar(255) 
				,OGRNOrgtocheck varchar(20) 
				,NMCheck varchar(5) 
				,Creationdate datetime 
				,NomerVD varchar(100) 
				,DatePayment datetime 
				,AssetQuality int 
				,ZaemOKOPF varchar(10) 
				,businessQuality  int 
				,OkvedOrgToCheck varchar(25) 
				,status char(1) 
				,DFROM datetime
				,nmISO char(3)
				,OperationDate datetime		
				,nuCred char(20)				
				,TempCost	money		
				,NmZaemshik int			
				,Company int			
				,numactive int			
				,typeactive varchar(2)
				,Nureg varchar(9)
				,NMKO varchar(250)
				,KPTU varchar(2)
				,NmStatus varchar(50)
				,TrancheNumber varchar(255)
				,Change_NmZaem tinyint
				,Change_NmOrgToCheck tinyint
				,Change_OGRNOrgtocheck tinyint
				,Change_NMCheck tinyint
				,Change_CreationDate tinyint
				,Change_NomerVD tinyint
				,Change_DatePayment tinyint
				,Change_nmISO tinyint
				,Change_AssetQuality tinyint
				,Change_ZaemOKOPF tinyint
				,Change_BusinessQuality tinyint
				,Change_OkvedOrgToCheck tinyint
				,Change_NmStatus tinyint
				,Change_TrancheNumber tinyint
				)
			  
INSERT INTO	@t (UnNum  
				,NmZaem 
				,NmOrgToCheck 
				,OGRNOrgtocheck 
				,NMCheck  
				,Creationdate 
				,NomerVD  
				,DatePayment 
				,AssetQuality  
				,ZaemOKOPF  
				,businessQuality  
				,OkvedOrgToCheck 
				,status 
				,DFROM
				,nmISO 
				,OperationDate 	
				,nuCred				
				,TempCost		
				,NmZaemshik 		
				,Company 		
				,numactive 			
				,typeactive 
				,Nureg
				,NMKO
				,KPTU 
				,NmStatus
				,TrancheNumber
				,Change_NmZaem 
				,Change_NmOrgToCheck 
				,Change_OGRNOrgtocheck 
				,Change_NMCheck 
				,Change_CreationDate 
				,Change_NomerVD 
				,Change_DatePayment 
				,Change_nmISO 
				,Change_AssetQuality 
				,Change_ZaemOKOPF 
				,Change_BusinessQuality 
				,Change_OkvedOrgToCheck 
				,Change_NmStatus
				,Change_TrancheNumber  
				)
SELECT 				
	a.unnum					--4
	,a.NmZaem			--6
	,a.NmOrgToCheck		--7
	,a.OGRNOrgtocheck	--8
	,a.NMCheck			--9
	,a.Creationdate		--10
	,a.NomerVD			--11
	,a.DatePayment		--12
	,a.AssetQuality		--14
	,a.ZaemOKOPF		--17
	,businessQuality	--18
	,a.OkvedOrgToCheck	--19
	,a.status			--20
	,a.DFROM				--1
	,Curr.nmISO			--13
	,a.OperationDate		--2	
	,a.nuCred				
	,a.TempCost			
	,a.NmZaemshik			
	,a.Company			
	,a.numactive			
	,a.typeactive			
	,c.Nureg				--5	
	,Ltrim(RTRIM(isNULL(c.NNKO,c.NMKO))) AS NMKO	--3 
	,(SELECT KPTU FROM rfSubdivision r WHERE r.idSubdivision=c.idSubdivision) AS KPTU
	,(CASE a.status 
		WHEN 'T' THEN (CASE 
							WHEN ras.tpsect in ('1' , '11D' , '12L') THEN 'Включен в состав активов (свободен от залога)' 
							WHEN ras.tpsect in ('2' , '21L', '22N', '3', '3N', '3L') THEN 'Включен в состав активов (в залоге)'
							ELSE '-'  
						END) 
		WHEN 'N' THEN 'Подлежит исключению (в залоге)' 
		WHEN 'R' THEN 'На рассмотрении' 
		WHEN 'W' THEN 'Рассмотрение приостановлено'
		WHEN 'E' THEN 'Исключен из состава активов' 
		WHEN 'Q' THEN 'Исключен из состава активов' 
		ELSE '-' 
	END) as NmStatus
	,a.TrancheNumber	
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
	,0
FROM hrActive a  
	inner join jrLetter b ON b.idletter=a.idletter  
	inner join jrKO c ON  c.idKO=b.idko 
	left join  jrCurrency Curr ON Curr.nuiso=a.Currency
	LEFT JOIN hrActiveSect has on a.NumActive = has.NumActive AND has.DateAssign = (SELECT dtwork FROM rfStatus)
	LEFT JOIN jrAccount ja on has.idAcc = ja.idAcc AND ja.tpAsset = 'B'
	LEFT JOIN rfActiveState ras on has.idActiveState = ras.idActiveState 
WHERE a.TypeActive in ('V','D')	
	AND  DFROM < @dtTo and DFROM > @dtFrom
	AND a.UnNum like '%'+ @UnNumIn+'%'
	
ORDER BY UnNum ASC, DFROM ASC --Важный момент! если нарушить сортировку - сломается хронология изменений по активу 

DECLARE @isNextAsset tinyint
DECLARE @myCursor CURSOR 
SET @myCursor = CURSOR SCROLL
FOR SELECT idRecord 
			,UnNum  
			,NmZaem
			,NmOrgToCheck 
			,OGRNOrgtocheck 
			,NMCheck 
			,Creationdate 
			,NomerVD  
			,DatePayment
			,AssetQuality 
			,ZaemOKOPF 
			,businessQuality   
			,OkvedOrgToCheck  
			,status 
			,DFROM 
			,nmISO  
			,NmStatus
			,TrancheNumber 
	FROM @t
--список параметров, по которым ведется отслеживание изменений между двумя последовательными записями
DECLARE
	@idRecord int 
	,@UnNum char(16) 
	,@NmZaem varchar(255)
	,@NmOrgToCheck varchar(255) 
	,@OGRNOrgtocheck varchar(20) 
	,@NMCheck varchar(5) 
	,@Creationdate datetime 
	,@NomerVD varchar(100) 
	,@DatePayment datetime 
	,@AssetQuality int 
	,@ZaemOKOPF varchar(10) 
	,@businessQuality  int 
	,@OkvedOrgToCheck varchar(25) 
	,@status char(1) 
	,@DFROM datetime
	,@nmISO char(3)
    ,@NmStatus varchar(50)
	,@TrancheNumber varchar(255)
	,@Change_NmZaem tinyint
	,@Change_NmOrgToCheck tinyint
	,@Change_OGRNOrgtocheck tinyint
	,@Change_NMCheck tinyint
	,@Change_CreationDate tinyint
	,@Change_NomerVD tinyint
	,@Change_DatePayment tinyint
	,@Change_nmISO tinyint
	,@Change_AssetQuality tinyint
	,@Change_ZaemOKOPF tinyint
	,@Change_BusinessQuality tinyint
	,@Change_OkvedOrgToCheck tinyint
	,@Change_NmStatus tinyint
	,@Change_TrancheNumber tinyint
  
	,@idRecord_1 int 
	,@UnNum_1 char(16) 
	,@NmZaem_1 varchar(255)
	,@NmOrgToCheck_1 varchar(255) 
	,@OGRNOrgtocheck_1 varchar(20) 
	,@NMCheck_1 varchar(5) 
	,@Creationdate_1 datetime 
	,@NomerVD_1 varchar(100) 
	,@DatePayment_1 datetime 
	,@AssetQuality_1 int 
	,@ZaemOKOPF_1 varchar(10) 
	,@businessQuality_1  int 
	,@OkvedOrgToCheck_1 varchar(25) 
	,@status_1 char(1) 
	,@DFROM_1 datetime
	,@nmISO_1 char(3)
    ,@NmStatus_1 varchar(50)
	,@TrancheNumber_1 varchar(255)
	,@Change_NmZaem_1 tinyint
	,@Change_NmOrgToCheck_1 tinyint
	,@Change_OGRNOrgtocheck_1 tinyint
	,@Change_NMCheck_1 tinyint
	,@Change_CreationDate_1 tinyint
	,@Change_NomerVD_1 tinyint
	,@Change_DatePayment_1 tinyint
	,@Change_nmISO_1 tinyint
	,@Change_AssetQuality_1 tinyint
	,@Change_ZaemOKOPF_1 tinyint
	,@Change_BusinessQuality_1 tinyint
	,@Change_OkvedOrgToCheck_1 tinyint
	,@Change_NmStatus_1 tinyint
	,@Change_TrancheNumber_1 tinyint


OPEN @myCursor

	FETCH first FROM @myCursor
	INTO @idRecord_1
		  ,@UnNum_1  
		  ,@NmZaem_1
		  ,@NmOrgToCheck_1
		  ,@OGRNOrgtocheck_1 
		  ,@NMCheck_1
		  ,@Creationdate_1
		  ,@NomerVD_1
		  ,@DatePayment_1
		  ,@AssetQuality_1 
		  ,@ZaemOKOPF_1
		  ,@businessQuality_1
		  ,@OkvedOrgToCheck_1
		  ,@status_1
		  ,@DFROM_1
		  ,@nmISO_1
		  ,@NmStatus_1
		  ,@TrancheNumber_1 
	FETCH NEXT FROM @myCursor
		INTO @idRecord
		  ,@UnNum  
		  ,@NmZaem 
		  ,@NmOrgToCheck 
		  ,@OGRNOrgtocheck 
		  ,@NMCheck  
		  ,@Creationdate 
		  ,@NomerVD  
		  ,@DatePayment 
		  ,@AssetQuality  
		  ,@ZaemOKOPF  
		  ,@businessQuality  
		  ,@OkvedOrgToCheck 
		  ,@status 
		  ,@DFROM
		  ,@nmISO 
		  ,@NmStatus
		  ,@TrancheNumber

		SET @Change_NmZaem_1			= 0			
		SET @Change_NmOrgToCheck_1		= 0	
		SET @Change_OGRNOrgtocheck_1	= 0	
		SET @Change_NMCheck_1			= 0			
		SET @Change_CreationDate_1		= 0	
		SET @Change_NomerVD_1			= 0			
		SET @Change_DatePayment_1		= 0		
		SET @Change_AssetQuality_1		= 0	
		SET @Change_ZaemOKOPF_1			= 0		
		SET @Change_BusinessQuality_1	= 0	
		SET @Change_OkvedOrgToCheck_1	= 0	
		SET @Change_NmStatus_1			= 0		
		SET @Change_nmISO_1				= 0
		SET @Change_TrancheNumber_1		= 0
		
	WHILE @@FETCH_STATUS = 0
	BEGIN			 
		SET @isNextAsset			= 0
		SET @Change_NmZaem			= 0			
		SET @Change_NmOrgToCheck	= 0	
		SET @Change_OGRNOrgtocheck	= 0	
		SET @Change_NMCheck			= 0			
		SET @Change_CreationDate	= 0	
		SET @Change_NomerVD			= 0			
		SET @Change_DatePayment		= 0		
		SET @Change_AssetQuality	= 0	
		SET @Change_ZaemOKOPF		= 0		
		SET @Change_BusinessQuality = 0	
		SET @Change_OkvedOrgToCheck = 0	
		SET @Change_NmStatus		= 0		
		SET @Change_nmISO			= 0
		SET @Change_TrancheNumber	= 0
			 
		IF  (ISNULL(@UnNum_1,'___')				<>	ISNULL(@UnNum,'___')			)	SET @isNextAsset = 1
		 					 
--В том случае, если следующая строка в таблице по другому @UnNum - оставляем последнюю строку по предыдущему активу в силу сортировки при заполнении таблицы @t
		IF  (ISNULL(@NmZaem_1,'___')			<>	ISNULL(@NmZaem,'___')			)	SET @Change_NmZaem = 1							
		IF  (ISNULL(@NmOrgToCheck_1,'___')		<>	ISNULL(@NmOrgToCheck,'___')		)	SET @Change_NmOrgToCheck = 1						
		IF  (ISNULL(@OGRNOrgtocheck_1,'___')	<>	ISNULL(@OGRNOrgtocheck,'___')	)	SET @Change_OGRNOrgtocheck = 1						
		IF  (ISNULL(@NMCheck_1,'___')			<>	ISNULL(@NMCheck,'___')			)	SET @Change_NMCheck = 1						
		IF  (ISNULL(@Creationdate_1, 0)			<>	ISNULL(@Creationdate, 0)		)	SET @Change_CreationDate = 1						
		IF  (ISNULL(@NomerVD_1,'___')			<>	ISNULL(@NomerVD,'___')			)	SET @Change_NomerVD = 1						
		IF  (ISNULL(@DatePayment_1, 0)			<>	ISNULL(@DatePayment, 0)			)	SET @Change_DatePayment = 1						
		IF  (ISNULL(@AssetQuality_1, -777)		<>	ISNULL(@AssetQuality, -777)		)	SET @Change_AssetQuality = 1						
		IF  (ISNULL(@ZaemOKOPF_1,'___')			<>	ISNULL(@ZaemOKOPF,'___')		)	SET @Change_ZaemOKOPF = 1					
		IF  (ISNULL(@businessQuality_1, -777)	<>	ISNULL(@businessQuality, -777)	)	SET @Change_BusinessQuality = 1					
		IF  (ISNULL(@OkvedOrgToCheck_1,'___')	<>	ISNULL(@OkvedOrgToCheck,'___')	)	SET @Change_OkvedOrgToCheck = 1						
			 --ISNULL(@Status_1,'_')				=	ISNULL(@Status,'_')				AND
		IF  (ISNULL(@NmStatus_1,'_')			<>	ISNULL(@NmStatus,'_')			)	SET @Change_NmStatus = 1						
		IF  (ISNULL(@nmISO_1,'___')				<>	ISNULL(@nmISO,'___')			)	SET @Change_nmISO = 1
		IF	(ISNULL(@TrancheNumber_1,'___')		<>	ISNULL(@TrancheNumber,'___')	)	SET @Change_TrancheNumber =1
		
		IF (@Change_NmZaem				+ @Change_NmOrgToCheck	+ @Change_OGRNOrgtocheck	+ @Change_NMCheck	+ @Change_CreationDate
			+ @Change_NomerVD			+ @Change_DatePayment	+ @Change_AssetQuality		+ @Change_ZaemOKOPF	+ @Change_BusinessQuality 
			+ @Change_OkvedOrgToCheck	+ @Change_NmStatus		+ @Change_nmISO				+ @Change_TrancheNumber) = 0	
		BEGIN
			IF @isNextAsset = 0					
			BEGIN
				DELETE FROM @t WHERE idRecord = @idRecord_1		
			END
		END
		ELSE
		BEGIN
			UPDATE @t
				SET Change_NmZaem			= @Change_NmZaem_1
					,Change_NmOrgToCheck	= @Change_NmOrgToCheck_1
					,Change_OGRNOrgtocheck	= @Change_OGRNOrgtocheck_1
					,Change_NMCheck			= @Change_NMCheck_1
					,Change_CreationDate	= @Change_CreationDate_1
					,Change_NomerVD			= @Change_NomerVD_1
					,Change_DatePayment		= @Change_DatePayment_1
					,Change_nmISO			= @Change_nmISO_1
					,Change_AssetQuality	= @Change_AssetQuality_1
					,Change_ZaemOKOPF		= @Change_ZaemOKOPF_1
					,Change_BusinessQuality = @Change_BusinessQuality_1
					,Change_OkvedOrgToCheck = @Change_OkvedOrgToCheck_1
					,Change_NmStatus 		= @Change_NmStatus_1
					,Change_TrancheNumber	= @Change_TrancheNumber_1
			WHERE idRecord = @idRecord_1
			IF @isNextAsset = 0					
			BEGIN
				SET @Change_NmZaem_1			= @Change_NmZaem
				SET @Change_NmOrgToCheck_1		= @Change_NmOrgToCheck
				SET @Change_OGRNOrgtocheck_1	= @Change_OGRNOrgtocheck
				SET @Change_NMCheck_1			= @Change_NMCheck
				SET @Change_CreationDate_1		= @Change_CreationDate
				SET @Change_NomerVD_1			= @Change_NomerVD
				SET @Change_DatePayment_1		= @Change_DatePayment
				SET @Change_nmISO_1				= @Change_nmISO
				SET @Change_AssetQuality_1		= @Change_AssetQuality
				SET @Change_ZaemOKOPF_1			= @Change_ZaemOKOPF
				SET @Change_BusinessQuality_1	= @Change_BusinessQuality
				SET @Change_OkvedOrgToCheck_1	= @Change_OkvedOrgToCheck
				SET @Change_NmStatus_1			= @Change_NmStatus
				SET @Change_TrancheNumber_1		= @Change_TrancheNumber
			END
			ELSE
			BEGIN
				SET @Change_NmZaem_1			= 0
				SET @Change_NmOrgToCheck_1		= 0
				SET @Change_OGRNOrgtocheck_1	= 0
				SET @Change_NMCheck_1			= 0
				SET @Change_CreationDate_1		= 0
				SET @Change_NomerVD_1			= 0
				SET @Change_DatePayment_1		= 0
				SET @Change_nmISO_1				= 0
				SET @Change_AssetQuality_1		= 0
				SET @Change_ZaemOKOPF_1			= 0
				SET @Change_BusinessQuality_1	= 0
				SET @Change_OkvedOrgToCheck_1	= 0
				SET @Change_NmStatus_1			= 0
				SET @Change_TrancheNumber_1		= 0
			END
		END
		
		
		SET	 @idRecord_1		=	@idRecord	
		SET	 @UnNum_1			=	@UnNum				  
		SET	 @NmZaem_1			=	@NmZaem				
		SET	 @NmOrgToCheck_1	=	@NmOrgToCheck		
		SET	 @OGRNOrgtocheck_1	=	@OGRNOrgtocheck		
		SET	 @NMCheck_1			=	@NMCheck			
		SET	 @Creationdate_1	=	@Creationdate		
		SET	 @NomerVD_1			=	@NomerVD			
		SET	 @DatePayment_1		=	@DatePayment		
		SET	 @AssetQuality_1	=	@AssetQuality		
		SET	 @ZaemOKOPF_1		=	@ZaemOKOPF			
		SET	 @businessQuality_1	=	@businessQuality	
		SET	 @OkvedOrgToCheck_1	=	@OkvedOrgToCheck	
		SET	 @Status			=	@Status	
		SET	 @nmISO_1			=	@nmISO
		SET	 @NmStatus_1		=	@NmStatus
		SET	 @TrancheNumber_1	=	@TrancheNumber
		
		FETCH NEXT FROM @myCursor
		INTO @idRecord
			,@UnNum  
			,@NmZaem 
			,@NmOrgToCheck 
			,@OGRNOrgtocheck 
			,@NMCheck  
			,@Creationdate 
			,@NomerVD  
			,@DatePayment 
			,@AssetQuality  
			,@ZaemOKOPF  
			,@businessQuality  
			,@OkvedOrgToCheck 
			,@status 
			,@DFROM
			,@nmISO
			,@NmStatus
			,@TrancheNumber
	END
	IF (@idRecord > 1)
	BEGIN
			UPDATE @t
				SET Change_NmZaem			= @Change_NmZaem_1
					,Change_NmOrgToCheck	= @Change_NmOrgToCheck_1
					,Change_OGRNOrgtocheck	= @Change_OGRNOrgtocheck_1
					,Change_NMCheck			= @Change_NMCheck_1
					,Change_CreationDate	= @Change_CreationDate_1
					,Change_NomerVD			= @Change_NomerVD_1
					,Change_DatePayment		= @Change_DatePayment_1
					,Change_nmISO			= @Change_nmISO_1
					,Change_AssetQuality	= @Change_AssetQuality_1
					,Change_ZaemOKOPF		= @Change_ZaemOKOPF_1
					,Change_BusinessQuality = @Change_BusinessQuality_1
					,Change_OkvedOrgToCheck = @Change_OkvedOrgToCheck_1
					,Change_NmStatus 		= @Change_NmStatus_1
					,Change_TrancheNumber	= @Change_TrancheNumber_1
			WHERE idRecord = @idRecord
	END
	

	CLOSE @myCursor
	DEALLOCATE @myCursor
	
	INSERT @tab
	SELECT  ha.UnNum  
			,ha.NmZaem 
			,ha.NmOrgToCheck 
			,ha.OGRNOrgtocheck 
			,ha.NMCheck  
			,ha.Creationdate 
			,ha.NomerVD  
			,ha.DatePayment 
			,ha.AssetQuality  
			,ha.ZaemOKOPF  
			,ha.businessQuality  
			,ha.OkvedOrgToCheck 
			,ha.status 
			,ha.DFROM
			,ha.nmISO
			,ha.OperationDate 	
			,ha.nuCred 				
			,ha.TempCost			
			,ha.NmZaemshik 		
			,ha.Company			
			,ha.numactive 			
			,ha.typeactive
			,ha.Nureg
			,ha.NMKO 
			,ha.KPTU
			,ha.NmStatus
			,(CASE ha.businessQuality WHEN 2 THEN 'среднее или крупное' WHEN 1 THEN 'малое' ELSE '' END) AS nmbusinessQuality
			,ha.TrancheNumber
			,ha.Change_NmZaem 
			,ha.Change_NmOrgToCheck 
			,ha.Change_OGRNOrgtocheck 
			,ha.Change_NMCheck 
			,ha.Change_CreationDate 
			,ha.Change_NomerVD 
			,ha.Change_DatePayment 
			,ha.Change_nmISO 
			,ha.Change_AssetQuality 
			,ha.Change_ZaemOKOPF 
			,ha.Change_BusinessQuality 
			,ha.Change_OkvedOrgToCheck 
			,ha.Change_NmStatus
			,ha.Change_TrancheNumber  
	FROM
		@t ha	
	RETURN 		
END
GO