﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 29.11.2019
-- Description:	Вычисление уникального номера нового актива
-- =============================================
CREATE FUNCTION [dbo].[fnGetNextUnNum1]
(
@idLetter int,  @typeActive varchar(2)
)
RETURNS VARCHAR(16)
AS
BEGIN
declare @idKO int
  declare @idSubDevision int
  declare @KPTU [char](2)
  
  declare @full_nuReg varchar(9)
  declare @nuReg varchar(50)
  DECLARE @NewNum int
  DECLARE @NewNum_str varchar(9)
  declare @dtWork datetime
  declare @unnum varchar(16)
  declare @tActive int

	set @tActive= case
  --@typeActive 1 D-Договор,3 H-МСП,4 E-Эксар,5 I-Инвестпроекты
    when @typeActive='D' then 1
    when @typeActive='H' then 3
    when @typeActive='E' then 4
    when @typeActive='I' then 5
  end
  select @dtWork=dtwork from rfStatus
  select @idKO=idKO from jrLetter where idLetter=@idLetter
  select @KPTU=KPTU from rfSubdivision where idSubdivision=(select idSubdivision from jrKO where @idKO=idKO)
  select @full_nuReg=jrGKD.nuReg from jrGKD join jrAccount on jrAccount.nuGKD=jrGKD.nuGKD where jrAccount.idKO=@idKO 
  and  (jrAccount.dtExcl is NULL or jrAccount.dtExcl>@dtWork)
  and  (jrGKD.dtGKDend is NULL or jrGKD.dtGKDend>@dtWork)
  declare @s varchar(1)

  set @nuReg=''
  declare @i int
  set @i=1
  set @s=SUBSTRING(@full_nuReg,@i,1)
  while PATINDEX('%[0-9]%',@s)>0
  begin
    set @i=@i+1
    set @nuReg=@nuReg+@s
    set @s=SUBSTRING(@full_nuReg,@i,1) 
  end 
  declare @nureg_int int
  if @nuReg='' set  @nureg_int=0 else set @nureg_int=cast(@nuReg as int)
  set @nureg=RIGHT(cast(10000+@nuReg_int as varchar), 4)
  --SELECT @NewNum = 1 +  MAX(unio.NewNum) from
		--(SELECT 0 as NewNum 
		--UNION 		
		--SELECT (case @tActive  when 3 then MAX (AssetNumMSP) when 4 then MAX (AssetNumEcsar) when 5 then MAX (AssetNumInvest) else  MAX (AssetNumNA) end)  as NewNum 
		--FROM LastAssetNumber LAN where 
		----idKO=@idKO --idKO на 28.11.2019
		--nuReg=@nuReg and KPTU=@KPTU --на будущее 
		--UNION
		--SELECT MAX(CAST(SUBSTRING(UnNum,9,8) as int)) as NewNum
		--FROM jrActive 
		--Where SUBSTRING(UnNum,1,2) = @KPTU and cast(SUBSTRING(UnNum,4,4) as int) = @NuReg and substring(UnNum,3,1)= @tActive
		--) unio
  
  SELECT @NewNum = 1 +  isnull(NewNum,0) from
		(
		SELECT  (case @tActive  when 3 then MAX (AssetNumMSP) when 4 then MAX (AssetNumEcsar) when 5 then MAX (AssetNumInvest) else  MAX (AssetNumNA) end)  as NewNum 
		FROM LastAssetNumber LAN where 
		nuReg=@nuReg and KPTU=@KPTU --на будущее 
		)a
  
  
  set @NewNum_str=right(cast(1000000000+@newnum as varchar),9)
  set @UnNum= @KPTU+cast(@tActive as varchar(1))+right(cast(100000+@nuReg as varchar),4)+@NewNum_str 


RETURN @UnNum
END
GO