﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-------------------------------------------------------------------------------- 
--Функция определения Стоимости Актива без учёта поправочного коеффицента 
--по передаваемым: МАХ-Дате и idStock, tpSect, idBlock, cdStock
--(Таб.5 - раздел 2.1 и 2.2)
--Доработано, ошибка в Таб.5 - устранена(22.05.2014)
-------------------------------------------------------------------------------- 
CREATE FUNCTION [dbo].[Opred_mnActive_BezCoef] (
	@dat date,
	@idS int, 
	@tpS char(3),
	@idB int,
	@cd int
)
RETURNS money
BEGIN
RETURN(SELECT (hss.mnCost * hss.qnStock) 
                        FROM hrStockSect hss
                       WHERE CONVERT(date, hss.dtSect) = @dat
                         AND hss.idStock = @idS
                         AND hss.tpSect = @tpS
                         AND hss.idBlock = @idB
                         AND hss.cdStock = @cd
       )
END
GO