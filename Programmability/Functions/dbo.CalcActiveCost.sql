﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[CalcActiveCost](@NumActive INT, @dtPayment DATETIME, @dtCourse DATETIME)
RETURNS DECIMAL(38, 15)
BEGIN
      DECLARE @TypeActive CHAR(1)
      DECLARE @Nominal MONEY
      DECLARE @Cost MONEY
      DECLARE @CostRub MONEY
      DECLARE @Currency CHAR(3)
      DECLARE @DatePayment   SMALLDATETIME
      DECLARE @Course DECIMAL(20, 5)
      DECLARE @ChemicalWeight DECIMAL(21, 1)
      DECLARE @Result DECIMAL(38, 15)
      DECLARE @InsurancePercent  DECIMAL(25,2)
      DECLARE @IsBaseRefusal TINYINT
      DECLARE @IsAKG TINYINT
      SELECT @TypeActive= LEFT(TypeActive, 1), @IsBaseRefusal=IsBaseRefusal, @InsurancePercent=InsurancePercent, @Nominal= Nominal, @Cost= Cost, @CostRub= CostRub, @Currency= Currency, @IsAKG=isAKG, @Course= Course,@ChemicalWeight = ROUND(ChemicalWeight, 1, 1), @DatePayment=DatePayment
      FROM jrActive
      WHERE NumActive = @NumActive
      IF @TypeActive = 'D'
      BEGIN
            --SET @Cost= (SELECT ISNULL(SUM(Payment), 0) FROM jrPaymentGrafic WHERE numActive = @NumActive AND (DatePay >= @dtPayment) AND NOT( ((@DatePayment = DatePay) AND (DATEPAY = @dtPayment)) OR (Advice='T')) )
            SET @Cost= (SELECT ISNULL(SUM(Payment), 0) FROM jrPaymentGrafic WHERE numActive = @NumActive AND Advice='F') 
            IF @Currency = '810'
                  SET @Result= @Cost
            ELSE
                  IF @Course IS NULL
                        SET @Result= @Cost* (SELECT nuRate/ nuScale FROM hrRateExch WHERE hrRateExch.nuISO = @Currency AND dtRate = ISNULL(@dtCourse, 0))
                  ELSE
                        SET @Result= @Cost* @Course
      END
      ELSE
      IF @TypeActive = 'V'
      BEGIN
            IF @Currency = '810'
                  SET @Result= @Nominal
            ELSE
                  IF @Course IS NULL
                        SET @Result= @Nominal* (SELECT nuRate/ nuScale FROM hrRateExch WHERE hrRateExch.nuISO = @Currency AND dtRate = ISNULL(@dtCourse, 0))
                  ELSE
                        SET @Result= @Nominal* @Course
            IF @CostRub < @Result
                  SET @Result= @CostRub
      END
      ELSE
      IF @TypeActive IN ('M', 'E', 'I', 'Y')
      BEGIN
            SET @Cost= (SELECT ISNULL(SUM(grafic.Payment), 0) FROM jrPaymentGrafic grafic, jrTransh transh WHERE grafic.idTransh = transh.idTransh AND transh.Status = 'T' AND grafic.numActive = @NumActive AND grafic.DatePay >= @dtPayment AND NOT ( ((@DatePayment = DatePay) AND (DATEPAY = @dtPayment)) OR (Advice='T')) )
            IF @Currency = '810'
                  SET @Result= @Cost
            ELSE
                  IF @Course IS NULL
                        SET @Result= @Cost* (SELECT nuRate/ nuScale FROM hrRateExch WHERE hrRateExch.nuISO = @Currency AND dtRate = ISNULL(@dtCourse, 0))
                  ELSE
                        SET @Result= @Cost* @Course
            IF @TypeActive = 'E' SET @Result = @Result*@IsBaseRefusal
      END
      ELSE 
      IF @TypeActive = 'G'
      BEGIN                   
            SET @Result= @ChemicalWeight * (SELECT ISNULL(mnCost, 0) FROM hrRateMetal WHERE dtRate = @dtCourse AND nuIso = 'A98')
      END   
      IF @TypeActive in ('V','D') and @IsAKG = 1 
      BEGIN
            IF @RESULT>@InsurancePercent 
                  BEGIN
                        SET @Result = @InsurancePercent
                  END   
      END
  RETURN(@Result)
END
GO