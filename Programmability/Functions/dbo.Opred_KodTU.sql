﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
--------------------------------------------------------------------------------- 
--------------------------------------------------------------------------------- 
--Функция Определения Кода ТУ
--------------------------------------------------------------------------------- 
CREATE FUNCTION [dbo].[Opred_KodTU] ()
RETURNS char(2)
BEGIN
RETURN (SELECT nmSign
		  FROM rfSign AS s
		 WHERE tpSign = 'kodtu'
		)
END
GO