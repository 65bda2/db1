﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[CalcCurrencyActiveCost](@NumActive INT, @dtPayment DATETIME)
 RETURNS DECIMAL(38, 15)
BEGIN
      DECLARE @TypeActive CHAR(1)
      DECLARE @Nominal MONEY
      DECLARE @Cost MONEY      
      DECLARE @DatePayment	SMALLDATETIME
      DECLARE @Result DECIMAL(38, 15)
      SELECT @TypeActive= LEFT(TypeActive, 1),@Nominal= Nominal, @Cost= Cost, @DatePayment=DatePayment
      FROM jrActive
      WHERE NumActive = @NumActive      
      IF @TypeActive = 'D'
      BEGIN
            --SET @Result= (SELECT ISNULL(SUM(Payment), 0) FROM jrPaymentGrafic WHERE numActive = @NumActive AND (DatePay >= @dtPayment) AND NOT( ((@DatePayment = DatePay) AND (DATEPAY = @dtPayment)) OR (Advice='T')) )   
            SET @Result= (SELECT ISNULL(SUM(Payment), 0) FROM jrPaymentGrafic WHERE numActive = @NumActive AND  (Advice='F'))      
      END
      ELSE
      IF @TypeActive IN ('M', 'E', 'I', 'Y')
      BEGIN
            SET @Result= (SELECT ISNULL(SUM(grafic.Payment), 0) FROM jrPaymentGrafic grafic, jrTransh transh WHERE grafic.idTransh = transh.idTransh AND transh.Status = 'T' AND grafic.numActive = @NumActive AND grafic.DatePay >= @dtPayment AND NOT ( ((@DatePayment = DatePay) AND (DATEPAY = @dtPayment)) OR (Advice='T')) )
      END
      ELSE
      BEGIN	                      
        SET @Result= @Cost
      END         
     RETURN(@Result)
END
GO