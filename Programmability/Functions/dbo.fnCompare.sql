﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnCompare]
--********************
-- ПЗ 6.1.1.7 	Функция fnCompare ()
-- =============================================
-- Author:<65baa2>
-- Create date: <2.11.2018>
-- Description:	<Функция выполняет сравнение @A и @B  по заданному критерию  @CodeCompare и возвращает результат 1 если true или 0 если false>
-- =============================================
(
	@A float, -- значение @A 
	@B float, -- значение @B
	@CodeCompare smallint--Код сравнения со следующими значениям:
		--  0 сравнение на равенство (@A=@B)
		--  1 сравнение на больше либо равно (@A>=@B )
		--  2 сравнение на больше (@A>@B)
		-- -1 сравнение на меньше либо равно (@A<=@B)
	    -- -2 сравнение на меньше (@A<@B) 
)
RETURNS bit
AS
BEGIN
   declare @res bit=0;
   select 
	 @res = case @codecompare
		when  0 then (case when @A=@B  then 1 else 0 end) 
		when  1 then (case when @A>@B  then 1 else 0 end)  
		when  2 then (case when @A>=@B then 1 else 0 end)  
		when -1 then (case when @A<@B  then 1 else 0 end)
		when -2 then (case when @A<=@B then 1 else 0 end)  
		else 0  
	 end     
 	-- Return the result of the function
	RETURN @res; 
END
GO