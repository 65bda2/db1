﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<65baa2>
-- Create date: <15.10.2018>
-- Description:	<Функция разделяет строку @text на елементы, в качестве разделителя использует второй параметр @delim,  
--  может быть как один сивол так и строка. П умолчанию разделитель ','
-- результат возращает в виде таблицы с двумя колонками номер элемента по порядку и сам элемент>
-- =============================================
--CREATE FUNCTION <strSplit> 
CREATE FUNCTION [dbo].[SplitStringChar](
	@text VARCHAR(2048),
	@delim VARCHAR(128) = ',' 
)
RETURNS @words TABLE(
	i SMALLINT,
	value VARCHAR(2048)
)
AS 
BEGIN
    DECLARE @word varchar(2048);
	DECLARE @i int=0;
	DECLARE @p int=0;

	SET @i=1;
	SET @p=1;
	WHILE (@p>0)
	BEGIN
	  SET @p=CHARINDEX(@delim,@text);
	  IF @p=0
	  BEGIN
		INSERT INTO @words VALUES(@i,@text);
	  END
	  ELSE
	  BEGIN
		SET @word=left(@text,@p-1);
		INSERT INTO @words VALUES(@i,@word);
		SET @i=@i+1;
		SET @text= substring(@text,@p+LEN(@delim),LEN(@text));
	  END
	END;
	RETURN
END
GO