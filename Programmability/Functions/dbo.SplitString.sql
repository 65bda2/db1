﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--CREATE FUNCTION <SplitString> 
CREATE FUNCTION [dbo].[SplitString](@text varchar(1024))
RETURNS @words TABLE(
	pos smallint,
	value varchar(1024)
)
AS 
BEGIN
set @text = RTRIM(@text)
DECLARE @delims varchar(10) 
SET @delims =' ,:;/-'
DECLARE @pos smallint, @i smallint, @s varchar(1024)

SET @pos=1
WHILE @pos< LEN(@text)
	AND CHARINDEX(SUBSTRING(@text, @pos,1), @delims)>0
	SET @pos =@pos+1
WHILE @pos<=LEN(@text)
BEGIN
	SET @i = PATINDEX('%['+ @delims + ']%',
		SUBSTRING(@text,@pos,len(@text)-@pos+1))
	IF @i>0
	BEGIN
		SET @i=@i+@pos-1
		IF @i>@pos
		BEGIN
			SET @s=SUBSTRING(@text,@pos,@i-@pos)
			INSERT INTO @words
			VALUES (@pos,@s)
		END
		SET @pos=@i+1
		WHILE @pos<LEN(@text)
			AND CHARINDEX(SUBSTRING(@text,@pos,1),@delims)>0
			SET @pos=@pos+1
	END
	ELSE
	BEGIN
		SET @s=SUBSTRING(@text,@pos,LEN(@text)-@pos+1)
		INSERT INTO @words
		VALUES (@pos, @s) SET @pos=LEN(@text)+1
	END
END
RETURN
END
GO