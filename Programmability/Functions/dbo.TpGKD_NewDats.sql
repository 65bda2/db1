﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
------------------------------------------------------ 
--Таб.1. Собираем даты ГКД/ ГДС/ ГС в строку 
--для каждого КО по какждому типу дог.(согл.)(столбец)
------------------------------------------------------ 
CREATE FUNCTION [dbo].[TpGKD_NewDats] (
	   @ko int, @tp char(1), 
	   @Dat1 date
)
RETURNS nvarchar(100)
BEGIN
DECLARE @m int
--dbo.CountTpGKD_Dat (@ko, @tp)
SET @m = (SELECT COUNT(dtGKD) FROM jrGKD WHERE idKO = @ko AND tpGKD = @tp)
DECLARE @z nvarchar(100)

IF @m <= 1 
	 SET @z = (SELECT CASE WHEN g.tpGKD = 'A' THEN 
						   CASE WHEN (g.dtGKDend > CONVERT(date, @Dat1, 104) OR g.dtGKDend IS NULL)
								      THEN dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30)))
						        ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30))) + '*' END 
				 WHEN g.tpGKD = 'C' THEN 
						   CASE WHEN (g.dtGKDend > CONVERT(date, @Dat1, 104) OR g.dtGKDend IS NULL)
								      THEN dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30)))
						        ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30))) + '*' END  
				 WHEN g.tpGKD = 'B' THEN 
						   CASE WHEN (g.dtGKDend > CONVERT(date, @Dat1, 104) OR g.dtGKDend IS NULL)
								      THEN dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30)))
						        ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30))) + '*' END    
				 WHEN g.tpGKD = 'E' THEN 
						   CASE WHEN (g.dtGKDend > CONVERT(date, @Dat1, 104) OR g.dtGKDend IS NULL)
								      THEN dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30)))
						        ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, g.dtGKD) AS nvarchar(30))) + '*' END  
				 ELSE '-'
		     END   
	   FROM jrGKD g 
	 WHERE g.idKO = @ko AND g.tpGKD = @tp) --AND (g.dtGKDend > @Dat OR g.dtGKDend IS NULL))
ELSE
    BEGIN
    --- Курсор ---
    SET @z =''
	DECLARE Cur CURSOR FOR
	        SELECT idKO, tpGKD, dtGKD, dtGKDend 
			  FROM jrGKD 
			 WHERE idKO = @ko AND tpGKD = @tp 
			       --AND (dtGKDend > CONVERT(date, Getdate(), 104) OR dtGKDend IS NULL);
	DECLARE @ii int, @tt char(1), @dd datetime, @d_end datetime
	
	OPEN Cur
	FETCH Cur INTO @ii, @tt, @dd, @d_end
	--Цикл работает , пока есть необработанные записи
	WHILE @@FETCH_STATUS = 0
	   BEGIN
	   SET @z = CASE @z WHEN '' THEN @z 
	                            + CASE WHEN (@d_end > CONVERT(date, @Dat1, 104) OR @d_end IS NULL)
											THEN dbo.textDatDMY(CAST(CONVERT(date, @dd) AS nvarchar(30)))
									   ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, @dd) AS nvarchar(30))) + '*' END 	
						ELSE @z + ' ' + char(13) 
						+ CASE WHEN (@d_end > CONVERT(date, @Dat1, 104) OR @d_end IS NULL)
									THEN dbo.textDatDMY(CAST(CONVERT(date, @dd) AS nvarchar(30)))
							   ELSE '*' + dbo.textDatDMY(CAST(CONVERT(date, @dd) AS nvarchar(30))) + '*' END
				 END;
	   FETCH Cur INTO @ii, @tt, @dd, @d_end;
	   END;
	
	CLOSE Cur;
	DEALLOCATE Cur;
	---------------
	END
SET @z = @z + ' '
RETURN (@z)
END
GO