﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------------------------------------- 
--Функция для форматирования текстовых значений ДАТЫ (ГГГГ-ММ-ДД) --> (ДД.ММ.ГГГГ)
----------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[textDatDMY] (
		@td1 nvarchar(30)
)
RETURNS nvarchar(30)
BEGIN
DECLARE @td2 nvarchar(30)
SET @td2 = ''
SET @td2 = RIGHT(@td1, 2) + '.' + LEFT(RIGHT(@td1, 5),2) + '.' + LEFT(@td1, 4)
	RETURN @td2
END
GO