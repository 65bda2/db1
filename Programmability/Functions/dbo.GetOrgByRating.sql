﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--CREATE FUNCTION <SplitString> 
CREATE FUNCTION [dbo].[GetOrgByRating](@CODE INTEGER) RETURNS varchar(1024) 
AS
BEGIN
DECLARE @sRes varchar(1024)
set @sRes=''
  select @sRes=@sRes+jrOrg.Rating
  from jrOrg.Rating
  left join jrORG ON rfOKVED.code = jrOrg.CodeOKVED
  where CodeOKVED=@CODE
  group by rfOKVED.KOD
  
  RETURN (select @sRes)
END
GO