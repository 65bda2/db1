﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------- 
--Функция для Определения для КО её idBlock
----------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[Opred_idBlock_KO_Kr_podActive](
		@dat date, 
		@ko int
)
RETURNS int
BEGIN
RETURN(SELECT tab.idBlock FROM 
        (SELECT DISTINCT g.nuGKD, CONVERT(date, g.dtGKD) AS dtGKD_g, 
				CONVERT(date, g.dtGKDend) AS dtGKDend_g, g.tpGKD, g.idKO AS KO_GKD, 
				kg.nmKO AS nmKO_GKD, a.idAcc, CONVERT(date, a.dtIncl) AS dtIncl_a, 
				CONVERT(date, a.dtExcl) AS dtExcl_a, a.idKO AS KO_Acc, 
				ka.nmKO AS nmKO_Acc, a.tpCredit, a.mnLimit, b.idBlock 
		  FROM  jrGKD AS g 
				INNER JOIN jrAccount AS a ON g.nuGKD = a.nuGKD
				LEFT OUTER JOIN jrKO AS ka ON a.idKO = ka.idKO 
				LEFT OUTER JOIN jrKO AS kg ON g.idKO = kg.idKO 
				INNER JOIN jrBlock AS b ON a.idAcc = b.idAcc 
		 WHERE  (g.dtGKDend > @dat OR g.dtGKDend IS NULL) 
				AND g.tpGKD = 'B'
				AND a.idKO IN (@ko) --Отбор для конкретной КО
		) AS tab
	   )
END
GO