﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
----------------------------------------------------
-- Переносим основной счет в jrKO
CREATE FUNCTION [dbo].[GetNuAccount](@idKO INTEGER) RETURNS CHAR(20) 
AS
BEGIN
  RETURN(SELECT nuAccount FROM jrKO WHERE idKO = @idKO)
END
GO