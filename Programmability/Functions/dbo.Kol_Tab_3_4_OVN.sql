﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
--------------------------------------------------------------------------------- 
--------------------------------------------------------------------------------- 
-- Функция - Подсчёт количества записей в Таб.3.4_OVN 
-- Обеспечение по кредитам для ОВН (Переход по столбцу (7) из Таб.3.7)
-- по каждому кредиту, обеспечением по которому являются активы.
--( Расчёт Стоимости Актива ) на дату @dat
--------------------------------------------------------------------------------- 
CREATE FUNCTION [dbo].[Kol_Tab_3_4_OVN](
       @ko int, 
       @dat date,  --входной параметр
       @cd int  -- cdStock (кредит)
)
RETURNS int
BEGIN
RETURN(SELECT COUNT(tac.NumActive) AS Kol_Activov_OVN FROM		
(SELECT ROW_NUMBER() OVER (ORDER BY tn.KO_Acc, tn.cdStock, tn.NumActive) AS 'NN', 
       tn.KO_Acc, tn.cdStock, tn.NumActive, tn.UnNum, tn.NomerVD, tn.CreationDate,
       RTRIM(CAST(RTRIM(CONVERT(char(50), tn.NomerVD)) + '/ ' + 
         CONVERT(char(10), dbo.textDatDMY(CONVERT(char(10), CAST(tn.CreationDate AS date)))) AS char(50))) AS NomDateKDV, 
       tn.Company, ISNULL(kk.nmKO, kk.nnKO) AS nmKO_Obyaz_poActivu, 
       tn.StoimostActivov, CAST(tn.DatePayment AS date) AS DatePayment 
  FROM 
 -- Выданные кредиты ОВН -- Универсальный набор
(SELECT  ROW_NUMBER() OVER (ORDER BY t1.KO_Acc, t1.idBlock, t1.cdStock) AS 'NN', 
        t1.KO_Acc, t1.nmKO_Acc, t1.idBlock, t1.cdStock, t1.nuCred, 
        CONVERT(date, t1.dtRekv) AS dtCredOVN, 
        t1.dtPogashKr_tpOVN, 
        t1.mnSaldo / 1000 AS mnSaldo_tis, t1.qnCred, 
	    t1.pcCred, t1.mnZalog / 1000 AS nmZalog_tis, 
	    t1.NmZaemshik, t1.UnNum, t1.NumActive, t1.NomerVD, t1.CreationDate, t1.Company,
	    t1.StoimostActivov, 
	    t1.DatePayment 
  FROM
(SELECT g.nuGKD, CONVERT(date, g.dtGKD) AS dtGKD_g, 
        CONVERT(date, g.dtGKDend) AS dtGKDend_g, g.tpGKD, g.idKO AS KO_GKD, 
        kg.nmKO AS nmKO_GKD, a.idAcc, CONVERT(date, a.dtIncl) AS dtIncl_a, 
        CONVERT(date, a.dtExcl) AS dtExcl_a, a.idKO AS KO_Acc, 
        ka.nmKO AS nmKO_Acc, a.tpCredit, a.mnLimit, b.idBlock, 
        sn.cdStock, sn.nuCred, sn.dtRekv, 
        /*CASE WHEN (SELECT dbo.Zadol_TpCred_OVN_1KO(a.idKO, @dat, sn.nuCred)) = 0 
		        THEN CONVERT(date, DATEADD(day,sn.qnCred,sn.dtRekv)) 
             ELSE NULL 
         END AS dtPogashKr_tpOVN, */
        CONVERT(date, DATEADD(day,sn.qnCred,sn.dtRekv)) AS dtPogashKr_tpOVN,
        sn.mnSaldo, sn.mnProc, 
        sn.qnCred, sn.pcCred, 
        sn.mnZalog, -- !!! У ОВН не может быть замены обеспечения!!!
		act.UnNum, act.NmZaemshik, act.NumActive, act.NomerVD, act.CreationDate, act.Company, 
		StoimostActivov = dbo.Opred_mnActive_OVN_naMaxDat_NEW(@dat, b.idBlock, sn.cdStock, ss.idStock), 
		act.DatePayment
  FROM  jrGKD AS g 
        INNER JOIN jrAccount AS a ON g.nuGKD = a.nuGKD
        LEFT OUTER JOIN jrKO AS ka ON a.idKO = ka.idKO 
        LEFT OUTER JOIN jrKO AS kg ON g.idKO = kg.idKO 
        INNER JOIN jrBlock AS b ON a.idAcc = b.idAcc 
        INNER JOIN hrStockNight AS sn ON b.idBlock = sn.idBlock 
        INNER JOIN hrStockSect AS ss ON sn.cdStock = ss.cdStock AND sn.idBlock = ss.idBlock AND ss.tpSect = '22N' AND ss.dtSect < = @dat
        INNER JOIN jrActive AS act ON ss.idStock = act.NumActive AND ss.tpSect = '22N'
 WHERE  g.idKO = a.idKO -- Головная КО
        AND (g.dtGKDend > @dat OR g.dtGKDend IS NULL) 
        AND CONVERT(date, sn.dtRekv) < = @dat
        --AND sn.tpRekv IN ('A')
        --*Кредиты Овернайт
        AND g.tpGKD = 'A' -- ГКД по 236-П (рыночные активы)
		AND (a.tpCredit IN ('A', 'B')) -- Все; Либо только ОВН.
  ) AS t1
WHERE t1.KO_Acc = @ko
--ORDER BY t1.KO_Acc, t1.idBlock, t1.cdStock
) AS tn
LEFT JOIN jrKO kk ON tn.Company = kk.idKO
WHERE tn.cdStock = @cd
--ORDER BY tn.KO_Acc, tn.cdStock, tn.NumActive
) AS tac
)
END
GO