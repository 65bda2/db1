﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IncWorkDay](@ADate DATETIME, @CountDay SMALLINT = 1)
 RETURNS DATETIME
BEGIN
  IF @CountDay = 0 RETURN @ADate
  DECLARE @i INTEGER
  SET @i = ABS(@CountDay)
  WHILE @i > 0
  BEGIN
    SET @ADate = DATEADD(DAY, SIGN(@CountDay), @ADate)
--    IF DATEPART(WEEKDAY, @ADate) IN (6, 7) -- суббота, воскресенье 
    IF (DATEPART(WEEKDAY, @ADate) + @@DATEFIRST) % 7 IN (0, 1) -- суббота, воскресенье 
    BEGIN 
      IF NOT EXISTS(SELECT dtDay FROM rfDays WHERE dtDay = @ADate AND tpDay = 'W') -- рабочие суббота, воскресенье
        SET @ADate = dbo.IncWorkDay(@ADate, SIGN(@CountDay))
    END
    ELSE
    BEGIN
      IF EXISTS(SELECT dtDay FROM rfDays WHERE dtDay = @ADate AND tpDay = 'F') -- будние дни, являющиеся выходными
        SET @ADate = dbo.IncWorkDay(@ADate, SIGN(@CountDay))
    END
    SET @i = @i - 1
  END
  RETURN @ADate
END
GO