﻿SET QUOTED_IDENTIFIER OFF

SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 14.08.2020
-- проверка jrActive.dopinf(26)
-- AHEBURG-(4365)4505 
-- =============================================
CREATE function [dbo].[fnCheck_dopinfo26] (@NumActive int)
returns varchar(1)
BEGIN
  Declare @result VarChar(1)
  Declare @IdKO Int, @IdOrg Int
  Select @IdKO = l.idKo, @IdOrg = Company From jrActive a Join jrLetter l On a.idLetter=l.idLetter Where a.NumActive = @NumActive;
  If (Not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(@IdOrg, @IdKO, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and
                   idKO = @IdKO And idOrg = @IdOrg) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null))
   
    Set @result = 'T' 
  Else 
    Set @result = 'F'
  return @result
END
GO