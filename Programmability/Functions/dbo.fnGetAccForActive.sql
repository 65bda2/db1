﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Author:		65vev3
-- Create date: 25.11.2019
-- Description:	Выбор действующего счета для актива
-- =============================================
CREATE FUNCTION [dbo].[fnGetAccForActive]
( @NumActive Int )
RETURNS Int
AS
BEGIN
  Declare @idAcc Int, @dtWork DateTime
  Select @dtWork = dtWork From rfStatus 
  Select 
     @idAcc = c.idAcc 
  From
     jrActive a Join jrLetter  l On a.idLetter = l.idLetter
                Join jrAccount c On c.idKO     = l.idKo
                Join jrGKD     g On g.nuGKD    = c.nuGKD And ( g.dtGKDEnd IS NULL OR g.dtGKDEnd > @dtWork And
 				                                               ( ( g.tpGKD <> 'K' And ( g.dtGKDStop IS NULL OR g.dtGKDStop >  @dtWork ) ) Or 
				                                                 ( g.tpGKD =  'K' And g.dtEKDBegin <=  @dtWork ) ) )
  Where 
    NumActive = @NumActive
    And ( Case g.tpGKD When 'K' Then c.tpAsset Else g.tpGKD End ) + a.TypeActive IN ( 'DM', 'FE', 'GI', 'BD' )
    And c.dtIncl <= @dtWork
    And ( c.dtExcl Is Null or c.dtExcl >= @dtWork )
	  
  RETURN( @idAcc )
END
GO