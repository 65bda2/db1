﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[LeftNuRegStr](@NuReg VARCHAR(9))
RETURNS VARCHAR(9)
BEGIN
	SET @nuReg = REPLACE(@nuReg, '\', '/')
	IF CHARINDEX('/', @nuReg) > 0 
		SET @nuReg= LEFT(@nuReg, CHARINDEX('/', @nuReg) - 1)
	ELSE
		SET @nuReg= RTRIM(@nuReg)
	RETURN(@nuReg)
END
GO