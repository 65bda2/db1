﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetListTUByHub]()
RETURNS TABLE 
AS
RETURN 
(
SELECT KPTU as CKPSubdivision
  ,d.NAME_RUS as CNAMESubdivision
  FROM rfSubdivision r
  left join rfDKKP d ON d.KP=r.KPTU
  WHERE r.KPHub=(SELECT [KPHub] FROM rfSubdivision rr
  WHERE rr.KPTU=(SELECT [nmSign] FROM rfSign WHERE tpSign='kodtu'))
)
GO