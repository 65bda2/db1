﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
---------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------- 
--Функция для Определения МАХ-даты (hrStockSect.dtSect) в рамках idBlock 
----------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[Opred_Max_dtSect_po_idBlock](
		@dat date, 
		@idB int
)
RETURNS date
BEGIN
RETURN(SELECT CONVERT(date, MAX(t1.dtSect)) dtSect_max 
					  FROM (SELECT ss.idStock, ss.idBlock, ss.tpSect, ss.dtSect
							  FROM hrStockSect ss
							 WHERE ss.dtSect <= @dat
							   AND ss.idBlock = @idB
							   --AND ss.tpSect IN ('1', '1U', '1C', '11D', '11Z', '11U', '12L', '12C', '12U', '15', '21L', '22N', '21C', '21U', '3L', '3N')
							) AS t1
							GROUP BY t1.idBlock)
END
GO