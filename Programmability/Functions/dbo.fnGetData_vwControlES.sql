﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnGetData_vwControlES]
(
@EDType VARCHAR(4),
@XMLBody XML,
@R VARCHAR(22)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @XML VARCHAR(MAX)
	DECLARE @V	 VARCHAR(MAX)
	DECLARE @S   VARCHAR(MAX)
	DECLARE @B	 BIT = 0
	DECLARE @Tbl TABLE (XMLBody XML)	

	SET @XML = CONVERT(VARCHAR(MAX), @XMLBody)

	IF CHARINDEX('encoding', @XML) > 0
		SET @XML = SUBSTRING(@XML, CHARINDEX('>', @XML, 1) + 1, LEN(@XML))

	SET @V = '<?xml version="1.0" encoding="windows-1251"?>'

	WHILE CHARINDEX('xsi:', @XML) > 0 
	BEGIN
		SET @S = @XML	
		SET @S = SUBSTRING(SUBSTRING(@S, CHARINDEX('xsi:', @S), LEN(@S)), CHARINDEX('"', SUBSTRING(@S, CHARINDEX('xsi:', @S), LEN(@S)))+1, LEN(SUBSTRING(@S, CHARINDEX('xsi:', @S), LEN(@S))))
		SET @S = SUBSTRING(@S, CHARINDEX('"', @S)+1, LEN(@S))
		SET @XML = SUBSTRING(@XML, 1, CHARINDEX('xsi:', @XML)-1) + @S
	END

	WHILE CHARINDEX('xmlns', @XML) > 0
	BEGIN
		SET @S = @XML
		SET @S = SUBSTRING(SUBSTRING(@S, CHARINDEX('xmlns', @S), LEN(@S)), CHARINDEX('"', SUBSTRING(@S, CHARINDEX('xmlns', @S), LEN(@S)))+1, LEN(SUBSTRING(@S, CHARINDEX('xmlns', @S), LEN(@S))))
		SET @S = SUBSTRING(@S, CHARINDEX('"', @S)+1, LEN(@S))
		SET @XML = SUBSTRING(@XML, 1, CHARINDEX('xmlns', @XML)-1) + @S
	END

	IF CHARINDEX('''',@XML)> 0 
		SET @B = 1

	SET @V = @V + REPLACE(REPLACE(@XML, 'edo:', ''), '''', '&quot;')

	INSERT INTO @Tbl 
	VALUES (CONVERT(XML, @V))

	SET @V = ''

	IF @EDType = 'C101' 
	BEGIN
		IF @R = 'BillNum' 
			SELECT @V = L.value('@BillNum[1]','varchar(15)') 
			FROM @Tbl 
			CROSS APPLY	XMLBody.nodes('/C101/AssetsInfo') AS x(L)

		IF @R = 'BillDate' 
			SELECT @V = L.value('@BillDate[1]','varchar(15)') 
			FROM @Tbl 
			CROSS APPLY	XMLBody.nodes('/C101/AssetsInfo') AS x(L)

		IF @R = 'DebtorRegID' 			
			SELECT @V = L.value('@DebtorRegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)			
											
		IF @R = 'ShortName' 
			SELECT @V =L.value('@ShortName[1]','varchar(128)')
			FROM @Tbl
			CROSS APPLY XMLBody.nodes('/C101/OrgInfo') AS x(L)
			
		IF @R = 'ShortName+DebtorRegID' 
			SELECT @V = t1.ShortName+ ' ('+t2.DebtorRegID+')'
			FROM (SELECT L.value('@ShortName[1]','varchar(128)') as ShortName, L.value('@RegID[1]','varchar(13)') as RegID
				  FROM @Tbl
			      CROSS APPLY XMLBody.nodes('/C101/OrgInfo') AS x(L)) as t1 INNER JOIN
				 (SELECT L.value('@DebtorRegID[1]','varchar(13)') as DebtorRegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)) as t2
				 ON t1.RegID = t2.DebtorRegID

		IF @R = 'ShortNameDebtor' 
			SELECT @V = t1.ShortName			
			FROM (SELECT L.value('@ShortName[1]','varchar(128)') as ShortName, L.value('@RegID[1]','varchar(13)') as RegID
				  FROM @Tbl
			      CROSS APPLY XMLBody.nodes('/C101/OrgInfo') AS x(L)) as t1 INNER JOIN
				 (SELECT L.value('@DebtorRegID[1]','varchar(13)') as DebtorRegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)) as t2
				 ON t1.RegID = t2.DebtorRegID

		IF @R = 'ObligedRegID' 
			SELECT @V = L.value('@ObligedRegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)			
				
		IF @R = 'ShortName+ObligedRegID' 
			SELECT @V = t1.ShortName+ ' ('+t2.ObligedRegID+')'
			FROM (SELECT L.value('@ShortName[1]','varchar(128)') as ShortName, L.value('@RegID[1]','varchar(13)') as RegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/OrgInfo') AS x(L)) as t1 INNER JOIN
				 (SELECT L.value('@ObligedRegID[1]','varchar(13)') as ObligedRegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)) as t2
				 ON t1.RegID = t2.ObligedRegID

		IF @R = 'ShortNameObliged' 
			SELECT @V = t1.ShortName
			FROM (SELECT L.value('@ShortName[1]','varchar(128)') as ShortName, L.value('@RegID[1]','varchar(13)') as RegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/OrgInfo') AS x(L)) as t1 INNER JOIN
				 (SELECT L.value('@ObligedRegID[1]','varchar(13)') as ObligedRegID
				  FROM @Tbl
				  CROSS APPLY XMLBody.nodes('/C101/AssetsInfo') AS x(L)) as t2
				 ON t1.RegID = t2.ObligedRegID
	END			
    --AheBurg afi 1098
	IF @EDType = 'C102' 
	BEGIN
		IF @R = 'TypeRepmnt'
			SELECT @V = XMLBody.value('(/C102/PropsList//TypeRepmnt/node())[1]','varchar(1)')
			FROM @Tbl

		IF @R = 'Sum' -- AHEBURG-3391 65bda2		
			SELECT @V = L.value('@Sum[1]','varchar(17)')
			FROM @Tbl
			CROSS APPLY XMLBody.nodes('/C102/RepaySchedInfo') AS x(L)			

	END
	
	IF @EDType = 'C104' 
	BEGIN
		IF @R = 'RegID'
			SELECT @V = L.value('@RegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C104') AS x(L)

		IF @R = 'ShortName' 
			SELECT @V = L.value('@NewValue[1]','varchar(128)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C104/Block/ShortName') AS x(L)

		IF @R = 'NewRegID' 
			SELECT @V = L.value('@NewValue[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C104/Block/RegID') AS x(L)
	END
	
	IF @EDType = 'C105' AND @R = 'RegID' 
		SELECT @V = L.value('@RegID[1]','varchar(13)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C105') AS x(L)

	IF @EDType = 'C106' AND @R = 'RegID' 
		SELECT @V = L.value('@RegID[1]','varchar(13)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C106') AS x(L)
	
	IF @EDType = 'C107'
	BEGIN 
		IF @R = 'TypeOrgChange' 
			SELECT @V = L.value('@TypeOrgChange[1]','varchar(1)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C107') AS x(L)
			
		IF @R = 'ShortName' 
			SELECT @V = L.value('@ShortName[1]','varchar(128)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C107/OrgInfo') AS x(L)
	
		IF @R = 'RegID'
			SELECT @V = L.value('@RegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C107/OrgInfo') AS x(L)
	END
						  
	IF @EDType = 'C200' AND @R = 'AssetNum' 
	BEGIN
		SELECT @V = @V + L.value('@AssetNum[1]','varchar(16)')+'; ' FROM @Tbl CROSS APPLY XMLBody.nodes('/C200/AssetsInfo') AS x(L)			
		SET @V = SUBSTRING(@V,1, Len(@V)-2)
	END
	
	IF @EDType = 'C201' AND @R = 'ProcessType' 
		SELECT @V = L.value('@ProcessType[1]','varchar(1)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C201') AS x(L)
	
	IF @EDType = 'C202' 
	BEGIN
		IF @R = 'BillNum' 
			SELECT @V = L.value('@NewValue[1]','varchar(15)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C202/PropsListExt/BillNum') AS x(L)
			
		IF @R = 'BillDate'
			SELECT @V = L.value('@NewValue[1]','varchar(15)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C202/PropsListExt/BillDate') AS x(L)
			
		--AHEBURG-107	
		IF @R = 'AssetStatus'
			SELECT @V = L.value('@AssetStatus[1]','varchar(1)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C202') AS x(L)	
			
	END		
	
	IF @EDType = 'C204' 
	BEGIN
		IF @R = 'ProcessCode' 
			SELECT @V = L.value('@ProcessCode[1]','varchar(1)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C204') AS x(L)
			
		IF @R = 'RegID'
			SELECT @V = L.value('@RegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C204') AS x(L)
			
		IF @R = 'ShortName'
			SELECT @V = L.value('@NewValue[1]','varchar(128)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C204/PropsListExt/ShortName') AS x(L) --AHEBURG-1746, 65ShitovAV, replace block with PropsListExt

		IF @R = 'NewRegID'
			SELECT @V = L.value('@NewValue[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C204/PropsListExt/RegID') AS x(L)	--AHEBURG-1746, 65ShitovAV, replace block with PropsListExt
	END
	IF @EDType = 'C205' 
	BEGIN
		IF @R = 'ProcessType'
			SELECT @V = L.value('@ProcessType[1]','varchar(1)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C205') AS x(L)

		IF @R = 'RegID'
			SELECT @V = L.value('@RegID[1]','varchar(13)')
			FROM @Tbl
			CROSS APPLY	XMLBody.nodes('/C205') AS x(L)
	END
	
	IF @EDType = 'C206' AND @R = 'RegID'
		SELECT @V = L.value('@RegID[1]','varchar(13)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C206') AS x(L)
	
	IF @EDType = 'C207' AND @R = 'ProcessCode' 
		SELECT @V = L.value('@ProcessCode[1]','varchar(1)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C207') AS x(L)
	
	IF @EDType = 'C208' AND @R = 'ResultCode'
		SELECT @V = L.value('@ResultCode[1]','varchar(1)')
		FROM @Tbl
		CROSS APPLY	XMLBody.nodes('/C208') AS x(L)

	RETURN @V

END
GO