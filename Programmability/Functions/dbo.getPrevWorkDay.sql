﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[getPrevWorkDay](@ADate DATETIME)
--********************
-- Функция getNextWorkDay ()
-- =============================================
-- Author:<65baa2>
-- Create date: <31.05.2019>
-- Description:	<Функция возвращает предыдущий дате @ADate рабочий день.>
-- =============================================

RETURNS DATETIME
BEGIN
  SET @ADate=DATEADD(day,-1,@ADate);
  WHILE (SELECT dbo.isworkday(@ADate))=0 
  BEGIN
	SET @ADate=DATEADD(day,-1,@ADate);
  END;
  RETURN @ADate
END
GO