﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[StartGetTableList] (@IsHUB BIT)
RETURNS varchar(8000)
AS
BEGIN
  DECLARE @tblList varchar (8000)
  IF @IsHUB = 1 
	SET @tblList = 'hub_rfBuhRecord,hub_rfBuhPeriod,hub_rfBuhFactorType,hub_rfTpClassBuh,hub_rfOKVED,'+--hub_rfOKVED2_R,
                                    'hub_rfOKOPF,hub_rfOKFS,hub_rfOKATO,hub_rfDKKP,hub_hrWaitCheck,hub_hrActive,hub_DKO_hrRepayment,hub_DKO_hrMutualParticipation,'+
                                    'hub_DKO_hrCompany,hub_DKO_hrBankInTU,hub_DKO_hrAssetState,hub_DKO_hrAsset,hub_hrBuhRecord,hub_hrOrg,hub_hrFactor,hub_jrCurrency,'+ 
                                    'hub_jrRKC,hub_rfDays,hub_rfFactor,hub_rfSignRekvizitASPPA,hub_rfSignASPPA,hub_rfSign,hub_rfReportsASPPA,hub_rfOrgList,hub_rfBuhFactor,'+
                                    'hub_rfASPPARestriction,hub_jrOrg,hub_jrKO,hub_hrRateExch,hub_JrBuhRecord,hub_jrGKD,hub_jrDopLetter,hub_hrOwnFunds,hub_jrLetter,hub_rfAffPers,'+
                                    /*'hub_rfFaceKO,*/'hub_jrAccount,hub_rfGKD,hub_JrJournalLetter,hub_jrBlock,hub_jrActive,hub_rfDopLetActive,hub_jrRequestDocActive,hub_jrPaymentGrafic,'+
                                    'hub_lgArchActive,hub_jrTransh,hub_jrNoteActive,hub_jrJournalActive,hub_hrStockSect,hub_hrPaymentGrafic,hub_hrActiveEvent,hub_hrStockPawn,hub_LastAssetNumber'+
                                    ',hub_rfStatus,hub_Counters,hub_hrTypeCredits' --AHEBURG-1418*65baa2
  ELSE
	SET @tblList = 'hub_rfBuhRecord,hub_rfBuhPeriod,hub_hrWaitCheck,hub_hrActive,hub_DKO_hrRepayment,hub_DKO_hrMutualParticipation,'+
                                    'hub_DKO_hrCompany,hub_DKO_hrBankInTU,hub_DKO_hrAssetState,hub_DKO_hrAsset,hub_hrBuhRecord,hub_hrOrg,hub_hrFactor,hub_jrCurrency,'+ 
                                    'hub_jrRKC,'+
                                    'hub_jrOrg,hub_jrKO,hub_JrBuhRecord,hub_jrGKD,hub_jrDopLetter,hub_hrOwnFunds,hub_jrLetter,hub_rfAffPers,'+
                                    /*'hub_rfFaceKO,*/'hub_jrAccount,hub_rfGKD,hub_JrJournalLetter,hub_jrBlock,hub_jrActive,hub_rfDopLetActive,hub_jrRequestDocActive,hub_jrPaymentGrafic,'+
                                    'hub_lgArchActive,hub_jrTransh,hub_jrNoteActive,hub_jrJournalActive,hub_hrStockSect,hub_hrPaymentGrafic,hub_hrActiveEvent,hub_hrStockPawn,hub_LastAssetNumber'+
                                    ',hub_rfStatus,hub_Counters,hub_hrTypeCredits' --AHEBURG-1418*65baa2
  RETURN @tblList
END
GO