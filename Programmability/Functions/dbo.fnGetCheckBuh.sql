﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE function [dbo].[fnGetCheckBuh](@idOrgBuhReport int)
RETURNS @tab TABLE(
	Numkod int ,
	RValue decimal)
AS 
BEGIN
	INSERT @tab
	SELECT  Numkod, ShowingAccSum as Rvalue
	FROM
		jrOrgBuhReport
		left outer join jrOrgBuhRecord on jrOrgBuhRecord.idOrgBuhReport = jrOrgBuhReport.idOrgBuhReport
		left outer join rfBuhRecord on jrOrgBuhRecord.idBuhRecord = rfBuhRecord.idBuhRecord
	WHERE
		jrOrgBuhReport.idOrgBuhReport = @idOrgBuhReport
	RETURN 
END
GO