﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		Бряков Д.А
-- Create date: 28.01.2020
-- Description:	Возвращает список отличающихся реквизитов
-- =============================================
CREATE FUNCTION [dbo].[fnDiffC101Org] (@idES1 int, @idES2 int) --@idES1 - исходное ,  @idES2 - найденное
RETURNS varchar(1000)
AS
BEGIN

	DECLARE @s varchar(1000)
	SET @s = ''   	
  
	SELECT @s = @s + b.NmFieldText + ', ' 
	FROM (
			(SELECT * FROM (SELECT idField, NewValue FROM EDOKO_jrChangeORG WHERE idES = @idES1) a1)
			 EXCEPT 
			(SELECT * FROM (SELECT idField, NewValue FROM EDOKO_jrChangeORG WHERE idES = @idES2) a2)
		  ) a INNER JOIN EDOKO_ListChangeField b ON a.idField = b.idField
  	
	IF LEN(@s) > 0
		SET @s= SUBSTRING(@s,1,len(@s)-1)

	RETURN @s

END
GO