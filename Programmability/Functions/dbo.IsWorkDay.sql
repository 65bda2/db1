﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[IsWorkDay](@ADate DATETIME)
 RETURNS TINYINT 
BEGIN
  DECLARE @Result TINYINT
  SET @Result = 1
    IF (DATEPART(WEEKDAY, @ADate) + @@DATEFIRST) % 7 IN (0, 1) -- суббота, воскресенье 
    BEGIN 
      IF NOT EXISTS(SELECT dtDay FROM rfDays WHERE dtDay = @ADate AND tpDay = 'W') -- рабочие суббота, воскресенье
        SET @Result = 0 
    END
    ELSE
    BEGIN
      IF EXISTS(SELECT dtDay FROM rfDays WHERE dtDay = @ADate AND tpDay = 'F') -- будние дни, являющиеся выходными
        SET @Result = 0 
    END
  RETURN (@Result)
 END
GO