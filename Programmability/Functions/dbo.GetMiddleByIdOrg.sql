﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetMiddleByIdOrg] 
(
	@id int --idOrg
)
RETURNS @temp table(
	idOrg int,							-- ID организации для Entity
	ShortNmORG varchar(255),			-- краткое наименование организации
	Rating varchar(2),					-- рейтинг запрошенной организации
	Middle decimal(10,2)				-- средняя по отрасли(модели)
)
AS
BEGIN

DECLARE @MODEL integer; -- модель
DECLARE @OKVED varchar(1024); -- полный список кодов для модели
DECLARE @KOD varchar(8); -- код ОКВЭД для организации до точки
DECLARE @Rating varchar(2); -- УКС
DECLARE @ShortNmORG varchar(255); -- наименование
DECLARE @Middle decimal(10,2); -- средняя

SET @Rating=dbo.GetLastRatingByIdOrg(@id); -- последний УКС организации]
SET @KOD=(select top 1 o.KOD from jrOrg jo, rfOKVED o where o.CODE = jo.CodeOKVED AND jo.idOrg=@id);
SET @MODEL=[dbo].[GetModelByOKVED] (@KOD);
SET @OKVED=[dbo].[GetOKVEDByModel] (@MODEL);
SET @Middle = NULL;
SET @ShortNmORG = (select TOP 1 jrORG.ShortNmORG from jrORG where idOrg=@id);

WITH OUT1 (idOrg,ShortNmORG,R) AS 
	(
		SELECT o.idOrg, o.ShortNmORG, R=[dbo].[GetLastRatingByIdOrgAsInt](o.idOrg)
			FROM jrOrg o 
			left join rfOKVED ok ON ok.Code = o.CODEOKVED
			WHERE substring(ok.KOD,1,2) in (select value from SplitString(@OKVED)) AND
			[dbo].[GetLastRatingByIdOrgAsInt](o.idOrg)>0 -- без учета дефолта. Заменить на -1, если понадобится
	)
SELECT TOP 1 @Middle=CONVERT(decimal(10,2),SUM(R))/CONVERT(decimal(10,2),COUNT(idOrg)) from OUT1;

IF ISNUMERIC(@Middle)=1 --OR @Rating='D'
	insert into @temp values (@id,@ShortNmORG,@Rating,@Middle);    
-- Return the result of the function
RETURN 
END
GO