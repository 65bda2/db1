﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnIsDistinctVarchar]
-- =============================================
-- Author:      <65FedorovEV>
-- Create date: <2019-12-21>
-- Description:	<Функция выполняет аналог @A is distinct from @B>
-- =============================================
(@A Varchar(8000), @B Varchar(8000))
RETURNS bit
AS
BEGIN
  RETURN case when (@A <> @B or @A is null or @B is null) and not (@A is null and @B is null) then 1 else 0 end;
END
GO