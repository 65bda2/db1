﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 29.01.2020
-- проверка jrActive.dopinf(27)
-- =============================================
-- Изменения AHEBURG-(4365)4505 65MakarovDA от 14.08.2020 
-- =============================================
-- Изменения AHEBURG-(4676) 65MakarovDA от 26.08.2020 
-- =============================================
-- Изменения (AHEBURG-4806) 65MakarovDA от 03.09.2020 Добавлено поле StartDate - Дата начала отчетного периода  
-- =============================================
CREATE function [dbo].[fnCheck_dopinfo27] (@NumActive int)
returns varchar(1)
BEGIN
  Declare @IdKO Int, @IdOrg Int, @result VarChar(1), @isToExcludeRep bit, @dtWork datetime, @dtExclude datetime, @BeginDate datetime
  ,@ObPeriod bit = 1
  Set @result = 'F'
  Select @BeginDate=a.Begindate, @dtExclude=a.dtExclude, @isToExcludeRep=a.isToExcludeRep, @IdKO = l.idKo, @IdOrg = Company 
  From jrActive a Join jrLetter l On a.idLetter=l.idLetter Where a.NumActive = @NumActive;
  select top 1 @dtWork=dtWork from rfStatus
  IF DATEPART(m,@dtWork) = 4 AND DATEPART(d,@dtWork) <= 3 AND
	   NOT EXISTS(SELECT idPeriod FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod=1)
		SET @ObPeriod = 0
  If @isToExcludeRep=0 and @dtExclude is null and
    Exists(Select * From 
                   (Select top 1 * From dbo.fnGetActualBuhPeriod(@IdOrg, @IdKO, DEFAULT ) where obPeriod=@ObPeriod --1 
                   order by idPeriod desc) LastPeriod --последний период
                   Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = @IdKO And idOrg = @IdOrg) b
                    On LastPeriod.idPeriod = b.idPeriod 
                   where (@BeginDate < LastPeriod.StartDate and b.DateReport<LastPeriod.StartDate)--ReportDate)
                   or (@BeginDate >= LastPeriod.StartDate)
             )
    Set @result = 'T' 
   
  
  If @isToExcludeRep=1 and @dtExclude is null and
    (Not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(@IdOrg, @IdKO, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = @IdKO And idOrg = @IdOrg) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null))
   
    Set @result = 'T' 
  
  if @dtExclude is not null and @dtExclude<=@dtWork and
    (Not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(@IdOrg, @IdKO, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = @IdKO And idOrg = @IdOrg and DateReport<@dtExclude) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null))
   
   Set @result = 'T' 
  
   
  /* AHEBURG-(4676) 65MakarovDA от 26.08.2020
  If (Not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(@IdOrg, @IdKO, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isArchive=0 And 
                   isRuleControl=1 and (isCorrect is null or isCorrect=1) and
                   idKO = @IdKO And idOrg = @IdOrg) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null))
   
    Set @result = 'T' 
  Else 
    Set @result = 'F'
  */
  /* 11.08.2020 Select @IdKO = l.idKo, @IdOrg = Company From jrActive a Join jrLetter l On a.idLetter=l.idLetter Where a.NumActive = @NumActive
  
  If Not Exists( Select * From 
                   dbo.fnGetActualBuhPeriod(DEFAULT, DEFAULT, DEFAULT ) periods 
                   Left Outer Join (select * from jrOrgBuhReport where isCorrect= 1 and isArchive=0 And idKO = @IdKO And idOrg = @IdOrg) b
                    On periods.idPeriod = b.idPeriod 
                 Where 
                   periods.obPeriod = 1 And  b.idOrgBuhReport is null)
    Set @result = 'T' 
  Else 
    Set @result = 'F'
  */
  /*if exists(select top 1 1 from jrActive a  join jrLetter l on a.idLetter=l.idLetter join jrOrgBuhReport b on b.idKO = l.idKO  and
  b.idOrg = a.Company where a.NumActive=@NumActive and isArchive=0 and isRuleControl=1 and idperiod in 
  (select idPeriod from dbo.fnGetActualBuhPeriod(DEFAULT, DEFAULT,@dtWork)))  set @result='T' else set @result='F'*/
  
  return @result
END
GO