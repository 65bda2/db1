﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[getNextWorkDay](@ADate DATETIME)
--********************
-- Функция getNextWorkDay ()
-- =============================================
-- Author:<65baa2>
-- Create date: <31.05.2019>
-- Description:	<Функция проверяет является ли дата @ADate не рабочим днем, если да, то возвращает следующий ближайший рабочий день,
-- иначе возвращается дата @ADate>
-- =============================================

RETURNS DATETIME
BEGIN
  WHILE (SELECT dbo.isworkday(@ADate))=0 
  BEGIN
	SET @ADate=DATEADD(day,1,@ADate);
  END;
  RETURN @ADate
END
GO