﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnSplitFormula] 
(
--********************
-- ПЗ 6.2.1.5 	Хранимая процедура fnSplitFormula (Расчет УКС)
-- =============================================
-- Author:<65baa2>
-- Create date: <01.10.2018>
-- Description:	<Функция раделяет текст формулы на составляющие.
-- Выделяется код из переменных по шаблону 'SXXXX' или 'SXXXXP' (где ХХХХ соответствует KodNum) и текст формулы,
-- результат помещается в таблицу.
-- Функция используется в хранимой процедуре spCalcByFormula2>
-- =============================================
	@formula varchar(2000) -- текст формулы расчета из таблицs rfCalcCoefficient
)
RETURNS @temp table(
	i int,					-- номер по порядку
	flag int,				-- признак:  0- текст формулы  1-переменная (SXXXX); 2-переменная за предыдущий период (SXXXXP)  
	variant varchar(1024)	-- текст формулы или код 'XXXX' из шаблона 'SXXXX' (SXXXXP) 
)
AS
BEGIN
    declare @f varchar(2000)=@formula;
	declare @w varchar(2000)='';
	declare @n int=0;
	declare @i int=1;
	declare @p int=1;
	while (@p>0)
	begin
	  set @p=PATINDEX('%S[0-9][0-9][0-9][0-9]%',@f) -- поиск по шаблону 'SXXXX' (где ХХХХ числовой код) 
      if @p=0 and len(@f)>0 
      begin
		 insert into @temp values (@i,0,@f);
		 set @i=@i+1;
      end
      else
      begin
		  if @p>1  
		  begin
			select @w = LEFT(@f,@p-1),@f=SUBSTRING(@f,@p,LEN(@f));
			insert into @temp values (@i,0,@w);
			set @i=@i+1;
		  end;
		  if len(@f)>0 
		  begin
			if SUBSTRING(@f,6,1)='P' set @n=6 else set @n=5; -- проверка на соответствие шаблону 'SXXXXP'
			select @w = LEFT(@f,@n),@f=SUBSTRING(@f,@n+1,LEN(@f));
			insert into @temp values (@i,@n-4,replace(replace(@w,'S',''),'P',''));
			set @i=@i+1;
	      end
	  end
	end;
 	-- Return the result of the function
	RETURN 
END
GO