﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetBuhValue](@idPeriodBO int,@idOrg int,@idOrgBuhReport int)
RETURNS @words TABLE(
	RValue decimal(25,5),
	RvalueM decimal(25,5),
	NMClass varchar(500),
	TpClass varchar(500),
	ForSort int,
	idBuhRecord int,
	kod varchar(10),
	numkod varchar(10),
	isrequired int,
	Numorder int,
	NMrequired varchar(5)
	 
)
AS 
BEGIN
       --Если не БО предоставлена,  то все данные берутся из справочника rfBuhRecord,иначе из таблицы jrOrgBuhRec
  IF NOT EXISTS(SELECT * FROM jrOrgBuhReport WHERE idOrgBuhReport=@idOrgBuhReport)
        INSERT INTO @words(RValue,RvalueM,NMClass,TpClass,ForSort,idBuhRecord,kod,numkod,isrequired,Numorder,NMrequired )
                    SELECT NULL,NULL ,NMClass, TpClass, 1,NULL,NULL,NULL,0,1,''  FROM rfTpClassBuh  WHERE idClass=(SELECT idclass FROM rfBuhPeriod WHERE idperiod=@idPeriodBO) 
                                                                                          AND tpclass <> 777   
                    UNION ALL  
                    SELECT NULL,m.ShowingAccSum,a.NMRecord,a.TpClass, 2, a.idBuhRecord,a.kod,a.numkod,a.isrequired,NumOrder,(Case a.isrequired when 1 then 'Да' else 'Нет' end) FROM rfBuhRecord a 
                    LEFT JOIN jrOrgBuhRecord m  ON  m.idBuhRecord = a.idBuhRecord  AND m.idOrgBuhReport =  (SELECT TOP 1 idOrgBuhReport FROM jrOrgBuhReport mc WHERE  mc.idperiod=@idPeriodBO  AND mc.idOrg=@idOrg  AND mc.isReferense=1 and isArchive=0 )
                    where a.TpClass<>777  AND a.IsactiveRecord=1 
               
    ELSE    
        INSERT INTO @words(RValue,RvalueM,NMClass,TpClass,ForSort,idBuhRecord,kod,numkod,isrequired,Numorder ,NMrequired) 
                    SELECT NULL,NULL, NMClass,TpClass,1 ,NULL AS idBuhRecord,NULL,NULL,0,1,'' FROM rfTpClassBuh  WHERE idClass=(SELECT idclass FROM rfBuhPeriod WHERE idperiod=@idPeriodBO)
                                                                                                                      AND tpclass <> 777 
                    UNION ALL 
                    SELECT jrorg.ShowingAccSum,m.ShowingAccSum,rfb.NMRecord,rfb.TpClass,2,rfb.idBuhRecord,rfb.kod, rfb.numkod,rfb.isrequired,Numorder,(Case rfb.isrequired when 1 then 'Да' else 'Нет' end)     
                    FROM jrOrgBuhRecord jrorg
                    INNER JOIN rfBuhRecord rfb ON rfb.idBuhRecord = jrorg.idBuhRecord AND rfb.TpClass<>777  
                    LEFT JOIN jrOrgBuhRecord m  ON  m.idBuhRecord = jrorg.idBuhRecord  AND m.idOrgBuhReport =  (SELECT TOP 1 idOrgBuhReport FROM jrOrgBuhReport mc WHERE  mc.idperiod=@idPeriodBO  AND mc.idOrg=@idOrg  AND mc.isReferense=1 and isArchive=0 )
                    WHERE jrorg.idOrgBuhReport = @idOrgBuhReport                  
RETURN
END
GO