﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetTUByKO]
(	
	@idKO int
) RETURNS Integer
AS
BEGIN
DECLARE @outTU int
DECLARE @V1 int
DECLARE @V2 int=NULL
DECLARE @V2temp int
DECLARE @V3 int

-- idKO=>idSubdivision=>CKPSubdivision Если пусто, то смотрим в ОКАТО
SET @V1=(select s.KPTU CKPSubdivision from rfSubdivision s, jrKO k where k.idSubdivision=s.idSubdivision and k.idKO=@idKO)

-- idKO=>Left(OKATO,2)
SET @V2temp=(select Rtrim(Left(k.OKATO,2)) from jrKO k where k.idKO=@idKO)
SET @V2 = case when @V2temp<>0 then @V2temp end; --если ОКАТО=0000000, то смотрим в текущий хаб

SET @V3=(select nmsign from rfSignRekvizitASPPA where tpsign='kodtu') -- если первый и второй варианты NULL, то берем номер из хаба

SET @outTU=ISNULL(@V1,ISNULL(@V2,@V3))
RETURN @outTU
END
GO