﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--CREATE FUNCTION <SplitString> 
CREATE FUNCTION [dbo].[FillAssetList]
(	
	-- Add the parameters for the function here
	@TA char(1),  --TypeActive (D or M) 
	@GKD char(1), --tpGKD      (K or D) 
	@Pool bit, --  in pool = 1, out pool = 0
	@OD datetime --  OperDate. Don`t use
)
RETURNS @temp table(
	numActive int, 
	unNum char(16), 
	TypeActive varchar(2)
)
AS
BEGIN

IF @Pool=1
BEGIN
  WITH OUT1 (numActive)
  AS
  ( SELECT distinct numActive from hrActiveSect has 
	left join jrAccount	ON jrAccount.idAcc=has.idAcc 
	left join jrGKD ON jrGKD.nuGKD=jrAccount.nuGKD AND tpGKD=@GKD)

insert into @temp SELECT distinct jrActive.numActive, unNum, TypeActive FROM jrActive, OUT1 WHERE TypeActive=@TA AND (status = 'T' OR jrActive.numActive=OUT1.numActive) ORDER BY unNum

END

ELSE
BEGIN
insert into @temp SELECT distinct numActive, unNum, TypeActive FROM jrActive WHERE TypeActive=@TA ORDER BY unNum
END

RETURN 

END
GO