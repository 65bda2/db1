﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
------------------------------------------------------------------ 
--Определяет Филиалы для указанной КО
--Используется в Таб.1 и Таб.2
------------------------------------------------------------------ 
CREATE FUNCTION [dbo].[FilKO] (
		@ko int
)
RETURNS TABLE
AS RETURN(SELECT DISTINCT tab.idKO_GKD, tab.nmKO_GKD, tab.idKO_Acc, tab.nuReg_Acc, tab.nmKO_Acc
			FROM
				(SELECT g.nuGKD, g.dtGKD, g.dtGKDend, g.tpGKD, g.idKO AS idKO_GKD, 
						kg.nmKO AS nmKO_GKD, kg.nuReg AS nuReg_GKD,
						a.idAcc, a.dtIncl, a.dtExcl, a.idKO AS idKO_Acc, 
						ka.nmKO AS nmKO_Acc, ka.nuReg AS nuReg_Acc, a.tpCredit, a.mnLimit
				   FROM jrGKD g 
						INNER JOIN jrAccount a ON g.nuGKD = a.nuGKD
						LEFT JOIN jrKO ka ON a.idKO = ka.idKO
						LEFT JOIN jrKO kg ON g.idKO = kg.idKO
				  WHERE	kg.idKO <> ka.idKO
				) tab
		   WHERE tab.idKO_GKD = @ko)
GO