﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnSplitFormulaEx]
--********************
-- ПЗ 6.1.1.7 	Хранимая процедура fnSplitFormulaEx
-- =============================================
-- Author:<65baa2>
-- Create date: <01.11.2018>
-- Description:	<Функция раделяет текст формулы на составляющие.
-- Выделяется код из переменных по шаблону 'XXXX' где ХХХХ соответствует KodNum и текст формулы,
-- результат возращается в виде таблицы.
-- Функция используется в хранимой процедуре 
-- =============================================
(
	@formula varchar(max), -- текст формулы расчета из таблицs
	@beg varchar(1),	-- символ начала, например '['
	@end varchar(1)		-- символ окончания, например ']'
	
)
RETURNS @temp table(
	i int,					-- номер по порядку
	flag int,				-- признак:  0- текст формулы  1-переменная (XXXX); 
	variant varchar(1024)	-- текст формулы  или  ХХХХ числовой код соответствует KodNum)  
)
AS
BEGIN
    declare @char varchar(1)='';
	declare @word varchar(max)='';
	declare @state int=0;
	declare @ee int=0;
	declare @n int=1;
	declare @cnt int=len(@formula);
	declare @i int=1;

	while @n<=@cnt
	begin
		set @char = substring(@formula,@n,1);
		if @char=@beg set @state=1
		else 
		if @char=@end set @state=2
		else
		begin
			set @word=@word+@char;
			if @n=@cnt set @state=1;
		end
	  
		if @state>0
		begin
			if LEN(@word)>0 
			begin
				insert into @temp values (@i,@state-1,@word);
				set @i=@i+1;
			end   
			set @word='';
			set @state=0;
		end
		set @n=@n+1;
	end;
	
	RETURN 
END
GO