﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

--------------------------------------------------------------------------------
-------------------------------------------------------------------------------- 
--Функция определения Стоимости Актива по передаваемым: МАХ-Дате и idStock
--Доработано, ошибка в Таб.3.4 - (Кредиты под Активы) - устранена(22.05.2014)
-------------------------------------------------------------------------------- 
CREATE FUNCTION [dbo].[Opred_mnActive] (
	@dat date,
	@idS int,
	@tpS char(3),
	@idB int,
	@cd int
)
RETURNS money
BEGIN
RETURN(SELECT (hss.mnCostCoef * hss.qnStock) 
                        FROM hrStockSect hss
                       WHERE CONVERT(date, hss.dtSect) = @dat
                         AND hss.idStock = @idS
                         AND hss.tpSect = @tpS
                         AND hss.idBlock = @idB
                         AND hss.cdStock = @cd
       )
END
GO