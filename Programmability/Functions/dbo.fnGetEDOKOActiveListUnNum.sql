﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Бряков Д.А.
-- Editor:		Бряков Д.А.
-- Create date: 08.04.2019
-- EditDate     29.07.2019
-- Description:	Формирование списка уникальных активов из EDOKO_ActiveList по ЭС
-- =============================================
CREATE FUNCTION [dbo].[fnGetEDOKOActiveListUnNum]
(
@idES int
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @V VARCHAR(MAX) = ''

    SELECT @V = @V + t.UnNum + ';' FROM (SELECT DISTINCT UnNum FROM EDOKO_ActiveList WHERE idES = @idES) as t ORDER BY t.UnNum

	if LEN(@V) > 1 SET @V = SUBSTRING(@V,1,LEN(@V)-1)
	
	RETURN @V
END
GO