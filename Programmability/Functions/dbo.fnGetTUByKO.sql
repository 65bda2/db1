﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnGetTUByKO]
-- =============================================
-- Author:		<65baa2>
-- Create date: <06/28/2019,>
-- Description:	<Функция возвращает код ТУ для КО>
-- =============================================
(	
	@idKO int
) RETURNS varchar(2) 
AS
BEGIN
	DECLARE @V1 varchar(2)
	DECLARE @V2 varchar(2)
	DECLARE @V3 varchar(2)
	-- idKO=>idSubdivision=>KPTU Если пусто, то смотрим в ОКАТО
	SET @V1=(select s.KPTU from rfSubdivision s, jrKO k where k.idSubdivision=s.idSubdivision and k.idKO=@idKO)
	-- idKO=>Left(OKATO,2)
	SET @V2=(select Rtrim(Left(k.OKATO,2)) from jrKO k where k.idKO=@idKO)
	SET @V2=case when @V2='00' then NULL else @V2 end; --если ОКАТО=0000000, то смотрим в текущий хаб
	SET @V3=(select nmsign from rfSignRekvizitASPPA where tpsign='kodtu') 
	RETURN COALESCE(@V1,@V2,@V3)
END
GO