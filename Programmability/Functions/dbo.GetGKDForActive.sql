﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetGKDForActive](@idko INTEGER,@TPGKD CHAR(1))
RETURNS INTEGER
BEGIN
  DECLARE @Result INTEGER
  SET @Result=
      ISNULL((select top 1 jrgkd.nugkd from jrgkd INNER JOIN jrAccount ON jrGKD.nuGKD = jrAccount.nuGKD AND jrAccount.tpAsset in (@TPGKD) where tpGKD ='K' AND dtEKDBegin <=(select dtwork from rfstatus) and jrAccount.idko=@idko order by dtGKD desc),
      ISNULL((select top 1 jrgkd.nugkd from jrgkd INNER JOIN jrAccount ON jrGKD.nuGKD = jrAccount.nuGKD where tpGKD in (@TPGKD) AND jrAccount.idko=@idko order by dtGKD desc),0))    
  RETURN @Result
END
GO