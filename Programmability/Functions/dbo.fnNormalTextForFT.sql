﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnNormalTextForFT] (@STR varchar(255))RETURNS VARCHAR(255)
AS
BEGIN
declare @result varchar(1000)

  select @result = REPLACE(@STR, ' ', ' CHR(0)')
  select @result = REPLACE(@result, 'CHR(0) ', '')
  select @result = REPLACE(@result, 'CHR(0)','')

  select @result =  REPLACE(@result,NCHAR(39),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(187),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(171),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(8222),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(8221),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(8249),NCHAR(34))
  select @result =  REPLACE(@result,NCHAR(8250),NCHAR(34))

  select @result = LOWER(@result)
  
  return @result
END
GO