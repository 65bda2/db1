﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetModelByOKVED](@KOD varchar(1024)) RETURNS INTEGER
AS
BEGIN
	DECLARE @iRes integer
	set @iRes=0
	select TOP 1 @iRes=idCalcModel
		from rfCalcModelOKVED
		left join rfOKVED ON rfCalcModelOKVED.code = rfOKVED.code
		where KOD=@KOD
	RETURN @iRes
END
GO