﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnSplitFormulaControlRule]
--********************
-- ПЗ 6.1.1.7 	Хранимая процедура fnSplitFormulaControlRule 
-- =============================================
-- Author:<65baa2>
-- Create date: <01.11.2018>
-- Description:	<Функция раделяет текст формулы на составляющие.
-- Выделяется код из переменных по шаблону '[XXXX]' где ХХХХ соответствует KodNum и текст формулы,
-- результат возращается в виде таблицы.
-- Функция используется в хранимой процедуре spCalcByFormulaControlRule>
-- =============================================
(
	@formula varchar(1024) -- текст формулы расчета из таблицs 
	--,@isPrev bit = 0  -- использовать значение 0-ShowingAccSum или 1-ShowingAccSumPrev
)
RETURNS @temp table(
	i int,					-- номер по порядку
	flag int,				-- признак:  0- текст формулы  1-переменная ([XXXX]); 
	variant varchar(1024)	-- текст формулы  или  ХХХХ числовой код соответствует KodNum)  
)
AS
BEGIN
    declare @f varchar(1024)=@formula;
	declare @w varchar(1024)='';
	declare @n int=0;
	declare @i int=1;
	declare @p int=1;
	while (@p>0)
	begin
	  set @p=PATINDEX('%[[0-9][0-9][0-9][0-9]]%',@f) -- поиск по шаблону '[XXXX]' (где ХХХХ числовой код) 
      if @p=0 and len(@f)>0 
      begin
		 insert into @temp values (@i,0,@f);
		 set @i=@i+1;
      end
      else
      begin
		  if (@p-1)>1  
		  begin
			select @w = LEFT(@f,@p-2),@f=SUBSTRING(@f,@p-1,LEN(@f));
			insert into @temp values (@i,0,@w);
			set @i=@i+1;
		  end;
		  if len(@f)>0 
		  begin
			select @w = LEFT(@f,6),@f=SUBSTRING(@f,7,LEN(@f));
			insert into @temp values (@i,1,replace(replace(@w,'[',''),']',''));
			set @i=@i+1;
	      end
	  end
	end;
	RETURN 
END
GO