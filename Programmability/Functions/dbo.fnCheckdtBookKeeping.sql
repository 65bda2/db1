﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 23.01.2020
-- проверка актуальности даты расчета показателей БО  
-- =============================================
CREATE function [dbo].[fnCheckdtBookKeeping] (@NumActive int)
returns bit
BEGIN
  declare @result bit
  declare @idOrg int
  declare @idKO int
  declare @dtBookKeeping datetime
  declare @isBookKeepingOK tinyint
  select @idOrg=Company, @idKO=idKo from jrActive a join jrLetter l on a.idLetter=l.idLetter where a.NumActive=@NumActive
  SELECT @dtBookKeeping=dtBookKeeping, @isBookKeepingOK=isBookKeepingOK FROM jrORG WHERE idOrg = @idOrg
  if exists(select top 1 1 from (SELECT MAX(ReportDate) as MaxReportDate  FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod = 1) a 
  where a.MaxReportDate<@dtBookKeeping) 
  or
  exists(select top 1 1 from (SELECT MAX(ReportDate) as MaxReportDate  FROM dbo.fnGetActualBuhPeriod(@idOrg, @idKO, DEFAULT) WHERE obPeriod = 0) a 
  where a.MaxReportDate<@dtBookKeeping)  
  set @result=1 else set @result=0
  return @result
END
GO