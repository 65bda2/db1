﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
--------------------------------------------------------------------------------- 
--------------------------------------------------------------------------------- 
-- Функция: (вызывается в Таб. 4.8)
-- Определения Суммы расчетного лимита ВДК и ОВН,
-- устанавливаемого по основному счёту, тыс. руб.
-- Укорачиваю (убираю лишние поля) для ускрения.
--------------------------------------------------------------------------------- 
-- !!! 15.12.2014 Исправления (dtUstan - вместо dtOrder)
-- 1) С вечера устанавливается Лимит ВДК и ОВН на завтра. 
-- 2) Но и в течение дня тоже может быть установлен новый Лимит.
-- 3) Тогда берём лимит на dtUstan по последнему idOrder (т.е. MAX)
--------------------------------------------------------------------------------- 
CREATE FUNCTION [dbo].[Opred_SUM_RLimVDK_OVN] (
	@d date,
	@ko int,
	@dat date, 
	@dat2 date
)
RETURNS money
AS 
BEGIN
DECLARE @Zn money
SET @Zn = (SELECT tab2.mnUst_tis 
             FROM (SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY ord.dtUstan DESC, 
                                                ord.idOrder,  ka.idKO) AS 'NN', 
                CONVERT(date, ord.dtUstan) AS SDat, 
				ka.idKO, g.dtGKD, g.dtGKDend, g.tpGKD,  
				a.dtIncl, a.dtExcl, a.idAcc, a.tpCredit,
				bl.idBlock, ord.idOrder , 
		        ISNULL(ord.mnUst/1000, 0) AS mnUst_tis, 
		        ord.isUstan, ord.dtUstan, ord.tmOrder, 
		        ord.tpOrder 
		   FROM jrGKD g 
				INNER JOIN jrAccount a ON g.nuGKD = a.nuGKD
				LEFT JOIN jrKO ka ON a.idKO = ka.idKO
				LEFT JOIN jrKO kg ON g.idKO = kg.idKO
		   	    INNER JOIN jrBlock bl ON a.idAcc = bl.idAcc
			    INNER JOIN lgOrder ord ON bl.idBlock = ord.idBlock 
WHERE  	Right(Left(ka.nuBIK, 4), 2) = dbo.Opred_KodTU() --Оставляем только наш Регион
        AND ka.idKO = @ko
        AND ord.dtUstan = @d 
		-- Дата расторжения генерального кредитного договора - пустая.
		AND (g.dtGKDend < @dat OR g.dtGKDend IS NULL) 
		-- Дата исключения основного счета из ГКД - пустая.
		AND (a.dtExcl > @dat OR a.dtExcl IS NULL) 
		--AND g.tpGKD = 'A' --СЭУ говорят, что может быть не только тип A--
		AND a.tpCredit <> 'C' -- Тип кредитов: А (Все кредиты) 
		                      -- и В (Только ВДК и Овернайт)
		AND ord.isUstan = 'T' -- Признак - Лимит установлен
		AND ord.tpOrder = 'D' -- Тип: Распоряжение  на установку лимита 
		                      -- внутридневного кредита
		AND ord.dtUstan BETWEEN @dat AND @dat2 -- Устанавливаем текущую дату
		-- Если определять 1ю запись за день как MIN ключ idOrder в таб. lgOrder
		AND ord.idOrder = (SELECT MAX(l.idOrder) 
		                     FROM lgOrder l 
		                    WHERE l.dtUstan = ord.dtUstan 
		                                      AND l.idBlock = bl.idBlock)
		) AS tab2
	WHERE tab2.dtUstan = @d AND tab2.idKO  = @ko
	)
RETURN @Zn
END
GO