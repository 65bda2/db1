﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

----------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[GetKOName] (@idKO INT)
RETURNS VARCHAR(250)
AS
BEGIN
	DECLARE @KOName VARCHAR(250)
	SET @KOName = ISNULL((SELECT TOP 1 nmKO FROM [jrKO] WHERE [idKO] = @idKO), '')
	RETURN(@KOName)
END;
GO