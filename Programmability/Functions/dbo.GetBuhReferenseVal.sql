﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetBuhReferenseVal] (
		@reportdate DateTime,
		@idorg int,
		@idMainDataES int
		
)
RETURNS TABLE
AS RETURN(	SELECT a.Code as code, a.RValue FROM ASBO_BuhValue a 
            JOIN  rfBuhRecord rbr ON a.code=rbr.numkod AND rbr.isASBO=1 
            JOIN jrOrgBuhRecord jbr ON rbr.idBuhRecord = jbr.idBuhRecord 
            JOIN rfBuhPeriod rfp on rfp.reportdate=a.reportdate AND rfp.reportdate=@reportdate   
            JOIN jrOrgBuhReport r ON r.idOrgBuhReport = jbr.idOrgBuhReport AND r.isReferense=1 AND r.idperiod= rfp.idperiod AND  r.isArchive=0
            WHERE rbr.NumKod <> 777 
            AND r.idorg=@idorg  
            AND a.idMainDataES=@idMainDataES)
GO