﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--Функция Стоимость конкретного Актива (idStock, dtSect_max, mnActive_Max)
--Свежая версия на 1-но значение
--Доработано, ошибка в Таб.3.4 - (Кредиты под Активы) - устранена(22.05.2014)
--------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[Opred_mnActive_naMaxDat_NEW] (
	@dat date, 
	-- Для Таб.3.4 передаём в @dat значение @dat
	-- Для Таб.4.4 передаём в @dat значение @dat2
	@idB int,
	@cd int,
	@idS int
)
RETURNS money
BEGIN
RETURN (SELECT t2.mnActive_Max FROM
(SELECT t1.idStock, CONVERT(date, MAX(t1.dtSect_d)) AS dtSect_max,
       mnActive_Max = (dbo.[Opred_mnActive](CONVERT(date, MAX(t1.dtSect_d)), t1.idStock, t1.tpSect, t1.idBlock, t1.cdStock))
  FROM 
(SELECT DISTINCT ss2.idBlock, ss2.cdStock, ss2.idStock,  ss2.mnCost, ss2.qnStock, 
        CONVERT(date, ss2.dtSect) AS dtSect_d, mnActive = (ss2.mnCostCoef * ss2.qnStock),  ss2.tpSect  
   FROM hrStockSect ss2
		 WHERE ss2.tpSect = '21L'
		   AND ss2.idBlock = @idB
		   AND ss2.cdStock = @cd --= ss.cdStock
		   --AND ss2.idStock = @idS 
		   AND CONVERT(date, ss2.dtSect) < = CONVERT(date, @dat)
 ) AS t1
 WHERE t1.idStock = @idS
GROUP BY t1.idStock, t1.tpSect, t1.tpSect, t1.idBlock, t1.cdStock) AS t2
) 
END
GO