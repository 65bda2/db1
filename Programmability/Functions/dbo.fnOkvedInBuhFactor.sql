﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fnOkvedInBuhFactor](@idorg int, @OKVED char(8))
RETURNS TINYINT 
BEGIN
	DECLARE @Result TINYINT
	DECLARE @priz INT
	DECLARE @N_RAZDEL CHAR(1)
  
	SET @Result = 1;
  
	if (select businessQuality from jrorg where idorg=@idorg) = 2 
		set @priz = 1
	else
		set @priz = 2

	WHILE (( SELECT count(a.OKVED) 
			from(
				select top 1 --Проверка присутствия в справочнике rfBuhFactor допустимых значений rfBuhFactor.DOPUST типа rfBuhFactor.TpFactor «П2» и «П3»
					rfBuhFactor.OKVED 
				from  
					rfBuhFactor 
				where 
					TpFactor='П2'
					and PRIZ=@priz 
					and OKVED=@OKVED 
					and REP_Date = (Select MAX(b.Rep_date) 
									from  rfBuhFactor b 
									where rfBuhFactor.tpfactor=b.tpfactor and rfBuhFactor.priz=b.priz) 
				UNION ALL
				select  top 1 --Проверка присутствия в справочнике rfBuhFactor допустимых значений rfBuhFactor.DOPUST типа rfBuhFactor.TpFactor «П2» и «П3»
					rfBuhFactor.OKVED 
				from  
					rfBuhFactor 
				where 
					TpFactor='П3'
					and PRIZ=@priz 
					and OKVED=@OKVED 
					and REP_Date = (Select MAX(b.Rep_date) 
									from  rfBuhFactor b 
									where rfBuhFactor.tpfactor=b.tpfactor and rfBuhFactor.priz=b.priz) ) a) <> 2)		
	BEGIN
		IF (LEN(ltrim(rtrim(@OKVED)))<=2)
		BEGIN
			SET @Result = 0;
			BREAK;
		END
		ELSE
		BEGIN
			set @OKVED = SUBSTRING(@OKVED, 1,LEN(ltrim(rtrim(@OKVED)))-1)
		END
	END
	IF @Result = 0
	BEGIN
		set @N_RAZDEL = (Select top 1 N_RAZDEL from rfOKVED where SUBSTRING(KOD,1,2) = @OKVED)	
		IF ( SELECT count(a.OKVED) 
			from(
				select top 1 
					rfBuhFactor.OKVED 
				from  
					rfBuhFactor 
				where 
					TpFactor='П2'
					and PRIZ=@priz 
					and OKVED in (SELECT KOD FROM rfOKVED WHERE N_RAZDEL = @N_RAZDEL) 
					and REP_Date = (Select MAX(b.Rep_date) 
									from  rfBuhFactor b 
									where rfBuhFactor.tpfactor=b.tpfactor and rfBuhFactor.priz=b.priz) 
				UNION ALL
				select  top 1 
					rfBuhFactor.OKVED 
				from  
					rfBuhFactor 
				where 
					TpFactor='П3'
					and PRIZ=@priz 
					and OKVED in (SELECT KOD FROM rfOKVED WHERE N_RAZDEL = @N_RAZDEL)  
					and REP_Date = (Select MAX(b.Rep_date) 
									from  rfBuhFactor b 
									where rfBuhFactor.tpfactor=b.tpfactor and rfBuhFactor.priz=b.priz) ) a) <> 2
			SET @Result = 0
		else
			SET @Result = 1
	END	
  RETURN (@Result)
END
GO