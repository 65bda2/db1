﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartCheckOwnFund] 
@MSG varchar(max) OUTPUT,
@ErrorMSG varchar(max) OUTPUT
AS
BEGIN
BEGIN TRY
	DECLARE @nuAccount varchar(20)
	DECLARE @nuGKD int
	DECLARE @mnOwnFund money

	DECLARE @nuAccount_ varchar(20)
	DECLARE @nuGKD_ int
	DECLARE @mnOwnFund_ money

	DECLARE @MsgText Varchar(3000)
	--DECLARE @MsgFull Varchar(max)

	SET @nuAccount=''
	SET @nuGKD=0
	SET @MsgText = 'Выявлены расхождения при сравнении величины собственных средств (капитала) для КО: '
	SET @MSG = ''

	DECLARE cr CURSOR
	FOR	SELECT nuAccount ,nuGKD, mnOwnFund
		FROM (
			SELECT a.idKO, b.nuAccount, b.nuGKD, ISNULL(c.mnOwnFund,0) as mnOwnFund
			FROM [jrAccount] a INNER JOIN
				(SELECT nuGKD, nuAccount FROM jrAccount GROUP BY nuGKD, nuAccount HAVING COUNT(*) > 1) b ON  a.nuAccount = b.nuAccount and a.nuGKD = b.nuGKD
				Left join (hrOwnFunds c inner join (select idKO, max(dtOwnFund) as dtOwnFund from hrOwnFunds group by idKO) d on c.idKO=d.idKO and c.dtOwnFund = d.dtOwnFund) on a.idKO = c.idKO
			) Tbl
		GROUP BY nuAccount, nuGKD, mnOwnFund
		HAVING COUNT(*) = 1

	OPEN cr

	FETCH NEXT FROM cr
	INTO @nuAccount, @nuGKD, @mnOwnFund

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF (@nuAccount<>@nuAccount_) AND (@nuGKD<>@nuGKD_) 
		BEGIN
			SET @MsgText = @MsgText + (SELECT a.nuReg+', ' FROM jrKO a INNER JOIN jrAccount b ON a.idKO=b.idKO WHERE b.nuAccount = @nuAccount AND b.nuGKD = @nuGKD FOR XML Path(''))
			SET @MSG = @MSG + SUBSTRING(@MsgText,1,LEN(@MsgText)-1)+'\n'
		END

		SET @nuAccount_=@nuAccount
		SET @nuGKD_=@nuGKD
		SET @MsgText = 'Выявлены расхождения при сравнении величины собственных средств (капитала) для КО: '

		FETCH NEXT FROM cr
		INTO @nuAccount, @nuGKD, @mnOwnFund
	END
	CLOSE cr
	DEALLOCATE cr
	 RETURN 1
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
END
GO