﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[selSysProcesses](@base_name NVARCHAR(128))
WITH EXECUTE AS SELF
AS
BEGIN
   SELECT 
     p.spid, RTRIM( nmRealUser ) AS nmRealUser, RTRIM( loginame ) AS loginame, 
	    RTRIM( p.hostname ) AS hostname, login_time, last_batch, CAST( 'АС Сибирь'  AS VarChar(15) ) AS ASName
   FROM 
     master..sysprocesses p 
     JOIN master..sysdatabases d ON p.dbid = d.dbid
     JOIN jrCurUsers u ON p.spid = u.spid AND p.hostName = CAST( u.hostName AS Char(50) ) COLLATE Cyrillic_General_CI_AS 
   WHERE d.name = @base_name AND p.hostname <> HOST_NAME() AND   p.program_name='siberia.exe' COLLATE Cyrillic_General_CI_AS
   
     UNION 
   
   SELECT 
        p.spid, RTRIM( nmRealUser ) AS nmRealUser, RTRIM( loginame ) AS loginame, 
	    RTRIM( p.hostname ) AS hostname, login_time, last_batch, CAST(  'АС ППА'  AS VarChar(15) ) AS ASName 
   FROM 
     master..sysprocesses p 
     JOIN master..sysdatabases d ON p.dbid = d.dbid
     JOIN jrCurUsersASPPA u ON p.spid = u.spid AND p.hostName = CAST( u.hostName AS Char(50) ) COLLATE Cyrillic_General_CI_AS
    WHERE d.name = @base_name  AND p.program_name='asppa.exe' COLLATE Cyrillic_General_CI_AS
END
GO