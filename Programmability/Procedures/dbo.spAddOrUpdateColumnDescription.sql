﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:	65baa2
-- Create date: 2018.09.30
-- Description:	процедура для добавления (если такого еще нет) или обновления(если такоге уже есть) свойства Description для заданной колонки в заданной таблице
-- =============================================
CREATE PROCEDURE [dbo].[spAddOrUpdateColumnDescription]
	@TVname varchar (24) ,-- имя таблицы или view
	@columnName varchar (24) ,-- имя колонки
	@Description varchar (255) -- знчение для свойства Description;
AS
BEGIN
	DECLARE @type varchar(8)='';
	SELECT @type=case type when 'U' then 'TABLE' when 'V' then 'VIEW' else '' end  FROM sysobjects WHERE name=@TVName AND type in ('U','V');
   	IF NOT EXISTS (select 1 from fn_listextendedproperty('MS_Description','schema','dbo',@type,@TVname,'COLUMN',@columnName))
		EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=@Description, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=@type,@level1name=@TVname, @level2type=N'COLUMN',@level2name=@columnName
    ELSE
		EXEC sys.sp_updateextendedproperty @name=N'MS_Description', @value=@Description, @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=@type,@level1name=@TVname, @level2type=N'COLUMN',@level2name=@columnName
END
GO