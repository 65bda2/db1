﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartMakeDKO] 
@IsHUB BIT,
@ErrorMSG varchar(max) output ,
@TU varchar(2)
AS

BEGIN TRY
  
  Declare @IdUpdate Int 

  If @IsHUB = 1 
  Begin
    Select @IdUpdate = IsNull( ValueCounter, 0 ) From Counters Where NameCounter = 'DKO_IdUpdate'
      
    If @IdUpdate > 0
    Begin
      SET IDENTITY_INSERT DKO_IdUpdate ON
      INSERT INTO DKO_IdUpdate ( IdUpdate ) VALUES ( @IdUpdate )
      SET IDENTITY_INSERT DKO_IdUpdate OFF
    End 
  End  

  If @IsHUB = 0 
  BEGIN

    Declare @DFrom SmallDateTime, @KodHub Char(2)
  
    Select @DFrom = Cast( IsNull( dtWork, GetDate() ) As SmallDateTime ) From rfStatus
    Select @KodHub = nmSign FROM rfSign WHERE tpSign= 'KodTU'
  
  
    ---- Перенос DKO_BankInTU ----  
    ---- КО без дублей ( Correction_Type = 2, изменяется только поле TU ) ---- 
  
    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  
    
    INSERT INTO DKO_hrBankInTU ( _IdKO, IdBankInTU, D_From, IdUpdate, StateSED, CorrectionType, BankRegNum, IsPartnerOfMSP, AlterComment, TU )
      Select 
        _IdKO
        , IdBankInTU
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By _IdKO Asc ) 
        , StateSED = 0
        , CorrectionType = 2
        , BankRegNum
        , IsPartnerOfMSP
        , AlterComment
        , @KodHub
      From
        DKO_hrBankInTU b Join hub_Keys k on b._IdKO = k.idHubKey And b.TU = k.TU
      Where 
        b.TU = @TU 
        And b.StateSED = 2
        And b.IsArchive = 0
        And k.NameTable = 'jrKO'
        And IsNull( k.isReplace, 0 ) = 0
      Order By
        _IdKO  
  
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrBankInTU b Join hub_Keys k on b._IdKO = k.idHubKey And b.TU = k.TU
      Where 
        b.TU = @TU 
        And b.StateSED = 2
        And b.IsArchive = 0
        And k.NameTable = 'jrKO'
        And IsNull( k.isReplace, 0 ) = 0
      Order By
        _IdKO 
    ---- Перенос DKO_BankInTU ---- 
    ---- КО без дублей ( Correction_Type = 2, изменяется только поле TU ) ---- 
    ---- Окончание ---- 
  
  
    ---- Перенос DKO_hrCompany ---- 
    ---- Организации без дублей ( Correction_Type = 2, изменяется только поле TU ) ---- 
    
    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  

    INSERT INTO DKO_hrCompany ( _IdOrg, IdCompany, D_From, IdUpdate, StateSED, CorrectionType, INN, EGRUL, CompanyName, CodeOKVED, CodeOKOPF, CodeOKFS,
                                SubjectType, IsConforming, IsInList, IsResident, BusinessQuality, D_LastRegDate, IsBookKeepingOK, AlterComment, TU ) 
      Select 
        _IdOrg
        , IdCompany
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By _IdOrg Asc ) 
        , StateSED = 0
        , CorrectionType = 2 
        , INN
        , EGRUL
        , CompanyName
        , CodeOKVED
        , CodeOKOPF
        , CodeOKFS
        , SubjectType
        , IsConforming
        , IsInList
        , IsResident
        , BusinessQuality
        , D_LastRegDate
        , IsBookKeepingOK
        , AlterComment
        , @KodHub
      From
        DKO_hrCompany c Join hub_Keys k on c._IdOrg = k.idHubKey And c.TU = k.TU
      Where 
        c.TU = @TU 
        And c.StateSED = 2
        --And ( c.IsArchive = 0 Or @TU = '57' )
        And k.NameTable = 'jrOrg'
        And IsNull( k.isReplace, 0 ) = 0   
      Order By
        _IdOrg
        
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrCompany c Join hub_Keys k on c._IdOrg = k.idHubKey And c.TU = k.TU
      Where 
        c.TU = @TU 
        And c.StateSED = 2
        --And ( c.IsArchive = 0 Or @TU = '57' )
        And k.NameTable = 'jrOrg'
        And IsNull( k.isReplace, 0 ) = 0   
      Order By
        _IdOrg
    ---- Перенос DKO_hrCompany ----
    ---- Организации без дублей ( Correction_Type = 2, изменяется только поле TU ) ----
    ---- Окончание ---- 
  
  
    ---- Создаём список организаций дублей, для удаления
    Declare @DelCompany Table ( idOrg        Int, 
                                idCompanyOld Numeric(16,0) Null,
                                idCompanyNew Numeric(16,0) Null,
                                TuEtalon     Char(2)       Null )
    
    Insert Into @DelCompany ( idOrg, idCompanyOld )
      Select 
        _IdOrg, IdCompany
      From
        DKO_hrCompany c Join hub_Keys k on c._IdOrg = k.idHubKey And c.TU = k.TU
      Where 
        c.TU = @TU 
        And c.StateSED = 2
        And c.IsArchive = 0
        And k.NameTable = 'jrOrg'
        And k.isReplace = 1
      Order By
        _IdOrg
        
    Update d Set idCompanyNew = c.IdCompany 
    From 
      @DelCompany d Join hub_Keys k On d.idOrg = k.idHubKey
                    Join DKO_hrCompany c On c.TU = k.TU And c._idOrg = d.IdOrg
    Where
      k.NameTable = 'jrOrg'
      And k.isReplace = 0
      And c.StateSED = 2
    ---- Окончание cоздания список организаций дублей, для удаления 
    
  
    ---- Перенос DKO_hrAsset
    ---- Активы
    
    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  
    
    INSERT INTO DKO_hrAsset ( 
      _NumActive, IdAsset, D_From, IdUpdate, StateSED, IsArchive, CorrectionType, 
      AssetType, AssetCode, AssetIssue, AssetNumber, AssetDate, 
	  D_Repayment, FaceValue, FaceValueCurrency, FvCurrencyRate, 
	  PurchaseValue, PurchaseCurrency, IsActive, AssetQuality, 
	  IdIssuer, IdOrgToCheck, 
	  GKDNum, GKDDate, MainAccount, PayOrgBIC, 
	  TUHolder, 
	  DebtorRegNum, AlterComment, isGuaranteedByRF, isMinFinAgreed, isGuaranteedByCrimea, OriginalsState )
    
      Select 
        _NumActive
        , IdAsset
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By _NumActive Asc ) 
        , StateSED = 0
        , IsArchive = 0
        , CorrectionType = 2    
        , AssetType, AssetCode, AssetIssue, AssetNumber, AssetDate
        , D_Repayment, FaceValue, FaceValueCurrency, FvCurrencyRate
        , PurchaseValue, PurchaseCurrency, IsActive, AssetQuality 
        , IdIssuer = ISNULL( d1.idCompanyNew, IdIssuer )
        , IdOrgToCheck = ISNULL( d2.idCompanyNew, IdOrgToCheck )
	    , GKDNum, GKDDate, MainAccount, PayOrgBIC
	    , TUHolder = @KodHub
	    , DebtorRegNum, AlterComment, isGuaranteedByRF, isMinFinAgreed, isGuaranteedByCrimea, OriginalsState
      From
        DKO_hrAsset a Left Outer Join @DelCompany d1 On a.IdIssuer = d1.idCompanyOld
                      Left Outer Join @DelCompany d2 On a.IdOrgToCheck = d2.idCompanyOld      
      Where
        a.TUHolder = @TU
        And a.StateSED = 2 
        And a.IsArchive = 0
      Order By
        _NumActive  
    
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrAsset a Left Outer Join @DelCompany d1 On a.IdIssuer = d1.idCompanyOld
                      Left Outer Join @DelCompany d2 On a.IdOrgToCheck = d2.idCompanyOld      
      Where
        a.TUHolder = @TU
        And a.StateSED = 2 
        And a.IsArchive = 0
      Order By
        _NumActive
    ---- Перенос DKO_hrAsset
    ---- Активы
    ---- Окончание
  
  
    ---- Перенос DKO_hrCompany ----
    ---- Организации дубли ( Correction_Type = 4 ) ---- 
    
    --- До удаления дублей надо убрать аффилированность для организаций

    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  
    
    INSERT INTO DKO_hrMutualParticipation
      ( _IdRecord, IdMutualParticipation, D_From, IdUpdate, StateSED, CorrectionType, IdCompany, CORegNum, IsCompanyOwner, AlterComment )
      Select
        m._IdRecord
        , m.IdMutualParticipation
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By m.IdRecord Asc )
        , StateSED = 0
        , CorrectionType = 4
        , m.IdCompany
        , m.CORegNum
        , m.IsCompanyOwner
        , m.AlterComment
      From
        DKO_hrMutualParticipation m Join @DelCompany c On m.IdCompany = c.idCompanyOld
      Where 
        m.StateSED = 2      
      Order By
        m.IdRecord     
    
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrMutualParticipation m Join @DelCompany c On m.IdCompany = c.idCompanyOld
      Where 
        m.StateSED = 2      
      Order By
        m.IdRecord 
    --- окончание удаления аффилированности для организаций ----
        
        
    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  

    INSERT INTO DKO_hrCompany ( _IdOrg, IdCompany, D_From, IdUpdate, StateSED, CorrectionType, INN, EGRUL, CompanyName, CodeOKVED, CodeOKOPF, CodeOKFS,
                                SubjectType, IsConforming, IsInList, IsResident, BusinessQuality, D_LastRegDate, IsBookKeepingOK, AlterComment, TU ) 
      Select 
        _IdOrg
        , IdCompany
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By _IdOrg Asc ) 
        , StateSED = 0
        , CorrectionType = 4 
        , INN
        , EGRUL
        , CompanyName
        , CodeOKVED
        , CodeOKOPF
        , CodeOKFS
        , SubjectType
        , IsConforming
        , IsInList
        , IsResident
        , BusinessQuality
        , D_LastRegDate
        , IsBookKeepingOK
        , AlterComment
        , @TU
      From
        DKO_hrCompany c Join @DelCompany d On c.IdCompany = d.idCompanyOld
      Where 
        c.TU = @TU 
        And c.StateSED = 2
        And c.IsArchive = 0
      Order By
        _IdOrg
        
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrCompany c Join @DelCompany d On c.IdCompany = d.idCompanyOld
      Where 
        c.TU = @TU 
        And c.StateSED = 2
        And c.IsArchive = 0
      Order By
        _IdOrg
    ---- Перенос DKO_hrCompany ---- 
    ---- Организации дубли ( Correction_Type = 4 ) ---- 
    ---- Окончание ---- 


    ---- Перенос DKO_BankInTU ---- 
    ---- КО дубли ( Correction_Type = 4 ) ---- 
    
    -- Выбор последнего IdUpdate
    SELECT @IdUpdate = IDENT_CURRENT( 'DKO_IdUpdate' )  
  
    INSERT INTO DKO_hrBankInTU ( _IdKO, IdBankInTU, D_From, IdUpdate, StateSED, CorrectionType, BankRegNum, IsPartnerOfMSP, AlterComment, TU )
      Select 
        _IdKO
        , IdBankInTU
        , D_From = @DFrom
        , IdUpdate = @IdUpdate + ROW_NUMBER() OVER ( Order By _IdKO Asc ) 
        , StateSED = 0
        , CorrectionType = 4
        , BankRegNum
        , IsPartnerOfMSP
        , AlterComment
        , @TU
      From
        DKO_hrBankInTU b Join hub_Keys k on b._IdKO = k.idHubKey And b.TU = k.TU
      Where 
        b.TU = @TU 
        And b.StateSED = 2
        And b.IsArchive = 0
        And k.NameTable = 'jrKO'
        And k.isReplace = 1
      Order By
        _IdKO  
  
    INSERT INTO DKO_IdUpdate ( TypeDKO )
      Select 
        'DKO201'
      From
        DKO_hrBankInTU b Join hub_Keys k on b._IdKO = k.idHubKey And b.TU = k.TU
      Where 
        b.TU = @TU 
        And b.StateSED = 2
        And b.IsArchive = 0
        And k.NameTable = 'jrKO'
        And k.isReplace = 1
      Order By
        _IdKO 
    ---- Перенос DKO_BankInTU ---- 
    ---- КО дубли ( Correction_Type = 4 ) ---- 
    ---- Окончание ---- 
  
  End   

  RETURN 1
END TRY
BEGIN CATCH

  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
  
END CATCH
GO