﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartImportKD] 
  @IsHUB bit, 
  @TU varchar(2),
  @ErrorMSG varchar(max) output,
  @logStatus varchar(4) output AS -- каждый символ означает успешно или нет загружена каждая таблица. может имет значение (Y -успешно, N - ошибка). порядок символов 1-jrGKD,2-jrAccount,3-rfGKD;4-hrTypeCredits
BEGIN TRY
  declare @return int,
          --@state smallint = 0,
          @count int; 
  SET @logStatus = ''        
  IF @IsHUB = 1
  BEGIN
    --КД
    SET @count = isnull((SELECT MAX(nuGKD) FROM jrGKD),0)+1
    exec @return = dbo.StartCreateTempTable @count ,@ErrorMSG OUTPUT
    IF @return = 0
      RETURN 0
    INSERT INTO ##TempTableForidHub (idTu) 
    SELECT nuGKD FROM hub_jrGKD INNER JOIN hub_Keys hKO on hub_jrGKD.idKO=hKO.idTUKey 
	WHERE /*tpGKD IN ('K', 'D', 'G', 'F') AND (dtGKDend > (SELECT dtWork FROM rfStatus) OR dtGKDend IS NULL) AND (CHARINDEX('/', nuReg) = 0) AND (CHARINDEX('-', nuReg) = 0)AND (hub_jrGKD.isSootv = 'T')
	      AND*/ nuGKD not in(SELECT idTUKey FROM hub_Keys where NameTable = 'jrGKD' and TU = @TU) and hKO.NameTable='jrKO' and hKO.TU=@tu 

    SET IDENTITY_INSERT jrGKD ON;
    INSERT INTO [jrGKD]([nugkd], [dtGKD],[idKO],[isSootv],[mnLimitAgg],[dtGKDend],[numGKD],[tpGKD],[isTreb],[dtGKDUved],[isPartner],[nuGKD236],[nuGKD312],[nuGKD362],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuEKD])
    SELECT t.idHub, [dtGKD],h.idHubKey,'T',[mnLimitAgg],[dtGKDend],[numGKD],[tpGKD],[isTreb],[dtGKDUved],[isPartner],[nuGKD236],[nuGKD312],[nuGKD362],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuEKD]
    FROM hub_jrGKD INNER JOIN hub_Keys h on h.idTUKey=hub_jrGKD.idKO INNER JOIN ##TempTableForidHub t on t.idTU=hub_jrGKD.nuGKD 
	WHERE h.NameTable='jrKO' and h.TU=@TU and isnull(h.isReplace,0) = 0
    SET IDENTITY_INSERT jrGKD OFF;
	
    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU)
    SELECT idHub,idTU,'jrGKD',@TU FROM  ##TempTableForidHub

    --SET @state = 1
	SET @logStatus = @logStatus + 'Y' --1

    --Account
    SET @count = isnull((SELECT MAX(idAcc) FROM jrAccount),0)+1
    exec @return = dbo.StartCreateTempTable @count ,@ErrorMSG OUTPUT
    IF @return = 0
      RETURN 0
    INSERT INTO ##TempTableForidHub (idTu) 
    SELECT [idAcc] FROM [hub_jrAccount] INNER JOIN hub_Keys hKD on hKD.idTUKey=hub_jrAccount.nuGKD 
    WHERE hKD.NameTable='jrGKD' and hkd.TU=@TU /*and hub_jrAccount.isSootv='T' and (dtExcl>(SELECT dtWork FROM rfStatus) or dtExcl is NULL)  */and idAcc not in (SELECt idTUKey from hub_Keys where NameTable='jrAccount' and TU = @TU)

    SET IDENTITY_INSERT jrAccount ON;
    INSERT INTO [jrAccount](idAcc, [tpCredit],[mnLimit],[tpRazdel],[idKO],[isSootv],[nuGKD],[mnLiability],[dtIncl],[dtExcl],[isPayForBKL],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nuAccount],[nmJurAddr],[nmFactAddr],[nmNadzTU],[idRKC],[tpInstr],[tpAsset])
    SELECT t.idHub, [tpCredit],[mnLimit],[tpRazdel],hKO.idHubKey,'T',hKD.idHubKey ,[mnLiability],[dtIncl],[dtExcl],[isPayForBKL],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nuAccount],[nmJurAddr],[nmFactAddr],[nmNadzTU],null,[tpInstr],[tpAsset] 
    FROM hub_jrAccount INNER JOIN ##TempTableForidHub t ON t.idTU = hub_jrAccount.idAcc INNER JOIN hub_Keys hKO on hKO.idTUKey = hub_jrAccount.idKO INNER JOIN hub_Keys hKD ON hKD.idTUKey = hub_jrAccount.nuGKD
    WHERE hKO.NameTable='jrKO' and hKO.TU=@TU and hKD.NameTable='jrGKD' and hKD.TU=@TU and ISNULL(hKO.isReplace,0)=0 and ISNULL(hKD.isReplace,0)=0 
    SET IDENTITY_INSERT jrAccount OFF;
    
    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU)
    SELECT idHub,idTU,'jrAccount',@TU FROM  ##TempTableForidHub

    --INSERT INTO [Counters]([NameCounter],[ValueCounter])
    --SELECT [NameCounter],[ValueCounter] FROM [hub_Counters]
    
    SET @logStatus = @logStatus +'Y' --2
    --SET @state = 2

	INSERT INTO [rfGKD]([nuGKD],[nuDopSogl],[dtDopSogl],[cmDopSogl])
	SELECT hKD.idHubKey,hub_rfGKD.[nuDopSogl],hub_rfGKD.[dtDopSogl],hub_rfGKD.[cmDopSogl] FROM hub_rfGKD INNER JOIN hub_Keys hKD ON hKD.idTUKey=hub_rfGKD.nuGKD 
	WHERE hKD.NameTable='jrGKD' and TU=@TU and ISNULL(hKD.isReplace,0)=0
        AND ISNULL(CAST(hKD.idHubKey as varchar),'') + '' + ISNULL(CAST(nuDopSogl as varchar),'') not IN (SELECT ISNULL(CAST(nuGKD as varchar),'') + '' + ISNULL(CAST(nuDopSogl as varchar),'') FROM jrKO);
	SET @logStatus = @logStatus + 'Y' --3

  END
  ELSE
  BEGIN
    SET @count = isnull((SELECT MAX(nuGKD) FROM jrGKD),1)+1
    exec @return = dbo.StartCreateTempTable @count ,@ErrorMSG OUTPUT
    IF @return = 0
      RETURN 0

    INSERT INTO ##TempTableForidHub (idTu)
    SELECT  nuGKD
    FROM hub_jrGKD INNER JOIN hub_Keys hKO on hub_jrGKD.idKO=hKO.idTUKey 
	WHERE /*tpGKD IN ('K', 'D', 'G', 'F') AND (dtGKDend > (SELECT dtWork FROM rfStatus) OR dtGKDend IS NULL) AND (CHARINDEX('/', nuReg) = 0) AND (CHARINDEX('-', nuReg) = 0)AND (hub_jrGKD.isSootv = 'T')
	      and*/ isnull((CAST(dtGKD as varchar)),'')+isnull((CAST(numGKD as varchar)),'') not in (SELECT isnull((CAST(dtGKD as varchar)),'')+isnull((CAST(numGKD as varchar)),'') FROM jrGKD)
	      and hKO.NameTable='jrKO' and hKO.TU=@tu 

    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU,isReplace)
    SELECT  ExistGKD.nuGKD ,hub_jrGKD.nuGKD,'jrGKD',@TU,1
    FROM hub_jrGKD 
    INNER JOIN (SELECT nugkd,isnull((CAST(jrGKD.dtGKD as varchar)),'')+isnull((CAST(jrGKD.numGKD as varchar)),'') ExistField FROM jrGKD) ExistGKD on ExistGKD.ExistField=isnull((CAST(hub_jrGKD.dtGKD as varchar)),'')+isnull((CAST(hub_jrGKD.numGKD as varchar)),'')
	--WHERE /*tpGKD IN ('K', 'D', 'G', 'F') AND (dtGKDend > (SELECT dtWork FROM rfStatus) OR dtGKDend IS NULL) AND (CHARINDEX('/', nuReg) = 0) AND (CHARINDEX('-', nuReg) = 0)AND (hub_jrGKD.isSootv = 'T')*/


    SET IDENTITY_INSERT jrGKD ON;
    INSERT INTO [jrGKD]([nugkd],[dtGKD],[idKO],[isSootv],[mnLimitAgg],[dtGKDend],[numGKD],[tpGKD],[isTreb],[dtGKDUved],[isPartner],[nuGKD236],[nuGKD312],[nuGKD362],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuEKD])
    SELECT  t.idHub,[dtGKD],h.idHubKey ,'T',[mnLimitAgg],[dtGKDend],[numGKD],[tpGKD],[isTreb],[dtGKDUved],[isPartner],[nuGKD236],[nuGKD312],[nuGKD362],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuEKD]
    FROM hub_jrGKD INNER JOIN hub_Keys h on h.idTUKey=hub_jrGKD.idKO INNER JOIN ##TempTableForidHub t on t.idTu=hub_jrGKD.nuGKD
	WHERE h.NameTable='jrKO' and h.TU=@TU 
    SET IDENTITY_INSERT jrGKD OFF;
    
    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU)
    SELECT idHub,idTU,'jrGKD',@TU FROM  ##TempTableForidHub


	SET @logStatus = @logStatus + 'Y' --1 
    --SET @state = 1
	
	-- jrAccount
    SET @count = isnull((SELECT MAX(idAcc) FROM jrAccount),0)+1
    exec @return = dbo.StartCreateTempTable @count ,@ErrorMSG OUTPUT
    IF @return = 0
      RETURN 0
    INSERT INTO ##TempTableForidHub (idTu) 
    SELECT [idAcc] FROM [hub_jrAccount] INNER JOIN hub_Keys hKD on hKD.idTUKey=hub_jrAccount.nuGKD 
    INNER JOIN hub_jrKO ON hub_jrKO.idKO=hub_jrAccount.idKO
    WHERE  ISNULL(cast(hub_jrAccount.nuAccount as varchar),'')+ISNULL(cast(hub_jrAccount.tpCredit as varchar),'')+ISNULL(cast(hub_jrKO.nuReg as varchar),'') not in (SELECT ISNULL(cast(jrAccount.nuAccount as varchar),'')+ISNULL(cast(jrAccount.tpCredit as varchar),'')+ISNULL(cast(jrko.nureg as varchar),'') FROM jrAccount INNER JOIN jrKO ON jrKO.idKO=jrAccount.idKO) --добавлена проверка на ко AHEBURG-1967
           and hKD.NameTable='jrGKD' and hkd.TU=@TU
    
    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU,isReplace)
      SELECT ExistjrAccount.idAcc,hub_jrAccount.idAcc,'jrAccount',@TU,1
    FROM   [hub_jrAccount] INNER JOIN hub_jrko ON hub_jrko.idko=[hub_jrAccount].idko  INNER JOIN (SELECT idAcc,(ISNULL(cast(jrAccount.nuAccount as varchar),'')+ISNULL(cast(jrAccount.tpCredit as varchar),'')+ISNULL(cast(jrko.nuReg as varchar),''))ExistField FROM jrAccount INNER JOIN  jrKO ON jrAccount.idKO=jrko.idko) ExistjrAccount 
           on ExistjrAccount.ExistField=ISNULL(cast([hub_jrAccount].nuAccount as varchar),'')+ISNULL(cast(hub_jrAccount.nuAccount as varchar),'')+ISNULL(cast(hub_jrko.nuReg as varchar),'') INNER JOIN hub_Keys hKD on hKD.idTUKey=hub_jrAccount.nuGKD --добавлена проверка на ко  AHEBURG-1967
    WHERE  hKD.NameTable='jrGKD' and hkd.TU=@TU -- and isnull(hkd.isReplace,0)=0
     
         
    SET IDENTITY_INSERT jrAccount ON;
    INSERT INTO [jrAccount](idAcc, [tpCredit],[mnLimit],[tpRazdel],[idKO],[isSootv],[nuGKD],[mnLiability],[dtIncl],[dtExcl],[isPayForBKL],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nuAccount],[nmJurAddr],[nmFactAddr],[nmNadzTU],[idRKC],[tpInstr],[tpAsset])
    SELECT t.idHub, [tpCredit],[mnLimit],[tpRazdel],hKO.idHubKey,'T',hKD.idHubKey ,[mnLiability],[dtIncl],[dtExcl],[isPayForBKL],[nmKO],[nnKO],[nuReg],[nuBIK],[nuINN],[nuAccount],[nmJurAddr],[nmFactAddr],[nmNadzTU],null,[tpInstr],[tpAsset] 
    FROM hub_jrAccount INNER JOIN ##TempTableForidHub t ON t.idTU = hub_jrAccount.idAcc INNER JOIN hub_Keys hKO on hKO.idTUKey = hub_jrAccount.idKO INNER JOIN hub_Keys hKD ON hKD.idTUKey = hub_jrAccount.nuGKD
    WHERE hKO.NameTable='jrKO' and hKO.TU=@TU and hKD.NameTable='jrGKD' and hKD.TU=@TU --and isnull(hkd.isReplace,0)=0
    SET IDENTITY_INSERT jrAccount OFF;

    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU)
    SELECT idHub,idTU,'jrAccount',@TU FROM  ##TempTableForidHub
    
    SET @logStatus = @logStatus + 'Y' --2
    --SET @state = 2

	INSERT INTO [rfGKD]([nuGKD],[nuDopSogl],[dtDopSogl],[cmDopSogl])
	SELECT hKD.idHubKey,hub_rfGKD.[nuDopSogl],hub_rfGKD.[dtDopSogl],hub_rfGKD.[cmDopSogl] FROM hub_rfGKD INNER JOIN hub_Keys hKD ON hKD.idTUKey=hub_rfGKD.nuGKD 
	WHERE hKD.NameTable='jrGKD' and TU=@TU and ISNULL(hKD.isReplace,0)=0
        AND ISNULL(CAST(hKD.idHubKey as varchar),'') + '' + ISNULL(CAST(nuDopSogl as varchar),'') + '' + ISNULL(CAST(dtDopSogl as varchar),'') not IN (SELECT ISNULL(CAST(nuGKD as varchar),'') + '' + ISNULL(CAST(nuDopSogl as varchar),'') + '' + ISNULL(CAST(dtDopSogl as varchar),'') FROM jrKO);
	SET @logStatus = @logStatus + 'Y' --3
      
  END;


  
  INSERT INTO [hrTypeCredits]([idAcc],[dtBegin],[tpCredit],[dtEnd],[cmCredit])
  SELECT  hkA.idHubKey,[dtBegin],[tpCredit],[dtEnd],[cmCredit] FROM hub_hrTypeCredits INNER JOIN hub_Keys hkA on hkA.idTUKey = hub_hrTypeCredits.idACC 
  WHERE NameTable ='jrAccount' and TU = @TU and ISNULL(isReplace,0)=0
  SET @logStatus = @logStatus + 'Y' --4

  RETURN 1
END TRY
BEGIN CATCH
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
  INSERT INTO	 rfSign (tpSign,nmSign) 
  VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution' 
  SET @logStatus = @logStatus + 'N' 	   

  begin try
    SET IDENTITY_INSERT jrGKD OFF;
    SET IDENTITY_INSERT jrAccount OFF;
    SET IDENTITY_INSERT rfGKD OFF;
  end try
  begin catch
  end catch; 
/*  IF @state = 0
    SELECT @ErrorMSG = '    При загрузке данных о КД произошла ошибка: ' + ERROR_MESSAGE() +'\n'
  IF @state = 1
    SELECT @ErrorMSG = @log + '    При загрузке данных об основных счетах произошла ошибка: ' + ERROR_MESSAGE() +'\n'
  IF @state = 2
    SELECT @ErrorMSG = @log + '    При загрузке данных об дополнительных соглашениях произошла ошибка: ' + ERROR_MESSAGE() +'\n'
*/    
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
GO