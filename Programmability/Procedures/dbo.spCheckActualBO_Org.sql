﻿SET QUOTED_IDENTIFIER OFF

SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 28.01.2020
-- Проверять наличие БО по организации - обязанному лицу за все обязательные периоды (алгоритм поиска БО за обязательные периоды 
-- Если БО за все обязательные периоды поступила, то присвоить dtExclude значение NULL, jrActive.DopInf[27] = T (True)
-- Если БО за все обязательные периоды не поступила, то значение dtExclude не менять, jrActive.DopInf[27] = F (False)
-- Editor:		Бряков Д.А.
-- Edit date:	31.01.2020
-- Проверяем наличие БО за каждый период и a.status in ('T','M')
-- Editor:		Макаров Д.А. AHEBURG-(4365)4505
-- Edit date:	14.08.2020
-- Проверяем наличие БО у активов a.status in ('R','W')
-- Если БО за все обязательные периоды поступила, то jrActive.DopInf[26] = T (True)
-- Если БО за все обязательные периоды не поступила, то jrActive.DopInf[26] = F (False)
-- =============================================
CREATE procedure [dbo].[spCheckActualBO_Org]
@idOrg int,
@idKO int
AS
BEGIN
  SET NOCOUNT ON;
   UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 27, 1,[dbo].[fnCheck_dopinfo27](a.numactive))
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('T','M')
    where a.Company=@idOrg and l.idKO=@idKO 
    and isnull(a.IsSimpleCheck,0)<>1
    and a.TypeActive='D'
    and substring(a.DopInf,27,1)<>[dbo].[fnCheck_dopinfo27](a.numactive)
  
  UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 26, 1,[dbo].[fnCheck_dopinfo26](a.numactive))
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('R','W','K')
    where a.Company=@idOrg and l.idKO=@idKO 
    and isnull(a.IsSimpleCheck,0)<>1
    and a.TypeActive='D'
    and substring(a.DopInf,26,1)<>[dbo].[fnCheck_dopinfo26](a.numactive)
    
  UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 27, 1,'0')
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('T','M')
    where a.Company=@idOrg and l.idKO=@idKO
    and isnull(a.IsSimpleCheck,0)=1 and a.TypeActive='D'
    and substring(a.DopInf,27,1)<>'0'
  UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 26, 1,'0')
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('R','W','K')
    where a.Company=@idOrg and l.idKO=@idKO 
    and isnull(a.IsSimpleCheck,0)=1 and a.TypeActive='D'
    and substring(a.DopInf,26,1)<>'0'
END
GO