﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spFill_EDOKO_XMLDetail]
@idES int
AS
BEGIN
	SET NOCOUNT ON;
	SET ARITHABORT ON;

	DECLARE @XML xml
	DECLARE @EDType varchar(6)
	DECLARE @T TABLE ([BillNum] [varchar](100) COLLATE Cyrillic_General_CI_AS NULL,
					  [BillDate] [datetime] NULL,
					  [ObligedRegID] [varchar](20) COLLATE Cyrillic_General_CI_AS NULL,
					  [ShortNameObliged] [varchar](255) COLLATE Cyrillic_General_CI_AS NULL,
					  [DebtorRegID] [varchar](20) COLLATE Cyrillic_General_CI_AS NULL,
					  [ShortNameDebtor] [varchar](255) COLLATE Cyrillic_General_CI_AS NULL,
					  [AssetStatus] [varchar](1) NULL,
					  [TypeRepmnt] [varchar](1) NULL  ) --AHEBURG-1098  - afi добавление TypeRepmnt

	SELECT @XML = XMLBody, @EDType = EDType FROM EDOKO_hrES WHERE idES = @idES
	
	BEGIN TRY
		BEGIN TRANSACTION
	
		DELETE FROM EDOKO_XMLDetail WHERE idES = @idES

		IF @EDType = 'C101'
			INSERT INTO @T ([BillNum],[BillDate],[ObligedRegID],[ShortNameObliged],[DebtorRegID],[ShortNameDebtor])
			VALUES (dbo.fnGetData_vwControlES(@EDType, @XML, 'BillNum')
				   ,CASE dbo.fnGetData_vwControlES(@EDType, @XML, 'BillDate') WHEN '' THEN NULL ELSE CONVERT(datetime,dbo.fnGetData_vwControlES(@EDType, @XML, 'BillDate'),102) END
				   ,dbo.fnGetData_vwControlES(@EDType, @XML, 'ObligedRegID')
				   ,dbo.fnGetData_vwControlES(@EDType, @XML, 'ShortNameObliged')
				   ,dbo.fnGetData_vwControlES(@EDType, @XML, 'DebtorRegID')
				   ,CASE dbo.fnGetData_vwControlES(@EDType, @XML, 'ObligedRegID') 
					WHEN dbo.fnGetData_vwControlES(@EDType, @XML, 'DebtorRegID') 
					THEN dbo.fnGetData_vwControlES(@EDType, @XML, 'ShortNameObliged')
					ELSE dbo.fnGetData_vwControlES(@EDType, @XML, 'ShortNameDebtor')
					END)

		IF @EDType in ('C104','C204')
			INSERT INTO @T ([ObligedRegID],[ShortNameObliged])
			VALUES(CASE dbo.fnGetData_vwControlES(@EDType, @XML, 'NewRegID') WHEN '' THEN dbo.fnGetData_vwControlES(@EDType, @XML, 'RegID') ELSE dbo.fnGetData_vwControlES(@EDType, @XML, 'NewRegID') END
				  ,dbo.fnGetData_vwControlES(@EDType, @XML, 'ShortName'))

		IF @EDType in ('C105','C205','C106','C206')
			INSERT INTO @T ([ObligedRegID])
			VALUES(dbo.fnGetData_vwControlES(@EDType, @XML, 'RegID'))

		IF @EDType = 'C202'
			INSERT INTO @T ([BillNum],[BillDate],[AssetStatus])
			VALUES(dbo.fnGetData_vwControlES(@EDType, @XML, 'BillNum')
				   ,CASE dbo.fnGetData_vwControlES(@EDType, @XML, 'BillDate') WHEN '' THEN NULL ELSE CONVERT(datetime,dbo.fnGetData_vwControlES(@EDType, @XML, 'BillDate'),102) END
				   ,dbo.fnGetData_vwControlES(@EDType, @XML, 'AssetStatus'))
        
        IF @EDType = 'C102' --AHEBURG-1098 
           INSERT INTO @T([TypeRepmnt])
           VALUES(dbo.fnGetData_vwControlES(@EDType, @XML, 'TypeRepmnt'))
           
		INSERT INTO EDOKO_XMLDetail ([idES],[BillNum],[BillDate],[ObligedRegID],[ShortNameObliged],[DebtorRegID],[ShortNameDebtor],[AssetStatus],[TypeRepmnt])
		SELECT @idES
			  ,CASE [BillNum] WHEN '' THEN NULL ELSE [BillNum] END
			  ,[BillDate] 
			  ,CASE [ObligedRegID] WHEN '' THEN NULL ELSE [ObligedRegID] END
			  ,CASE [ShortNameObliged] WHEN '' THEN NULL ELSE [ShortNameObliged] END
			  ,CASE [DebtorRegID] WHEN '' THEN NULL ELSE [DebtorRegID] END
			  ,CASE [ShortNameDebtor] WHEN '' THEN NULL ELSE [ShortNameDebtor] END
			  ,CASE [AssetStatus] WHEN '' THEN NULL ELSE [AssetStatus] END
			  ,CASE [TypeRepmnt] WHEN '' THEN NULL ELSE [TypeRepmnt] END  --AHEBURG-1098
		FROM @T 
		WHERE [BillNum] <> ''
		OR    [BillDate] IS NOT NULL
		OR    [ObligedRegID] <> ''
		OR    [ShortNameObliged] <> ''
		OR    [DebtorRegID] <> ''
		OR    [ShortNameDebtor] <> ''
		OR    [AssetStatus] <> ''
		OR    [TypeRepmnt]<>'' 

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION    
		SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_MESSAGE() AS ErrorMessage;
	END CATCH
END
GO