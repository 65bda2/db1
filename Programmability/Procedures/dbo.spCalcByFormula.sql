﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- ================================================
CREATE PROCEDURE [dbo].[spCalcByFormula]
------------------------------------------------
--ПЗ 6.2.1.5.	Хранимая процедура spCalcByFormula (Расчет УКС)
--  Вычисляет значение по фомуле из таблицы rfCalcCoefficient.
--  Использутся в хранимой процедуре spCalcLevelSolvency для Расчета УКС.
-------------------------------
-- =============================================
-- Author:		<65baa2>
-- Create date: <01.11.2018>
-- Description:	<Хранимая процедура вычисляет значение по фомуле из таблицы rfCalcCoefficient>
-- =============================================

	@idOrgBuhReport int, -- ид записи в таблице jrOrgBuhReportg для которой выбираются данные для расчета
	@formula varchar(2000), --текст формулы расчета из таблицы rfCalcCoefficient
	@calc decimal(38,19) OUTPUT -- возвращает вычисленное значение  
AS
BEGIN
    declare @text varchar(max)='';
	declare @K decimal(38,19)=1;
	
	declare @temp table(
		i int,					-- номер по порядку
		flag int,				-- признак:  0- текст формулы  1- код SXXXX (ShowingAccSum) 2- SXXXXP (ShowingAccSumPrev); 
		variant varchar(1024),  -- текст формулы  или код SXXXX (ShowingAccSum) / SXXXXP (ShowingAccSumPrev) ; 
		calc decimal(38,19)              -- вычисленное значение  для кода  XXXX (ShowingAccSum) или SXXXXP (ShowingAccSumPrev);
	);
	/*
		select @K=
		case  CodeOKEI 
			when 383 then 0.001
			when 384 then 1 
			when 385 then 1000 
			else null
	    end 
	from jrOrgBuhReport r where r.idOrgBuhReport=@idOrgBuhReport;
	
	if @K is null  return 2; 
	*/
	-- select * from  fnSplitFormula(@formula) s ;

	insert into @temp 
		(i,
		flag,
	    variant,
        calc)  
	select 
	    s.i,
	    s.flag,
	    s.variant,
	    (case when s.flag>0 
			then
				 (select 
			          r.ShowingAccSum 
				  from jrOrgBuhRecord r,rfBuhRecord b 
					where r.idBuhRecord = b.idBuhRecord
					    and r.idOrgBuhReport=@idOrgBuhReport
						and b.numkod=s.variant)
		  				
		    else null		 
		end)
	from fnSplitFormula(@formula) s ;
	
	select * from @temp s ;
	
	select @text=@text+
		(case when flag=0
			then variant 
			else '('+CAST(calc as varchar(39))+')'
		end)
	from @temp order by i;

    if @text is null  return 1; 
    
    exec spCalcByFormulaSQL @text,@calc=@calc out;
	
	return 0 
END
GO