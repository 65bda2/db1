﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[spUpdate_AutoRejectChangesFromQuarntine] (@idES int, @doer VARCHAR(255))
AS
BEGIN
 --DECLARE @idEs         INT = 2480
 DECLARE @ChangeStatus CHAR(1) = 'F'
 DECLARE @cursor CURSOR
 DECLARE @EDType VARCHAR(4)
 DECLARE @unnum VARCHAR (16)
 DECLARE @NumActive2 int
 DECLARE @Annotation VARCHAR(255) = 'Отклонено на основании рассмотрения ЭС '
 DECLARE @Changes_original VARCHAR(1000)
 DECLARE @OperationDate datetime
 DECLARE @id_isAffKO INT = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedOrg')
 DECLARE @id_isAffOrg INT = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedKO')
 DECLARE @idKO INT = (SELECT idko FROM EDOKO_hrES WHERE idES = @idES )
 
 SELECT @OperationDate = (SELECT dtWork FROM rfStatus)
 
 SET @cursor =  CURSOR for

 SELECT distinct eq.IdEs
 FROM EDOKO_hrES curr
   join EDOKO_hrES eq on eq.idES <> curr.idEs
   join EDOKO_jrChangeORG jCO1 on jco1.idES = curr.idES
   join EDOKO_jrChangeORG jCO2 on jco2.idES = eq.idES
	WHERE jCO1.idORG = jCO2.idORG
	   and  jCO2.ChangeStatus = 'R'
	   and  curr.idES = @idES

 DECLARE @DifferenceCount_1 INT --исходные изменения отсутствуют в данных
 DECLARE @DifferenceCount_2 INT --найденные изменения отсутствуют в исходных

 DECLARE @idEq Int
 OPEN @cursor
 FETCH NEXT FROM @cursor into @idEq
 WHILE @@FETCH_STATUS = 0
 BEGIN
   SELECT @DifferenceCount_1 = count(*)
   FROM (
   (SELECT co.idField, dbo.fnNormalTextForFT(co.NewValue) NewValue --исходные минус найденные = пусто
		FROM EDOKO_jrChangeORG co 
			join EDOKO_hrES as Change_hrES on Change_hrES.idES = @idEq
		WHERE co.idES = @idES and co.ChangeStatus = 'R'
		  	/*AND
			( 
				(co.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
			)*/
    EXCEPT
    SELECT co.idField, dbo.fnNormalTextForFT(co.NewValue) NewValue 
		FROM EDOKO_jrChangeORG co
			join EDOKO_hrES as Change_hrES on Change_hrES.idES = @idEq 
		WHERE co.idES = @idEq and co.ChangeStatus = 'R' 
			AND
			(
				(co.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
			)	)) t
   
      
  /* SELECT @DifferenceCount_2 = count(*)
   FROM (
   (SELECT co.idField, co.NewValue FROM EDOKO_jrChangeORG co WHERE co.idES = @idEq and co.ChangeStatus = 'R' --найденные минус исходные = пусто
    EXCEPT
    SELECT co.idField, co.NewValue FROM EDOKO_jrChangeORG co WHERE co.idES = @idES and co.ChangeStatus = 'R' )) t
*/

	
  /* IF  @DifferenceCount_2 = 0
   BEGIN
     UPDATE EDOKO_jrChangeORG SET ChangeStatus = @ChangeStatus, Annotation = @Annotation + (SELECT EDFileName FROM EDOKO_hrES WHERE idES = @idES) WHERE idEs = @idEq 
   END
 */
 --BEGIN автоматическое изменение в таблице EDOKO_jrChangeORG
   IF  @DifferenceCount_1 = 0
   BEGIN
	UPDATE EDOKO_jrChangeORG 
	SET ChangeStatus = @ChangeStatus, Annotation = @Annotation  + (SELECT EDFileName FROM EDOKO_hrES WHERE idES = @idES), idESProcess = @idES
	WHERE idEs = @idEq and idField in  ( SELECT jrCO1.idField FROM EDOKO_jrChangeORG jrCO1 INNER JOIN [EDOKO_ListChangeField] lcf ON jrCO1.idField = lcf.idField 
                        	   INNER JOIN [EDOKO_jrChangeORG] jrCO2 ON jrCO2.idField = lcf.idField AND jrCO1.idORG = jrCO2.idORG 
                        	   INNER JOIN EDOKO_hrES hrES ON hrES.IdES = jrCO1.IdES
                        	   WHERE lcf.tpTable = 'ORG' 
							    --AND jrCO1.ChangeStatus = 'R' 
								AND jrCO1.IdES <> jrCO2.IdES 
								AND jrCO2.IdES = @idES
								AND dbo.fnNormalTextForFT(jrCO1.NewValue) = dbo.fnNormalTextForFT(jrCO2.NewValue) 
								--AND jrCO1.idfield NOT IN (SELECT t1.idfield FROM [EDOKO_jrChangeORG] t1 INNER JOIN [EDOKO_jrChangeORG] t2 ON t1.idField = t2.idField AND t1.idORG = t2.idORG 
                                --                                         AND t1.IdES <> t2.IdES AND t1.NewValue <> t2.NewValue AND t2.IdES = jrCO2.IdES 
                                --       							WHERE t1.idField in (SELECT idField FROM [EDOKO_ListChangeField] WHERE tpTable = 'ORG' ))
                                 ) 
                                                     

    
    SELECT @Changes_original = (SELECT NmFieldEDOKO  + ',' AS 'data()' FROM EDOKO_ListChangeField
																				inner join EDOKO_jrChangeORG on EDOKO_ListChangeField.idField = EDOKO_jrChangeORG.idField
																				WHERE EDOKO_jrChangeORG.idES = @idES for XML path (''))  
    SELECT @Changes_original = SUBSTRING(@Changes_original,1,LEN(@Changes_original)-1) -- AHEBURG-3652 cast(reverse(stuff(reverse(@Changes_original),1,1,'''')) AS VARCHAR)    
	
	UPDATE EDOKO_jrChangeORG 
	SET ChangeStatus = @ChangeStatus, 
	Annotation = 'Отклонено без рассмотрения вместе с отклонением изменений реквизитов "' + @Changes_original + '" при принятии решения по изменениям ЭС ' +  (SELECT EDFileName FROM EDOKO_hrES WHERE idES = @idES)
--	SELECT * FROM EDOKO_jrChangeORG
	WHERE idEs = @idEq and  idField not in ( SELECT jrCO1.idField FROM EDOKO_jrChangeORG jrCO1 INNER JOIN [EDOKO_ListChangeField] lcf ON jrCO1.idField = lcf.idField 
                        	   INNER JOIN [EDOKO_jrChangeORG] jrCO2 ON jrCO2.idField = lcf.idField AND jrCO1.idORG = jrCO2.idORG 
                        	   INNER JOIN EDOKO_hrES hrES ON hrES.IdES = jrCO1.IdES
                        	   WHERE lcf.tpTable = 'ORG' 
		                       -- AND jrCO1.ChangeStatus = 'R' 
			                    AND jrCO1.IdES <> jrCO2.IdES 
								AND jrCO2.IdES = @idES
								AND dbo.fnNormalTextForFT(jrCO1.NewValue) = dbo.fnNormalTextForFT(jrCO2.NewValue) 
								--AND jrCO1.idField NOT IN (SELECT t1.idField FROM [EDOKO_jrChangeORG] t1 INNER JOIN [EDOKO_jrChangeORG] t2 ON t1.idField = t2.idField AND t1.idORG = t2.idORG 
                                --                                         AND t1.IdES <> t2.IdES AND t1.NewValue <> t2.NewValue AND t2.IdES = jrCO2.IdES 
                                --       							WHERE t1.idField in (SELECT idField FROM [EDOKO_ListChangeField] WHERE tpTable = 'ORG' )
                                )                                                       
   END
   --END автоматическое изменение в таблице EDOKO_jrChangeORG
   
   --BEGIN изменение в С101 для найденного ЭС
 
   SELECT @EDType=(SELECT EDType FROM EDOKO_hrES WHERE idES = @idEq) 
   
   IF @EDType = 'C104' and (@DifferenceCount_1 = 0) --шаг 3.3 п 3 в 3.1.2 для ЭС типа С104 в таблице EDOKO_hrES поле EDStatus изменить на P, Doer заполнить исполнителем, отклонившим решение по ЭС 
   BEGIN
     UPDATE EDOKO_hrES
		SET Doer = @doer, EDStatus = 'P'
		WHERE idES = @idEq		
   END
   
   
   IF @EDType = 'C101' and (@DifferenceCount_1 = 0) 
   BEGIN
      SELECT @unnum=isnull((SELECT UnNum FROM EDOKO_hrES WHERE idES = @idEq),'')
      IF @unnum <> ''
      BEGIN
        UPDATE jrActive SET DopInf = SUBSTRING( DopInf, 1, 22 ) + 'F' + SUBSTRING( DopInf, 24, 4 ) WHERE UnNum = @unnum
        SELECT  @NumActive2 = (SELECT NumActive FROM jrActive WHERE UnNum =  @unnum)
        IF not exists (SELECT IdNoteActive FROM jrNoteActive WHERE Tag = 368 AND NumActive = @NumActive2) 
        BEGIN
          INSERT INTO jrNoteActive ( Tag, DateNote, Note, NumActive, isCreateTag ) VALUES ( 368, @OperationDate,'В С101 указана неверная информация об организации', @NumActive2, 0 )
        END                       
        else
        BEGIN
          UPDATE jrNoteActive SET DateNote = @OperationDate, Note = 'В С101 указана неверная информация об организации', isCreateTag = 0 WHERE Tag = 368 AND NumActive = @NumActive2 
        END  
        INSERT INTO jrJournalActive (NumActive, DateIt, SysDate, Note, Kategory, Doer) 
          VALUES (@NumActive2, @OperationDate, @OperationDate,'В C101 указана неверная информация об организации', 'AE', @doer)   
	  END
     
   END
   FETCH NEXT FROM @cursor into @idEq
   
 END
 CLOSE @cursor
 DEALLOCATE @cursor
END
GO