﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartCreateTempTable]
@maxIdentity VARCHAR (1000),
@ErrorMSG varchar(max) output AS
BEGIN TRY   
    DECLARE @create  NVARCHAR(1000) 
    set @create=' IF (OBJECT_ID(N''tempdb..##TempTableForidHub'',N''U'') IS NOT NULL) '+
        +'DROP TABLE ##TempTableForidHub ; CREATE TABLE ##TempTableForidHub (idTu int, idHub int identity('+isnull(@maxIdentity,'1')+',1));' 
    EXEC sp_executesql @create
    RETURN 1
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
GO