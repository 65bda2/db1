﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Author:		65vev3 (change 65moe2 04/09/2019)
-- Create date: 17/07/2019
-- Description:	Список активов для формирования А400
-- =============================================
CREATE PROCEDURE [dbo].[spGetActiveForA400]
AS
BEGIN

  Begin Try	

    ---- Список активов, находящихся в залоге
    Declare @ZalogActive Table ( NumActive Int, 
                                 tpSect    VarChar(4) )	
    
    Insert Into @ZalogActive 
    Select 
      h.NumActive, r.tpSect 
    From 
      hrActiveSect h Join rfActiveState r On r.idActiveState = h.idActiveState
    Where 
      DateAssign = ( Select dtWork From rfStatus ) And r.tpSect IN ( '22N', '21L', '3N', '3L' )
    
    
    ---- Список событий из журнала активов
    Declare @JrActive Table ( NumActive Int, 
                              Kategory  Char(2), 
                              DateIt    DateTime )	
    
    Insert Into @JrActive 
    Select 
      b.NumActive, b.Kategory, b.DateIt 
    From
      jrJournalActive b Join jrActive a On a.NumActive = b.NumActive
                   Left Join @ZalogActive z On a.NumActive = z.NumActive
    Where
      ( a.status IN ( 'T', 'R' ) Or ( z.tpSect Is Not Null  ) ) 
       And b.Kategory IN ( 'P', 'F', 'Q', 'T', 'N', 'U', 'I', 'ZA', 'Z', 'ZB' )
       And a.TypeActive IN ( 'V', 'D' )


    ---- Итоговая таблица
    Declare @tActive Table ( UnNum          Char(16),
                             AssetQuality   Int           Null,
-----                             TempCost       Decimal(18,2) Null,
                             Cost       Decimal(18,2) Null,
-----                             TempCostRub    Decimal(18,2) Null,
                             FirstCostRub    Decimal(18,2) Null,
                             DtFail         DateTime      Null,
                             DtReincl       DateTime      Null,
                             inclDate       DateTime      Null,
                             DtIN           DateTime      Null,
                             DtCheck        DateTime      Null,
                             CurrStatus     Int,
                             StatusActiveCA Int,
                             KPTU           Char(2) )
    
-----    Insert @tActive ( UnNum, AssetQuality, TempCost, TempCostRub, DtFail, DtReincl, inclDate, DtIN, DtCheck, CurrStatus, StatusActiveCA, KPTU )
    Insert @tActive ( UnNum, AssetQuality, Cost, FirstCostRub, DtFail, DtReincl, inclDate, DtIN, DtCheck, CurrStatus, StatusActiveCA, KPTU )
    Select 
      a.UnNum
      , a.AssetQuality
---      , IsNull( ( Case TypeActive When 'D' Then ( Select SUM( n.Payment ) From jrPaymentGrafic n Where a.NumActive = n.NumActive And n.Advice = 'F' )
---                                        Else TempCost End ), 0 ) as TempCost
      , a.Cost
---      , a.TempCostRub
      , a.FirstCostRub
      , ( Select Top 1 DateIt From @JrActive j1 Where j1.NumActive = a.NumActive And j1.Kategory IN ( 'P', 'F', 'Q' ) Order By DateIt Desc ) As DtFail
      , ( Case a.status
               When 'T' Then Null 
                        Else ( Case a.inclDate 
                                    When Null Then Null
                                    Else ( Select Top 1 DateIt From @JrActive j2 Where 
                                             j2.NumActive = a.NumActive And j2.Kategory IN ( 'P', 'F', 'ZA', 'Q', 'Z', 'ZB' ) Order By DateIt Desc ) 
                               End )
          End ) as DtReincl
      , a.inclDate
      , d.DateLetterTU
      , ( Select Top 1 DateIt From @JrActive j3 Where j3.NumActive = a.NumActive AND j3.Kategory IN ( 'P','F','Q','I','U' ) Order By DateIt ) as DtCheck
      , ( Case When t.tpSect Is Null Then 0 Else 1 End ) as CurrStatus
      , ( CASE a.status When 'R' Then 0 When 'T' Then 1 When 'N' Then 2 Else 2 End ) As StatusActiveCA
      , r.KPTU as KPTU
    From jrActive a Join jrLetter d On d.idLetter = a.idLetter
                    Join jrKO k On k.idKO = d.idKo
                    Join rfSubdivision r On r.idSubdivision = k.idSubdivision
               Left Join @ZalogActive t On t.NumActive = a.NumActive 
    Where 
      a.TypeActive IN ( 'V', 'D' ) 
      And ( a.Status IN ( 'T', 'R', 'N' ) Or t.tpSect Is Not Null ) 
      
    Select * From @tActive Order By KPTU, DtIN Desc

 End Try
  Begin Catch
    
    Declare @ErrMsg VarChar(255)
    Select @ErrMsg = ERROR_MESSAGE()
    Select @ErrMsg = 'При формировании списка активов произошла ошибка. ' + @ErrMsg
    RaisError( @ErrMsg, 16, 1  )
  End Catch	
	
END
GO