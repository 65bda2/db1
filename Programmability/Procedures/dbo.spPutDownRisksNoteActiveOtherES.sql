﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spPutDownRisksNoteActiveOtherES]
/********************
   2020-03-18 65fev aheburg-3242 восстанавливаю аффилированность утеряную в aheburg-2873

-- AHEBURG-1278	Процедура [spPutDownRisksNoteActiveOtherES]
-- =============================================
-- Author:<65eav2>
-- Create date: <12.12.2019>
-- Description:	<проставление рисков в карточку актива на рассмотрении>
-- =============================================
*/
	@idES int				-- ид ЭС с изменениями по организации(С101, С104)
	,@UserName Varchar(255) = '' -- имя исполнителя
AS
BEGIN

DECLARE @EDName VARCHAR(33) = (SELECT EDFileName FROM EDOKO_hrES WHERE idES = @idES);
DECLARE @Table TABLE(idES INT,idORG INT)
DECLARE @Note VARCHAR(500) 
DECLARE @id_isAffKO int = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedOrg') -- 65fev aheburg-3242
DECLARE @id_isAffOrg int = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedKO') -- 65fev aheburg-3242
DECLARE @idKO int = (SELECT idko FROM EDOKO_hrES WHERE idES = @idES ) -- 65fev aheburg-3242

INSERT INTO @Table
SELECT distinct jrChangeORG.idES, jrChangeORG.idORG
FROM EDOKO_jrChangeORG jrChangeORG
	join EDOKO_hrES hrES on hrES.idES = jrChangeORG.idES 
WHERE jrChangeORG.idOrg in(SELECT Company FROM jrActive WHERE STATUS in ('R', 'W') AND isElectronForm = 1
							UNION
							SELECT NmZaemshik FROM jrActive WHERE STATUS in ('R', 'W') AND isElectronForm = 1)
	AND EDType = 'C101'
	AND jrChangeORG.idES <> @idES
	AND jrChangeORG.idES <> @idES
	AND  -- 65fev aheburg-3242
	 (
	    (jrChangeORG.idField not IN (@id_isAffKO,@id_isAffOrg)) or (hrES.idKO = @idKO)  --условие на аффилированность
	  )
	
	
	DECLARE @SQL VARCHAR(MAX)
	SET @SQL = N'';
	SELECT @SQL = coalesce(@SQL + NmFieldEDOKO + ',','' )
	FROM EDOKO_ListChangeField WHERE idField in (SELECT idField FROM EDOKO_jrChangeORG WHERE idES = @idES)
	IF LEN(@SQL) > 0
		SET @SQL = SUBSTRING(@SQL, 1, LEN(@SQL)-1)
		
	DECLARE @es INT
	DECLARE cr CURSOR 
-- 65fev aheburg-3242 start
	FOR (SELECT distinct jrChangeORG.ides
			FROM EDOKO_jrChangeORG jrChangeORG
				join EDOKO_hrES as Change_hrES on jrChangeOrg.idES = Change_hrES.idES	
			WHERE jrChangeORG.ides <> @idES
				AND (SELECT count(idfield) FROM 
				(
					(SELECT o1.idField, o1.idOrg,(dbo.fnNormalTextForFT (o1.NewValue)) NewValue 
						FROM EDOKO_jrChangeORG o1
						WHERE o1.ides = @idES
						 AND
						( 
							(o1.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
						)
					)
				EXCEPT
					(SELECT o2.idField, o2.idOrg,(dbo.fnNormalTextForFT (o2.NewValue)) NewValue		
						FROM EDOKO_jrChangeORG o2	
						WHERE o2.ides = jrChangeORG.ides
						 AND
						( 
							(o2.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
						)
					)
				) T)<>0
	AND jrChangeORG.idES in (SELECT ides FROM @Table)
	AND jrChangeORG.idORG in (SELECT org.idorg FROM EDOKO_jrChangeORG org WHERE org.ides = @idES))	
-- 65fev aheburg-3242 finito
		OPEN cr
	FETCH NEXT FROM cr 
	INTO @es
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @Note = 'В принятых изменениях по реквизитам <' + @SQL + '> ЭС <' + @EDName + '> указана иная информация об организации' 
		EXECUTE spPutDownRisksNoteActive @es, @Note, @UserName
     	
		FETCH NEXT FROM cr
		INTO @es
	END
	CLOSE cr
	DEALLOCATE cr	
END
GO