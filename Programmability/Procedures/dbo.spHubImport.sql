﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spHubImport](@idDTGo int, @ModeTest bit, @IsHUB bit,@TU varchar (2)) AS
BEGIN TRY
      
  DECLARE	
        @log varchar(max),
        @return_value int,
		@ErrorMSG varchar(max),
		@isNotErr bit
  SET @isNotErr = 1
  SET @log = ''
  SET @ErrorMSG = ''
  IF @IsHUB = 1 
  BEGIN		
	DECLARE @sr varchar(max)
	SET @sr = ''
	IF NOT EXISTS(SELECT * FROM rfSign WHERE tpSign not in ('LKFileSizeMb','StartSolution'))	
	BEGIN
		EXEC	@return_value = [dbo].[StartImportRF] @ErrorMSG = @ErrorMSG OUTPUT, @TU=@TU ,@log=@sr OUTPUT
		IF (@return_value = 1) and (@isNotErr = 1)
		  SET @isNotErr = 1 
		ELSE
		  SET @isNotErr = 0
		IF @return_value = 1 SET @log = @log +'\n    Загрузка справочников в ХАБ\n'+@sr ELSE SET @log= @Log + '\n    Загрузка справочников в ХАБ\n' + @ErrorMSG + '\n\nЗагрузка данных невозможна'
		UPDATE DataTransferGo
		SET    isSuccess=@return_value, dtFinish=getdate(), 
			 xmlLog='<log><text>' + @log + '</text></log>'
		WHERE  idDTGo=@idDTGo

		IF @return_value = 0
		RETURN 0
		---*/print 'a'
		INSERT INTO Counters SELECT * FROM hub_Counters
	END
	ELSE
	BEGIN
		SET @log = @log +'\n    Загрузка справочников в ХАБ уже проводилась\n' 
		UPDATE DataTransferGo
		SET    isSuccess=@return_value, dtFinish=getdate(), 
			 xmlLog='<log><text>' + @log + '</text></log>'
		WHERE  idDTGo=@idDTGo
	END
  END
  
  -- Сохранение количесвенных метрик до загрузки
  IF EXISTS(SELECT NameCounter FROM hub_Counters WHERE NameCounter='jrKO')
	UPDATE hub_Counters SET ValueCounter = (SELECT COUNT(*) FROM jrKO) WHERE NameCounter = 'jrKO'
  ELSE
	INSERT INTO hub_Counters (NameCounter, ValueCounter) VALUES ('jrKO', (SELECT COUNT(*) FROM jrKO));

  IF EXISTS(SELECT NameCounter FROM hub_Counters WHERE NameCounter='jrGKD')
	UPDATE hub_Counters SET ValueCounter = (SELECT COUNT(*) FROM jrGKD) WHERE NameCounter = 'jrGKD'  
  ELSE
	INSERT INTO hub_Counters (NameCounter, ValueCounter) VALUES ('jrGKD', (SELECT COUNT(*) FROM jrGKD));

  IF EXISTS(SELECT NameCounter FROM hub_Counters WHERE NameCounter='jrAccount')
	UPDATE hub_Counters SET ValueCounter = (SELECT COUNT(*) FROM jrAccount) WHERE NameCounter = 'jrAccount'
  ELSE
	INSERT INTO hub_Counters (NameCounter, ValueCounter) VALUES ('jrAccount', (SELECT COUNT(*) FROM jrAccount));

  IF EXISTS(SELECT NameCounter FROM hub_Counters WHERE NameCounter='jrOrg')
	UPDATE hub_Counters SET ValueCounter = (SELECT COUNT(*) FROM jrOrg) WHERE NameCounter = 'jrOrg'
  ELSE
	INSERT INTO hub_Counters (NameCounter, ValueCounter) VALUES ('jrOrg', (SELECT COUNT(*) FROM jrOrg));

  IF EXISTS(SELECT NameCounter FROM hub_Counters WHERE NameCounter='jrActive')
	UPDATE hub_Counters SET ValueCounter = (SELECT COUNT(*) FROM jrActive) WHERE NameCounter = 'jrActive'
  ELSE
	INSERT INTO hub_Counters (NameCounter, ValueCounter) VALUES ('jrActive', (SELECT COUNT(*) FROM jrActive));


  -- Проверка ссылочной целостности
  --SET @log = @log + '--------------------------------------------------------------------------------'+'\n'
   SET @log = @log + '\n\n'
  EXEC	@return_value = [dbo].[StartStateConstraint]
 	    @ErrorMSG = @ErrorMSG OUTPUT
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  IF @return_value = 1 
    SET @log = @log +'    Проверка ссылочной целостности ТУ ' + @TU + ' зaвершено успешно.' 
  ELSE 
    SET @log= @Log + '    Проверка ссылочной целостности ТУ ' + @TU + ' завершена с ошибками: '+@ErrorMSG 
  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo
  IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
  BEGIN
    SET @log = @log + '\n\n    Загрузка данных невозможна'
    UPDATE DataTransferGo
    SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
    WHERE  idDTGo=@idDTGo
    
    RETURN 0
  END  
  IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
  BEGIN
    SET @log = @log + '\n\n    Загрузка данных будет продолжена'
    UPDATE DataTransferGo
    SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
    WHERE  idDTGo=@idDTGo
    
  END  


-- SET @log = @log + '--------------------------------------------------------------------------------'+'\n'
  SET @log = @log + '\n\n'
  EXEC	@return_value = [dbo].[StartImportKo] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  DECLARE @strE varchar(max),@strNE varchar(max)
  SET @strE='' SET @strNE=''
  IF @return_value = 1 
  BEGIN
    SET @strE = ISNULL((SELECT nuReg +','  from jrKO INNER JOIN hub_Keys ON jrKO.idKO=idHubKey where ISNULL(isReplace,0)=0 and TU = @TU and NameTable='jrKO' FOR XML path('')),'')
    IF @strE<>'' 
      SET @strE=SUBSTRING(@strE,1,LEN(@strE)-1) 
    SET @strE = @strE +'\n'
    
    SET @StrNE=ISNULL((SELECT nuReg +','  from jrKO INNER JOIN hub_Keys ON jrKO.idKO=idHubKey where isReplace=1 and TU = @TU and NameTable='jrKO' FOR XML path('')),'')
    IF @strNE<>'' 
      SET @strNE=SUBSTRING(@strNE,1,LEN(@strNE)-1) +'\n'
    SET @strNE = @strNE +'\n'
    
    SET @log = @log +'    Данные о КО загружены\n' + 
                     '    Перечень эталонных КО: ' + @strE	+
                     '    Перечень КО, по которым проведено слияние данных: ' + @strNE
  END                     
  ELSE
    SET @log= @Log + '    При загрузке данных с КО произошла ошибка: '+@ErrorMSG 
  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo
  IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
  BEGIN
    SET @log = @log + '\n\nЗагрузка данных невозможна'
    UPDATE DataTransferGo
    SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
    WHERE  idDTGo=@idDTGo
    
    RETURN 0
  END
  IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
  BEGIN
    SET @log = @log + 'Данные ТУ ' +@TU+' загрузить невозможно\n    Загрузка данных будет продолжена'
    UPDATE DataTransferGo
    SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
    WHERE  idDTGo=@idDTGo
    
  END  
  
 
--  SET @log = @log + '\n---KD-----------------------------------------------------------------------------'+'\n'
  SET @log = @log + '\n\n'
  DECLARE @logStatus varchar(50)
  SET @logStatus = ''
  EXEC	@return_value = [dbo].[StartImportKD] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU, @logStatus=@logStatus OUTPUT
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  SET @strE = ISNULL((SELECT numGKD+',' FROM (SELECT DISTINCT numGKD, dtGKD FROM jrGKD WHERE numGKD in (SELECT numGKD FROM jrGKD WHERE tpGKD='K' GROUP BY numGKD HAVING COUNT(*) > 1 )) dd GROUP BY numGKD, dtGKD HAVING COUNT(*) > 1 FOR XML path('')),'')
  IF @strE<>''
  BEGIN	
    SET @strE=SUBSTRING(@strE,1,LEN(@strE)-1) 
	SET @log = @log + '\n    При загрузке данных ТУ '+@TU+' обнаружены КД с одинаковыми номерами, но разными датами начала действия: '+@strE+'\n'
	IF @ModeTest = 0
	BEGIN
		SET @log = @log + '\nЗагрузка данных невозможна'
		IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
			INSERT INTO	 rfSign (tpSign,nmSign) VALUES('StartSolution',null) 
		UPDATE rfSign SET nmSign = '-1' WHERE tpSign = 'StartSolution' 
	END 
  END
	
  IF @strE = ''
  BEGIN
	IF @return_value = 1
	BEGIN
		SET @strE = ISNULL((SELECT numGKD +','  from jrGKD INNER JOIN hub_Keys ON jrGKD.nuGKD=hub_Keys.idHubKey where ISNULL(hub_Keys.isReplace,0)=0 and hub_Keys.TU = @TU and hub_Keys.NameTable='jrGKD' FOR XML path('')),'')
		IF @strE<>'' 
		  SET @strE=SUBSTRING(@strE,1,LEN(@strE)-1)
    
		SET @StrNE=ISNULL((SELECT numGKD +','  from jrGKD INNER JOIN hub_Keys ON jrGKD.nuGKD=hub_Keys.idHubKey where hub_Keys.isReplace=1 and hub_Keys.TU = @TU and hub_Keys.NameTable='jrGKD' FOR XML path('')),'')
		  IF @strNE<>'' 
		SET @strNE=SUBSTRING(@strNE,1,LEN(@strNE)-1)

		IF SUBSTRING(@logStatus,1,1)='Y'
		begin
		  SET @log = @log + '    Данные по КД загружены.\n'
		  SET @log = @log + '    Перечень добавленных КД: '+@strE+'\n'
		  SET @log = @log + '    Перечень КД, по которым проведено слияние данных: '+@strNE+'\n'
		end
		IF SUBSTRING(@logStatus,2,1)='Y'
		  SET @log = @log + '        Данные по основным счетам загружены.\n'
		IF SUBSTRING(@logStatus,3,1)='Y'
		  SET @log = @log + '        Данные по дополнительным соглашениям загружены.\n'
	END
	IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
	BEGIN
		IF SUBSTRING(@logStatus,1,1)='N'
		  SET @log = @log + '    При загрузке данных о КД произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,2,1)='N'
		  SET @log = @log + '    При загрузке данных об основных счетах произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,3,1)='N'
		  SET @log = @log + '    При загрузке данных о дополнительных соглашениях произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,4,1)='N'
		  SET @log = @log + '    При загрузке данных о хронологии видов предоставленых кредитах произошла ошибка: '+@ErrorMSG+'\n'

		SET @log = @log + '\nЗагрузка данных невозможна'
		RETURN 0
	END
	IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
	BEGIN
		IF SUBSTRING(@logStatus,1,1)='N'
		  SET @log = @log + '    При загрузке данных о КД произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,2,1)='N'
		  SET @log = @log + '    При загрузке данных об основных счетах произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,3,1)='N'
		  SET @log = @log + '    При загрузке данных о дополнительных соглашениях произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,4,1)='N'
		  SET @log = @log + '    При загрузке данных о хронологии видов предоставленых кредитах произошла ошибка: '+@ErrorMSG+'\n'

		SET @log = @log + 'Данные ТУ ' +@TU+' загрузить невозможно\n'
		SET @log = @log + 'Загрузка данных будет продолжена\n'    
	END
  END


  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo  
 
  
--  SET @log = @log + '\n---- ORG ----------------------------------------------------------------------------'+'\n'	
  SET @log = @log +'\n\n'
  SET @logStatus = ''
  EXEC	@return_value = [dbo].[StartImportOrg] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU, @logStatus=@logStatus OUTPUT
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

	IF @return_value = 1
	BEGIN
		SET @strE = ISNULL((SELECT EGRUL +','  from jrOrg INNER JOIN hub_Keys ON jrOrg.idOrg=hub_Keys.idHubKey where ISNULL(hub_Keys.isReplace,0)=0 and hub_Keys.TU = @TU and hub_Keys.NameTable='jrOrg' FOR XML path('')),'')
		IF @strE<>'' 
		  SET @strE=SUBSTRING(@strE,1,LEN(@strE)-1)
    
		SET @StrNE=ISNULL((SELECT EGRUL +','  from jrOrg INNER JOIN hub_Keys ON jrOrg.idOrg=hub_Keys.idHubKey where hub_Keys.isReplace=1 and hub_Keys.TU = @TU and hub_Keys.NameTable='jrOrg' FOR XML path('')),'')
		  IF @strNE<>'' 
		SET @strNE=SUBSTRING(@strNE,1,LEN(@strNE)-1)

		IF (SUBSTRING(@logStatus,1,1)='Y') AND (SUBSTRING(@logStatus,2,1)='Y') AND (SUBSTRING(@logStatus,6,1)='Y')
		begin
		  SET @log = @log + '    Данные в справочник Список лиц, обязанных по активам загружены.\n'
		  SET @log = @log + '    Перечень ОГРН добавленных организаций: '+@strE+'\n'
		  SET @log = @log + '    Перечень ОГРН организаций, по которым проведено слияние данных: '+@strNE+'\n'
		end
		IF SUBSTRING(@logStatus,3,1)='0'
		  SET @log = @log + '        Данные об индивидуальных поправочных коэффициентах по организации отсутствуют.\n'
		IF SUBSTRING(@logStatus,3,1)='Y'
		  SET @log = @log + '        Данные об индивидуальных поправочных коэффициентах по организации загружены.\n'
		IF (SUBSTRING(@logStatus,4,1)='Y') AND (SUBSTRING(@logStatus,5,1)='Y')
		  SET @log = @log + '        Данные об аффилированности загружены.\n'
	END
	IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
	BEGIN
		IF (SUBSTRING(@logStatus,1,1)='N') OR (SUBSTRING(@logStatus,2,1)='N') OR (SUBSTRING(@logStatus,6,1)='N')
		  SET @log = @log + '    При загрузке данных об организациях произошла ошибка: '+@ErrorMSG+'\n'
		IF SUBSTRING(@logStatus,3,1)='0'
		  SET @log = @log + '        Данные об индивидуальных поправочных коэффициентах по организации отсутствуют.\n'
		IF SUBSTRING(@logStatus,3,1)='N'
		  SET @log = @log + '        При загрузке данных об индивидуальных поправочных коэффициентах по организации произошла ошибка: '+@ErrorMSG+'\n'
		IF (SUBSTRING(@logStatus,4,1)='N') AND (SUBSTRING(@logStatus,5,1)='N')
		  SET @log = @log + '        При загрузке данных об аффилированности произошла ошибка: '+@ErrorMSG+'\n'

		SET @log = @log + '\nЗагрузка данных невозможна'
		RETURN 0
	END
	IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
	BEGIN
		IF (SUBSTRING(@logStatus,1,1)='N') OR (SUBSTRING(@logStatus,2,1)='N') OR (SUBSTRING(@logStatus,6,1)='N')
		  SET @log = @log + '    При загрузке данных об организациях произошла ошибка: '+@ErrorMSG+'\n'+'        Загрузка данных будет продолжена\n'
		IF SUBSTRING(@logStatus,3,1)='0'
		  SET @log = @log + '        Данные об индивидуальных поправочных коэффициентах по организации отсутствуют.\n'
		IF SUBSTRING(@logStatus,3,1)='N'
		  SET @log = @log + '        При загрузке данных об индивидуальных поправочных коэффициентах по организации произошла ошибка: '+@ErrorMSG+'\n'+'        Загрузка данных будет продолжена\n'
		IF (SUBSTRING(@logStatus,4,1)='N') AND (SUBSTRING(@logStatus,5,1)='N')
		  SET @log = @log + '        При загрузке данных об аффилированности произошла ошибка: '+@ErrorMSG+'\n'+
							'Данные ТУ ' +@TU+' загрузить невозможно\n'+
							'Загрузка данных будет продолжена\n'    		
	END

  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo  

  
  --SET @log = @log + '--------------------------------------------------------------------------------'+'\n'
  EXEC	@return_value = [dbo].[StartImportBO] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  IF @return_value = 1 
    SET @log = @log +'        Данные о показателях бухгалтерской отчетности загружены.\n' 
  ELSE 
    SET @log= @Log + '        При загрузке данных о показателях бухгалтерской отчетности произошла ошибка: '+@ErrorMSG 
  IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
  BEGIN
    SET @log = @log + '\n\nЗагрузка данных невозможна'   
    RETURN 0
  END  
  IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
  BEGIN
    SET @log = @log + '\n\n        Загрузка данных будет продолжена'
  END  
  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo

  
--  SET @log = @log + '\n---Active-----------------------------------------------------------------------------'+'\n'
  SET @log = @log +'\n\n'
  SET @logStatus = ''
  EXEC	@return_value = [dbo].[StartImportActive] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU, @logStatus = @logStatus OUTPUT
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0
 
  IF @return_value = 1
  BEGIN 
	IF SUBSTRING(@logStatus, 1,1)='Y' SET @log = @log +'    Данные таблицы Ходатайства загружены.\n' 
	IF SUBSTRING(@logStatus, 2,1)='Y' SET @log = @log +'    Данные таблицы Активы загружены.\n' 
	IF SUBSTRING(@logStatus, 3,1)='Y' SET @log = @log +'    Данные таблицы История изменения активов загружены.\n'
	IF SUBSTRING(@logStatus, 4,1)='Y' SET @log = @log +'    Данные таблицы Транши загружены.\n'
	IF SUBSTRING(@logStatus, 5,1)='Y' SET @log = @log +'    Данные таблицы История событий по активам загружены.\n'
	IF SUBSTRING(@logStatus, 6,1)='Y' SET @log = @log +'    Данные таблицы Состояние обеспечения загружены.\n'
	IF SUBSTRING(@logStatus, 7,1)='Y' SET @log = @log +'    Данные таблицы Поправочные коэффициенты по активам загружены.\n'
	IF SUBSTRING(@logStatus, 8,1)='Y' SET @log = @log +'    Данные таблицы График платежей по активам загружены.\n'
	IF SUBSTRING(@logStatus, 9,1)='Y' SET @log = @log +'    Данные таблицы История изменения графика платежей загружены.\n'
	IF SUBSTRING(@logStatus,10,1)='Y' SET @log = @log +'    Данные таблицы История приостановления рассмотрения загружены.\n'
	IF (SUBSTRING(@logStatus,11,1)='Y') AND (SUBSTRING(@logStatus,12,1)='Y') SET @log = @log +'    Данные таблицы Дополнительные письма в КО загружены.\n'
	IF SUBSTRING(@logStatus,13,1)='Y' SET @log = @log +'    Данные таблицы Журнал операций с активами загружены.\n'
	IF SUBSTRING(@logStatus,14,1)='Y' SET @log = @log +'    Данные таблицы Журнал событий по ходатайствам загружены.\n'
	IF SUBSTRING(@logStatus,15,1)='Y' SET @log = @log +'    Данные таблицы Запросы документов по активам загружены.\n'
	IF SUBSTRING(@logStatus,16,1)='Y' SET @log = @log +'    Данные таблицы Жрнал операций для отчета по карточке актива загружены.\n'
	IF SUBSTRING(@logStatus,17,1)='Y' SET @log = @log +'    Данные таблицы Замечания по активам загружены.\n'
	IF SUBSTRING(@logStatus,18,1)='Y' SET @log = @log +'    Данные таблицы Журнал отчетов АС ППА загружены.\n'
	IF (SUBSTRING(@logStatus,19,1)='Y') AND (SUBSTRING(@logStatus,20,1)='Y') AND (SUBSTRING(@logStatus,21,1)='Y') SET @log = @log +'    Данные таблицы Обмен информации по активам загружены.\n'
  END	
  IF @return_value = 0
  BEGIN
	IF SUBSTRING(@logStatus, 1,1)='N' SET @log = @log +'    При загрузке данных таблицы Ходатайства произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 2,1)='N' SET @log = @log +'    При загрузке данных таблицы Активы произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 3,1)='N' SET @log = @log +'    При загрузке данных таблицы История изменения активов произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 4,1)='N' SET @log = @log +'    При загрузке данных таблицы Транши произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 5,1)='N' SET @log = @log +'    При загрузке данных таблицы История событий по активам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 6,1)='N' SET @log = @log +'    При загрузке данных таблицы Состояние обеспечения произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 7,1)='N' SET @log = @log +'    При загрузке данных таблицы Поправочные коэффициенты по активам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 8,1)='N' SET @log = @log +'    При загрузке данных таблицы График платежей по активам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus, 9,1)='N' SET @log = @log +'    При загрузке данных таблицы История изменения графика платежей произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,10,1)='N' SET @log = @log +'    При загрузке данных таблицы История приостановления рассмотрения произошла ошибка: '+@ErrorMSG 
	IF (SUBSTRING(@logStatus,11,1)='N') OR (SUBSTRING(@logStatus,12,1)='N') SET @log = @log +'    При загрузке данных таблицы Дополнительные письма в КО произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,13,1)='N' SET @log = @log +'    При загрузке данных таблицы Журнал операций с активами произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,14,1)='N' SET @log = @log +'    При загрузке данных таблицы Журнал событий по ходатайствам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,15,1)='N' SET @log = @log +'    При загрузке данных таблицы Запросы документов по активам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,16,1)='N' SET @log = @log +'    При загрузке данных таблицы Жрнал операций для отчета по карточке актива произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,17,1)='N' SET @log = @log +'    При загрузке данных таблицы Замечания по активам произошла ошибка: '+@ErrorMSG 
	IF SUBSTRING(@logStatus,18,1)='N' SET @log = @log +'    При загрузке данных таблицы Журнал отчетов АС ППА произошла ошибка: '+@ErrorMSG 
	IF (SUBSTRING(@logStatus,19,1)='N') OR (SUBSTRING(@logStatus,20,1)='N') OR (SUBSTRING(@logStatus,21,1)='N') SET @log = @log +'    При загрузке данных таблицы Обмен информации по активам произошла ошибка: '+@ErrorMSG 
  END
  IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
  BEGIN
    SET @log = @log + '\n\nЗагрузка данных невозможна'   
    RETURN 0
  END
  IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
    SET @log = @log + '\n\n    Загрузка данных будет продолжена'  

  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo

  -- Проверка количественных метрик
--  SET @log = @log + '--------------------------------------------------------------------------------'+'\n'
  SET @log = @log +'\n\n'
  EXEC	@return_value = [dbo].[StartCheckCounters] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  IF (@return_value = 1) and (@ErrorMSG = '') -- успешно
	SET @log = @log +'    Количественые проверки пройдены успешно.\n' 

  IF (@return_value = 1) and (@ErrorMSG <> '') -- не успешно
  BEGIN
	SET @log = @log +'При проведении проверок количественных метрик на прошел/ли контроль/ли: '+@ErrorMSG 
	IF (@ModeTest = 0)
	BEGIN
	  SET @log = @log + '\n\nЗагрузка данных невозможна. Обратитесь к разработчику системы для проведения анализа ситуации.'
	  RETURN 0
	END
  END
	
  IF @return_value = 0
  BEGIN
	SET @log = @log + 'Ошибка при проведении проверок количественных метрик: '+@ErrorMSG
	IF (@ModeTest = 0)
	BEGIN
	  SET @log = @log + '\n\nЗагрузка данных невозможна. Обратитесь к разработчику системы для проведения анализа ситуации.'
	  RETURN 0
	END		
  END

  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo

  
--  SET @log = @log + '--------------------------------------------------------------------------------'+'\n'
  SET @log = @log +'\n\n'

  EXEC	@return_value = [dbo].[StartMakeDKO] @IsHUB, @ErrorMSG = @ErrorMSG  OUTPUT,@TU=@TU
  IF (@return_value = 1) and (@isNotErr = 1)
	SET @isNotErr = 1 
  ELSE
	SET @isNotErr = 0

  IF @return_value = 1
  BEGIN 
    SET @log = @log +'Данные для корректировки ТУ '+@TU+' сформированы.\n'
	-- Проверка количественных метрик после успешной корректировки
	DECLARE @rv int, @EM varchar(max)
	SET @EM = ''
	EXEC @rv = [dbo].[StartCheckCountersAfterDKO] @IsHUB, @ErrorMSG = @EM OUTPUT, @TU=@TU
	IF (@rv = 1) and (@isNotErr = 1)
	  SET @isNotErr = 1 
	ELSE
	  SET @isNotErr = 0

	IF (@rv = 1) 
	BEGIN
	  SET @log = @log + @EM
	  IF (@ModeTest=1) AND (CHARINDEX('НЕуспешно',@EM,1)>0)
		SET @log = @log + '\n\n    Данные ТУ '+@TU+' загрузились с ошибками.\n'
	END
	IF @rv = 0
	  SET @log = @log + 'Ошибка при проведении проверок количественных метрик после корректировки: '+@EM
  END
  ELSE 
    SET @log= @Log + '    При создании данных для корректиовки с АС ДКО произошла ошибка: '+@ErrorMSG 
  IF ((@return_value = 0) and  (@isHUB = 1)) or ((@ModeTest = 0) and (@return_value = 0))
  BEGIN
    SET @log = @log + '\n\nЗагрузка данных невозможна'   
    RETURN 0
  END  
  IF (@return_value = 0) and  (@isHUB = 0) and (@ModeTest = 1)
  BEGIN
    SET @log = @log + '    Загрузка данных будет продолжена.'
  END  
  UPDATE DataTransferGo
  SET    isSuccess=@return_value, dtFinish=getdate(), 
         xmlLog='<log><text>' + @log + '</text></log>'
  WHERE  idDTGo=@idDTGo

  -- isLoad = 1
	UPDATE rfSubdivision SET isLoad = 1, isNotErr = @isNotErr WHERE KPTU = @TU

  RETURN 1

END TRY
BEGIN CATCH
  SELECT ERROR_MESSAGE()
  RETURN 0
END CATCH
GO