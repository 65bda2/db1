﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartStateConstraint]
@ErrorMSG varchar(max) output
as 
BEGIN TRY
      IF OBJECT_ID(N'tempdb..#TempChkInt', N'U') IS NOT NULL DROP TABLE #TempChkInt;
      SELECT DISTINCT tp='hub_DKO_hrAsset', ID = '_NumActive', IDValue = cast(hub_DKO_hrAsset._NumActive as varchar) INTO #TempChkInt  
      FROM hub_DKO_hrAsset 
      LEFT JOIN hub_jrActive ON hub_jrActive.NumActive=hub_DKO_hrAsset._NumActive
      LEFT JOIN hub_jrtransh ON hub_jrtransh.IdTransh=hub_DKO_hrAsset._NumActive
      WHERE (hub_jrActive.NumActive is null and hub_DKO_hrAsset.IdParentAsset is null) and (hub_DKO_hrAsset.IdParentAsset is not null and hub_jrtransh.IdTransh is not null)--Aheburg 1763 проверка по траншам
      
      UNION ALL
      SELECT DISTINCT tp='hub_DKO_hrBankInTU', ID = '_IdKO', IDValue = cast(hub_DKO_hrBankInTU._IdKO as varchar) FROM hub_DKO_hrBankInTU LEFT JOIN hub_jrKO ON hub_DKO_hrBankInTU._IdKO=hub_jrKO.idKO WHERE hub_jrKO.idKO is null
      UNION ALL
      
      SELECT DISTINCT tp='hub_DKO_hrCompany', ID = '_IdOrg', IDValue = cast(h._IdOrg as varchar)  FROM hub_DKO_hrCompany h LEFT JOIN hub_jrKO h2 ON h._IdOrg =h2.idKO + 200000000 WHERE h2.idKO is null And h._IdOrg >= 200000000
      UNION ALL
      
      SELECT DISTINCT tp='hub_DKO_hrCompany', ID = '_IdOrg', IDValue = cast(h._IdOrg as varchar)  FROM hub_DKO_hrCompany h LEFT JOIN hub_jrOrg h2 ON h._IdOrg =h2.idOrg WHERE h2.idOrg is null And h._IdOrg < 200000000
      UNION ALL
      
      --SELECT DISTINCT tp='hub_DKO_hrMutualParticipation', ID = 'CORegNum', IDValue = cast(h.CORegNum as varchar) FROM hub_DKO_hrMutualParticipation h LEFT JOIN hub_jrKO h2 ON h.CORegNum =h2.nuReg WHERE h2.nuReg is null
      --UNION ALL
      -- 65bda2 SELECT DISTINCT tp='hub_DKO_hrRepayment', ID = 'IdAssetIdRecord', IDValue = cast(h.IdAssetIdRecord as varchar) FROM hub_DKO_hrRepayment h LEFT JOIN hub_DKO_hrAsset h2 ON h.IdAssetIdRecord =h2.IdRecord WHERE h2.IdRecord is null
	  SELECT DISTINCT tp='hub_DKO_hrRepayment', ID = 'IdAsset', IDValue = cast(h.IdAsset as varchar) FROM hub_DKO_hrRepayment h LEFT JOIN hub_DKO_hrAsset h2 ON h.IdAsset =h2.IdAsset WHERE h2.IdRecord is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_hrActive h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL

      SELECT DISTINCT tp='hub_hrActive', ID = 'Company', IDValue = cast(h.Company as varchar) FROM hub_hrActive h LEFT JOIN hub_jrOrg h2 ON h.Company =h2.idOrg WHERE h2.idOrg is null and h.TypeActive<>'M'
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'Company', IDValue = cast(h.Company as varchar) FROM hub_hrActive h LEFT JOIN hub_jrKO h2 ON h.Company =h2.idKO WHERE h2.idKO is null and h.TypeActive='M'

      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'NmZaemshik', IDValue = cast(h.NmZaemshik as varchar) FROM hub_hrActive h LEFT JOIN hub_jrOrg h2 ON h.NmZaemshik =h2.idOrg WHERE h2.idOrg is null and h.TypeActive<>'M'
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'NmZaemshik', IDValue = cast(h.NmZaemshik as varchar) FROM hub_hrActive h LEFT JOIN hub_jrKO h2 ON h.NmZaemshik =h2.idKO WHERE h2.idKO is null and h.TypeActive='M'

      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'Currency', IDValue = cast(h.Currency as varchar) FROM hub_hrActive h LEFT JOIN hub_jrCurrency h2 ON h.Currency =h2.nuISO WHERE h2.nuISO is null and cast(h.Currency as varchar)!=-1  --aheburg 1760 
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'idLetter', IDValue = cast(h.idLetter as varchar) FROM hub_hrActive h LEFT JOIN hub_jrLetter h2 ON h.idLetter =h2.idLetter WHERE h2.idLetter is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'firstCurrency', IDValue = cast(h.firstCurrency as varchar) FROM hub_hrActive h LEFT JOIN hub_jrCurrency h2 ON h.firstCurrency =h2.nuISO WHERE h2.nuISO is null and cast(h.firstCurrency as varchar)!=-1 --aheburg 1760 
      UNION ALL
      SELECT DISTINCT tp='hub_hrActive', ID = 'CostCurrency', IDValue = cast(h.CostCurrency as varchar) FROM hub_hrActive h LEFT JOIN hub_jrCurrency h2 ON h.CostCurrency =h2.nuISO WHERE h2.nuISO is null and cast(h.CostCurrency as varchar)!=-1 --aheburg 1760 
      UNION ALL
      SELECT DISTINCT tp='hub_hrActiveEvent', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_hrActiveEvent h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrFactor', ID = 'idAsset', IDValue = cast(h.idAsset  as varchar)  FROM hub_hrFactor h LEFT JOIN hub_jrActive h2 ON h.idAsset =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrOrg', ID = 'idOrg', IDValue = cast(h.idOrg as varchar) FROM hub_hrOrg h LEFT JOIN hub_jrOrg h2 ON h.idOrg =h2.idOrg WHERE h2.idOrg is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrOwnFunds', ID = 'idKO', IDValue = cast(h.idKO as varchar)  FROM hub_hrOwnFunds h LEFT JOIN hub_jrKO h2 ON h.idKO =h2.idKO WHERE h2.idKO is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrPaymentGrafic', ID = 'NumActive', IDValue = cast(h.NumActive as varchar)  FROM hub_hrPaymentGrafic h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrPaymentGrafic', ID = 'IdTransh', IDValue = cast(h.IdTransh as varchar)  FROM hub_hrPaymentGrafic h LEFT JOIN (SELECT * FROM hub_jrTransh WHERE IdTransh is NOT null) as h2 ON h.IdTransh =h2.IdTransh WHERE h2.IdTransh is null and h.IdTransh is not null
      UNION ALL
      SELECT DISTINCT tp='hub_hrStockSect', ID = 'idStock', IDValue = cast(h.idStock as varchar) FROM hub_hrStockSect h LEFT JOIN hub_jrActive h2 ON h.idStock =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrStockSect', ID = 'idBlock', IDValue = cast(h.idBlock as varchar) FROM hub_hrStockSect h LEFT JOIN hub_jrBlock h2 ON h.idBlock =h2.idBlock WHERE h2.idBlock is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrWaitCheck', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_hrWaitCheck h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrWaitCheck', ID = 'IdTransh', IDValue = cast(h.IdTransh as varchar) FROM hub_hrWaitCheck h LEFT JOIN (SELECT * FROM hub_jrTransh WHERE IdTransh is NOT null) as h2 ON h.IdTransh =h2.IdTransh WHERE h2.IdTransh is null and h.IdTransh is not null
      UNION ALL
      SELECT DISTINCT tp='hub_JrBuhRecord', ID = 'idOrg', IDValue = cast(h.idOrg as varchar) FROM hub_JrBuhRecord h LEFT JOIN hub_jrOrg h2 ON h.idOrg =h2.idOrg WHERE h2.idOrg is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrAccount', ID = 'idKO', IDValue = cast(h.idKO as varchar) FROM hub_jrAccount h LEFT JOIN hub_jrKO h2 ON h.idKO =h2.idKO WHERE h2.idKO is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrAccount', ID = 'nuGKD', IDValue = cast(h.nuGKD as varchar) FROM hub_jrAccount h LEFT JOIN hub_jrGKD h2 ON h.nuGKD =h2.nuGKD WHERE h2.nuGKD is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrAccount', ID = 'idRKC', IDValue = cast(h.idRKC as varchar) FROM hub_jrAccount h LEFT JOIN hub_jrRKC h2 ON h.idRKC =h2.idRKC WHERE h2.idRKC is null
      UNION ALL

      SELECT DISTINCT tp='hub_jrActive', ID = 'Company', IDValue = cast(h.Company as varchar) FROM hub_jrActive h LEFT JOIN hub_jrOrg h2 ON h.Company =h2.idOrg WHERE h2.idOrg is null and h.TypeActive<>'M'
      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'Company', IDValue = cast(h.Company as varchar) FROM hub_jrActive h LEFT JOIN hub_jrKO h2 ON h.Company =h2.idKO WHERE h2.idKO is null and h.TypeActive='M'

      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'NmZaemshik', IDValue = cast(h.NmZaemshik as varchar) FROM hub_jrActive h LEFT JOIN hub_jrOrg h2 ON h.NmZaemshik =h2.idOrg WHERE h2.idOrg is null and h.TypeActive<>'M'
      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'NmZaemshik', IDValue = cast(h.NmZaemshik as varchar) FROM hub_jrActive h LEFT JOIN hub_jrKO h2 ON h.NmZaemshik =h2.idKO WHERE h2.idKO is null and h.TypeActive='M'

      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'idLetter', IDValue = cast(h.idLetter as varchar) FROM hub_jrActive h LEFT JOIN hub_jrLetter h2 ON h.idLetter =h2.idLetter WHERE h2.idLetter is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'firstCurrency', IDValue = cast(h.firstCurrency as varchar) FROM hub_jrActive h LEFT JOIN hub_jrCurrency h2 ON h.firstCurrency =h2.nuISO WHERE h2.nuISO is null and  cast(h.firstCurrency as varchar)!=-1  --aheburg 1760 
      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'CostCurrency', IDValue = cast(h.CostCurrency as varchar) FROM hub_jrActive h LEFT JOIN hub_jrCurrency h2 ON h.CostCurrency =h2.nuISO WHERE h2.nuISO is null and cast(h.CostCurrency as varchar)!=-1 --aheburg 1760 
      UNION ALL
      SELECT DISTINCT tp='hub_jrActive', ID = 'Currency', IDValue = cast(h.Currency as varchar) FROM hub_jrActive h LEFT JOIN hub_jrCurrency h2 ON h.Currency =h2.nuISO WHERE h2.nuISO is null 
      UNION ALL
      SELECT DISTINCT tp='hub_jrBlock', ID = 'idBlock', IDValue = cast(h.idBlock as varchar) FROM hub_jrBlock h LEFT JOIN hub_hrStockSect h2 ON h.idBlock =h2.idBlock WHERE h2.idBlock is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrDopLetter', ID = 'idKO', IDValue = cast(h.idKO as varchar) FROM hub_jrDopLetter h LEFT JOIN hub_jrKO h2 ON h.idKO =h2.idKO WHERE h2.idKO is null
      UNION ALL
      SELECT DISTINCT tp='hub_rfDopLetActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_rfDopLetActive h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrGKD', ID = 'idKO', IDValue = cast(h.idKO as varchar) FROM hub_jrGKD h LEFT JOIN hub_jrKO h2 ON h.idKO =h2.idKO WHERE h2.idKO is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrJournalActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_jrJournalActive h LEFT JOIN hub_jrActive  h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_JrJournalLetter', ID = 'idletter', IDValue = cast(h.idletter as varchar) FROM hub_JrJournalLetter h LEFT JOIN hub_jrLetter h2 ON h.idletter =h2.idletter WHERE h2.idletter is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrKO', ID = 'idRKC', IDValue = cast(h.idRKC as varchar) FROM hub_jrKO h LEFT JOIN hub_jrRKC h2 ON h.idRKC =h2.idRKC WHERE h2.idRKC is null and h.idRKC is not null
      UNION ALL
      SELECT DISTINCT tp='hub_jrLetter', ID = 'idKo', IDValue = cast(h.idKo as varchar) FROM hub_jrLetter h LEFT JOIN hub_jrKO h2 ON h.idKo =h2.idKo WHERE h2.idKo is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrNoteActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar)  FROM hub_jrNoteActive h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrNoteActive', ID = 'IdTransh', IDValue = cast(h.IdTransh as varchar) FROM hub_jrNoteActive h LEFT JOIN (SELECT * FROM hub_jrTransh WHERE IdTransh is NOT null) as h2 ON h.idTransh =h2.idTransh WHERE h2.IdTransh is null and h.IdTransh is not null
      UNION ALL
      SELECT DISTINCT tp='hub_jrPaymentGrafic', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_jrPaymentGrafic h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrPaymentGrafic', ID = 'IdTransh', IDValue = cast(h.IdTransh as varchar) FROM hub_jrPaymentGrafic h LEFT JOIN (SELECT * FROM hub_jrTransh WHERE IdTransh is NOT null) as h2 ON h.idTransh =h2.idTransh WHERE h2.IdTransh is null and h.IdTransh is not null
      UNION ALL
      SELECT DISTINCT tp='hub_jrRequestDocActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_jrRequestDocActive h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrTransh', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_jrTransh h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_jrTransh', ID = 'idLetter', IDValue = cast(h.idLetter as varchar) FROM hub_jrTransh h LEFT JOIN hub_jrLetter h2 ON h.IdLetter =h2.idLetter WHERE h2.idLetter is null
      UNION ALL
      SELECT DISTINCT tp='hub_lgArchActive', ID = 'NumActive', IDValue = cast(h.NumActive as varchar) FROM hub_lgArchActive h LEFT JOIN hub_jrActive h2 ON h.NumActive =h2.NumActive WHERE h2.NumActive is null
      UNION ALL
      SELECT DISTINCT tp='hub_rfAffPers', ID = 'idKO', IDValue = cast(h.idKO as varchar) FROM hub_rfAffPers h LEFT JOIN hub_jrKO h2 ON h.idKO =h2.idKO WHERE h2.idKO is null
      UNION ALL
      SELECT DISTINCT tp='hub_rfAffPers', ID = 'idOrg', IDValue = cast(h.idOrg as varchar) FROM hub_rfAffPers h LEFT JOIN hub_jrOrg h2 ON h.idOrg =h2.idOrg WHERE h2.idOrg is null
      UNION ALL
      SELECT DISTINCT tp='hub_hrBuhRecord', ID = 'idOrg', IDValue = cast(h.idOrg as varchar) FROM hub_hrBuhRecord h LEFT JOIN hub_jrOrg h2 ON h.idOrg =h2.idOrg WHERE h2.idOrg is null
      UNION ALL
      SELECT DISTINCT tp='hub_rfGKD', ID = 'nuGKD', IDValue = cast(h.nuGKD as varchar) FROM hub_rfGKD h LEFT JOIN hub_jrGKD h2 ON h.nuGKD =h2.nuGKD WHERE h2.nuGKD is null
      
      SET @ErrorMSG = (SELECT '    '+tp+';'+ ID+';' + IDValue+'\n' FROM #TempChkInt FOR XML Path(''))
      SET @ErrorMSG = SUBSTRING(@ErrorMSG ,1, len(@ErrorMSG)-2) + '\n' 
       
      IF (Select COUNT (*) FROM #TempChkInt)>0
        RETURN 0
      ELSE  
        RETURN 1
      
END TRY

BEGIN CATCH
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
  INSERT INTO	 rfSign (tpSign,nmSign) 
  VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution'    

  SELECT @ErrorMSG = ERROR_MESSAGE()
  --ROLLBACK TRANSACTION    
  RETURN 0
END CATCH
GO