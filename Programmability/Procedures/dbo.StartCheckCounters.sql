﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartCheckCounters]
@IsHUB BIT,
@ErrorMSG varchar(max) output ,
@TU varchar(2)
AS
BEGIN
  DECLARE @c int
  DECLARE @h int
  DECLARE @i int
  SET @ErrorMSG = ''

BEGIN TRY
  IF @IsHUB = 1 RETURN 1
  ELSE BEGIN
	-- jrKO
	SELECT @c = ValueCounter FROM hub_Counters WHERE NameCounter = 'jrKO'
	SELECT @i = COUNT(*) FROM jrKO
	SELECT @h = COUNT(ko.idKO) FROM jrKO ko INNER JOIN hub_Keys hkey ON hkey.idHubKey = ko.idKO
		WHERE hkey.TU = @TU AND hkey.NameTable = 'jrKO' AND ISNULL(hkey.isReplace, 0) = 0

	IF @i <> (@c + @h)
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrKO не равно сумме количества строк до начала загрузки '+LTRIM(STR(@c))+' и количества строк добавленных в Хаб '+LTRIM(STR(@h))+'.\n'

	-- jrGKD
	SELECT @c = ValueCounter FROM hub_Counters WHERE NameCounter = 'jrGKD'
	SELECT @i = COUNT(*) FROM jrGKD
	SELECT @h = COUNT(gkd.nuGKD) FROM jrGKD gkd INNER JOIN hub_Keys hkey ON hkey.idHubKey = gkd.nuGKD
		WHERE hkey.TU = @TU AND hkey.NameTable = 'jrGKD' AND ISNULL(hkey.isReplace, 0) = 0

	IF @i <> (@c + @h)
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrGKD не равно сумме количества строк до начала загрузки '+LTRIM(STR(@c))+' и количества строк добавленных в Хаб '+LTRIM(STR(@h))+'.\n'

	-- jrAccount
	SELECT @c = ValueCounter FROM hub_Counters WHERE NameCounter = 'jrAccount'
	SELECT @i = COUNT(*) FROM jrAccount
	SELECT @h = COUNT(acc.idAcc) FROM jrAccount acc INNER JOIN hub_Keys hkey ON hkey.idHubKey = acc.idAcc
		WHERE hkey.TU = @TU AND hkey.NameTable = 'jrAccount' AND ISNULL(hkey.isReplace, 0) = 0

	IF @i <> (@c + @h)
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrAccount не равно сумме количества строк до начала загрузки '+LTRIM(STR(@c))+' и количества строк добавленных в Хаб '+LTRIM(STR(@h))+'.\n'

	-- jrOrg
	SELECT @c = ValueCounter FROM hub_Counters WHERE NameCounter = 'jrOrg'
	SELECT @i = COUNT(*) FROM jrOrg
	SELECT @h = COUNT(org.idOrg) FROM jrOrg org INNER JOIN hub_Keys hkey ON hkey.idHubKey = org.idOrg
		WHERE hkey.TU = @TU AND hkey.NameTable = 'jrOrg' AND ISNULL(hkey.isReplace, 0) = 0

	IF @i <> (@c + @h)
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrOrg не равно сумме количества строк до начала загрузки '+LTRIM(STR(@c))+' и количества строк добавленных в Хаб '+LTRIM(STR(@h))+'.\n'

	-- jrActive
	SELECT @c = ValueCounter FROM hub_Counters WHERE NameCounter = 'jrActive'
	SELECT @i = COUNT(*) FROM jrActive
	SELECT @h = COUNT(active.NumActive) FROM jrActive active INNER JOIN hub_Keys hkey on hkey.idHubKey = active.NumActive
		WHERE hkey.TU = @TU AND hkey.NameTable = 'jrActive' AND ISNULL(hkey.isReplace, 0) = 0

	IF @i <> (@c + @h)
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrActive не равно сумме количества строк до начала загрузки '+LTRIM(STR(@c))+' и количества строк добавленных в Хаб '+LTRIM(STR(@h))+'.\n'

    --AHEBURG-2096
	SELECT @i = COUNT(*) FROM jrActive where (status = 'N' or status = 'T') and SUBSTRING(UnNum,1,2) = @TU 
	SELECT @h = COUNT(*) FROM hrActiveSect WHERE NumActive in (SELECT NumActive FROM jrActive WHERE (status = 'N' or status = 'T') and SUBSTRING(UnNum,1,2) = @TU) and
												 DateAssign in (select dtWork FROM rfStatus)

	IF @i <> @h
		SET @ErrorMSG = @ErrorMSG + 'Количество строк '+LTRIM(STR(@i))+' в таблице jrActive '+LTRIM(STR(@i))+' не равно сумме количества строк количества строк в таблице hrActiveSect '+LTRIM(STR(@h))+'.\n'

	IF @ErrorMSG <> ''
	BEGIN
		IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
		  INSERT INTO rfSign (tpSign,nmSign) VALUES('StartSolution',null) 

		UPDATE rfSign SET nmSign = '-1' WHERE tpSign = 'StartSolution' 
	END
	
	RETURN 1
  END
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
END
GO