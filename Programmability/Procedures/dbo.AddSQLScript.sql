﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		Бряков Д.А.
-- Create date: 18.03.2020
-- Description:	Сохранение истории выполнения скриптов на БД
-- =============================================
CREATE PROCEDURE [dbo].[AddSQLScript]
(
@NameScript varchar(50),       -- Наименование файла скрипта
@SQLScript varchar(max) = '',  -- Скрипт
@VersionAfter varchar(10) = '',-- Версия БД, после выполнения скрипта
@DBname varchar(50),		   -- Имя БД, на которой выполняется скрипт
@R bit,						   -- Успешное выполнение или нет
@ErrorText varchar(max) = ''   -- Текст ошибки
)
AS
BEGIN
	SET NOCOUNT ON; 
	DECLARE @A varchar(100)
	IF @VersionAfter = '' 
		SELECT @VersionAfter = [version] FROM rfStatus
	
	SELECT @A = program_name FROM sys.sysprocesses WHERE spid = @@SPID

	INSERT INTO hrSQLScripts (NameScript, VersionAfter, [Application], DBname, SQLScript, Result, ErrorText) 
	VALUES(@NameScript, @VersionAfter, @A, @DBname, @SQLScript, @R, @ErrorText)
END
GO