﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spGetOutsysNonmarket](@dtWork SMALLDATETIME, @idAcc INT, @numRazdel varchar(10)) AS
BEGIN 
select rfAS.tpSect
	, jrA.NomerVD + ' от ' + CONVERT (varchar, jrA.CreationDate, 104) as NomerDateVD
	, jrA.CreationDate as CreationDate
    , jrA.UnNum as nuReg
        , jrA.NumActive as idStock
	, jrA.DatePayment as dtStockOut
	--, jrA.TempCostRub as mnPrice
	,(CASE 
		WHEN (rfAS.tpSect IN ('22N', '21U', '21L')) THEN jrA.TempCostRub
		ELSE dbo.CalcActiveCost(jrA.NumActive, dbo.IncWorkDay(@dtWork, 61),@dtWork)
	  END
	) as mnPrice -- Стоимость актива должна считаться с учетом погашения платежей в ближайшие 60 дней 65eav2
	, jrO.CompanyName
	,(case
			 when (isnull(hrF.pcFactor,0))=0 
			 then 0 
			 else 
			  hrF.pcFactor
	 end
		)  as pcFactor 
	--,(case
	--		 when (isnull(hrF.pcFactor,0))=0 
	--		 then 0 
	--		 else 
	--		  hrF.pcFactor * jrA.TempCostRub
	-- end
	--	)  as mnPriceCoeff
	,(case
			 when (isnull(hrF.pcFactor,0))=0 
			 then 0 
			 else 
			  hrF.pcFactor * (CASE 
								WHEN (rfAS.tpSect IN ('22N', '21U', '21L')) THEN jrA.TempCostRub
								ELSE dbo.CalcActiveCost(jrA.NumActive, dbo.IncWorkDay(@dtWork, 61),@dtWork)
							  END)
	 end
		)  as mnPriceCoeff -- Стоимость актива должна считаться с учетом погашения платежей в ближайшие 60 дней 65eav2
	, jrO.CompanyName
	, jrA.status
	, hrAS.DateAssign as dtSect
from jrActive jrA
	join hrActiveSect hrAS on jrA.NumActive=hrAS.NumActive
	join jrAccount jrAcc on hrAS.idAcc=jrAcc.idAcc
	join jrGKD jrG on jrG.nuGKD=jrAcc.nuGKD
	join rfActiveState rfAS on rfAS.idActiveState=hrAS.idActiveState
	join jrORG jrO on jrO.idOrg=jrA.Company
	left outer join hrFactor hrF on hrF.idAsset=jrA.NumActive and hrF.tpAsset = 'B' and
						 (hrf.dtBegin= (select MAX(hrF1.dtBegin) from hrFactor hrF1 
						 where hrF1.idAsset=jrA.NumActive and hrF1.dtBegin<=@dtWork and  hrF1.tpAsset = 'B'  ))
where hrAS.DateAssign = @dtWork
	and  jrAcc.dtIncl <= @dtWork 
	and  (jrAcc.dtExcl >= @dtWork or jrAcc.dtExcl is null)
	and  jrAcc.tpAsset = 'B'
	and (jrG.dtGKDend > @dtWork or dtGKDend is null)
	and jrAcc.idAcc=@idacc
	and (
			(@numRazdel = '1' and rfAS.tpSect in ('1', '1U', '1C', '11D', '11Z', '11U', '12L')) 
			or
			(@numRazdel = '1.1' and rfAS.tpSect in ('1U', '1C', '11D', '11Z', '11U'))
			or
			(@numRazdel = '1.2' and rfAS.tpSect in ('12L'))
			or
			(@numRazdel = '1.3' and rfAS.tpSect in ('1'))
			or
			(@numRazdel = '2.1' and rfAS.tpSect in ('21U', '21C', '21L'))
			or
			(@numRazdel = '2.2' and rfAS.tpSect in ('22N'))
			or
			(@numRazdel = '3' and rfAS.tpSect in ('3L', '3N'))
		)
order by jrA.unnum
END
GO