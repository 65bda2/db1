﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Author:		65vev3
-- Create date: 22/07/2019
-- Description:	Вставка в hrActiveSect раздела 11D при исключении актива из пула
-- =============================================
CREATE PROCEDURE [dbo].[spSetRazdelForRefusalActive]
@NumActive Int
AS
BEGIN
  Declare @idAcc Int, @dtIncl DateTime, @dtWork DateTime, @Razdel11D Int

  --- idActiveState,соответствующий разделу 11D
  Select @Razdel11D = idActiveState From rfActiveState Where tpSect = '11D'

  --- Дата операционного дня
  Select  @dtWork = dtWork From rfStatus
  --- Дата включения актива в пул обеспечения
  Select @dtIncl = inclDate from jrActive Where NumActive = @NumActive

  --- Если дата включения в пул больше даты опнрационного дня, перемещение делаем датой включения в пул
  If CAST( @dtIncl as Date ) > CAST( @dtWork as Date )
    Select @dtWork = @dtIncl
  
  --- Ищем счет для актива 
  Select Top 1 @idAcc = idAcc From hrActiveSect Where NumActive = @NumActive Order By DateAssign Desc
  
  --- Если в hrActiveSect нет записей для актива, ищем действующий счет по КО 
  If @idAcc Is Null
    Select @idAcc = dbo.fnGetAccForActive( @NumActive )
    
  --- Если счет не найден, то ошибка  
  If @idAcc Is Null
  Begin
    Declare @UnNum VarChar(16), @ErrMsg VarChar(255)
    Select @UnNum = UnNum From jrActive Where NumActive = @NumActive
    Select @ErrMsg = 'Для актива - ' + @UnNum + ' не найден действующий счёт'
    RaisError( @ErrMsg, 16, 1  )  
  End  
                         
  --- Удаляем существующие записи                       
  Delete From hrActiveSect WHERE DateAssign = @dtWork AND NumActive = @NumActive
  --- Вставляем новые
  Insert Into hrActiveSect ( DateAssign, NumActive, idAcc,  idActiveState )
                    Values ( @dtWork,   @NumACtive, @idAcc, @Razdel11D    )
END
GO