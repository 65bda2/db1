﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartImportBO]
@IsHUB BIT,
@TU varchar(2),
@ErrorMSG varchar(max) output AS
BEGIN TRY
  IF @IsHUB = 1
  BEGIN
	DELETE FROM jrOrgBuhRecord;
	DELETE FROM jrOrgBuhReport;
  END;

  DECLARE @T TABLE (idPeriod int, idOrg int, idKO int, idOrgBuhReport int)
  DECLARE @idPeriod int
  DECLARE @idOrg int
  DECLARE @idKO int

  INSERT INTO @T (idPeriod, idOrg, idKO)
  SELECT DISTINCT f.idPeriod, d1.idHubKey as idOrg, a1.idHubKey as idKO
  FROM hub_jrKO a INNER JOIN 
		 hub_Keys a1 ON a.idKO=a1.idTUKey AND a1.NameTable='jrKO' AND a1.TU=@tu INNER JOIN
		 hub_jrLetter b ON a.idKO=b.idKO INNER JOIN
		 hub_jrActive c ON b.idLetter=c.idLetter INNER JOIN
		 hub_jrBuhRecord d ON c.Company=d.idOrg INNER JOIN
		 hub_Keys d1 ON d.idOrg=d1.idTUKey AND d1.NameTable='jrOrg' AND d1.TU=@tu INNER JOIN
		 hub_rfBuhPeriod e ON d.idPeriod=e.idPeriod INNER JOIN
		 rfBuhPeriod f ON e.NumPeriod=f.NumPeriod AND e.YearPeriod=f.YearPeriod 
  ORDER BY f.idPeriod, d1.idHubKey, a1.idHubKey;

  DECLARE cr CURSOR
  FOR SELECT idPeriod, idOrg, idKO FROM @T
  OPEN cr
  FETCH NEXT FROM cr
  INTO @idPeriod, @idOrg, @idKO	
  WHILE @@FETCH_STATUS = 0
  BEGIN
  	IF NOT EXISTS(SELECT idOrgBuhReport FROM jrOrgBuhReport WHERE idPeriod=@idPeriod AND idOrg=@idOrg AND idKO=@idKO) 
	BEGIN
	  INSERT INTO jrOrgBuhReport (idES,idPeriod,DateReport,idKO,idOrg,OKVED,isRuleControl,isCorrect,isReferense,isBookKeepingOK,isArchive)
	  SELECT NULL, a.idPeriod, MAX(f.OperationDate) as DateReport, a.idKO, a.idOrg, d.codeOKVED, 1, 1, 0, d.isBookKeepingOK, 0 
	  FROM @T a INNER JOIN rfBuhPeriod b ON a.idPeriod=b.idPeriod 
					  INNER JOIN hub_rfBuhPeriod c ON b.NumPeriod=c.NumPeriod AND b.YearPeriod=c.YearPeriod
					  INNER JOIN jrOrg d ON a.idOrg=d.idOrg
					  INNER JOIN hub_Keys e ON a.idOrg=e.idHUBKey AND e.NameTable='jrOrg' AND e.TU=@TU 
					  INNER JOIN hub_jrBuhRecord f ON c.idPeriod=f.idPeriod and e.idTUKey=f.idOrg 					  
	  WHERE a.idPeriod=@idPeriod AND a.idOrg=@idOrg AND a.idKO=@idKO
	  GROUP BY a.idPeriod, a.idOrg, a.idKO, d.codeOKVED, d.isBookKeepingOK;	
			
	  UPDATE @T SET idOrgBuhReport = IDENT_CURRENT('jrOrgBuhReport') WHERE idPeriod=@idPeriod AND idOrg=@idOrg AND idKO=@idKO;
	END;
	FETCH NEXT FROM cr
	INTO @idPeriod, @idOrg, @idKO
  END
  CLOSE cr
  DEALLOCATE cr

  UPDATE jrOrgBuhReport SET isReferense=1 WHERE idOrgBuhReport IN (SELECT MIN(idOrgBuhReport) FROM @T WHERE idOrgBuhReport IS NOT NULL 
																	AND idOrgBuhReport NOT IN (SELECT idOrgBuhReport FROM jrOrgBuhReport WHERE isReferense=1 AND idPeriod=@idPeriod AND idOrg=@idOrg) 
																	GROUP BY idPeriod,idOrg);

  INSERT INTO jrOrgBuhRecord (idOrgBuhReport,idBuhRecord,ShowingAccSum)
  SELECT DISTINCT a.idOrgBuhReport, h.idBuhRecord, f.RValue
  FROM jrOrgBuhReport a INNER JOIN
		 @T b ON a.idOrgBuhReport=b.idOrgBuhReport INNER JOIN
		 hub_Keys c ON a.idOrg=c.idHubKey AND c.NameTable='jrOrg' AND c.TU=@TU INNER JOIN
		 rfBuhPeriod d ON a.idPeriod=d.idPeriod INNER JOIN 
		 hub_rfBuhPeriod e ON d.NumPeriod=e.NumPeriod AND d.YearPeriod=e.YearPeriod INNER JOIN
		 hub_jrBuhRecord f ON c.idTUKey=f.idOrg AND e.idPeriod=f.idPeriod AND a.DateReport=f.OperationDate INNER JOIN 
		 hub_rfBuhRecord g ON f.idRecord=g.idRecord	INNER JOIN 
		 rfBuhRecord h ON g.NumKod=h.NumKod			 
  ORDER BY h.idBuhRecord;

  RETURN 1;
END TRY
BEGIN CATCH
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
  INSERT INTO	 rfSign (tpSign,nmSign) 
  VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution'

  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
GO