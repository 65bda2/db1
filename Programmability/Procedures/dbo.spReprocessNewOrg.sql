﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


-- =============================================
-- Author:		65psv1
-- Create date: 20200702
-- Description:	Установка признака возможности повторной обработки новой организации
-- =============================================
CREATE PROCEDURE [dbo].[spReprocessNewOrg] 
	@UnNum Char(16)
AS
BEGIN
DECLARE @idES INT --id ЭС, по которому отклоняем актив
DECLARE @idKO INT --id КО у отозванного ЭС
DECLARE @idES_New INT -- id ЭС, из которого берется новыйэталон для организации
DECLARE @idKO_New INT --id КО для новой "Новой организации" 
DECLARE @idOrg INT --организация, которая была внесена в список новых, из ЭС с idES = @idES
DECLARE @count_Org INT =0 -- количество организаций, которые были внесены в список новых, из ЭС с idES = @idES
DECLARE @count_Active INT = 0 --количество найденных активов с "удаляемой организацией"
DECLARE @count_Change INT = 0 --количество расхождений в значениях удаляемого и нового эталонов организации 
DECLARE @i_Org INT = 1 --счетчик для цикла по организациям из с101
DECLARE @i_Active INT = 1 -- счетчик для найденных активов с "удаляемой" организацией
DECLARE @i_Change INT = 1 --счетчик для найденных расхождений в значениях удаляемого и нового эталонов организации 
DECLARE @nmFieldASPPA VARCHAR(50)
DECLARE @NewValue VARCHAR(500) 
DECLARE @idField INT
DECLARE @sql_str VARCHAR(6000)
  Begin Try
    Begin Transaction
	  --1.Найти idES соответствующее С101 (по соответствующему UnNum в таблице hrES найти ЭС типа EDType='C101')
	  SELECT @idES = idES, @idKO = idKO FROM EDOKO_hrES WHERE EDType = 'C101' AND UnNum = @UnNum
	  --2. Если при обработке С101 (далее С101-НО) была создана НО
	  --В таблице jrNewORG найти строки с idES найденном на предыдущем шаге, запомнить соответствующие idOrg  (организаций может быть 2)
	  --* Если вместе с активом не была создана НО, то следует выполнить действия 2, 3, 4, 5 пункта 4.1.1.
	  SET @count_Org = (SELECT count(idNewOrg) FROM jrNewORG WHERE idES = @idES)
	  
	  IF @count_Org > 0
	  BEGIN
	    CREATE TABLE #Temp_Org (id int Identity(1,1), idNewOrg int, idORG int) 
	    INSERT INTO #Temp_Org
	      SELECT idNewOrg, idOrg FROM jrNewORG WHERE idES = @idES

		--3. Проверить статус НО (прошла ли НО рассмотрение)
		--* Если найденная на предыдущем шаге НО находится в другом статусе OrgStatus<>R, то следует выполнить действия 2, 3, 4, 5 пункта 4.1.1. и алгоритм согласно Таблице 1 не выполнять.
		--Если найденная на предыдущем шаге НО находится в статусе OrgStatus=R необходимо произвести обработку данной ситуации согласно Таблице 1.
		--Если на предыдущем шаге было определено 2 организации (такое может быть если и заемщик и обязанное лицо являются НО), то алгоритм в Таблице 1 необходимо применить к каждой и них.
		--Организацию, к которой применяется алгоритм согласно Таблице 1, обозначим ОРГ-НО.	
		WHILE @i_Org <= @count_Org
		BEGIN	
		  IF EXISTS (SELECT * FROM jrORG WHERE idOrg = (SELECT idOrg FROM #Temp_Org WHERE id = @i_Org) AND OrgStatus = 'R')
		  BEGIN
		    SELECT @idOrg = idOrg FROM #Temp_Org WHERE id = @i_Org
			-- таблица 1
			--1.Осуществить поиск других (соответствующих другим idES) активов (jrActive) в статусах на рассмотрении/рассмотрение приостановлено (R/W) вошедших электронно (isElectronForm=1)
			--с организацией ОРГ-НО в качестве заемщика (NmZaemshik) или обязанного лица (Company).
			--Запомнить массив активов С101-Массив
			-- таблица 1
			--1.1.Выстроить данные в С101-Массив в порядке их приема в АС ППА, упорядочить по возрастанию idES, взять для обработки первое С101-Массив[1] с наименьшим idES
			CREATE TABLE #Temp_ActiveC101 (id int identity (1,1), NumActive int, UnNum char(16), idES int, idKO int)
			INSERT INTO #Temp_ActiveC101 (NumActive, UnNum, idES, idKO)		  
		    SELECT jrActive.NumActive, jrActive.UnNum, EDOKO_hrES.idES, EDOKO_hrES.idKO 
				FROM jrActive 
				INNER JOIN EDOKO_hrES ON jrActive.UnNum = EDOKO_hrES.UnNum 
				WHERE jrActive.status in ('R', 'W') 
					AND jrActive.isElectronForm = 1 
					AND (jrActive.NmZaemshik = @idOrg OR Company = @idOrg)
					AND jrActive.UnNum <> @UnNum
			ORDER BY EDOKO_hrES.idES
			-- таблица 1
			--1.1.продолжение
			--Согласно реализации обработки в ПЗ НО. Признак Возможность повторной обработки. Обмен ЭДО КО при обработке С101 в таблице EDOKO_jrChangeORG находятся строки со статусами:
			--N - когда CurrentValue(С101-НО)=NewValue(С101-Массив[1]), не отображаются в "карантине"
			--R - когда CurrentValue(С101-НО)<>NewValue(С101-Массив[1]), отображаются в "карантине"
			--Далее следует произвести действия, которые обновят С101-НО до состояния С101-Массив[1], а во всех последующих С101-Массив[2..n] изменят данные в таблице EDOKO_jrChangeORG
			--1.1.1.	К организации С101-НО применить изменения С101-Массив[1]
			--Составить запрос на обновление данных в таблице jrOrg по строкам из EDOKO_jrChangeORG(С101-Массив[1]) в статусе R
			--Изменить в jrNewORG поле idES на значение idES для С101-Массив[1]
			--Если С101-Массив[1] пришло от КО, которая отзывает актив, то требуется помотреть наличие данных в карантине по аффилированности и применить их к информации в rfAffPers, связывающей НО и КО, направившую С101-НО
			--Если С101-Массив[1] пришло от другой КО (не совпадающещей с КО, направившей С101-НО), то требуется очистить данные по аффилированности rfAffPers, связывающие НО и КО, направившую С101-НО
			SET @count_Active = (SELECT COUNT(*) FROM #Temp_ActiveC101)
			IF @count_Active > 0
			BEGIN
			  SELECT @idES_New = idES, @idKO_New = idKO FROM #Temp_ActiveC101 WHERE id = 1
			  IF @idKO <> @idKO_New
			  BEGIN
			    IF (SELECT COUNT(*) FROM rfAffPers WHERE idOrg = @idOrg AND idKO = @idKO) > 0
			      DELETE FROM rfAffPers WHERE idOrg = @idOrg AND idKO = @idKO
			  END
			  CREATE TABLE #Temp_ChangeField(id int identity (1,1), idField int, nmFieldASPPA varchar(50), NewValue varchar(500))
			  INSERT INTO #Temp_ChangeField(idField, nmFieldASPPA, NewValue)
			  SELECT JC.idField, NMFieldASPPA, NewValue FROM EDOKO_jrChangeORG JC INNER JOIN EDOKO_ListChangeField LC ON JC.idField = LC.idField  WHERE ChangeStatus = 'R' AND idES = @idES_New
			  UPDATE #Temp_ChangeField
			    SET NMFieldASPPA = 'CODEOKVED', NewValue = (select CODE from  rfOKVED where KOD = NewValue)
			    WHERE NMFieldASPPA = 'KodOKVED'
			  UPDATE #Temp_ChangeField
			    SET NMFieldASPPA = 'CODEOKFS', NewValue = (select CODE from  rfOKFS where KOD = NewValue)
			    WHERE NMFieldASPPA = 'KodOKFS'			  
			  SELECT @count_Change = COUNT(*) FROM #Temp_ChangeField
			  IF @count_Change > 0
			  WHILE @i_Change <= @count_Change
			  BEGIN		
			    SELECT @idField =idField, @nmFieldASPPA = nmFieldASPPA, @NewValue = NewValue FROM #Temp_ChangeField WHERE id = @i_Change
			    IF @nmFieldASPPA NOT IN ('IsAffiliatedOrg','IsAffiliatedKO','D_lastRegDate')
				BEGIN
				  SELECT @sql_str = N'UPDATE jrOrg
						SET ' + @nmFieldASPPA + N' = ''' + REPLACE(@NewValue,'''','''''')  +						  
						N''' WHERE idOrg = ' + CAST(@idOrg AS VARCHAR(10))
	    		  exec (@sql_str)						
	    		END
				ELSE
				BEGIN
				  IF @nmFieldASPPA = 'D_lastRegDate'
				  BEGIN
				    UPDATE jrOrg
						SET D_lastRegDate = CONVERT(DATETIME, @NewValue, 102)					  
						WHERE idOrg = @idOrg 
				  END 
				  IF @idKO = @idKO_New
				  IF @nmFieldASPPA = 'IsAffiliatedOrg' 
				  BEGIN
				    IF (SELECT COUNT(*) FROM rfAffPers WHERE idKO = @idKO AND idOrg = @idOrg AND IsCompanyOwner = 0 AND dtEnd is NULL) > 0
				    BEGIN
				      IF @NewValue = 1
				        DELETE FROM rfAffPers 
				        WHERE IDKO = @idKO  
				          AND idOrg = @idOrg
				          AND IsCompanyOwner = 0
				    END
				    ELSE
				    BEGIN
				      IF @NewValue = 0
				        INSERT INTO rfAffPers (idKO, idOrg, IsCompanyOwner, dtBegin )
				        VALUES (@idKO, @idOrg, 0, (select dtWork from rfStatus)) 
				    END				
				  END
				  IF @nmFieldASPPA = 'IsAffiliatedKO' 
				  BEGIN
				    IF (SELECT COUNT(*) FROM rfAffPers WHERE idKO = @idKO AND idOrg = @idOrg AND IsCompanyOwner = 1 AND dtEnd is NULL) > 0
				    BEGIN
				      IF @NewValue = 1
				        DELETE FROM rfAffPers 
				        WHERE IDKO = @idKO  
				          AND idOrg = @idOrg
				          AND IsCompanyOwner = 1
				    END
				    ELSE
				    BEGIN
				      IF @NewValue = 0
				        INSERT INTO rfAffPers (idKO, idOrg, IsCompanyOwner, dtBegin )
				        VALUES (@idKO, @idOrg, 0, (select dtWork from rfStatus)) 
				    END				
				  END
				  
				END
				--UPDATE jrNewORG 
				--SET idES = @idES_New
				--WHERE idOrg = @idOrg
				
				-- таблица 1				
				--1.1.2.	Обработать изменения по ЭС С104 с ОРГ-НО, т.к. в следующем шаге 1.1.3. будут очищены данные EDOKO_jrChangeORG по ЭС С101-Массив[1] и ОРГ-НО
				--Если ЭС С104 нет, то перейти сразу к шагу 1.1.3
				--По всем ЭС С104 с ОРГ-НО и номером idES меньшим чем idES от С101-Массив[1] отправить С204(-) со значением ProcessCode=2 (Отказ в изменениях) Annotation=Изменение реквизитов <Указать реквизиты, которые подлежали изменению> отклонено. Актив с указанной организацией отозван по заявлению КО, Doer - пользователь, прекративший рассмотрение актива
				--По всем ЭС С104 с ОРГ-НО и номером idES большим чем idES от С101-Массив[1], если эти ЭС С104 относятся к той же КО, что и ЭС С101-НО, отправить С204(-) со значением ProcessCode=2 (Отказ в изменениях) Annotation=Изменение реквизитов <Указать реквизиты, которые подлежали изменению> отклонено. Актив с указанной организацией отозван по заявлению КО, Doer - пользователь, прекративший рассмотрение актива
				--Данные из ЭС С104 с ОРГ-НО и номером idES большим чем idES от С101-Массив[1], либо по ЭС С104 с ОРГ-НО другой КО, отличной от КО из С101-НО
				--должны отображаться в "карантине" по отношению к обновленной организации в jrOrg согласно данным из С101-Массив[1], для этого по каждому такому ЭС С104 провести действия:
				--Если EDOKO_jrChangeORG.NewValue(С101-Массив[1])=EDOKO_jrChangeORG.NewValue(С104) по одному и тому же idField, то в EDOKO_jrChangeORG.ChangeStatus(С104) записать значение N
				--Если EDOKO_jrChangeORG.NewValue(С101-Массив[1])<>EDOKO_jrChangeORG.NewValue(С104) по одному и тому же idField, то в EDOKO_jrChangeORG.ChangeStatus(С104) записать значение R и в EDOKO_jrChangeORG.CurrentValue(С104) записать EDOKO_jrChangeORG.NewValue(С101-Массив[1])
				--Данные по аффилированности, соответствующие строки EDOKO_jrChangeORG с idField IsAffiliatedOrg и IsAffiliatedKO, пришедшие в EDOKO_jrChangeORG.NewValue(С104) остаются в том же статусе и с теми же значениями, как при входе в АС ППА
				--1.1.3.	
				--В С101-Массив[2..n] изменить данные в таблице EDOKO_jrChangeORG
				--Если данных в массиве С101-Массив[2..n] не было, то достаточно только очистить данные в таблице EDOKO_jrChangeORG по ЭС С101-Массив[1] и ОРГ-НО
				--Если данные в массиве С101-Массив[2..n] есть, то выполнить следующие изменения:
				--Если EDOKO_jrChangeORG.NewValue(С101-Массив[1])=EDOKO_jrChangeORG.NewValue(С101-Массив[2..n]) по одному и тому же idField, то в EDOKO_jrChangeORG.ChangeStatus(С101-Массив[2..n]) записать значение N
				--Если EDOKO_jrChangeORG.NewValue(С101-Массив[1])<>EDOKO_jrChangeORG.NewValue(С101-Массив[2..n]) по одному и тому же idField, то в EDOKO_jrChangeORG.ChangeStatus(С101-Массив[2..n]) записать значение R и в EDOKO_jrChangeORG.CurrentValue(С101-Массив[2..n]) записать EDOKO_jrChangeORG.NewValue(С101-Массив[1])

				
			    SELECT @idES = idES, @idKO = idKO FROM EDOKO_hrES WHERE EDType = 'C104' AND UnNum = @UnNum
			    
				UPDATE EDOKO_jrChangeORG
				SET ChangeStatus = 'F', Annotation = '', idESProcess = @idES
				WHERE idOrg = @idOrg AND idES IN (SELECT idES FROM EDOKO_hrES WHERE EDType = 'C104' AND idKO = @idKO)	

				IF @nmFieldASPPA = 'CODEOKVED'
				SET @NewValue = (SELECT KOD FROM rfOKVED WHERE CODE = @NewValue)
				
				IF @nmFieldASPPA = 'CODEOKFS'
				SET @NewValue = (SELECT KOD FROM rfOKFS WHERE CODE = @NewValue)
				
												

				IF @nmFieldASPPA NOT IN ('IsAffiliatedOrg','IsAffiliatedKO')
				BEGIN
  				  UPDATE EDOKO_jrChangeORG
  				  SET ChangeStatus = 'N', 
  				      CurrentValue = @NewValue
				  WHERE idOrg = @idOrg 
				      AND ChangeStatus <> 'F'
					  AND idField = @idField
					  AND 
					  (
					    dbo.fnNormalTextForFT(NewValue) = dbo.fnNormalTextForFT(@NewValue)-- AND (SELECT FieldType FROM EDOKO_ListChangeField WHERE NMFieldASPPA = @nmFieldASPPA) = 'S')
					  )
					    
				  UPDATE EDOKO_jrChangeORG
				  SET ChangeStatus = 'R',
  				      CurrentValue = @NewValue
				  WHERE idOrg = @idOrg 
					  AND ChangeStatus <> 'F' 
					  AND idField = @idField
					  AND dbo.fnNormalTextForFT(NewValue) <> dbo.fnNormalTextForFT(@NewValue)
				END	
						
				SET @i_Change = @i_Change + 1
			  END --WHILE @i_Change <= @count_Change
			  
			  
			  
			  
			  UPDATE jrNewORG 
			  SET idES = @idES_New
			  WHERE idOrg = @idOrg
			  
			  
			  
			  SET @i_change = 1
			END --IF @count_Active > 0
			ELSE
			BEGIN
			  --1.2.Если массив С101-Массив пуст (т.е. на шаге 1 не нашлось ни одного актива на рассмотрении кроме С101-НО с организацией с ОРГ-НО), то обработать ОРГ-НО следующим образом:
			  --idOrg.OrgStatus присвоить статус F
			  --idOrg.isReprocess присвоить значение 1
			  UPDATE jrORG
			  SET OrgStatus = 'F'
			      ,isReprocess = 1
			  WHERE idOrg = @idOrg
			  
			  --Удалить строку из jrNewORG, соответствующую ОРГ-НО и С101-НО
			  DELETE 
			  FROM jrNewORG
			  WHERE idOrg = @idOrg
			  
			  --Если в системе есть ЭС С104 с ОРГ-НО, то следует их обработать.
			  --В случае когда С101-Массив пуст при отзыве ходатайства по всем ЭС С104 следует отправить С204(-) со значением ProcessCode=2 (Отказ в изменениях) Annotation=Изменение реквизитов <Указать реквизиты, которые подлежали изменению> отклонено. Актив с указанной организацией отозван по заявлению КО, Doer - пользователь, прекративший рассмотрение актива.
			  UPDATE EDOKO_jrChangeORG
			  SET ChangeStatus = 'F', Annotation = '', idESProcess = @idES
			  WHERE idOrg = @idOrg 				  
			  
			
			END
			DROP TABLE #Temp_ActiveC101
		  END --IF EXISTS (SELECT * FROM jrORG WHERE idOrg = (SELECT idOrg FROM #Temp_Org WHERE id = @i_Org) AND OrgStatus = 'R')
		  
		  -- таблица 1
		  --1.1.3. Продолжение 
		  --После изменения строк по всему массиву С101-Массив[2..n] очистить данные в таблице EDOKO_jrChangeORG по ЭС С101-Массив[1] и ОРГ-НО
		  DELETE 
		  FROM EDOKO_jrChangeORG
		  WHERE idES IN (@idES, @idES_New)
		  
		  SET @i_Org = @i_Org + 1
		END --WHILE @i_Org <= @count_Org
		DROP TABLE #Temp_Org
	  END --IF @count_Org > 0
   Commit Transaction
 End Try
 Begin Catch
    Rollback Transaction
    Declare @ErrMsg VarChar(255)
    Select @ErrMsg = ERROR_MESSAGE()
    Select @ErrMsg = 'При отклонении актива с Новой организацией возникли ошибки. ' + @ErrMsg
    RaisError( @ErrMsg, 16, 1  )
  End Catch	
END
GO