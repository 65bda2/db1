﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		<>
-- Create date: <31.08.2018>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[spCreateEntity](
	@NameTable [varchar](24), 
	@TypeEntity [varchar](2) = 'rf') 
AS
BEGIN
	DECLARE @id INT = 0;       
    DECLARE @Description varchar(50) = ''; 
    DECLARE @type varchar(8)='';
    
	IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE name=@NameTable AND type in ('U','V'))
	BEGIN	   
	   PRINT 'Таблица/представление ['+@NameTable+'] не существует в БД.';   
	   RETURN 0;
	END  
	
	IF @TypeEntity in ('rf','vw') 
	BEGIN
		SELECT @type=case type when 'U' then 'TABLE' when 'V' then 'VIEW' else '' end  FROM sysobjects WHERE name=@NameTable AND type in ('U','V');
		SELECT @Description=cast(value as varchar(50)) from fn_listextendedproperty('MS_Description','schema','dbo',@type,@NameTable,null,null);
		 
		IF EXISTS (SELECT 1 FROM dbo.bsEntity e WHERE e.TableEntity = @NameTable)
		BEGIN
		   PRINT 'Сущность ['+@NameTable+'] уже существует в [bsEntity].' ; --+@NameTable,16,1);
		   
		   UPDATE dbo.bsEntity  SET
	   				TypeEntity=@TypeEntity
				   ,NameEntity=@Description
				   ,CaptionEntity=@Description
		   WHERE TableEntity = @NameTable
		   
		END
		ELSE
		BEGIN
		   INSERT INTO dbo.bsEntity 
			   (TypeEntity
			   ,NameEntity
			   ,TableEntity
			   ,CaptionEntity
			   )
		  VALUES
				(@TypeEntity
				,@Description
				,@NameTable
				,@Description
				)
			;
			SET @id=SCOPE_IDENTITY();
			PRINT 'Сущность ['+@NameTable+'] добавлена в [bsEntity].';
	     END 
	 END     
	 ELSE
	 BEGIN
	     PRINT 'Для типа сущности отличного от rf или vw создание записей в bsEntity пока не предусмотренно'
	 END
	
	
	EXEC [dbo].[spCreateEntityAttribute] @NameTable;
	RETURN @id;
END
GO