﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:	<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCreateEntityAttribute](
	@NameTable [varchar](24), --параметр для указания таблицы, по которой необходимо создать/пересоздать атрибуты
	@isGridOrder [bit] = 1 --параметр для указния необходимости нумерации/перенумерации колонок
	)
AS
BEGIN
	DECLARE @id INT=0;
	DECLARE @TypeEntity varchar(2);
	DECLARE @cols CURSOR;
	DECLARE @column_name varchar(128); 
	DECLARE @idEntity int;
    DECLARE @Description varchar(255)='';
    DECLARE @isVisible bit;
	DECLARE @GridOrder int = NULL;
	DECLARE @Width int=50;
	DECLARE @type varchar(8)='';
	
	SELECT @TypeEntity=TypeEntity,@idEntity=[idEntity] FROM dbo.bsEntity e WHERE e.TableEntity = @NameTable;
	
	IF @@ROWCOUNT=0
	BEGIN
	   PRINT 'Сущность ['+@NameTable+'] не существует в [bsEntity].';
	   RETURN 0;
	END   
	ELSE
	BEGIN
	  IF @TypeEntity in ('rf','vw') --необходима реализация для rp тоже
	  BEGIN
	     SET @cols=CURSOR SCROLL  
	     FOR SELECT c.COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS c WHERE c.TABLE_NAME=@NameTable and c.TABLE_SCHEMA='dbo';
	     OPEN @cols
	     FETCH NEXT FROM @cols INTO @column_name
	     WHILE @@FETCH_STATUS=0
	     BEGIN
	        SELECT @type=case type when 'U' then 'TABLE' when 'V' then 'VIEW' else '' end  FROM sysobjects WHERE name=@NameTable AND type in ('U','V');
	        
	        SELECT @Description=cast(value as varchar(50)) from fn_listextendedproperty('MS_Description','schema','dbo',@type,@NameTable,'column',@column_name);
	        
	        SET @isVisible = case when SUBSTRING(@column_name,1,2)='id' then 0 else 1 end;
	        
	        SET @GridOrder = case when @isGridOrder=0 then NULL else (SELECT TableColumns.ORDINAL_POSITION
											FROM INFORMATION_SCHEMA.COLUMNS TableColumns
											WHERE TableColumns.TABLE_NAME=@NameTable
												and TableColumns.TABLE_SCHEMA='dbo' 
												and TableColumns.COLUMN_NAME=@column_name)
			end; --нумерация/перенумерация только если @isGridOrder<>0 в порядке указания колонок в таблице/представлении									
			SET @Width = case (SELECT top 1TableColumns.DATA_TYPE
							FROM INFORMATION_SCHEMA.COLUMNS TableColumns
							WHERE TableColumns.TABLE_NAME=@NameTable
								and TableColumns.TABLE_SCHEMA='dbo' 
								and TableColumns.COLUMN_NAME=@column_name)
							when 'int' then 50
							when 'varchar' then 400
							when 'float' then 150
							else 50
						 end;--[Width] Ширина колонки выставляется в зависимости от типа поля						
	        
        
	        IF EXISTS (SELECT 1 FROM dbo.bsEntityAttribute  WHERE [idEntity]=@idEntity and NameField=@column_name) 
	        BEGIN				
				UPDATE  dbo.bsEntityAttribute 
					   SET 
				--	   [idLookupEntity] = null
				--	  ,[idLookupAttribute] = null
				--	  ,[idLookupField] = null
				--	  , [idEntity] = @idEntity
				--	  ,[NameField] = @column_name
					  [Description] = @Description -- обновление описания поля
				--	  ,[isVisible] = @isVisible
				--	  ,[Width] = 20
				--	  ,[isEdit] = <isEdit, bit,>
					  ,[GridOrder] = case when @isGridOrder=0 then [GridOrder] else @GridOrder end --[GridOrder] 
					  -- перенумерация только если @isGridOrder<>0
				--	  ,[WidthExcel] = <WidthExcel, int,>
				--	  ,[OrderExcel] = <OrderExcel, int,>
				--	  ,[isVisibleExcel] = <isVisibleExcel, bit,>
				WHERE [idEntity]=@idEntity and NameField=@column_name;
			
				PRINT 'Атрибут ['+@column_name+'] для сущности ['+@NameTable+'] обновлен в [bsEntityAttribute].';
			END
	        ELSE
	        BEGIN 
				INSERT INTO dbo.bsEntityAttribute (
			--    [idLookupEntity]
		 --      ,[idLookupAttribute]
		 --      ,[idLookupField]
				  [idEntity]
				 ,[NameField]
				 ,[Description]
				 ,[isVisible]
				 ,[Width]
		         ,[isEdit]
				 ,[GridOrder]
		 --      ,[WidthExcel]
		 --      ,[OrderExcel]
		         ,[isVisibleExcel]
				)
				VALUES (
		 --      null
		 --      ,null
		 --      ,null
				 @idEntity
				 ,@column_name
				 ,@Description
				 ,@isVisible  -- [isVisible]
				 ,@Width --[Width] Ширина колонки выставляется в зависимости от типа поля
				 ,0 --[isEdit] По-умолчанию все колонки нередактируемы
		         ,@GridOrder --[GridOrder] нумерация только если @isGridOrder<>0 в порядке указания колонок в таблице/представлении
		 --      ,null
		 --      ,null
		         ,@isVisible --[isVisibleExcel]
				);
				PRINT 'Атрибут ['+@column_name+'] для сущности ['+@NameTable+'] добавлен в [bsEntityAttribute].';
	        END;
	        FETCH NEXT FROM @cols INTO @column_name
	     END 
	     CLOSE @cols
	  END
	END	
	RETURN @id;
END
GO