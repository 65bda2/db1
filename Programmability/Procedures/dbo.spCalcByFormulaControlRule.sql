﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- ================================================
CREATE PROCEDURE [dbo].[spCalcByFormulaControlRule] 
------------------------------------------------
--ПЗ 6.1.1.7.	Хранимая процедура spCalcByFormulaControlRule 
-- (Функции/процедуры в БД для реализации контроля бух. отчетности)
--  Вычисляет значение по фомуле из таблицы rfAccCodeControlRule.
--  Использутся в хранимой процедуре spAccCodeControlSum .
-------------------------------
-- =============================================
-- Author:		<65baa2>
-- Create date: <01.11.2018>
-- Description:	<Хранимая процедура вычисляет значение по фомуле из таблицы rfAccCodeControlRule>
-- =============================================
	@idOrgBuhReport int, -- ид записи в таблице jrOrgBuhReportg для которой выбираются данные для расчета
	@formula varchar(2000), --текст формулы расчета из таблицы rfCalcCoefficient
	--@isPrev bit = 0,  --использовать значение 0-ShowingAccSum или 1-ShowingAccSumPrev
	@calc float OUTPUT -- возвращает вычисленное значение  
AS
BEGIN
    declare @text varchar(max)='';
    --declare @K float=0.001; -- Вычисления в тыс. руб.
	--declare @sql nvarchar(max)='';
	--declare @params nvarchar(100)='';
	declare @temp table(
		i int,					-- номер по порядку
		flag int,				-- признак:  0- текст формулы  1- код XXXX (ShowingAccSum); 
		variant varchar(1024), --  текст формулы  или код XXXX (ShowingAccSum); 
		calc float         -- вычисленное значение  для кода  XXXX (ShowingAccSum);
	);
	
	/*
	select @K=
		case  CodeOKEI 
			when 383 then 0.001
			when 384 then 1 
			when 385 then 1000 
			else null
	    end 
	select * from jrOrgBuhReport r where r.idOrgBuhReport=@idOrgBuhReport;
	--select * from rfBuhRecord
	*/
	--if @K is null  return 2; 

	insert into @temp 
		(i,
		flag,
	    variant,
        calc)  
	select 
	    s.i,
	    s.flag,
	    s.variant,
	    (case when s.flag=1 
			then (select 
				  
					 r.ShowingAccSum
					
				 
				from jrOrgBuhRecord r ,rfBuhRecord b 
					where r.idBuhRecord=b.idBuhRecord
					    and r.idOrgBuhReport=@idOrgBuhReport
						and b.NumKod=s.variant)
		    else null		 
		end)
	from fnSplitFormulaControlRule(@formula) s ;
	  
	select @text=@text+
		(case when flag=0
			then variant 
			else '('+CAST(calc as varchar(20))+')'
		end)
	from @temp order by i;
	
	if @text is null  return 1; 
	
	exec spCalcByFormulaSQL @text,@calc=@calc out;

	return 0 
END
GO