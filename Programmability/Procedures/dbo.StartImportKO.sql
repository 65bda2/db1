﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[StartImportKO]
 @IsHub bit,
 @TU varchar (2),
 @ErrorMSG varchar(max) output
as
BEGIN TRY
  SET @ErrorMSG = '';
 
  DECLARE @idKONew int,
          @idKO int,
          @nmKO varchar(250),
          @nuReg varchar(9),
          @nuBIK varchar(9),
          @nuINN varchar(10),
          @nmJurAddr varchar(250),
          @nmFactAddr varchar(250),
          @nmNadzTU varchar(200),
          @nuAccount varchar(20),
          @nuRkcBik varchar(9),
          @nnKO varchar(128),
          @OKATO varchar(11),
          @OKPO varchar(8),
          @OGRN varchar(13),
          @nuAccRNKO varchar(20),
          @idRNKO int,
          @isInList tinyint,
          @IsSubject tinyint,
          @isElectronFormKO tinyint,
          @ESODVersion int,
          @idSubdivision int
     
  DECLARE curHub_JRKOExist CURSOR LOCAL FOR
  SELECT hub_jrKO.idKo idKOTU, jrko.IDKO idKONew FROM hub_jrKO INNER JOIN jrKO ON jrko.nureg=hub_jrKO.nureg
  OPEN curHub_JRKOExist

  FETCH NEXT FROM curHub_JRKOExist 
  INTO @idKO,@idKONew
  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO [hub_Keys]([idTUKey],[idHubKey],[NameTable],[TU],[isReplace])
    VALUES(@idKO,@idKONew,'jrKO',@TU,1)
    
    FETCH NEXT FROM curHub_JRKOExist 
    INTO @idKO,@idKONew
  END
  
  close curHub_JRKOExist
  deallocate curHub_JRKOExist

  DECLARE curHub_JRKONew CURSOR LOCAL FOR
  SELECT idKo,[nmKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuAccount],
         IsNull( [hub_jrRKC].[nuBikRkc], [nuRkcBik] ),  --[nuRkcBik],
         [nnKO],[OKATO],[OKPO],[OGRN],[nuAccRNKO],[idRNKO],[isInList],[IsSubject],[isElectronFormKO],[ESODVersion],
         --CASE rf1.KPTU WHEN NULL THEN ( CASE rf2.KPTU WHEN  NULL  THEN NULL ELSE rf2.IdSubdivision end ) ELSE rf1.IdSubdivision  END idSubdivision 
         ISNULL( rf1.IdSubdivision , rf2.IdSubdivision)
  FROM   hub_jrKO LEFT JOIN 
  hub_jrRKC on hub_jrRKC.idRKC= hub_jrKO.idRKC LEFT JOIN 
  rfSubdivision rf1 ON SUBSTRING(hub_jrRKC.nuBIKRKC,3,2)=rf1.KPTU LEFT JOIN 
  rfSubdivision rf2 ON SUBSTRING(hub_jrKO.nuBik,3,2)=rf2.KPTU
  WHERE nureg not in (Select NuReg FROM jrKO) and IDKO NOT IN (SELECT idTUKey from Hub_Keys h where h.nametable ='jrKO' and TU = @TU) And IsSubject = 0 
  OPEN curHub_JRKONew
  
  FETCH NEXT FROM curHub_JRKONew 
  INTO @idKO,@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject,@isElectronFormKO,@ESODVersion,@idSubdivision
  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO [jrKO]([nmKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuAccount],[nuRkcBik],[nnKO],[OKATO],[OKPO],[OGRN],[nuAccRNKO],[idRNKO],[isInList],[IsSubject],[isElectronFormKO],[ESODVersion],[idSubdivision])
    VALUES (@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject, 1/*@isElectronFormKO*/,1/*@ESODVersion*/,@idSubdivision) -- AHEBURG-1691
    SELECT @idKONew=SCOPE_IDENTITY()
    
    INSERT INTO [hrOwnFunds]([idKO],[dtOwnFund],[mnOwnFund],[cmOwnFund])
    SELECT @idKONew, [dtOwnFund],[mnOwnFund],[cmOwnFund] FROM hub_hrOwnFunds 
    WHERE idKO=@idKO and not EXISTS (Select * FROM hrOwnFunds where hrOwnFunds.idKO=@idKONew and hrOwnFunds.[dtOwnFund]=hub_hrOwnFunds.dtOwnFund)

    INSERT INTO [hub_Keys]([idTUKey],[idHubKey],[NameTable],[TU],[isReplace])
    VALUES(@idKO,@idKONew,'jrKO',@TU,null)


    FETCH NEXT FROM curHub_JRKONew 
    INTO @idKO,@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject,@isElectronFormKO,@ESODVersion,@idSubdivision
  END
  close curHub_JRKONew
  deallocate curHub_JRKONew
  
  -- 65vev МФО и лизинговые 
  DECLARE curHub_JRKO_MFO CURSOR LOCAL FOR
  SELECT idKo,[nmKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuAccount],[nuRkcBik],[nnKO],[OKATO],[OKPO],[OGRN],[nuAccRNKO],[idRNKO],[isInList],[IsSubject],[isElectronFormKO],[ESODVersion],
         --CASE rf1.KPTU WHEN NULL THEN ( CASE rf2.KPTU WHEN  NULL  THEN NULL ELSE rf2.IdSubdivision end ) ELSE rf1.IdSubdivision  END idSubdivision 
         ISNULL( rf1.IdSubdivision , rf2.IdSubdivision)
  FROM   hub_jrKO LEFT JOIN 
  hub_jrRKC on hub_jrRKC.idRKC= hub_jrKO.idRKC LEFT JOIN 
  rfSubdivision rf1 ON SUBSTRING(hub_jrRKC.nuBIKRKC,3,2)=rf1.KPTU LEFT JOIN 
  rfSubdivision rf2 ON SUBSTRING(hub_jrKO.nuBik,3,2)=rf2.KPTU
  WHERE IDKO NOT IN (SELECT idTUKey from Hub_Keys h where h.nametable ='jrKO' and TU = @TU) And IsSubject <> 0 
  OPEN curHub_JRKO_MFO
  
  FETCH NEXT FROM curHub_JRKO_MFO 
  INTO @idKO,@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject,@isElectronFormKO,@ESODVersion,@idSubdivision
  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO [jrKO]([nmKO],[nuReg],[nuBIK],[nuINN],[nmJurAddr],[nmFactAddr],[nmNadzTU],[nuAccount],[nuRkcBik],[nnKO],[OKATO],[OKPO],[OGRN],[nuAccRNKO],[idRNKO],[isInList],[IsSubject],[isElectronFormKO],[ESODVersion],[idSubdivision])
    VALUES (@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject, Null, Null, Null )         
    SELECT @idKONew=SCOPE_IDENTITY()
    
    INSERT INTO [hub_Keys]([idTUKey],[idHubKey],[NameTable],[TU],[isReplace])
    VALUES(@idKO,@idKONew,'jrKO',@TU,null)

    FETCH NEXT FROM curHub_JRKO_MFO 
    INTO @idKO,@nmKO,@nuReg,@nuBIK,@nuINN,@nmJurAddr,@nmFactAddr,@nmNadzTU,@nuAccount,@nuRkcBik,@nnKO,@OKATO,@OKPO,@OGRN,@nuAccRNKO,@idRNKO,@isInList,@IsSubject,@isElectronFormKO,@ESODVersion,@idSubdivision
  END
  close curHub_JRKO_MFO
  deallocate curHub_JRKO_MFO
  -- 65vev МФО и лизинговые
  
  
  DECLARE @IdRecord int,
          @IdRecordNew int, 
          @IdBankInTU numeric(38),
          @D_From smalldatetime,
          @IdUpdate bigint,
          @StateSED int,
          @IsArchive tinyint,
          @CorrectionType tinyint,
          @BankRegNum varchar(20),
          @IsPartnerOfMSP tinyint,
          @AlterComment varchar(500),
          @ESDate smalldatetime,
          @ESNo int



  DECLARE curHub_hrBankInTU CURSOR LOCAL FOR
  SELECT h.idHubKey, hub_DKO_hrBankInTU.IdRecord, hub_DKO_hrBankInTU.[IdBankInTU],hub_DKO_hrBankInTU.[D_From],hub_DKO_hrBankInTU.[IdUpdate],hub_DKO_hrBankInTU.[StateSED],hub_DKO_hrBankInTU.[IsArchive],hub_DKO_hrBankInTU.[CorrectionType],
         hub_DKO_hrBankInTU.[BankRegNum],hub_DKO_hrBankInTU.[IsPartnerOfMSP],hub_DKO_hrBankInTU.[AlterComment],hub_DKO_hrBankInTU.[ESDate],hub_DKO_hrBankInTU.[ESNo]
  FROM hub_DKO_hrBankInTU INNER JOIN hub_keys h on h.idTUKey=hub_DKO_hrBankInTU._idKO
  WHERE h.TU=@TU and h.NameTable='jrKO' --and (h.isReplace=0  or h.isReplace is null)

  OPEN curHub_hrBankInTU
  FETCH NEXT FROM curHub_hrBankInTU
  INTO @idko,@IdRecord,@IdBankInTU,@D_From,@IdUpdate,@StateSED,@IsArchive,@CorrectionType,@BankRegNum,@IsPartnerOfMSP,@AlterComment,@ESDate,@ESNo
  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO [DKO_hrBankInTU]([_IdKO],[IdBankInTU],[D_From],[IdUpdate],[StateSED],[IsArchive],[CorrectionType],[BankRegNum],[IsPartnerOfMSP],[AlterComment],[ESDate],[ESNo],[TU])
    VALUES (@idko,@IdBankInTU,@D_From,
      Case @IsHub When 1 Then @IdUpdate Else 0 End,@StateSED,@IsArchive,@CorrectionType,@BankRegNum,@IsPartnerOfMSP,@AlterComment,@ESDate,@ESNo,@TU)
    
    SELECT @IdRecordNew = SCOPE_IDENTITY()

    INSERT INTO [hub_Keys]([idTUKey],[idHubKey],[NameTable],[TU],[isReplace])
    VALUES(@idRecord,@IdRecordNew,'DKO_hrBankInTU',@TU,null)
    
    FETCH NEXT FROM curHub_hrBankInTU
    INTO @idko,@IdRecord,@IdBankInTU,@D_From,@IdUpdate,@StateSED,@IsArchive,@CorrectionType,@BankRegNum,@IsPartnerOfMSP,@AlterComment,@ESDate,@ESNo
  END
  close curHub_hrBankInTU
  deallocate curHub_hrBankInTU
  /*
  IF @IsHub = 1 
  BEGIN
    DECLARE @create  VARCHAR(8000) ,
            @Count int = isnull((SELECT MAX([idrfFaceKO]) from rfFaceKO),0)+1
  
    SET  @create=' IF (OBJECT_ID(N''tempdb..##TempTableForidHub'',N''U'') IS NOT NULL) '+
             ' DROP TABLE ##TempTableForidHub '+
             ' CREATE TABLE ##TempTableForidHub (idTu int, idHub int identity('+isnull(cast(@Count as varchar),'1')+',1))' 
    exec(@create)
    INSERT INTO ##TempTableForidHub(idTU)
    SELECT idrfFaceKO FROM hub_rfFaceKO 
    WHERE idrfFaceKO not in (SELECT idTUKey from Hub_Keys h where h.nametable ='rfFaceKO' and TU = @TU)
    
    SET IDENTITY_INSERT rfFaceKO ON;
    INSERT INTO [rfFaceKO](idrfFaceKO, [idKO],[nmFIO],[nmPost],[cmPers],[nuTrust],[dtTrust],[dtEndTrust],[nuLetter],[dtLetter],[dtEndLetter],[nmOtherDoc],[nuOtherDoc],[dtOtherDoc],[dtEndOtherDoc],[tpPersSign],[cmPersSign],[imSignCard],[dtSignCardLoad],[fNameSignCard])
    SELECT t.idHUB, h.idHubKey,[nmFIO],[nmPost],[cmPers],[nuTrust],[dtTrust],[dtEndTrust],[nuLetter],[dtLetter],[dtEndLetter],[nmOtherDoc],[nuOtherDoc],[dtOtherDoc],[dtEndOtherDoc],[tpPersSign],[cmPersSign],[imSignCard],[dtSignCardLoad],[fNameSignCard] 
    FROM hub_rfFaceKO INNER JOIN hub_keys h on h.idTUKey=hub_rfFaceKO.idKO INNER JOIN ##TempTableForidHub t ON t.idTU=hub_rfFaceKO.idrfFaceKO
    WHERE h.NameTable='jrKO' and h.TU=@TU 
    SET IDENTITY_INSERT rfFaceKO OFF;

    INSERT INTO hub_Keys (idHubKey,idTUKey,NameTable,TU)
    SELECT idHub,idTU,'rfFaceKO',@TU FROM  ##TempTableForidHub
  END;
  */
  RETURN 1  
END TRY
BEGIN CATCH
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
  INSERT INTO	 rfSign (tpSign,nmSign) 
  VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution'    

  SET @ErrorMSG = ERROR_MESSAGE();
  RETURN 0
END CATCH
GO