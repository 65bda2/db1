﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartCheckCountersAfterDKO]
@IsHUB BIT,
@ErrorMSG varchar(max) output ,
@TU varchar(2)
AS
BEGIN
  DECLARE @ds int = 0
  DECLARE @d int = 0  
  DECLARE @cs int = 0
  DECLARE @c int = 0
  SET @ErrorMSG=''
BEGIN TRY 
  IF @IsHUB = 1 RETURN 1
  ELSE BEGIN
	SET @ErrorMSG = '    При создании данных для корректировки с АС ДКО количественная проверка выполнилась для: \n'
	---- 1. По количеству, загруженных активов 
	Select @c = COUNT(*) From hub_jrActive a Join hub_Keys k On a.NumActive  = k.idTuKey Where k.NameTable = 'jrActive' And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrAsset a Join hub_Keys k On a._NumActive = k.idHubKey Where k.NameTable = 'jrActive' And k.TU = @TU And StateSED = 0
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество загруженных активов ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество загруженных активов ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	---- 2. По количеству, загруженных организаций, для которых не нашёлся эталон 
	Select @c = COUNT(*) From hub_jrOrg o Join hub_Keys k On o.idOrg = k.idTUKey Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 0 And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrCompany o Join hub_Keys k On o._IdOrg = k.idHubKey Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 0 And k.TU = @TU And StateSED = 0 And CorrectionType = 2
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество загруженных организаций ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество загруженных организаций ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	---- 4. По количеству аффилированных организаций организаций, для которых нашёлся эталон 
	Select @c = COUNT(*) From hub_DKO_hrMutualParticipation b Join hub_Keys k On b.IdCompany = k.idTUKey Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrMutualParticipation b Join hub_Keys k     On b.IdCompany = k.idHubKey Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU And StateSED = 0 And CorrectionType = 4
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество аффилированностей организаций ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество аффилированностей организаций ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	---- 5. По количеству, загруженных организаций, для которых нашёлся эталон 
	Select @c = COUNT(*) From hub_jrOrg o Join hub_Keys k On o.idOrg = k.idTUKey Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrCompany o Join hub_Keys k On o._IdOrg = k.idHubKey AND o.TU=k.TU Where k.NameTable = 'jrOrg' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU And StateSED = 0 And CorrectionType = 4
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество организаций дублей ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество организаций дублей ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	---- 6. По количеству, загруженных KO, для которых не нашёлся эталон 
	Select @c = COUNT(*) From hub_DKO_hrBankInTU b Join hub_Keys k On b._IdKO = k.idTUKey Where k.NameTable = 'jrKO' And IsNull( isReplace, 0 ) = 0 And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrBankInTU b Join hub_Keys k     On b._IdKO = k.idHubKey Where k.NameTable = 'jrKO' And IsNull( isReplace, 0 ) = 0 And k.TU = @TU And StateSED = 0 And CorrectionType = 2
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество обновлений ТУ у КО-эталонов ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество обновлений ТУ у КО-эталонов ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	---- 7. По количеству, загруженных KO, для которых нашёлся эталон 
	Select @c = COUNT(*) From hub_DKO_hrBankInTU b Join hub_Keys k On b._IdKO = k.idTUKey Where k.NameTable = 'jrKO' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU
	Select @d = COUNT(*) From DKO_hrBankInTU b Join hub_Keys k     On b._IdKO = k.idHubKey AND b.TU=k.TU Where k.NameTable = 'jrKO' And IsNull( isReplace, 0 ) = 1 And k.TU = @TU And StateSED = 0 And CorrectionType = 4
	SET @ds = @ds + @d
	SET @cs = @cs + @c
	IF @c = @d 
		SET @ErrorMSG = @ErrorMSG + '        Количество удаленных сущностей BankInTU при слиянии КО ('+LTRIM(STR(@d))+') - Успешно \n'
	ELSE
		SET @ErrorMSG = @ErrorMSG + '        Количество удаленных сущностей BankInTU при слиянии КО ('+LTRIM(STR(@d))+' и '+LTRIM(STR(@c))+') - НЕуспешно \n'

	IF @cs <> @ds
		SET @ErrorMSG = @ErrorMSG + '    Обратитесь к разработчику для проверки корректности создания корректировки.\n'

  RETURN 1
  END
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
END
GO