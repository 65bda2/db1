﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 17.02.2020
-- Отчет Внесистемный учет нерыночных активов ЕКД АС Сибирь
-- =============================================

CREATE PROCEDURE [dbo].[spOutsysNonmarketEKD_report](@idAcc INT) AS
BEGIN 
  declare @dtwork DATETIME
  select top 1 @dtwork=dtwork from rfStatus
  IF OBJECT_ID(N'tempdb..#TempRazdSect', N'U') IS NOT NULL DROP TABLE #TempRazdSect
  create table #TempRazdSect 
  (
  id int IDENTITY(1,1) NOT NULL,
  NumRazdel varchar(5)null,
  NameRazdel varchar(255)null,
  tpSect varchar(5) null
  )
--1
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','1')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','1U')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','1C')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','11D')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','11Z')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','11U')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('01', '1.Активы, принятые в ТУ для обеспечения возможности получения банком кредитов Банка России','12L')
--1.1
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.1', '1.1 Активы, ожидающие вывода/возврата банку из состава активов, принятых ТУ для обеспечения возможности получения банком кредитов Банка России','1U')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.1', '1.1 Активы, ожидающие вывода/возврата банку из состава активов, принятых ТУ для обеспечения возможности получения банком кредитов Банка России','1C')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.1', '1.1 Активы, ожидающие вывода/возврата банку из состава активов, принятых ТУ для обеспечения возможности получения банком кредитов Банка России','11D')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.1', '1.1 Активы, ожидающие вывода/возврата банку из состава активов, принятых ТУ для обеспечения возможности получения банком кредитов Банка России','11Z')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.1', '1.1 Активы, ожидающие вывода/возврата банку из состава активов, принятых ТУ для обеспечения возможности получения банком кредитов Банка России','11U')
--1.2
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('1.2', '1.2 Активы, отобранные в обеспечение по рассматриваемой(му) заявке/заявлению кредитной организацией на кредиты Банка России','12L')
--1.3
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
 ('1.3', '1.3 Активы, принимаемые в обеспечение кредитов Банка России','1')
--2.1
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('2.1', '2.1 Раздел «принято в залог под иные кредиты Банка России»','21U')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('2.1', '2.1 Раздел «принято в залог под иные кредиты Банка России»','21C')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('2.1', '2.1 Раздел «принято в залог под иные кредиты Банка России»','21L')
--2.2
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('2.2', '2.2 Раздел «Принято в залог под кредиты овернайт Банка России»','22N')
--3
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('3.', '3.Торговый раздел/Активы, ожидающие перехода на баланс Банка России','3L')
  insert #TempRazdSect (NumRazdel,NameRazdel,tpSect) values
  ('3.', '3.Торговый раздел/Активы, ожидающие перехода на баланс Банка России','3N')
  select
      @dtWork dtWOrk 
    , tmp.Numrazdel
    , tmp.NameRazdel
    , rfAS.NonsystemName
	, jrA.NomerVD 
	, jrA.CreationDate 
	, jrA.TrancheNumber
    , jrA.UnNum 
    , jrAcc.nmKO
	, jrA.DatePayment 
	--, jrA.TempCostRub as mnPrice
	,(CASE 
		WHEN (rfAS.tpSect IN ('22N', '21U', '21L')) THEN jrA.TempCostRub
		ELSE dbo.CalcActiveCost(jrA.NumActive, dbo.IncWorkDay(@dtWork, 61),@dtWork)
	  END
	) as mnPrice -- Стоимость актива должна считаться с учетом погашения платежей в ближайшие 60 дней 65eav2
	--, jrO.CompanyName
	,(case
			 when (isnull(hrF.pcFactor,0))=0 
			 then 0 
			 else 
			  hrF.pcFactor
	 end
		)  as pcFactor 
	--,(case
	--		 when (isnull(hrF.pcFactor,0))=0 
	--		 then 0 
	--		 else 
	--		  hrF.pcFactor * jrA.TempCostRub
	-- end
	--	)  as mnPriceCoeff
	,(case
			 when (isnull(hrF.pcFactor,0))=0 
			 then 0 
			 else 
			  hrF.pcFactor * (CASE 
								WHEN (rfAS.tpSect IN ('22N', '21U', '21L')) THEN jrA.TempCostRub
								ELSE dbo.CalcActiveCost(jrA.NumActive, dbo.IncWorkDay(@dtWork, 61),@dtWork)
							  END)
	 end
		)  as mnPriceCoeff -- Стоимость актива должна считаться с учетом погашения платежей в ближайшие 60 дней 65eav2
	, jrO.CompanyName
	, jrA.status
	, hrAS.DateAssign as dtSect
	
	
  from jrActive jrA
	join hrActiveSect hrAS on jrA.NumActive=hrAS.NumActive
	join jrAccount jrAcc on hrAS.idAcc=jrAcc.idAcc
	join jrGKD jrG on jrG.nuGKD=jrAcc.nuGKD
	join rfActiveState rfAS on rfAS.idActiveState=hrAS.idActiveState
	join jrORG jrO on jrO.idOrg=jrA.Company
	join #TempRazdSect tmp on tmp.tpSect=rfAS.tpSect
	left outer join hrFactor hrF on hrF.idAsset=jrA.NumActive and hrF.tpAsset = 'B' and
						 (hrf.dtBegin= (select MAX(hrF1.dtBegin) from hrFactor hrF1 
						 where hrF1.idAsset=jrA.NumActive and hrF1.dtBegin<=@dtWork and  hrF1.tpAsset = 'B'  ))
  where hrAS.DateAssign = @dtWork
	and  jrAcc.dtIncl <= @dtWork 
	and  (jrAcc.dtExcl >= @dtWork or jrAcc.dtExcl is null)
	and  jrAcc.tpAsset = 'B'
	and (jrG.dtGKDend > @dtWork or dtGKDend is null)
	and jrAcc.idAcc=@idacc

  order by tmp.NumRazdel, jrA.unnum
END
GO