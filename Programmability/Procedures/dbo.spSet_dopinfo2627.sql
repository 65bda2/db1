﻿SET QUOTED_IDENTIFIER OFF

SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 17.08.2020
-- установка jrActive.dopinf[26], jrActive.dopinf[27]
-- =============================================
CREATE procedure [dbo].[spSet_dopinfo2627] 
@NumActive int
as 
begin
  SET NOCOUNT ON;
  UPDATE jrActive 
    set DopInf=STUFF(DopInf, 27, 1,[dbo].[fnCheck_dopinfo27](@NumActive))
    from jrActive 
    where NumActive=@NumActive
    and [status] in ('T','M') 
    and isnull(IsSimpleCheck,0)<>1
    and TypeActive='D'
    and substring(DopInf,27,1)<>[dbo].[fnCheck_dopinfo27](@numactive)
  UPDATE jrActive 
    set DopInf=STUFF(DopInf, 26, 1,[dbo].[fnCheck_dopinfo26](@NumActive))
    from jrActive 
    where NumActive=@NumActive
    and [status] in ('R','W', 'K') 
    and isnull(IsSimpleCheck,0)<>1
    and TypeActive='D'
    and substring(DopInf,26,1)<>[dbo].[fnCheck_dopinfo26](@numactive)
  UPDATE jrActive 
    set DopInf=STUFF(DopInf, 27, 1,'0')
    from jrActive 
    where NumActive=@NumActive
    and [status] in ('T','M') 
    and isnull(IsSimpleCheck,0)=1
    and TypeActive='D'
    and substring(DopInf,27,1)<>'0'
  UPDATE jrActive 
    set DopInf=STUFF(DopInf, 26, 1,'0')
    from jrActive 
    where NumActive=@NumActive
    and [status] in ('R','W', 'K') 
    and isnull(IsSimpleCheck,0)=1
    and TypeActive='D'
    and substring(DopInf,26,1)<>'0' 
end
grant execute on [dbo].[spSet_dopinfo2627] to [public]
GO