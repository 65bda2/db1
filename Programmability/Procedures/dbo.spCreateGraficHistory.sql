﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		65vev3
-- Create date: 19.02.2020
-- Description: Сохранение истории графика платежей
-- =============================================
CREATE PROCEDURE [dbo].[spCreateGraficHistory]
  @NumActive Int,
  @IdTransh  Int
AS
BEGIN
  Declare @IdGroup Int, @DtOper DateTime
  
  Select @DtOper =dtWork From rfStatus
  
  Select @IdGroup = IsNull( Max( IdGroup ), 0 ) + 1 from hrPaymentGrafic 
  Where numactive= @NumActive
  
  If @IdTransh = 0
    Insert into hrPaymentGrafic ( NumActive, Payment, DatePay, Advice, TpActive, Dfrom, idGroup, AlterComment, Currency )
      Select 
        NumActive, Payment, DatePay, Advice, TpActive, @DtOper, @IdGroup, AlterComment, Currency
      From
        jrPaymentGrafic
      Where
        NumActive = @NumActive    
  Else      
    Insert into hrPaymentGrafic ( NumActive, Payment, DatePay, Advice, IdTransh, TpActive, Dfrom, idGroup, AlterComment, Currency )
      Select 
        NumActive, Payment, DatePay, Advice, IdTransh, TpActive, @DtOper, @IdGroup, AlterComment, Currency
      From
        jrPaymentGrafic
      Where
        NumActive = @NumActive And IdTransh = @IdTransh   
END
GO