﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[spAccCodeControlSum] 
--********************
-- ПЗ 6.1.1.7 	Процедура spAccCodeControlSum
-- =============================================
-- Author:<65baa2>
-- Create date: <31.10.2018>
-- Description:	<рассчитывает стандартным образом контроли и возвращает результат 1 или 0 о прохождении/не прохождении контроля>
-- =============================================
	@idAccCodeControlRule int, -- ид правила контроля
	@idOrgBuhReport int, -- ид на записи из таблицы jrOrgBuhReport
	@isPrev int=0, -- 
	@resout int output -- возвращает результат 1 или 0 о прохождении/не прохождении контроля 
AS
BEGIN
   declare @sum float;
   declare @formula varchar(2000); 
   declare @CodeCompare smallInt;
   declare @sumCR float;
   --declare @CodeOKEI int;
   declare @res int=0;
   
   --select @CodeOKEI=CodeOKEI from jrOrgBuhReport where idOrgBuhReport=@idOrgBuhReport;
  
   select @formula=formula,@sumCR=SumControlRule,@codecompare=codecompare from rfAccCodeControlRule  where idAccCodeControlRule=@idAccCodeControlRule; 
   --select * from rfAccCodeControlRule
   
   if  @formula is null or @sumCR is null or @codecompare is null 
      RETURN 0; 
   
   exec spCalcByFormulaControlRule @idOrgBuhReport,@formula,@isPrev,@calc=@sum output;
   
   if @sum is null 
       RETURN 0;
 
   --select @sum,@SumCR,@codecompare
   
    set @res=dbo.fnCompare(@sum,@sumCR,@codecompare);

	set @resout=isnull(@res,0);      
    
 	-- Return the result of the
	RETURN @resout; 
END
GO