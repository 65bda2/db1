﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spPutDownRisksNoteActive] 
--********************
-- AHEBURG-1278	Процедура spPutDownRisksNoteActive
-- =============================================
-- Author:<65eav2>
-- Create date: <12.12.2019>
-- Description:	<проставление рисков в карточку актива на рассмотрении>
-- =============================================
	@idES int,				-- ид ЭС с изменениями по организации(С101, С104)
	@note varchar(500),
	@UserName Varchar(255) = '' --имя исполнителя
AS
BEGIN
	DECLARE @numActiveFromRejectedChanges TABLE(numActive int);
	DECLARE @insertForjrNote TABLE(numActive int);
	DECLARE @dtWork DateTime = (SELECT dtWork FROM rfStatus)		-- Текущий ОД
	
-- Выбор активов, принятых на рассмотрение при приеме ЭС С101, при этом изменения по С101 отклонены в ходе принятия решения по другим ЭС
	INSERT into @numActiveFromRejectedChanges (numActive) 
	(	
	SELECT numActive 
			FROM jrActive
			WHERE UnNum in 
				(SELECT UnNum 
				FROM EDOKO_hrES 
				WHERE idES in (SELECT idES 
								FROM EDOKO_jrChangeORG 
								WHERE ChangeStatus = 'F' and idESProcess = @idES
							  )
						AND EDType = 'C101'
					AND isNull(UnNum, 'EMPTY') <> 'EMPTY'
				) 
	)
	
-- Если по активу в таблице JrNoteActive существует запись с параметрами Tag =368 и IsCreateTag=1 (данные введены вручную), то существующую запись необходимо изменить
	if exists(SELECT * FROM jrNoteActive where NumActive in (SELECT NumActive FROM @numActiveFromRejectedChanges))
	UPDATE jrNoteActive 
	SET IsCreateTag = 0
		,Note = @note
		,DateNote = @dtWork
	WHERE 
		NumActive in (SELECT numActive FROM @numActiveFromRejectedChanges)
		AND IsCreateTag = 1
		AND Tag = 368
-- Если при принятии решения по ЭС производилось отклонение изменений ЭС типа С101, то в актив соответствующий этому ЭС необходимо добавить информацию о рисках 
	INSERT into @insertForjrNote (numActive) 
	SELECT Rej.numActive 
	FROM @numActiveFromRejectedChanges Rej join jrNoteActive Note on Rej.numActive = Note.NumActive WHERE Note.Tag = 368

	INSERT INTO jrNoteActive(Tag, DateNote, Note, NumActive, IsCreateTag)	
	SELECT 
		368
		,@dtWork
		,@note
		,numActive
		,0
	FROM @insertForjrNote
	WHERE numActive not in (SELECT numActive FROM jrNoteActive WHERE Tag = 368 and IsCreateTag = 0)
	        
	INSERT INTO jrJournalActive (NumActive, DateIt, SysDate, Note, Kategory, Doer) 
     SELECT NumActive, @dtWork, @dtWork,'В C101 указана неверная информация об организации', 'AE', @UserName from @numActiveFromRejectedChanges
		
		
END
GO