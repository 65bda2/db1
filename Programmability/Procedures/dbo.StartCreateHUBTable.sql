﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartCreateHUBTable]
@IsHUB bit,
@ErrorMSG varchar(max) output
AS
BEGIN TRY
--  BEGIN TRANSACTION
   CREATE TABLE [dbo].[hub_hrTypeCredits](
	    [idAcc] [int] NOT NULL,
	    [dtBegin] [datetime] NOT NULL,
	    [tpCredit] [char](1) NOT NULL,
	    [dtEnd] [datetime] NULL,
	    [cmCredit] [char](250) NULL)
    
    CREATE TABLE [dbo].[hub_rfStatus](
	[version] [varchar](20) NOT NULL,
	[dtWork] [datetime] NULL,
	[ASPPAversion] [varchar](20) NULL,
	[DataMartVersion] [varchar](20) NULL,
	[dtBeginTechWork] [datetime] NULL,
	[dtEndTechWork] [datetime] NULL
    ) ON [PRIMARY]

   --CREATE TABLE [dbo].[Counters](
	--[NameCounter] [varchar](48) NULL,
	--[ValueCounter] [int] NULL) 

   CREATE TABLE [dbo].[hub_Counters](
	[NameCounter] [varchar](48) NULL,
	[ValueCounter] [int] NULL) 

   IF @IsHUB = 1
   CREATE TABLE [dbo].[hub_LastAssetNumber](
	[nuReg] [varchar](4) NOT NULL,
	[KPTU] [char](2) NOT NULL,
	[AssetNumNA] [int] NULL,
	[AssetNumMSP] [int] NULL,
	[AssetNumEcsar] [int] NULL,
	[AssetNumInvest] [int] NULL)

	CREATE TABLE [hub_rfBuhRecord](
		[idClass] [int] NOT NULL,
		[idRecord] [int]  NOT NULL,
		[NmRecord] [varchar](500) NOT NULL,
		[TpClass] [int] NULL,
		[Kod] [varchar](10) NULL,
		[NumKod] [varchar](10) NOT NULL,
		[isrequired] [int] NULL,
		[KodFNS] [varchar](50) NULL,
		[IsactiveRecord] [tinyint] NULL
	) 


	CREATE TABLE [hub_rfBuhPeriod](
		[idPeriod] [int] NOT NULL,
		[NmPeriod] [varchar](255) NOT NULL,
		[idclass] [int] NULL,
		[NumPeriod] [int] NOT NULL,
		[YearPeriod] [int] NOT NULL,
		[dtShow] [datetime] NOT NULL,
		[isBuhcheck] [int] NULL,
		[ReportDate] [datetime] NOT NULL
	)  

	CREATE TABLE [hub_rfBuhFactorType](
		[FSymbol] [char](2) NOT NULL,
		[FNumber] [varchar](2) NOT NULL
	)  

	CREATE TABLE [hub_rfTpClassBuh](
		[NmClass] [varchar](500) NULL,
		[TpClass] [int] NOT NULL,
		[idClass] [int] NOT NULL
	)  
/*
	CREATE TABLE [hub_rfOKVED2_R](
		[ID] [numeric](12, 0) NOT NULL,
		[CODE] [numeric](10, 0) NOT NULL,
		[N_Razdel] [char](1) NOT NULL,
		[NAME_RUS] [varchar](1000) NULL,
		[CB_Date] [datetime] NULL,
		[CE_Date] [datetime] NULL
	)  
*/
	CREATE TABLE [hub_rfOKVED](
		[CODE] [int] NOT NULL,
		[KOD] [char](8) NOT NULL,
		[NAME_RUS] [varchar](1000) NOT NULL,
		[N_RAZDEL] [char](1) NULL,
		[CB_Date] [datetime] NULL,
		[CE_Date] [datetime] NULL,
		[ID] [numeric](12, 0) NULL,
		[CODEFORPPA] [int] NULL
	)  

	CREATE TABLE [hub_rfOKOPF](
		[CODE] [int] NOT NULL,
		[KOD] [char](5) NOT NULL,
		[NAME_RUS] [varchar](250) NOT NULL
	)  

	CREATE TABLE [hub_rfOKFS](
		[CODE] [int] NOT NULL,
		[KOD] [char](2) NOT NULL,
		[NAME_RUS] [varchar](250) NOT NULL
	)  

	CREATE TABLE [hub_rfOKATO](
		[CODE] [int] NOT NULL,
		[KOD] [char](11) NOT NULL,
		[NAME_RUS] [varchar](250) NOT NULL
	)  

	CREATE TABLE [hub_rfDKKP](
		[CODE] [int] NOT NULL,
		[KP] [varchar](2) NOT NULL,
		[NAME_RUS] [varchar](250) NULL
	)  

/*	CREATE TABLE [hub_jrSavePlace](
		[idMesto] [int] NOT NULL,
		[Mesto] [varchar](500) NOT NULL
	)  
*/
	CREATE TABLE [hub_hrWaitCheck](
		[NumActive] [int] NULL,
		[dtBegin] [datetime] NOT NULL,
		[dtEnd] [datetime] NULL,
		[idTransh] [int] NULL
	)  
	CREATE TABLE [hub_hrActive](
		[OperationDate] [datetime] NOT NULL,
		[HrStatus] [tinyint] NOT NULL,
		[DFROM] [datetime] NOT NULL,
		[TypeActive] [varchar](2) NOT NULL,
		[NumActive] [int] NOT NULL,
		[NmZaemshik] [int] NULL,
		[Nominal] [money] NULL,
		[DatePayment] [datetime] NULL,
		[Cost] [money] NULL,
		[firstCost] [money] NULL,
		[TempCost] [money] NULL,
		[Company] [int] NULL,
		[DopInf] [char](25) NOT NULL,
		[idLetter] [int] NOT NULL,
		[UnNum] [char](16) NULL,
		[status] [char](1) NOT NULL,
		[Currency] [char](3) NULL,
		[firstCurrency] [char](3) NULL,
		[CostCurrency] [char](3) NULL,
		[CostRub] [money] NULL,
		[TempCostRub] [money] NULL,
		[inclDate] [datetime] NULL,
		[Begindate] [datetime] NULL,
		[Decidedate] [datetime] NULL,
		[getdate] [datetime] NULL,
		[dateTUletter] [datetime] NULL,
		[Note] [varchar](500) NULL,
		[NomerVD] [varchar](100) NULL,
		[AssetIssue] [char](20) NULL,
		[TypeVexel] [varchar](50) NULL,
		[CreationDate] [datetime] NULL,
		[AuditDate] [datetime] NULL,
		[Course] [decimal](20, 5) NULL,
		[SavePlace] [varchar](500) NULL,
		[IsActNumber] [char](1) NULL,
		[isHundredBillion] [tinyint] NULL,
		[AssetQuality] [int] NULL,
		[IndosamentDate] [datetime] NULL,
		[FirstCostRub] [decimal](18, 2) NULL,
		[IsOriginal] [int] NULL,
		[idGrouphrPG] [int] NULL,
		[Nucred] [char](20) NULL,
		[OkvedOrgToCheck] [varchar](25) NULL,
		[NmZaem] [varchar](255) NULL,
		[ZaemOKOPF] [varchar](10) NULL,
		[businessQuality] [int] NULL,
		[NmOrgToCheck] [varchar](255) NULL,
		[OGRNOrgtocheck] [varchar](20) NULL,
		[NMCheck] [varchar](5) NULL
	)  

	CREATE TABLE [hub_DKO_hrRepayment](
		[IdRecord] [bigint]  NOT NULL,
		[_IdRecord] [int] NOT NULL,
		[IdRepayment] [numeric](16, 0) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NOT NULL,
		[StateSED] [int] NOT NULL,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[IdAsset] [numeric](16, 0) NOT NULL,
		[D_Payment] [smalldatetime] NOT NULL,
		[Amount] [numeric](22, 7) NOT NULL,
		[AlterComment] [varchar](500) NULL,
		[IdAssetIdRecord] [bigint] NOT NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL
	)  

	CREATE TABLE [hub_DKO_hrMutualParticipation](
		[IdRecord] [bigint]  NOT NULL,
		[_IdRecord] [int] NOT NULL,
		[IdMutualParticipation] [numeric](16, 0) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NOT NULL,
		[StateSED] [int] NOT NULL,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[IdCompany] [numeric](16, 0) NOT NULL,
		[CORegNum] [varchar](20) NOT NULL,
		[IsCompanyOwner] [tinyint] NOT NULL,
		[AlterComment] [varchar](500) NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL
	)  

	CREATE TABLE [hub_DKO_hrCompany](
		[IdRecord] [bigint]  NOT NULL,
		[_IdOrg] [int] NOT NULL,
		[IdCompany] [numeric](16, 0) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NOT NULL,
		[StateSED] [int] NOT NULL,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[INN] [varchar](12) NULL,
		[EGRUL] [varchar](20) NULL,
		[CompanyName] [varchar](255) NOT NULL,
		[CodeOKVED] [int] NULL,
		[CodeOKOPF] [int] NULL,
		[CodeOKFS] [int] NULL,
		[SubjectType] [tinyint] NOT NULL,
		[IsConforming] [tinyint] NULL,
		[IsInList] [tinyint] NULL,
		[IsInMFOList] [tinyint] NULL,
		[IsResident] [tinyint] NULL,
		[BusinessQuality] [tinyint] NULL,
		[D_LastRegDate] [smalldatetime] NULL,
		[IsBookKeepingOK] [tinyint] NULL,
		[AlterComment] [varchar](500) NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL,
		[IsLOConforming] [tinyint] NULL
	)  

	CREATE TABLE [hub_DKO_hrBankInTU](
		[IdRecord] [bigint]  NOT NULL,
		[_IdKO] [int] NOT NULL,
		[IdBankInTU] [numeric](16, 0) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NOT NULL,
		[StateSED] [int] NOT NULL,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[BankRegNum] [varchar](20) NULL,
		[IsPartnerOfMSP] [tinyint] NULL,
		[AlterComment] [varchar](500) NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL
	)  

	CREATE TABLE [hub_DKO_hrAssetState](
		[IdRecord] [bigint]  NOT NULL,
		[_IdRecord] [int] NOT NULL,
		[IdAssetState] [varchar](17) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NULL,
		[StateSED] [int] NOT NULL,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[IdAsset] [numeric](16, 0) NOT NULL,
		[PartitionType] [char](2) NOT NULL,
		[CrdNum] [varchar](10) NULL,
		[ReqType] [tinyint] NULL,
		[ReqNum] [varchar](20) NULL,
		[ReqDate] [smalldatetime] NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL
	)  

	CREATE TABLE [hub_DKO_hrAsset](

		[IdRecord] [bigint]  NOT NULL,
		[_NumActive] [int] NOT NULL,
		[IdAsset] [numeric](16, 0) NOT NULL,
		[D_From] [smalldatetime] NOT NULL,
		[IdUpdate] [bigint] NOT NULL,
		[StateSED] [int] NOT NULL ,
		[IsArchive] [tinyint] NOT NULL,
		[CorrectionType] [tinyint] NOT NULL,
		[AssetType] [numeric](5, 0) NOT NULL,
		[AssetCode] [varchar](20) NULL,
		[AssetIssue] [varchar](20) NULL,
		[AssetNumber] [varchar](100) NOT NULL,
		[AssetDate] [smalldatetime] NULL,
		[D_Repayment] [smalldatetime] NULL,
		[FaceValue] [numeric](22, 7) NULL,
		[FaceValueCurrency] [numeric](3, 0) NULL,
		[FvCurrencyRate] [numeric](21, 7) NULL,
		[PurchaseValue] [numeric](22, 7) NULL,
		[PurchaseCurrency] [numeric](3, 0) NULL,
		[PureMass] [numeric](10, 2) NULL,
		[NetMass] [numeric](10, 2) NULL,
		[Standard] [numeric](7, 4) NULL,
		[IsActive] [tinyint] NOT NULL,
		[AssetQuality] [numeric](1, 0) NULL,
		[IdIssuer] [numeric](16, 0) NULL,
		[IdOrgToCheck] [numeric](16, 0) NULL,
		[MainAccount] [varchar](20) NULL,
		[PayOrgBIC] [varchar](9) NULL,
		[TUHolder] [varchar](2) NULL,
		[DebtorRegNum] [varchar](20) NULL,
		[IdParentAsset] [numeric](16, 0) NULL,
		[OriginalsState] [tinyint] NULL,
		[isGuaranteedByCrimea] [tinyint] NULL,
		[isGuaranteedByRF] [tinyint] NULL,
		[isMinFinAgreed] [tinyint] NULL,
		[AlterComment] [varchar](500) NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL,
		[GKDNum] [varchar](15) NULL,
		[GKDDate] [datetime] NULL
	)  

	CREATE TABLE [hub_hrBuhRecord](
		[OperationDate] [datetime] NOT NULL,
		[DFrom] [datetime] NOT NULL,
		[idRecord] [int] NOT NULL,
		[RValue] [decimal](25, 2) NULL,
		[idOrg] [int] NOT NULL,
		[idperiod] [int] NOT NULL
	)  

	CREATE TABLE [hub_hrOrg](
		[OperationDate] [datetime] NOT NULL,
		[HrStatus] [tinyint] NOT NULL,
		[DFROM] [datetime] NULL,
		[idOrg] [int] NULL,
		[INN] [varchar](12) NULL,
		[EGRUL] [varchar](20) NULL,
		[NonResidentID] [varchar](30) NULL,
		[CompanyName] [varchar](255) NOT NULL,
		[CodeOKVED] [int] NULL,
		[KodOKVED] [char](8) NULL,
		[CodeOKOPF] [int] NULL,
		[KODOKOPF] [char](5) NULL,
		[CodeOKFS] [int] NULL,
		[KODOKFS] [char](2) NULL,
		[IsSubject] [tinyint] NOT NULL,
		[tpDopInfo] [char](5) NOT NULL,
		[IsConforming] [tinyint] NULL,
		[IsInList] [tinyint] NULL,
		[IsResident] [tinyint] NULL,
		[BusinessQuality] [tinyint] NULL,
		[D_lastRegDate] [smalldatetime] NULL,
		[IsBookKeepingOK] [tinyint] NULL,
		[IsFederalLaw] [tinyint] NULL,
		[AlterComment] [varchar](255) NULL,
		[isSootv] [char](1) NOT NULL,
		[ShortNmORG] [varchar](255) NULL,
		[TpDtReg] [tinyint] NULL,
		[dtBookKeeping] [datetime] NULL,
		[AnotherInf] [int] NULL,
		[AnotherInfNote] [varchar](255) NULL,
		[TpCheck] [varchar](5) NULL,
		[nmOrg]  [varchar](255)
	)  

	CREATE TABLE [hub_hrFactor](
		[dtBegin] [datetime] NOT NULL,
		[idAsset] [int] NOT NULL,
		[tpAsset] [char](1) NOT NULL,
		[tpFactor] [char](1) NOT NULL,
		[pcFactor] [decimal](5, 4) NOT NULL
	)  

	CREATE TABLE [hub_jrCurrency](
		[nuISO] [char](3) NOT NULL,
		[nmISO] [char](3) NOT NULL,
		[nmCurr] [char](100) NOT NULL
	)  

	CREATE TABLE [hub_jrRKC](
		[idRKC] [int]  NOT NULL,
		[nmRKC] [varchar](250) NOT NULL,
		[nmPostRKC] [varchar](200) NULL,
		[nmBossRKC] [varchar](100) NULL,
		[nuBIKRKC] [char](9) NOT NULL,
		[izFormIzvesh] [char](1) NULL,
		[tpRKC] [char](1) NOT NULL,
		[nuRNKOinRKC] [char](20) NULL
	)  

	CREATE TABLE [hub_rfDays](
		[dtDay] [datetime] NOT NULL,
		[tpDay] [char](1) NOT NULL,
		[cmDay] [char](100) NULL
	)  

	CREATE TABLE [hub_rfFactor](
		[dtFactor] [datetime] NOT NULL,
		[tpFactor] [char](2) NOT NULL,
		[pcFactor] [decimal](7, 4) NOT NULL,
		[idOrg] [int] NULL,
		[dtEnd] [datetime] NULL
	)  

	CREATE TABLE [hub_rfSignRekvizitASPPA](
		[tpSign] [varchar](50) NOT NULL,
		[nmSign] [varchar](400) NULL,
		[tpGroup] [char](1) NOT NULL
	)  

	CREATE TABLE [hub_rfSignASPPA](
		[idsign] [int]  NOT NULL,
		[tpSign] [char](1) NOT NULL,
		[nmSign] [varchar](400) NOT NULL,
		[nmPost] [varchar](400) NOT NULL,
		[Dopinf] [char](15) NOT NULL,
		[NumOrder] [int] NOT NULL,
		[tpGroup] [char](2) NULL,
		[foundation] [varchar](255) NULL
	)  

	CREATE TABLE [hub_rfSign](
		[tpSign] [varchar](60) NOT NULL,
		[nmSign] [varchar](500) NULL
	)  

	CREATE TABLE [hub_rfReportsASPPA](
		[idReport] [int]  NOT NULL,
		[dtreport] [datetime] NOT NULL,
		[nuReport] [int] NOT NULL,
		[tpreport] [char](1) NOT NULL,
		[nuIshod] [varchar](30) NOT NULL,
		[idKO] [int] NULL,
		[idORG] [int] NULL,
		[Numactive] [int] NULL,
		[nmUser] [varchar](50) NOT NULL,
		[komment] [varchar](255) NULL
	)  

	CREATE TABLE [hub_rfOrgList](
		[idRecord] [int]  NOT NULL,
		[nmOrg] [varchar](255) NOT NULL,
		[EGRUL] [varchar](20) NOT NULL,
		[Comment] [varchar](255) NULL,
		[dtBegin] [datetime] NOT NULL,
		[dtEnd] [datetime] NULL,
		[NmdtBeginOrg] [varchar](255) NULL
	)  

	CREATE TABLE [hub_rfBuhFactor](
		[idrecord] [int]  NOT NULL,
		[OKVED] [char](8) NOT NULL,
		[REP_Date] [datetime] NOT NULL,
		[TpFactor] [char](2) NOT NULL,
		[PRIZ] [int] NOT NULL,
		[SRED] [decimal](22, 10) NOT NULL,
		[DOPUST] [decimal](22, 10) NOT NULL
	)  

	CREATE TABLE [hub_rfASPPARestriction](
		[idKod] [int]  NOT NULL,
		[DownKod] [char](11) NOT NULL,
		[UpKod] [char](11) NOT NULL,
		[dtBegin] [datetime] NULL,
		[dtend] [datetime] NULL
	)  

	CREATE TABLE [hub_jrOrg](
		[idOrg] [int]  NOT NULL,
		[nmAddr] [varchar](255) NULL,
		[businessQuality] [int] NULL,
		[OKPO] [varchar](8) NULL,
		[INN] [varchar](12) NULL,
		[EGRUL] [varchar](20) NULL,
		[CompanyName] [varchar](255) NULL,
		[CodeOKVED] [int] NULL,
		[CodeOKOPF] [int] NULL,
		[CodeOKFS] [int] NULL,
		[IsSubject] [tinyint] NULL,
		[IsConforming] [tinyint] NULL,
		[IsInList] [tinyint] NULL,
		[IsResident] [tinyint] NULL,
		[D_lastRegDate] [smalldatetime] NULL,
		[IsBookKeepingOK] [tinyint] NULL,
		[IsFederalLaw] [tinyint] NULL,
		[AlterComment] [varchar](255) NULL,
		[isSootv] [char](1) NOT NULL,
		[nmOrg]  [varchar](255),
		[belonging] [tinyint] NULL,
		[dtOgrn] [datetime] NULL,
		[CodeOKATO] [char](11) NULL,
		[ShortNmORG] [varchar](255) NULL,
		[TpDtReg] [tinyint] NULL,
		[NmLaw] [varchar](255) NULL,
		[dtBookKeeping] [datetime] NULL,
		[AnotherInf] [int] NULL,
		[AnotherInfNote] [varchar](255) NULL,
		[Rating] [varchar](255) NULL,
		[NonResidentID] [varchar](30) NULL,
		[tpDopInfo] [char](5) NOT NULL
	)  

	CREATE TABLE [hub_jrKO](
		[idKO] [int]  NOT NULL,
		[nmKO] [varchar](250) NOT NULL,
		[nuReg] [varchar](9) NOT NULL,
		[nuBIK] [varchar](9) NULL,
		[nuINN] [varchar](10) NULL,
		[nmJurAddr] [varchar](250) NULL,
		[nmFactAddr] [varchar](250) NULL,
		[nmNadzTU] [varchar](200) NULL,
		[nuAccount] [varchar](20) NULL,
		[nuRkcBik] [varchar](9) NULL,
		[nnKO] [varchar](128) NULL,
		[idRKC] [int] NULL,
		[OKATO] [varchar](11) NULL,
		[OKPO] [varchar](8) NULL,
		[OGRN] [varchar](13) NULL,
		[nuAccRNKO] [varchar](20) NULL,
		[idRNKO] [int] NULL,
		[isInList] [tinyint] NULL,
		[IsSubject] [tinyint] NOT NULL,
		[isElectronFormKO] [tinyint] NULL,
		[ESODVersion] [int] NULL
	)  

	CREATE TABLE [hub_hrRateExch](
		[dtRate] [datetime] NOT NULL,
		[nuISO] [char](3) NOT NULL,
		[nuScale] [int] NOT NULL,
		[nuRate] [decimal](20, 5) NOT NULL
	)  

	CREATE TABLE [hub_JrBuhRecord](
		[idRecord] [int] NOT NULL,
		[RValue] [decimal](25, 2) NULL,
		[idOrg] [int] NOT NULL,
		[idperiod] [int] NOT NULL,
		[OperationDate] datetime
	)  
	CREATE TABLE [hub_jrGKD](
		[nuGKD] [int]  NOT NULL,
		[dtGKD] [datetime] NOT NULL,
		[idKO] [int] NOT NULL,
		[isSootv] [char](1) NOT NULL,
		[mnLimitAgg] [money] NULL,
		[dtGKDend] [datetime] NULL,
		[numGKD] [varchar](15) NOT NULL,
		[tpGKD] [char](1) NOT NULL,
		[isTreb] [char](1) NOT NULL,
		[dtGKDUved] [datetime] NULL,
		[isPartner] [char](1) NOT NULL,
		[nuGKD236] [int] NULL,
		[nuGKD312] [int] NULL,
		[nuGKD362] [int] NULL,
		[nmKO] [varchar](250) NULL,
		[nnKO] [varchar](128) NULL,
		[nuReg] [varchar](9) NULL,
		[nuBIK] [varchar](9) NULL,
		[nuINN] [varchar](10) NULL,
		[nmJurAddr] [varchar](250) NULL,
		[nmFactAddr] [varchar](250) NULL,
		[nmNadzTU] [varchar](200) NULL,
		[nuEKD] [int] NULL,
		[dtEKDBegin]  datetime null ,
		[dtGKDStop]  datetime null
	)  

	CREATE TABLE [hub_jrDopLetter](
		[idDopLetter] [int]  NOT NULL,
		[Note] [varchar](500) NULL,
		[NumLetter] [varchar](20) NOT NULL,
		[DateLetter] [datetime] NOT NULL,
		[idKO] [int] NOT NULL,
		[tpLetter] [char](1) NOT NULL
	)  

	CREATE TABLE [hub_hrOwnFunds](
		[idKO] [int] NOT NULL,
		[dtOwnFund] [datetime] NOT NULL,
		[mnOwnFund] [money] NULL,
		[cmOwnFund] [varchar](500) NULL
	)  

	CREATE TABLE [hub_jrLetter](
		[idLetter] [int]  NOT NULL,
		[NumSADD] [varchar](50) NULL,
		[DateLetter] [datetime] NOT NULL,
		[DateLetterTU] [datetime] NOT NULL,
		[idKo] [int] NOT NULL,
		[Kolvo] [int] NULL,
		[Status] [char](1) NOT NULL,
		[P6] [char](1) NULL,
		[TrueKolvo] [char](1) NULL,
		[VestnikBR] [char](1) NULL,
		[GrLetter] [char](1) NULL,
		[Komment] [varchar](300) NULL,
		[typeLetter] [char](1) NOT NULL,
		[Q1] [char](1) NULL,
		[Q2] [char](1) NULL,
		[Q3] [char](1) NULL,
		[Q4] [char](1) NULL,
		[NumLetter] [varchar](100) NULL,
		[WarrantDate] [datetime] NULL,
		[LBeginDate] [datetime] NULL,
		[NumLetKo] [varchar](100) NULL,
		[DtLetKo] [datetime] NULL,
		[NumLetStore] [varchar](100) NULL,
		[dtLetstore] [datetime] NULL,
		[NumLetIncl] [varchar](100) NULL,
		[DtLetIncl] [datetime] NULL,
		[DtIncl] [datetime] NULL,
		[idStore] [int] NULL,
		[dtRemoval] [datetime] NULL,
		[countTransh] [int] NULL,
		[Qdopinf] [char](10) NULL
	)  

	CREATE TABLE [hub_rfAffPers](
		[idKO] [int] NOT NULL,
		[idOrg] [int] NOT NULL,
		[pcShare] [decimal](7, 4) NULL,
		[IsCompanyOwner] [tinyint] NOT NULL,
		[dtBegin] [datetime] NULL,
		[dtEnd] [datetime] NULL,
		[idRecord] [int]  NOT NULL,
		[AlterComment] [varchar](500) NULL
	)  
	/*
	CREATE TABLE [hub_rfFaceKO](
		[idKO] [int] NOT NULL,
		[nmFIO] [char](100) NOT NULL,
		[nmPost] [char](250) NULL,
		[cmPers] [varchar](2000) NULL,
		[nuTrust] [char](20) NULL,
		[dtTrust] [datetime] NULL,
		[dtEndTrust] [datetime] NULL,
		[nuLetter] [char](20) NULL,
		[dtLetter] [datetime] NULL,
		[dtEndLetter] [datetime] NULL,
		[nmOtherDoc] [char](100) NULL,
		[nuOtherDoc] [char](20) NULL,
		[dtOtherDoc] [datetime] NULL,
		[dtEndOtherDoc] [datetime] NULL,
		[tpPersSign] [char](3) NULL,
		[cmPersSign] [varchar](500) NULL,
		[imSignCard] [image] NULL,
		[dtSignCardLoad] [datetime] NULL,
		[fNameSignCard] [varchar](100) NULL,
		[idrfFaceKO] [int]  NOT NULL
	)    
	*/
	CREATE TABLE [hub_jrAccount](
		[idAcc] [int]  NOT NULL,
		[tpCredit] [char](1) NULL,
		[mnLimit] [money] NULL,
		[mnLiability] [money] NULL,
		[tpRazdel] [char](1) NULL,
		[idKO] [int] NOT NULL,
		[isSootv] [char](1) NOT NULL,
		[nuGKD] [int] NOT NULL,
		[dtIncl] [datetime] NOT NULL,
		[dtExcl] [datetime] NULL,
		[isPayForBKL] [char](1) NOT NULL,
		[nmKO] [varchar](250) NULL,
		[nnKO] [varchar](128) NULL,
		[nuReg] [varchar](9) NULL,
		[nuBIK] [varchar](9) NULL,
		[nuINN] [varchar](10) NULL,
		[nuAccount] [varchar](20) NULL,
		[nmJurAddr] [varchar](250) NULL,
		[nmFactAddr] [varchar](250) NULL,
		[nmNadzTU] [varchar](200) NULL,
		[idRKC] [int] NULL,
		[nuCorrAcc]  [varchar](20),
		[tpInstr] [char](1) NOT NULL,
		[tpAsset] [char](1) NULL
	)  

	CREATE TABLE [hub_rfGKD](
		[nuGKD] [int] NOT NULL,
		[nuDopSogl] [char](30) NOT NULL,
		[dtDopSogl] [datetime] NOT NULL,
		[cmDopSogl] [varchar](500) NOT NULL
	)  

	CREATE TABLE [hub_JrJournalLetter](
		[dateIt] [datetime] NOT NULL,
		[SysDate] [datetime] NOT NULL,
		[Note] [varchar](500) NOT NULL,
		[idletter] [int] NOT NULL,
		[Kategory] [char](2) NOT NULL,
		[idJournal] [int]  NOT NULL,
		[NumLetter] [varchar](500) NULL
	)  

	CREATE TABLE [hub_jrBlock](
		[idBlock] [int]  NOT NULL,
		[nuBlock] [char](17) NULL,
		[nuMain] [char](17) NULL,
		[nuDepo] [char](20) NULL,
		[nmDepo] [char](250) NULL,
		[cdDepo] [char](12) NULL,
		[dtOut] [datetime] NULL,
		[dtLast] [datetime] NULL,
		[idAcc] [int] NOT NULL
	)  

	CREATE TABLE [hub_jrActive](
		[TypeActive] [varchar](2) NOT NULL,
		[NumActive] [int]  NOT NULL,
		[NmZaemshik] [int] NULL,
		[Nominal] [money] NULL,
		[DatePayment] [datetime] NULL,
		[Cost] [money] NULL,
		[Company] [int] NULL,
		[DopInf] [char](25) NOT NULL,
		[idLetter] [int] NOT NULL,
		[UnNum] [char](16) NULL,
		[status] [char](1) NOT NULL,
		[Currency] [char](3) NULL,
		[Begindate] [datetime] NULL,
		[Decidedate] [datetime] NULL,
		[getdate] [datetime] NULL,
		[dateTUletter] [datetime] NULL,
		[Note] [varchar](500) NULL,
		[NomerVD] [varchar](100) NULL,
		[TypeVexel] [varchar](50) NULL,
		[CreationDate] [datetime] NULL,
		[AuditDate] [datetime] NULL,
		[Course] [decimal](20, 5) NULL,
		[SavePlace] [varchar](500) NULL,
		[IsActNumber] [char](1) NULL,
		[CostCurrency] [char](3) NULL,
		[CostRub] [money] NULL,
		[inclDate] [datetime] NULL,
		[AssetIssue] [char](20) NULL,
		[TempCost] [money] NULL,
		[TempCostRub] [money] NULL,
		[firstCost] [money] NULL,
		[firstCurrency] [char](3) NULL,
		[MarkBullion] [varchar](50) NULL,
		[LigatureWeight] [numeric](21, 4) NULL,
		[ChemicalWeight] [numeric](21, 4) NULL,
		[fineness] [numeric](10, 4) NULL,
		[tpStatus] [char](1) NULL,
		[Metaltype] [char](3) NULL,
		[NotInStore] [int] NULL,
		[AssetQuality] [int] NULL,
		[InsurancePercent] [decimal](25, 2) NULL,
		[IndosamentDate] [datetime] NULL,
		[FirstCostRub] [decimal](18, 2) NULL,
		[IsBaseRefusal] [tinyint] NULL,
		[isAKG] [int] NULL,
		[IsOriginal] [int] NULL,
		[isPartnerMSP] [char](1) NULL,
		[dtRequestDocum] [datetime] NULL,
		[dtRequestOriginal] [datetime] NULL,
		[isHundredBillion] [tinyint] NULL,
		[istrigger] [int] NULL,
		[IsSimpleCheck] [int] NULL,
		[isElectronForm] [tinyint] NULL,
		[CreditAcc] [varchar](20) NULL,
		[SecuringReq] [varchar](2000) NULL,
		[IsNoKreditBlockage] [int] NULL
	)  

	CREATE TABLE [hub_rfDopLetActive](
		[NumActive] [int] NOT NULL,
		[idDopLetter] [int] NOT NULL
	)  

	CREATE TABLE [hub_jrRequestDocActive](
		[dtBegin] [datetime] NOT NULL,
		[RequestStatus] [char](1) NOT NULL,
		[dtControl] [datetime] NULL,
		[TpRequest] [char](1) NOT NULL,
		[dtEnd] [datetime] NULL,
		[Numactive] [int] NOT NULL,
		[NumRequest] [int] NOT NULL,
		[Comment] [varchar](500) NULL
	)  

	CREATE TABLE [hub_jrPaymentGrafic](
		[NumActive] [int] NULL,
		[Payment] [money] NOT NULL,
		[DatePay] [datetime] NOT NULL,
		[Advice] [char](1) NOT NULL,
		[IdTransh] [int] NULL,
		[TpActive] [char](1) NOT NULL,
		[AlterComment] [char](255) NULL,
		[IdRecord] [bigint]  NOT NULL
	)  

	CREATE TABLE [hub_lgArchActive](
		[idArhActive] [int]  NOT NULL,
		[Dateit] [datetime] NULL,
		[RekvOrg] [varchar](500) NULL,
		[CheckP36] [char](15) NULL,
		[CheckP35] [char](15) NULL,
		[Cost] [varchar](100) NULL,
		[Kategory] [char](10) NULL,
		[Kurs] [char](20) NULL,
		[Koef] [char](10) NULL,
		[costrub] [varchar](100) NULL,
		[SavePlace] [varchar](200) NULL,
		[status] [varchar](2048) NULL,
		[DSign1] [varchar](150) NULL,
		[DSign2] [varchar](150) NULL,
		[FIo1] [varchar](100) NULL,
		[FIO2] [varchar](100) NULL,
		[NumActive] [int] NOT NULL,
		[Numorder] [int] NULL,
		[Komment] [varchar](150) NULL,
		[happen] [char](1) NULL,
		[NmCurrency] [varchar](20) NULL
	)  

	CREATE TABLE [hub_jrTransh](
		[IdTransh] [int]  NOT NULL,
		[Cost] [money] NOT NULL,
		[NomerVD] [varchar](100) NULL,
		[CreationDate] [datetime] NOT NULL,
		[NumActive] [int] NOT NULL,
		[PaymentDate] [datetime] NOT NULL,
		[Status] [char](1) NOT NULL,
		[Dopinf] [char](25) NOT NULL,
		[IdLetter] [int] NOT NULL,
		[BeginDate] [datetime] NULL,
		[inclDate] [datetime] NULL,
		[IsOriginal] [char](1) NULL,
		[AlterComment] [varchar](500) NULL
	)  

	CREATE TABLE [hub_jrNoteActive](
		[Tag] [int] NOT NULL,
		[DateNote] [datetime] NULL,
		[Note] [varchar](500) NULL,
		[NumActive] [int] NOT NULL,
		[idTransh] [int] NULL
	)  

	CREATE TABLE [hub_jrJournalActive](
		[DateIt] [datetime] NOT NULL,
		[SysDate] [datetime] NULL,
		[Note] [varchar](2048) NULL,
		[NumActive] [int] NOT NULL,
		[Kategory] [char](2) NULL,
		[idJournal] [int]  NOT NULL,
		[NumLetter] [varchar](2048) NULL,
		[Doer] [varchar](255) NULL
	)  

	CREATE TABLE [hub_hrStockSect](
		[dtSect] [datetime] NOT NULL,
		[idBlock] [int] NOT NULL,
		[tpSect] [char](3) NOT NULL,
		[idStock] [int] NOT NULL,
		[cdStock] [int] NOT NULL,
		[qnStock] [int] NOT NULL,
		[dtPrice] [datetime] NOT NULL,
		[mnCost] [money] NULL,
		[mnCostCoef] [money] NULL,
		[dtCoeff] [datetime] NOT NULL,
		[dtCoeffNew] [datetime] NULL,
		[tpActive] [smallint] NOT NULL,
		[IdRecord] [bigint]  NOT NULL
	)  

	CREATE TABLE [hub_hrPaymentGrafic](
		[Dfrom] [datetime] NOT NULL,
		[idGroup] [int] NOT NULL,
		[NumActive] [int] NOT NULL,
		[tpActive] [char](1) NOT NULL,
		[DatePay] [smalldatetime] NOT NULL,
		[Payment] [money] NOT NULL,
		[Advice] [char](1) NULL,
		[IdTransh] [int] NULL,
		[AlterComment] [char](255) NULL
	)  

	CREATE TABLE [hub_hrActiveEvent](
		[idEvent] [int]  NOT NULL,
		[tpEvent] [char](1) NOT NULL,
		[dtEvent] [datetime] NOT NULL,
		[NumActive] [int] NOT NULL,
		[StatusEvent] [tinyint] NOT NULL,
		[StatusEventEDOKO] [tinyint] NOT NULL
	)  

	CREATE TABLE [hub_hrStockPawn](
		[dtRekv] [datetime] NOT NULL,
		[idBlock] [int] NOT NULL,
		[tpRekv] [char](1) NOT NULL,
		[nuRegZayav] [int] NULL,
		[dtPostup] [datetime] NULL,
		[tmPostup] [char](5) NULL,
		[nuZayav] [char](20) NULL,
		[dtZayav] [datetime] NULL,
		[dtCred] [datetime] NOT NULL,
		[mnCred] [money] NOT NULL,
		[qnCredDecl] [int] NOT NULL,
		[qnCred] [int] NOT NULL,
		[pcCred] [decimal](7, 4) NOT NULL,
		[tpSatisf] [char](1) NOT NULL,
		[tpZayav] [char](1) NULL,
		[tpRazdel] [char](1) NULL,
		[nuInfo] [char](15) NULL,
		[dtInfo] [datetime] NULL,
		[nuCred] [char](20) NULL,
		[mnProc] [money] NULL,
		[mnZalog] [money] NULL,
		[mnZalog91311] [money] NULL,
		[mnZalogRepl] [money] NULL,
		[mnPenya] [money] NULL,
		[nuDepo] [char](17) NULL,
		[nmUser] [char](50) NOT NULL,
		[cdStock] [int]  NOT NULL,
		[cmPawn] [varchar](500) NULL,
		[isGuarant] [char](1) NOT NULL,
		[cdClass] [int] NULL,
		[dtIzvesh] [datetime] NULL,
		[nuCredDolgAcc] [char](20) NULL,
		[nuProcDolgAcc] [char](20) NULL,
		[nuPenyDolgAcc] [char](20) NULL,
		[nuBIKDebt] [char](9) NULL,
		[nuBIKMail] [char](9) NULL,
		[nuDealMMVB] [char](20) NULL,
		[mnZalogRepl91311] [money] NULL,
		[dtSpread] [datetime] NULL,
		[pcSpread] [decimal](7, 4) NULL,
		[cdStockForRollOver] [int] NULL,
		[dtNotCashCover] [datetime] NULL,
		[nuAccDebt] [char](20) NULL,
		[mnProc30] [money] NULL,
		[mnCredDecl] [money] NULL,
		[ESDate] [smalldatetime] NULL,
		[ESNo] [int] NULL,
		[ESDate507] [smalldatetime] NULL,
		[ESNo507] [int] NULL,
		[ESDate509] [smalldatetime] NULL,
		[ESNo509] [int] NULL,
		[isBKL] [char](1) NOT NULL)

	ALTER TABLE [hub_DKO_hrAsset] ADD  DEFAULT ((0)) FOR [StateSED] 
	ALTER TABLE [hub_DKO_hrAsset] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_DKO_hrAssetState] ADD  DEFAULT ((0)) FOR [StateSED]
	ALTER TABLE [hub_DKO_hrAssetState] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_DKO_hrBankInTU] ADD  DEFAULT ((0)) FOR [StateSED]
	ALTER TABLE [hub_DKO_hrBankInTU] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_DKO_hrCompany] ADD  DEFAULT ((0)) FOR [StateSED]
	ALTER TABLE [hub_DKO_hrCompany] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_DKO_hrMutualParticipation] ADD  DEFAULT ((0)) FOR [StateSED]
	ALTER TABLE [hub_DKO_hrMutualParticipation] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_DKO_hrRepayment] ADD  DEFAULT ((0)) FOR [StateSED]
	ALTER TABLE [hub_DKO_hrRepayment] ADD  DEFAULT ((0)) FOR [IsArchive]
	ALTER TABLE [hub_hrActiveEvent] ADD  DEFAULT ((0)) FOR [StatusEvent]
	ALTER TABLE [hub_hrActiveEvent] ADD  DEFAULT ((0)) FOR [StatusEventEDOKO]
	ALTER TABLE [hub_hrOrg] ADD  DEFAULT ((0)) FOR [HrStatus]
	ALTER TABLE [hub_hrOrg] ADD  DEFAULT ('FFFFF') FOR [tpDopInfo]
	ALTER TABLE [hub_hrOrg] ADD  DEFAULT ('F') FOR [isSootv]
	ALTER TABLE [hub_hrOrg] ADD  DEFAULT ((0)) FOR [AnotherInf]
	ALTER TABLE [hub_hrStockSect] ADD  DEFAULT (0) FOR [cdStock]
	ALTER TABLE [hub_hrStockSect] ADD  DEFAULT ((0)) FOR [tpActive]
	ALTER TABLE [hub_jrAccount] ADD  DEFAULT ('T') FOR [isSootv]
	ALTER TABLE [hub_jrAccount] ADD  DEFAULT ('F') FOR [isPayForBKL]
	ALTER TABLE [hub_jrAccount] ADD  DEFAULT ('C') FOR [tpInstr]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT (0) FOR [NotInStore]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT ('F') FOR [isPartnerMSP]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT ((0)) FOR [istrigger]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT ((0)) FOR [IsSimpleCheck]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT ((0)) FOR [isElectronForm]
	ALTER TABLE [hub_jrActive] ADD  DEFAULT ((1)) FOR [IsNoKreditBlockage]
	ALTER TABLE [hub_jrGKD] ADD  DEFAULT ('T') FOR [isSootv]
	ALTER TABLE [hub_jrGKD] ADD  DEFAULT ('F') FOR [isTreb]
	ALTER TABLE [hub_jrGKD] ADD  DEFAULT ('F') FOR [isPartner]
	ALTER TABLE [hub_jrKO] ADD  DEFAULT ((0)) FOR [IsSubject]
	ALTER TABLE [hub_jrKO] ADD  DEFAULT ((0)) FOR [isElectronFormKO]
	ALTER TABLE [hub_jrLetter] ADD  DEFAULT ('L') FOR [typeLetter]
	ALTER TABLE [hub_jrLetter] ADD  DEFAULT ('0000000000') FOR [Qdopinf]
	ALTER TABLE [hub_jrOrg] ADD  DEFAULT ('F') FOR [isSootv]
	ALTER TABLE [hub_jrOrg] ADD  DEFAULT ((0)) FOR [AnotherInf]
	ALTER TABLE [hub_jrOrg] ADD  DEFAULT ('FFFFF') FOR [tpDopInfo]
	ALTER TABLE [hub_jrPaymentGrafic] ADD  DEFAULT ('F') FOR [Advice]
	ALTER TABLE [hub_jrRKC] ADD  DEFAULT ('A') FOR [tpRKC]
	ALTER TABLE [hub_jrTransh] ADD  DEFAULT ('F') FOR [IsOriginal]
	ALTER TABLE [hub_rfAffPers] ADD  DEFAULT (0) FOR [IsCompanyOwner]
	ALTER TABLE [hub_rfBuhPeriod] ADD  DEFAULT ((0)) FOR [isBuhcheck]
	ALTER TABLE [hub_rfBuhRecord] ADD  DEFAULT ((1)) FOR [isrequired]
	ALTER TABLE [hub_rfBuhRecord] ADD  DEFAULT ((0)) FOR [IsactiveRecord]
	ALTER TABLE [hub_rfSignASPPA] ADD  DEFAULT ('000000000000000') FOR [Dopinf]
	ALTER TABLE [hub_rfSignRekvizitASPPA] ADD  DEFAULT ('A') FOR [tpGroup]
    ALTER TABLE [hub_hrStockPawn] ADD  DEFAULT ('F') FOR [isGuarant]
    ALTER TABLE [hub_hrStockPawn] ADD  DEFAULT ('F') FOR [isBKL]
--	COMMIT TRANSACTION
    --SELECT EventName = CAST('Создание временных таблиц' as varchar(100)),EventStatus=CAST(1 as bit),EventMSG=CAST('Успешно' as varchar(max)),EventDate=GETDATE()
	RETURN 1
END TRY
BEGIN CATCH
--  ROLLBACK TRANSACTION    
  --SELECT EventName = CAST('Создание временных таблиц' as varchar(100)),EventStatus=CAST(0 as bit),EventMSG=CAST(ERROR_MESSAGE() as varchar(max)),EventDate=GETDATE()
  SELECT @ErrorMSG = ERROR_MESSAGE() + '\n'
  RETURN 0
END CATCH
GO