﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CreateTempTable]
@Tablename VARCHAR(1000),
@idTable VARCHAR(1000),
@maxIdentity VARCHAR (1000)
AS 
BEGIN TRY   
    DECLARE @create  NVARCHAR(1000) 
    set @create=' IF (OBJECT_ID(N''tempdb..##TempTableForidHub'',N''U'') IS NOT NULL) '+
        +'DROP TABLE ##TempTableForidHub ; CREATE TABLE ##TempTableForidHub (idTu int, idHub int identity('+isnull(@maxIdentity,'1')+',1)); INSERT INTO ##TempTableForidHub (idTu) SELECT '+@idTable+' FROM '+@Tablename+';'
    EXEC sp_executesql @create
END TRY
BEGIN CATCH
END CATCH
GO