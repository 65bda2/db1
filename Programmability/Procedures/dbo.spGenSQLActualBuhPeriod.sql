﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spGenSQLActualBuhPeriod] (@dt datetime = NULL, @sql varchar(max) OUTPUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET @sql = ''
	DECLARE @wd DATETIME  
	SET @wd = ISNULL(@dt,(SELECT TOP 1 dtWork FROM rfstatus))
	DECLARE @idOrg int, 
			@idKO int
	DECLARE cr CURSOR 
	FOR SELECT DISTINCT idOrg, idKO
		FROM jrOrgBuhReport
		WHERE idPeriod in (SELECT idPeriod FROM dbo.fnGetActualBuhPeriod(DEFAULT, DEFAULT, @wd))
		OPEN cr
	FETCH NEXT FROM cr 
	INTO @idOrg, @idKO
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = @sql + 'SELECT '+LTRIM(Str(@idOrg))+' as idOrg, '+LTRIM(STR(@idKO))+' as idKO, a.* FROM dbo.fnGetActualBuhPeriod('+LTRIM(STR(@idOrg))+','+LTRIM(STR(@idKO))+', '''+CONVERT(varchar(10),@wd,104)+''')'+
		           'a WHERE YEARPERIOD IN (SELECT MAX (Year(REP_Date)) from rfBuhFactor) '+CHAR(13) --добавлено условие AHEBURG-2861 AFI
		SET @sql = @sql + 'UNION '+CHAR(13)
		FETCH NEXT FROM cr
		INTO @idOrg, @idKO
	END
	CLOSE cr
	DEALLOCATE cr
	IF LEN(@sql) = 0 
		SET @sql = 'SELECT 0 as idOrg, 0 as idKO, a.* FROM dbo.fnGetActualBuhPeriod(DEFAULT,DEFAULT, '''+CONVERT(varchar(10),@wd,104)+''') a  '
	ELSE
		SET @sql = SUBSTRING(@sql,1,LEN(@sql) - 7)
END
GO