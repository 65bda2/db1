﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE procedure [dbo].[spReport6_MBK]
-- 65fev 2019-11-24
-- Процедура формирования документа 6 для МСП
@idKo Int
as
begin try
  declare @Num Integer, @dWork DateTime = (select dtWork from rfStatus )

  select @Num = isnull(max(nuReport), 0) + 1
  from rfReports
  where tpGKD = 'D' And DATEPART(yy, dtReport) = DATEPART(yy, @dWork)

  select
    Info_Num      = @Num
    ,Info_Date     = @dWork
    ,NumActive     = j.NumActive
    ,RGN_Code      = (select KPTU from rfSubdivision where idSubdivision = k.idSubdivision)
    ,CO_Name       = k.nmKO
    ,CO_Cregn      = k.nuReg
    ,Asset_Id      = j.UnNum
    ,Incl_Date     = j.inclDate
    ,Asset_Type    = j.TypeActive
    ,Borr_Name     = Borr.nmKO
    ,Borr_CRegn    = case Borr.IsSubject when 0 then Borr.nuReg else Borr.OGRN end
    ,Org_Name      = ''
    ,Org_List      = '' 
    ,Asset_Value   = isnull((select sum(Payment) from jrPaymentGrafic g where NumActive = j.NumActive and Advice = 'F' and exists(select * from jrTransh where idTransh = g.idTransh and Status = 'T')), 0)
    ,Asset_Curr    = cur.nmISO
    ,Asset_Quality = j.AssetQuality
    ,Maturity_Date = g.DatePay
    ,Maturity_Sum  = isnull(g.Payment, 0)
    ,Maturity_Curr = cur.nmISO
    ,Ex_Rate       = isnull(cast(j.Course as varchar(20)), case when cur.nuISO = '810' then '1' else 'CBR' end)
    ,Block         = Case When IsNull( r.tpSect, '0' ) In ( '0', '1', '12L', '15' ) Then 0 Else 1 End
  from jrActive j
    join jrLetter l on l.idLetter = j.idLetter
    join jrAccount a on a.idKO = l.idKO and (a.dtExcl is null or a.dtExcl > @dWork)
    join jrKO k on l.idKo = k.idKO
    join jrGKD gk on a.nuGKD = gk.nuGKD and gk.idKO = k.idKO and gk.tpGKD = 'D' and isNull(gk.dtGKDend, @dWork) >= @dWork
    LEFT OUTER join hrActiveSect s on s.NumActive = j.NumActive 
    LEFT OUTER join rfActiveState r on r.idActiveState = s.idActiveState 
    join jrKO borr on borr.idKO = j.NmZaemshik
    left join jrCurrency cur on j.Currency = cur.nuISO
    left join jrPaymentGrafic g on j.NumActive = g.NumActive and g.Advice = 'F' and exists(select * from jrTransh where idTransh = g.idTransh and Status in ('T','F'))
  where j.TypeActive = 'M' and @idKo in (0, l.idKo)
  AND
  (
	(s.DateAssign = @dWork and r.tpSect in ('1', '12L', '15', '2', '21L', '22N', '3', '3N', '3L'))
	OR
	( j.status = 'T'  and  j.Begindate = @dWork )  
  )
  order by CO_Cregn, j.UnNum, Maturity_Date

end try
begin catch
  Declare @ErrMsg VarChar(255)
  Select @ErrMsg = ERROR_MESSAGE()
  Select @ErrMsg = 'Ошибка формирования документа 6. ' + @ErrMsg
  RaisError( @ErrMsg, 16, 1  )
end catch
GO