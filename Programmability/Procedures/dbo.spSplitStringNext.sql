﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<65baa2>
-- Create date: <15.10.2018>
-- Description:	<прцедура разделяет строку @text на две части, в качестве разделителя использует первый параметр @delim,  
-- может быть как один символ так и строка.  Результат возвращает в выходные параметры @tex1 и @text2 
-- если в результате деления text2 пустой то возвращает 1 иначе 0 в RETURN.>
-- =============================================
CREATE PROCEDURE [dbo].[spSplitStringNext](
	
	@delim VARCHAR(128), 
	@text VARCHAR(2048),
	@text1 VARCHAR(2048) OUTPUT,
	@text2 VARCHAR(2048) OUTPUT
)
AS 
BEGIN
	DECLARE @p int=0;
	set @text1=@text;
    set @text2='';
	SET @p=CHARINDEX(@delim,@text);
	IF @p>1
	BEGIN
	    SET @text1=LEFT(@text,@p-1)
		SET @text2=substring(@text,@p+1,LEN(@text));
	END
	ELSE
	IF @p=1
	BEGIN
		SET @text1='';
		SET @text2=substring(@text,2,LEN(@text));
	END
	RETURN case when len(@text2)>0 then 0 else 1 end 
END
GO