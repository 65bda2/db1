﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- =============================================
-- Author:		Макаров Д.А.
-- Create date: 29.11.2019
-- Description:	Фиксация (обновление/вставка) нового идентификатора актива jrActive.NumActive в таблице LastAssetNumber
-- =============================================
CREATE procedure [dbo].[spWriteLastAssetNumber]
@numActive int
AS
BEGIN
  declare @idKO int
  declare @KPTU [char](2)
  declare @AssetNumNA int
  declare @AssetNumMSP int
  declare @AssetNumEcsar int
  declare @AssetNumInvest int
  declare @nuReg varchar(50)
  declare @tActive int
  declare @AssetNum int
  select @KPTU=SUBSTRING(UnNum,1,2), @NuReg=SUBSTRING(UnNum,4,4), @tActive=substring(UnNum,3,1), @AssetNum=cast(SUBSTRING(UnNum,8,9) as int)    from jrActive where @numActive=numactive
  --select @idKO=jrLetter.idKo from jrActive join jrLetter on jrActive.idLetter=jrLetter.idLetter where jrActive.NumActive=@numActive 
  if exists(select 1 from LastAssetNumber where nuReg=@nuReg and KPTU=@KPTU) 
    update LastAssetNumber set
    AssetNumMSP = case @tActive when 3 then @AssetNum else AssetNumMSP end,
    AssetNumEcsar = case @tActive when 4 then @AssetNum else AssetNumEcsar end,
    AssetNumInvest = case @tActive when 5 then @AssetNum else AssetNumInvest end,
    AssetNumNA = case when @tActive not in (3,4,5) then @AssetNum else AssetNumNA end
   where nuReg=@nuReg and KPTU=@KPTU --на будущее 
  else
    INSERT INTO [LastAssetNumber]
           (
           [KPTU]
           ,nuReg
           ,[AssetNumNA]
           ,[AssetNumMSP]
           ,[AssetNumEcsar]
           ,[AssetNumInvest]
           )
    VALUES
    (
    @KPTU
    ,@nuReg
    ,case when @tActive not in (3,4,5) then @AssetNum else 0 end
    ,case @tActive when 3 then @AssetNum else 0 end
    ,case @tActive when 4 then @AssetNum else 0 end
    ,case @tActive when 5 then @AssetNum else 0 end
    )
  RETURN 1
END
GO