﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		<65psv1>
-- Create date: <01.07.2019>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[spIsDiffC101Org] (@idES1 int, @idES2 int) --@idES1 - исходное ,  @idES2 - найденное
AS
BEGIN
  DECLARE @result int
  DECLARE @count int
  
  SELECT @result=0
  
  CREATE TABLE #Temp1 (idField int, NewValue varchar(500)) 
  CREATE TABLE #Temp2 (idField int, NewValue varchar(500)) 
  
  INSERT INTO #Temp1 
  SELECT idField, NewValue FROM EDOKO_jrChangeORG WHERE idES = @idES1
  
  INSERT INTO #Temp2 
  SELECT idField, NewValue FROM EDOKO_jrChangeORG WHERE idES = @idES2
  
  select @count = COUNT(*) FROM (
											(SELECT * FROM #Temp1 except SELECT * FROM #Temp2) 
										 
										
										) a
				
  
  If @count>0
  select @result=1
  
  select @result
	
  DROP TABLE #Temp1	
  DROP TABLE #Temp2
	
END
GO