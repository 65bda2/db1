﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spReport6_EKD]
-- 65vev3 18/11/2019
-- Процедура формирования документа 6 для ЕКД
@idKo Int
AS 
BEGIN TRY 
  Declare @dWork DateTime, @Num Integer
  Select @dWork = dtWork From rfStatus 
  
  Select @Num = ISNULL( MAX( nuReport ), 0 ) + 1 From rfReports Where 
     tpGKD = 'K' And tpReport = 'G' And DATEPART( yy, dtReport ) = DATEPART( yy, @dWork )

  Select 
    Info_Num      = @Num, 
    Info_Date     = @dWork,
    NumActive     = j.NumActive,
    RGN_Code      = d.KPTU, 
    CO_Name       = k.nmKO,
    CO_Cregn      = k.nuReg,
    Asset_Id      = j.UnNum,
    incl_Date     = j.inclDate,
    Asset_Type    = j.TypeActive,
    Borr_Name     = o1.ShortNmORG,
    Borr_CRegn    = '',
    Org_Name      = ISNULL( o2.ShortNmORG, '' ) + ISNULL( ' (' + o2.EGRUL + ')', '' ), 
    Org_List      = Case When o2.IsSubject IN ( 1, 2, 3 ) Then 'РФ' 
                       When o2.IsSubject = 4 Then 'АКГ' 
                       Else Case o2.IsInList When 1 Then 'НП' Else 'ФА' End 
                    End,
    Asset_Value   = ( SELECT ISNULL( SUM( Payment ), 0 ) FROM jrPaymentGrafic WHERE 
                        jrPaymentGrafic.NumActive = j.NumActive AND 
                        jrPaymentGrafic.Advice = 'F' ),           
    Asset_Curr    = cur1.nmISO,
    Asset_Quality = j.AssetQuality,
    Maturity_Date = g.DatePay,
    Maturity_Sum  = g.Payment,
    Maturity_Curr = cur1.nmISO,
    Ex_Rate       = ISNULL( CAST( j.Course As VarChar(20) ), Case When cur1.nuISO = '810' Then '1' Else 'CBR' End ),
    Block         = Case When IsNull( r.tpSect, '0' ) In ( '0', '1', '12L', '15' ) Then 0 Else 1 End
  From 
    jrActive j Join jrLetter        l On l.idLetter = j.idLetter
               Join jrKO            k On l.idKo = k.idKO
          Left Join rfSubdivision   d On d.idSubdivision = k.idSubdivision     
          Left Join jrAccount       a On a.idKO = l.idKO And ( a.dtExcl Is Null OR a.dtExcl > @dWork )
          Left Join jrGKD          gk On a.nuGKD = gk.nuGKD And ( gk.dtGKDEnd Is Null OR gk.dtGKDEnd > @dWork ) And 
                                                                ( gk.dtGKDStop Is Null OR gk.dtGKDStop > @dWork )        
          Left Join jrOrg          o1 On j.NmZaemshik = o1.idOrg
          Left Join jrOrg          o2 On j.Company = o2.idOrg        
          Left Join jrPaymentGrafic g On j.NumActive = g.NumActive
          Left Join jrCurrency   cur1 On j.Currency = cur1.nuISO  
          Left outer Join hrActiveSect s on s.NumActive = j.NumActive
          Left outer Join rfActiveState r On r.idActiveState = s.idActiveState
  Where
    j.TypeActive = 'D' 
    And ( ( CASE gk.tpGKD WHEN 'K' THEN a.tpAsset ELSE gk.tpGKD END ) = 'B' ) 
    And (
		((s.DateAssign = @dWork)
		  And (r.tpSect In ( '1', '12L', '15', '2', '21L', '22N', '3', '3N', '3L' ))
		 ) 
		 Or (
		( j.status = 'T' ) and ( j.Begindate = @dWork ))
    )
    And g.Advice = 'F'
    And ( @idKo = 0 Or @idKo = l.idKo )
  Order By
    CO_Cregn, j.UnNum, Maturity_Date  
    
END TRY
BEGIN CATCH
  Declare @ErrMsg VarChar(255)
  Select @ErrMsg = ERROR_MESSAGE()
  Select @ErrMsg = 'Ошибка формирования документа 6. ' + @ErrMsg
  RaisError( @ErrMsg, 16, 1  )
END CATCH
GO