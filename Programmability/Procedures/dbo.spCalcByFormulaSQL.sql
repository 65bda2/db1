﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- ================================================
CREATE PROCEDURE [dbo].[spCalcByFormulaSQL]
	@formula varchar(2000), --текст формулы расчета из таблицы rfCalcCoefficient
	@calc decimal(38,19) OUTPUT -- возвращает вычисленное значение  
AS
BEGIN
	declare @sql nvarchar(max)='';
	declare @params nvarchar(100)='';
	
	set @sql=N'select @calc='+@formula;
	set @params=N'@calc decimal(38,19) out';
	exec sp_executesql @sql,@params,@calc=@calc out;
	
	return 0 
END
GO