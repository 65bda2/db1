﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[StartImportRF]
@TU varchar(2), 
@ErrorMSG varchar(max) output,
@log varchar(max) output AS
	-- 65bda2 перенесено из create DB
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_Keys]') AND type in (N'U'))
		DROP TABLE [dbo].[hub_Keys]

	CREATE TABLE [dbo].[hub_Keys](
		[idTUKey] [int] NULL,
		[idHubKey] [int] NULL,
		[NameTable] [varchar](25) NULL,
		[TU] [varchar](2) NULL,
		[isReplace] [bit] NULL
	) 
BEGIN TRY
  DECLARE @apostr varchar(50) = '    '   
  INSERT INTO [dbo].[rfSign]([tpSign],[nmSign])
  SELECT [tpSign],[nmSign] FROM hub_rfSign WHERE tpSign<>'StartSolution'
  SET @log = @log + @apostr + 'rfSign - загружен\n'

  INSERT INTO [rfSignRekvizitASPPA]([tpSign],[nmSign],[tpGroup])
  SELECT [tpSign],[nmSign],[tpGroup] FROM hub_rfSignRekvizitASPPA
  UPDATE rfSignRekvizitASPPA SET nmSign = 'urn:cbr-ru:edo:v1.0.9' WHERE tpSign = 'XML_CXX_ASPPA' -- 65bda2	
  UPDATE rfSignRekvizitASPPA SET nmSign = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:cbr-ru:edo:v1.0.9 main.xsd"' WHERE tpSign = 'CXX_Schemalocation' -- 65bda2	
  SET @log = @log + @apostr + 'rfSignRekvizitASPPA - загружен\n'
  
  INSERT INTO [rfSignASPPA]([tpSign],[nmSign],[nmPost],[Dopinf],[NumOrder],[tpGroup],[foundation])
  SELECT [tpSign],[nmSign],[nmPost],[Dopinf],[NumOrder],[tpGroup],[foundation] FROM hub_rfSignASPPA
  SET @log = @log + @apostr + 'rfSignASPPA - загружен\n'
  
  INSERT INTO [rfASPPARestriction]([DownKod],[UpKod],[dtBegin],[dtend])
  SELECT [DownKod],[UpKod],[dtBegin],[dtend] FROM hub_rfASPPARestriction
  SET @log = @log + @apostr + 'rfASPPARestriction - загружен\n'
  
  INSERT INTO [rfDays]([dtDay],[tpDay],[cmDay])
  SELECT [dtDay],[tpDay],[cmDay] FROM hub_rfDays
  SET @log = @log + @apostr + 'rfDays - загружен\n'

  INSERT INTO [jrCurrency]([nuISO],[nmISO],[nmCurr])
  SELECT [nuISO],[nmISO],[nmCurr] FROM hub_jrCurrency
  SET @log = @log + @apostr + 'jrCurrency - загружен\n'

  INSERT INTO [hrRateExch]([dtRate],[nuISO],[nuScale],[nuRate])
  SELECT [dtRate],[nuISO],[nuScale],[nuRate] FROM hub_hrRateExch
  SET @log = @log + @apostr + 'hrRateExch - загружен\n'
  
  INSERT INTO [rfOrgList]([nmOrg],[EGRUL],[Comment],[dtBegin],[dtEnd],[NmdtBeginOrg])
  SELECT [nmOrg],[EGRUL],[Comment],[dtBegin],[dtEnd],[NmdtBeginOrg] FROM hub_rfOrgList
  SET @log = @log + @apostr + 'rfOrgList - загружен\n'
  
  INSERT INTO [rfOKATO]([CODE],[KOD],[NAME_RUS])
  SELECT [CODE],[KOD],[NAME_RUS] FROM hub_rfOKATO
  SET @log = @log + @apostr + 'rfOKATO - загружен\n'

  INSERT INTO [rfOKFS]([CODE],[KOD],[NAME_RUS])
  SELECT [CODE],[KOD],[NAME_RUS] FROM hub_rfOKFS
  SET @log = @log + @apostr + 'rfOKFS - загружен\n'
  
  INSERT INTO [rfOKOPF]([CODE],[KOD],[NAME_RUS])
  SELECT [CODE],[KOD],[NAME_RUS] FROM hub_rfOKOPF
  SET @log = @log + @apostr + 'rfOKOPF - загружен\n'

  INSERT INTO [rfOKVED]([CODE],[KOD],[NAME_RUS],[N_RAZDEL],[CB_Date],[CE_Date],[ID],[CODEFORPPA])
  SELECT [CODE],[KOD],[NAME_RUS],[N_RAZDEL],[CB_Date],[CE_Date],[ID],[CODEFORPPA] FROM hub_rfOKVED
  SET @log = @log + @apostr + 'rfOKVED - загружен\n'
/*
  INSERT INTO [rfOKVED2_R]([ID],[CODE],[N_Razdel],[NAME_RUS],[CB_Date],[CE_Date])
  SELECT [ID],[CODE],[N_Razdel],[NAME_RUS],[CB_Date],[CE_Date] FROM hub_rfOKVED2_R
  SET @log = @log + @apostr + 'rfOKVED2_R - загружен\n'
*/  
  INSERT INTO [rfTpClassBuh]([NmClass],[TpClass],[idClass])
  SELECT [NmClass],[TpClass],[idClass] FROM hub_rfTpClassBuh
  SET @log = @log + @apostr + 'rfTpClassBuh - загружен\n'

  INSERT INTO [rfBuhPeriod]([idPeriod],[NmPeriod],[idclass],[NumPeriod],[YearPeriod],[dtShow],[isBuhcheck],[ReportDate])
  SELECT [idPeriod],[NmPeriod],[idclass],[NumPeriod],[YearPeriod],[dtShow],[isBuhcheck],[ReportDate] FROM hub_rfBuhPeriod
  SET @log = @log + @apostr + 'rfBuhPeriod - загружен\n'

  INSERT INTO [rfBuhRecord]([idClass],[NmRecord],[TpClass],[Kod],[NumKod],[isrequired],[KodFNS],[IsactiveRecord])
  SELECT [idClass],[NmRecord],[TpClass],[Kod],[NumKod],[isrequired],[KodFNS],[IsactiveRecord] FROM hub_rfBuhRecord
  SET @log = @log + @apostr + 'rfBuhRecord - загружен\n'

  INSERT INTO [rfBuhFactorType]([FSymbol],[FNumber])
  SELECT [FSymbol],[FNumber] FROM hub_rfBuhFactorType
  SET @log = @log + @apostr + 'rfBuhFactorType - загружен\n'

  IF NOT EXISTS(SELECT TOP 1 * FROM rfDKKP)
  BEGIN
	DELETE FROM [rfDKKP] -- т.к. уже есть записи, добавлены в Create_Siberia_HUB для логирования 
	INSERT INTO [rfDKKP]([CODE],[KP],[NAME_RUS])
	SELECT [CODE],[KP],[NAME_RUS] FROM hub_rfDKKP
	SET @log = @log + @apostr + 'rfDKKP - загружен\n'
  END
  ELSE
	SET @log = @log + @apostr + 'rfDKKP - уже загружен\n'
  
  INSERT INTO [rfReportsASPPA]([dtreport],[nuReport],[tpreport],[nuIshod],[idKO],[idORG],[Numactive],[nmUser],[komment])
  SELECT [dtreport],[nuReport],[tpreport],[nuIshod],[idKO],[idORG],[Numactive],[nmUser],[komment] FROM hub_rfReportsASPPA
  SET @log = @log + @apostr + 'rfReportsASPPA - загружен\n'
   
  INSERT INTO [rfBuhFactor]([OKVED],[REP_Date],[TpFactor],[PRIZ],[SRED],[DOPUST])
  SELECT [OKVED],[REP_Date],[TpFactor],[PRIZ],[SRED],[DOPUST] FROM hub_rfBuhFactor
  SET @log = @log + @apostr + 'rfBuhFactor - загружен\n'

  INSERT INTO [rfFactor]([dtFactor],[tpFactor],[pcFactor],[idOrg],[dtEnd])
  SELECT [dtFactor],[tpFactor],[pcFactor],[idOrg],[dtEnd] FROM hub_rfFactor WHERE idOrg is NULL
  SET @log = @log + @apostr + 'rfFactor - загружен\n'
  --AHEBURG-1611-65SuvorkovMP>>>
  UPDATE [rfBuhRecord]
  SET NmRecord='Финансовые вложения'
  where numkod='1170'

  UPDATE [rfBuhRecord]
  SET NmRecord='Итого по разделу I "ВНЕОБОРОТНЫЕ АКТИВЫ"'
  where numkod='1100'
 
  
  UPDATE [rfBuhRecord]
  SET NmRecord='Дебиторская задолженность'
  where numkod='1230'
  
  UPDATE [rfBuhRecord]
  SET Kod='12301',
      NmRecord='В том числе краткосрочная дебиторская задолженность' 
  where numkod='12301'
  
  UPDATE [rfBuhRecord]
  SET NmRecord='Итого по разделу II "ОБОРОТНЫЕ АКТИВЫ"'
  where numkod='1200'

  UPDATE [rfBuhRecord]
  SET NmRecord='Уставный капитал'
  where numkod='1310'
  
  UPDATE [rfBuhRecord]
  SET NmRecord='Добавочный капитал (без переоценки)'
  where numkod='1350'

  UPDATE [rfBuhRecord]
  SET NmRecord='Итого по разделу III "КАПИТАЛ И РЕЗЕРВЫ"'
  where numkod='1300'

  UPDATE [rfBuhRecord]
  SET NmRecord='Итого по разделу IV "ДОЛГОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"'
  where numkod='1400'

  UPDATE [rfBuhRecord]
  SET NmRecord='Итого по разделу V "КРАТКОСРОЧНЫЕ ОБЯЗАТЕЛЬСТВА"'
  where numkod='1500'

  UPDATE [rfBuhRecord]
  SET NmRecord='Просроченная задолженность по кредитам, займам'
  where numkod='650'

  UPDATE [rfBuhRecord]
  SET NmRecord='Просроченная задолженность по выданным векселям'
  where numkod='651'
											 
  UPDATE [rfBuhRecord]
  SET NmRecord='Данные о величине чистых активов'
  where numkod='3000'
  
  UPDATE [rfBuhRecord]
  SET NmRecord='Просроченная кредиторская задолженность длительностью свыше 3 месяцев'
  where numkod='652'
  
  --<<<AHEBURG-1611

  SET @log = @log + '    Справочники загружены'
	
  UPDATE rfStatus SET dtWork = (select dtWork from hub_rfStatus)
  UPDATE rfSignRekvizitASPPA SET nmSign = (SELECT Convert(varchar(10),dtWork,104) FROM rfStatus) WHERE tpSign='LastVerificationDate' -- 65bda2
  UPDATE rfSubdivision SET isload = 0 where KPTU = @TU
  RETURN 1
END TRY
BEGIN CATCH
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
  INSERT INTO	 rfSign (tpSign,nmSign) 
  VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution'    

  SELECT @ErrorMSG = @log + '\n\' + ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
GO