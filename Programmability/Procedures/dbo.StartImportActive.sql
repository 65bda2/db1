﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartImportActive] 
@IsHUB BIT,
@TU varchar(2),
@ErrorMSG varchar(max) output,
@logStatus varchar(21) output AS -- каждый символ означает успешно или нет загружена каждая таблица. может имет значение (Y -успешно, N - ошибка). порядок символов для таблиц: 1-jrLetter,2-jrActive,3-hrActive,4-jrTransh,5-hrActiveEvent, 6-hrActiveSect,7-hrFactor,8-jrPaymentGrafic,9-hrPaymentGrafic,10-hrWaitCheck,11-jrDopLetter,12-rfDopLetActive,13-jrJournalActive,14-jrJournalLetter,15-jrRequestDocActive,16-lgArchActive,17-jrNoteActive,18-rfReportsASPPA,19-DKO_hrAsset,20-DKO_hrAssetState,21-DKO_hrRepayment 
BEGIN TRY
  SET @logStatus = ''
  IF @IsHUB = 1
  BEGIN   
    DECLARE @maxid VARCHAR(1000)
    --jrletter HUB  1
    EXEC CreateTempTable 'hub_jrLetter','idLetter','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrLetter',@TU,0 FROM ##TempTableForidHub 

    EXEC CreateTempTable 'hub_jrActive','NumActive','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrActive',@TU,0 FROM ##TempTableForidHub     

    EXEC CreateTempTable 'hub_jrTransh','idTransh','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrTransh',@TU,0 FROM ##TempTableForidHub     

    EXEC CreateTempTable 'hub_hrActiveEvent','idEvent','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'hrActiveEvent',@TU,0 FROM ##TempTableForidHub 

    EXEC CreateTempTable 'hub_jrPaymentGrafic','idRecord','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrPaymentGrafic',@TU,0 FROM ##TempTableForidHub  

    EXEC CreateTempTable 'hub_jrDopLetter','idDopLetter','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrDopLetter',@TU,0 FROM ##TempTableForidHub   

    --EXEC CreateTempTable 'hub_rfDopLetActive','idDopLetter','1'  
    --INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    --SELECT  [idTu],[idHub],'rfDopLetActive',@TU,0 FROM ##TempTableForidHub   

    EXEC CreateTempTable 'hub_jrJournalActive','idJournal','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrJournalActive',@TU,0 FROM ##TempTableForidHub   

    EXEC CreateTempTable 'hub_jrJournalLetter','idJournal','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrJournalLetter',@TU,0 FROM ##TempTableForidHub   

    EXEC CreateTempTable 'hub_lgArchActive','idArhActive','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'lgArchActive',@TU,0 FROM ##TempTableForidHub   

  --  EXEC CreateTempTable 'hub_rfReportsASPPA','idReport','1'  
  --  INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
  --  SELECT  [idTu],[idHub],'rfReportsASPPA',@TU,0 FROM ##TempTableForidHub  

    EXEC CreateTempTable 'hub_DKO_hrAsset','idRecord','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrAsset',@TU,0 FROM ##TempTableForidHub     

    EXEC CreateTempTable 'hub_DKO_hrAssetState','idRecord','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrAssetState',@TU,0 FROM ##TempTableForidHub   

    EXEC CreateTempTable 'hub_DKO_hrRepayment','idRecord','1'  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrRepayment',@TU,0 FROM ##TempTableForidHub        
  END
  ELSE
  BEGIN
    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrLetter') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrletter','idletter',@maxid
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrletter',@TU,0 FROM ##TempTableForidHub 

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrActive') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrActive','NumActive',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrActive',@TU,0 FROM ##TempTableForidHub  

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrTransh') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrTransh','idTransh',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrTransh',@TU,0 FROM ##TempTableForidHub   

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='hrActiveEvent') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_hrActiveEvent','idEvent',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'hrActiveEvent',@TU,0 FROM ##TempTableForidHub 

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrPaymentGrafic') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrPaymentGrafic','idRecord',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrPaymentGrafic',@TU,0 FROM ##TempTableForidHub  

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrDopLetter') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrDopLetter','idDopLetter',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrDopLetter',@TU,0 FROM ##TempTableForidHub   

   -- SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrDopLetter') as VARCHAR(1000))
  --  EXEC CreateTempTable 'hub_rfDopLetActive','idDopLetter',@maxid  
  --  INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
  --  SELECT  [idTu],[idHub],'rfDopLetActive',@TU,0 FROM ##TempTableForidHub   

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrJournalActive') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrJournalActive','idJournal',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrJournalActive',@TU,0 FROM ##TempTableForidHub   

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='jrJournalLetter') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_jrJournalLetter','idJournal',@maxid 
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'jrJournalLetter',@TU,0 FROM ##TempTableForidHub   

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='lgArchActive') as VARCHAR(1000)) 
    EXEC CreateTempTable 'hub_lgArchActive','idArhActive',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'lgArchActive',@TU,0 FROM ##TempTableForidHub   

   -- SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='rfReportsASPPA') as VARCHAR(1000))
   -- EXEC CreateTempTable 'hub_rfReportsASPPA','idReport',@maxid  
   -- INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
   -- SELECT  [idTu],[idHub],'rfReportsASPPA',@TU,0 FROM ##TempTableForidHub  

    --SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='DKO_hrAsset') as VARCHAR(1000)) 
    SELECT @maxid=CAST((SELECT MAX([IdRecord])+1 FROM DKO_hrAsset) as VARCHAR(1000))  
    EXEC CreateTempTable 'hub_DKO_hrAsset','idRecord',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrAsset',@TU,0 FROM ##TempTableForidHub     

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='DKO_hrAssetState') as VARCHAR(1000))   
    EXEC CreateTempTable 'hub_DKO_hrAssetState','idRecord',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrAssetState',@TU,0 FROM ##TempTableForidHub   

    SELECT @maxid=CAST((SELECT MAX([idHubKey])+1 FROM [hub_Keys] WHERE [NameTable]='DKO_hrRepayment') as VARCHAR(1000))
    EXEC CreateTempTable 'hub_DKO_hrRepayment','idRecord',@maxid  
    INSERT INTO [hub_Keys]([idTuKey],[idHubKey],[NameTable],[TU],[isReplace])
    SELECT  [idTu],[idHub],'DKO_hrRepayment',@TU,0 FROM ##TempTableForidHub 
  END;

  SET IDENTITY_INSERT [dbo].[jrLetter] ON 
  INSERT INTO [dbo].[jrLetter]([idLetter],[NumSADD],[DateLetter],[DateLetterTU],[idKo],[Kolvo],[Status],[P6],[TrueKolvo],[VestnikBR],[GrLetter],[Komment],[typeLetter],[Q1]  
			   ,[Q2],[Q3],[Q4],[NumLetter],[WarrantDate],[LBeginDate],[NumLetKo],[DtLetKo],[NumLetStore],[dtLetstore],[NumLetIncl],[DtLetIncl],[DtIncl],[idStore],[dtRemoval]
			   ,[countTransh],[Qdopinf])
  SELECT      keys.idHubKey,[NumSADD],[DateLetter],[DateLetterTU],ko.idhubKey,[Kolvo],[Status],[P6],[TrueKolvo],[VestnikBR],[GrLetter],[Komment],[typeLetter],[Q1]
			   ,[Q2],[Q3],[Q4],[NumLetter],[WarrantDate],[LBeginDate],[NumLetKo],[DtLetKo],[NumLetStore],[dtLetstore],[NumLetIncl],[DtLetIncl],[DtIncl],[idStore],[dtRemoval]
			   ,[countTransh],[Qdopinf] 
			   FROM [dbo].[hub_jrLetter] INNER JOIN 
			   (select idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrLetter') keys ON keys.[idTuKey]=[dbo].[hub_jrLetter].[idletter] INNER JOIN 
			   (SELECT idTUKey,idHubKey FROM hub_Keys where TU=@TU and NameTable='jrKO') KO on  hub_jrLetter.idKo = ko.idTUKey                   
  SET IDENTITY_INSERT [dbo].[jrLetter] OFF;
  SET @logStatus = @logStatus + 'Y' -- 1
   
  SET IDENTITY_INSERT [dbo].[jrActive] ON
    INSERT INTO [dbo].[jrActive]([TypeActive],[NumActive],[NmZaemshik],[Nominal],[DatePayment],[Cost],[Company],[DopInf],[idLetter],[UnNum],[status],[Currency],[Begindate],[Decidedate],[getdate] 
							  ,[dateTUletter],[Note],[NomerVD],[TypeVexel],[CreationDate],[AuditDate],[Course],[IsActNumber],[CostCurrency], [CostRub],[inclDate],[AssetIssue],[TempCost],[TempCostRub] ,[firstCost],
							   [firstCurrency],[MarkBullion],[LigatureWeight], [ChemicalWeight],[fineness],[tpStatus], [Metaltype],[NotInStore],[AssetQuality],
							   [InsurancePercent],[IndosamentDate],[FirstCostRub],[IsBaseRefusal],[isAKG],[IsOriginal], [isPartnerMSP],[dtRequestDocum],[dtRequestOriginal], [isHundredBillion],[istrigger],[IsSimpleCheck],
							   [isElectronForm],[CreditAcc],[SecuringReq],[IsNoKreditBlockage],[idSubdivisionSave],[MaturityDate]) 
   
   SELECT                       [TypeActive],keys1.[idHubKey],CASE jr.TypeActive  WHEN 'M'  THEN keys6.[idHubKey] ELSE keys3.[idHubKey] END, 
                               [Nominal],[DatePayment],[Cost], CASE jr.TypeActive  WHEN 'M'  THEN keys5.[idHubKey] ELSE keys2.[idHubKey] END Company,
                               [DopInf]=Substring([DopInf],1,25)+'00',keys4.[idHubKey], [UnNum],[status],[Currency],
							   [Begindate],[Decidedate],[getdate],[dateTUletter],[Note],[NomerVD], [TypeVexel],[CreationDate],[AuditDate], [Course],[IsActNumber],[CostCurrency],
							   [CostRub],[inclDate],[AssetIssue],  [TempCost],[TempCostRub],[firstCost], [firstCurrency],[MarkBullion],[LigatureWeight],[ChemicalWeight],[fineness],[tpStatus],
							   [Metaltype],[NotInStore],[AssetQuality],[InsurancePercent],[IndosamentDate],[FirstCostRub],[IsBaseRefusal],[isAKG],[IsOriginal], [isPartnerMSP],[dtRequestDocum], [dtRequestOriginal],
							   [isHundredBillion],[istrigger],[IsSimpleCheck],[isElectronForm],[CreditAcc],[SecuringReq],[IsNoKreditBlockage],
							   (SELECT CASE jr.isOriginal WHEN 1 THEN (select idSubdivision from rfSubdivision where KPTU=SUBSTRING(jr.[unnum],1,2)) ELSE null END) AS [idSubdivisionSave],jrP.[MaturityDate] 
  FROM                         [dbo].[hub_jrActive] jr INNER JOIN 
							   (SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrActive') keys1 ON keys1.[idTuKey]=jr.Numactive left JOIN 
							   (SELECT idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrORG') keys2 ON jr.Company=keys2.idTUKey LEFT JOIN 
							   (SELECT idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrOrg') keys3 ON jr.NmZaemshik=keys3.idTUKey LEFT JOIN 
							   (SELECT idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrLetter') keys4 ON jr.idLetter=keys4.idTUKey LEFT JOIN 
							   (SELECT idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='JRko') keys5 ON jr.Company=keys5.idTUKey LEFT JOIN 
							   (SELECT idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='JRko') keys6 ON jr.NmZaemshik=keys6.idTUKey LEFT JOIN 
							   (SELECT MAX(DATEPAY) MaturityDate,NumActive  FROM [dbo].[hub_jrPaymentGrafic] GROUP BY NumActive ) jrP ON jrP.NumActive= jr.[NumActive]			
  SET IDENTITY_INSERT [dbo].[jrActive] OFF;
  SET @logStatus = @logStatus + 'Y' -- 2
  
  INSERT INTO [hrActive]([OperationDate],[HrStatus],[DFROM],[TypeActive],[NumActive],[NmZaemshik],[Nominal],[DatePayment],[Cost],[firstCost],[TempCost],[Company],[DopInf],[idLetter],[UnNum],
						   [status],[Currency],[firstCurrency],[CostCurrency],[CostRub],[TempCostRub],[inclDate],[Begindate],[Decidedate],[getdate],[dateTUletter],[Note] ,[NomerVD],[AssetIssue],[TypeVexel],
						   [CreationDate],[AuditDate],[Course],[IsActNumber],[isHundredBillion],[AssetQuality],[IndosamentDate],[FirstCostRub],[IsOriginal],[idGrouphrPG],[Nucred],[OkvedOrgToCheck],[NmZaem],[ZaemOKOPF],
						   [businessQuality],[NmOrgToCheck],[OGRNOrgtocheck],[NMCheck],[idSubdivisionSave],[MaturityDate])
  SELECT                   [OperationDate],[HrStatus],[DFROM],[TypeActive],keys5.[idHubKey],CASE hr.TypeActive  WHEN 'M'  THEN keys7.[idHubKey] ELSE keys3.[idHubKey] END,
                           [Nominal],[DatePayment],[Cost],[firstCost],[TempCost],CASE hr.TypeActive  WHEN 'M'  THEN keys6.[idHubKey] ELSE keys2.[idHubKey] END,
                           [DopInf]=Substring([DopInf],1,25)+'00',
						   keys4.[idHubKey],[UnNum],[status],[Currency],[firstCurrency],[CostCurrency],[CostRub],[TempCostRub] ,[inclDate],[Begindate],[Decidedate], [getdate],[dateTUletter],[Note], [NomerVD],[AssetIssue],
						   [TypeVexel],[CreationDate],[AuditDate],[Course],[IsActNumber] ,[isHundredBillion],[AssetQuality],[IndosamentDate],[FirstCostRub],[IsOriginal],[idGrouphrPG],[Nucred],[OkvedOrgToCheck],
						   [NmZaem],[ZaemOKOPF],[businessQuality], [NmOrgToCheck],[OGRNOrgtocheck],[NMCheck],(select case hr.isOriginal when 1 then (select idSubdivision from rfSubdivision where KPTU=SUBSTRING(HR.[unnum],1,2)) else null end) as [idSubdivisionSave],jrP.[MaturityDate] 
   FROM                    [dbo].[hub_hrActive] hr INNER JOIN 
						   hub_keys keys2 ON hr.Company=keys2.idTUKey AND keys2.TU=@TU AND keys2.NameTable='jrORG' LEFT JOIN 
						   hub_keys keys3 ON hr.NmZaemshik=keys3.idTUKey AND  keys3.TU=@TU AND keys3.NameTable='jrOrg' LEFT JOIN 
						   hub_keys keys4 ON hr.idLetter=keys4.idTUKey AND  keys4.TU=@TU and keys4.NameTable='jrLetter' LEFT JOIN 
						   hub_keys keys5 ON hr.numactive=keys5.idTUKey AND  keys5.TU=@TU and keys5.NameTable='jrActive' LEFT JOIN
						   hub_keys keys6 ON hr.Company=keys6.idTUKey AND keys6.TU=@TU and keys6.NameTable='JRko'	LEFT JOIN
						   hub_keys keys7 ON hr.NmZaemshik=keys7.idTUKey AND keys7.TU=@TU and keys7.NameTable='JRko' LEFT JOIN
						  (SELECT MAX(DATEPAY) MaturityDate,NumActive  FROM [dbo].[hub_jrPaymentGrafic] GROUP BY NumActive ) jrP ON jrP.NumActive= hr.[NumActive]					  
--AHEBURG-2096  
  INSERT INTO LastAssetNumber (nuReg,kptu,AssetNumNA,AssetNumMSP,AssetNumEcsar,AssetNumInvest)
  SELECT NuReg,kptu,AssetNumNA,AssetNumMSP,AssetNumEcsar,AssetNumInvest FROM hub_LastAssetNumber

/*  Select idTUKey,kptu, MAX(AssetNumNA) AssetNumNA ,max(AssetNumMSP)AssetNumMSP,max(AssetNumEcsar)AssetNumEcsar,
  max(AssetNumInvest)AssetNumInvest from   hub_LastAssetNumber 
  INNER JOIN hub_Keys hk on hk.idTUKey=hub_LastAssetNumber.idKO
  where hk.NameTable ='jrKO' and hk.TU=@TU
  group by  idTUKey,kptu
  */
--  SELECT hk.idHubKey,kptu,AssetNumNA,AssetNumMSP,AssetNumEcsar,AssetNumInvest FROM hub_LastAssetNumber inner join hub_Keys hk on hk.idTUKey=hub_LastAssetNumber.idKO
--  where hk.NameTable ='jrKO' and hk.TU=@TU


	

  SET @logStatus = @logStatus + 'Y' -- 3

SET IDENTITY_INSERT [dbo].[jrTransh] ON
  INSERT INTO [dbo].[jrTransh]([IdTransh],[Cost],[NomerVD],[CreationDate],[NumActive],[PaymentDate],[Status],[Dopinf],[IdLetter],[BeginDate],[inclDate],[IsOriginal],[AlterComment])
  SELECT      keys.[idHubKey],[Cost],[NomerVD],[CreationDate],jra.idHubKey,[PaymentDate],[Status],[Dopinf],jrl.[idHubKey], [BeginDate],[inclDate],[IsOriginal],[AlterComment] 
  FROM        [dbo].[hub_jrTransh] INNER JOIN 
				(select idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrTransh') keys ON keys.idTUKey=IdTransh INNER JOIN 
				(select idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrActive') jra ON jra.idTUKey=NumActive INNER JOIN
				(select idTUKey,idHubKey from hub_keys  WHERE TU=@TU and NameTable='jrletter') jrl ON jrl.idTUKey=IdLetter
  SET IDENTITY_INSERT [dbo].[jrTransh] OFF;
  SET @logStatus = @logStatus + 'Y' -- 4

  SET IDENTITY_INSERT [dbo].[hrActiveEvent] ON
  INSERT INTO         [dbo].[hrActiveEvent]([idEvent],[tpEvent],[dtEvent],[NumActive],[StatusEvent],[StatusEventEDOKO])
  SELECT              keys.[idHubKey],[tpEvent],[dtEvent],jrA.[idHubKey],[StatusEvent],[StatusEventEDOKO] 
  FROM                [dbo].[hub_hrActiveEvent] INNER JOIN 
						(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='hrActiveEvent') keys ON keys.[idTuKey]=idEvent INNER JOIN 
						(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =[NumActive]
  SET IDENTITY_INSERT [dbo].[hrActiveEvent] OFF;
  SET @logStatus = @logStatus + 'Y' -- 5

  --AHEBURG-2096>>
  INSERT INTO dbo.[hrActiveSect]([DateAssign],[NumActive],[idAcc],[idActiveState])
  SELECT                           hrs.[dtSect],jrA.[idHubKey],jrC.[idHubKey],(SELECT CASE rfA.idActiveState  WHEN 10 THEN 1 ELSE rfA.idActiveState  END) 
  FROM                           hub_hrStockSect hrS INNER JOIN  
								 hub_Keys jrA on jrA.idTUKey=hrs.idStock  AND jrA.TU=@TU AND jrA.NameTable='jrActive' INNER JOIN 
								 hub_jrBlock jrB ON jrB.idBlock=hrS.idBlock INNER JOIN  
								 rfActiveState rfA ON rfA.tpSect=hrS.tpSect INNER JOIN
								 hub_keys jrC ON jrC.idTUKey=jrB.idAcc  AND jrC.TU=@TU AND jrC.NameTable='jrAccount'
  WHERE idRecord IN (SELECT max(IdRecord) AS IdRecord  FROM  hub_hrStockSect  GROUP BY dtSect,idStock); 
  
  INSERT INTO dbo.[hrActiveSect]([DateAssign],[NumActive],[idAcc],[idActiveState])
  SELECT CASE WHEN (select dtWork FROM rfStatus) > a.inclDate THEN (select dtWork FROM rfStatus) ELSE a.inclDate END, NumActive,ac.idAcc,1
  FROM jrActive a INNER JOIN
	   jrLetter l ON a.idLetter = l.idLetter INNER JOIN
       jrKO k ON l.idKo = k.idKO INNER JOIN
	   jrAccount ac ON k.idKO = ac.idKO INNER JOIN
	   jrGKD g ON ac.nuGKD = g.nuGKD
  WHERE a.[status] = 'T'
  AND NumActive not in (select NumActive FROM hrActiveSect)
  AND ((g.tpGKD = 'K' and a.TypeActive='D') 
   OR  (g.tpGKD = 'J' and a.TypeActive='M')
   OR  (g.tpGKD = 'G' and a.TypeActive='I')
   OR  (g.tpGKD = 'F' and a.TypeActive='E'))

/*
  INSERT INTO dbo.[hrActiveSect]([DateAssign],[NumActive],[idAcc],[idActiveState])

  SELECT     jrA.inclDate,JRA.NumActive,jAc.idAcc,1
  FROM         jrActive AS JRA INNER JOIN
                      hub_Keys AS hk_jrA ON JRA.NumActive = hk_jrA.idHubKey AND hk_jrA.TU = @TU AND hk_jrA.NameTable = 'jrActive' INNER JOIN
                      jrLetter ON JRA.idLetter = jrLetter.idLetter INNER JOIN
                      jrKO ON jrLetter.idKo = jrKO.idKO INNER JOIN
                      jrAccount AS jAc ON jrKO.idKO = jAc.idKO INNER JOIN
                      jrGKD ON jAc.nuGKD = jrGKD.nuGKD
  WHERE     (jAc.dtExcl IS NULL OR
                      jAc.dtExcl >
                          (SELECT     TOP (1) dtWork
                            FROM          rfStatus)) AND (jrGKD.tpGKD = 'K' OR
                      jrGKD.tpGKD = 'J' OR
                      jrGKD.tpGKD = 'G' OR
                      jrGKD.tpGKD = 'F') AND (JRA.Begindate =
                          (SELECT     TOP (1) dtWork
                            FROM          rfStatus)) AND (hk_jrA.idTUKey NOT IN
                          (SELECT     idStock
                            FROM          hub_hrStockSect)) AND (JRA.status = 'T')

  --<<<AHEBURG-2096*/

  SET @logStatus = @logStatus + 'Y' -- 6
  
  INSERT INTO [dbo].[hrFactor]([dtBegin],[tpAsset],[pcFactor],[idAsset])
  SELECT      [dtBegin],[tpAsset], MAX([pcFactor]) as pcFactor ,jrf.[idHubKey] FROM [dbo].[hub_hrFactor] hr INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys where TU=@TU and NameTable='jrActive') jrf on  jrf.idTUKey= hr.idAsset
  WHERE       hr.tpAsset='B' 
   GROUP BY   jrf.[idHubKey],[tpAsset], [dtBegin];   
  SET @logStatus = @logStatus + 'Y' -- 7

  INSERT INTO [dbo].[jrPaymentGrafic]([NumActive],[Payment],[DatePay],[Advice],[IdTransh],[TpActive],[AlterComment])
  SELECT      jrA.[idHubKey],[Payment],[DatePay],[Advice],jrT.[idHubKey],[TpActive],[AlterComment]
  FROM        [dbo].[hub_jrPaymentGrafic] jrP LEFT JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrTransh') jrT ON jrT.[idTuKey]=idTransh LEFT JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =jrP.NumActive ;
  SET @logStatus = @logStatus + 'Y' -- 8

  INSERT INTO [dbo].[hrPaymentGrafic]([DFROM],[idGroup],[NumActive],[TpActive],[DatePay],[Payment],[Advice],[IdTransh],[AlterComment])
  SELECT      [DFROM],[idGroup],jrA.[idHubKey],[TpActive],[DatePay],[Payment],[Advice],[IdTransh],[AlterComment] 
  FROM        [dbo].[hub_hrPaymentGrafic] LEFT JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrTransh') jrT ON jrT.[idTuKey]=idTransh LEFT JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive
  SET @logStatus = @logStatus + 'Y' -- 9
  
  INSERT INTO [dbo].[hrWaitCheck]([NumActive],[dtBegin],[dtEnd],jrT.[idTransh])
  SELECT      jrA.idHubKey,[dtBegin],[dtEnd],jrT.[idHubKey] 
  FROM        [dbo].[hub_hrWaitCheck] INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrTransh') jrT ON jrT.[idTuKey]=idTransh INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive;
  SET @logStatus = @logStatus + 'Y' -- 10

  SET IDENTITY_INSERT dbo. [jrDopLetter] ON
  INSERT INTO dbo. [jrDopLetter]([idDopLetter],[Note],[NumLetter],[DateLetter],[idKO],[tpLetter])
  SELECT      jrD.[idHubKey],[Note],jrko.[idHubKey],[DateLetter],jrKO.[idHubKey],[tpLetter] 
  FROM        [dbo].[hub_jrDopLetter] INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrDopLetter') jrD ON jrD.[idTuKey]=idDopLetter INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrKO') jrko ON   jrKO.idTUKey =idko
  SET IDENTITY_INSERT dbo. [jrDopLetter] OFF;
  SET @logStatus = @logStatus + 'Y' -- 11

  INSERT INTO [dbo].[rfDopLetActive]([NumActive],[idDopLetter])
  SELECT      jrA.[idHubKey],jrDl.[idHubKey] 
  FROM        [dbo].[hub_rfDopLetActive] INNER JOIN 
				--(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='rfDopLetActive') jrD ON jrD.[idTuKey]=idDopLetter INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrDopletter') jrDl ON   jrDl.idTUKey=idDopLetter INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive;
  SET @logStatus = @logStatus + 'Y' -- 12
  
  SET IDENTITY_INSERT dbo.[jrJournalActive] ON
  INSERT INTO [dbo].[jrJournalActive]([DateIt],[SysDate],[Note],[NumActive],[Kategory],[idJournal],[NumLetter],[Doer])
  SELECT      [DateIt],[SysDate],[Note],jrA.[idHubKey],[Kategory],jrJ.[idHubKey],[NumLetter],[Doer] 
  FROM        [dbo].[hub_jrJournalActive] INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrJournalActive') jrJ ON jrJ.[idTuKey]=idJournal INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive
  SET IDENTITY_INSERT dbo. [jrJournalActive] OFF;
  SET @logStatus = @logStatus + 'Y' -- 13
  
   SET IDENTITY_INSERT dbo. [JrJournalLetter] ON;
  INSERT INTO [dbo].[JrJournalLetter] ([dateIt],[SysDate],[Note],[idletter],[Kategory],[idJournal],[NumLetter])
  SELECT    [dateIt],[SysDate],[Note],jrL.[idHubKey],[Kategory],jrJJ.[idHubKey],[NumLetter]
  FROM      [hub_JrJournalLetter] INNER JOIN 
			(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrLetter') jrL ON jrL.[idTuKey]=idLetter INNER JOIN 
		    (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='JrJournalLetter') jrJJ  ON jrJJ.idTUKey=idJournal  ;
  SET IDENTITY_INSERT dbo. [JrJournalLetter] OFF;		    
  SET @logStatus = @logStatus + 'Y' -- 14

  INSERT INTO [dbo].[jrRequestDocActive]([dtBegin],[RequestStatus],[dtControl],[TpRequest],[dtEnd],[Numactive],[NumRequest],[Comment])
  SELECT      [dtBegin],[RequestStatus],[dtControl],[TpRequest],[dtEnd],jrA.[idHubKey],[NumRequest],[Comment]
  FROM        [dbo].[hub_jrRequestDocActive] INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive;
  SET @logStatus = @logStatus + 'Y' -- 15

  SET IDENTITY_INSERT dbo.[lgArchActive] ON
  INSERT INTO [dbo].[lgArchActive]([idArhActive],[Dateit],[RekvOrg],[CheckP36],[CheckP35],[Cost],[Kategory],[Kurs],[Koef],[costrub],
				[status],[DSign1],[DSign2],[FIo1],[FIO2],[NumActive],[Numorder],[Komment],[happen],[NmCurrency])
  SELECT      lg.[idHubKey],[Dateit],[RekvOrg],[CheckP36],[CheckP35],[Cost],[Kategory],[Kurs],[Koef],[costrub]
				,[status],[DSign1],[DSign2],[FIo1],[FIO2],jrA.[idHubKey],[Numorder],[Komment],[happen],[NmCurrency] 
  FROM        [dbo].[hub_lgArchActive] INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive INNER JOIN
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='lgArchActive') lg ON   lg.idTUKey =idArhActive
  SET IDENTITY_INSERT dbo.[lgArchActive] OFF;
  SET @logStatus = @logStatus + 'Y' -- 16
  
  INSERT INTO [dbo].[jrNoteActive]([Tag],[DateNote],[Note],[NumActive],[idTransh])
  SELECT      [Tag],[DateNote],[Note],jrA.[idHubKey],idTransh
  FROM        [dbo].[hub_jrNoteActive] LEFT JOIN -- 65bda2
				(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrTransh') jrT ON jrT.[idTuKey]=idTransh INNER JOIN 
				(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =NumActive;
  SET @logStatus = @logStatus + 'Y' -- 17
				
  INSERT INTO [dbo].[rfReportsASPPA]([dtreport],[nuReport],[tpreport],[nuIshod],[idKO],[idORG],[Numactive],[nmUser],[komment])
  SELECT      [dtreport],[nuReport],[tpreport],[nuIshod],idKO, CASE jrA2.TypeActive  WHEN 'M'  THEN jra2.Company ELSE rfa.idORG END idorg,jrA.[idHubKey],[nmUser],[komment] 
  FROM        [dbo].[hub_rfReportsASPPA] rfa INNER JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrOrg') Org ON   ORG.idTUKey =rfa.idOrg INNER JOIN 
			  (SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrKO') KO ON KO.[idTuKey]=rfa.idKO INNER JOIN 
		      (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =rfa.NumActive INNER JOIN
		      (SELECT TypeActive,NumActive,Company FROM jrActive) jra2 ON jrA2.NumActive=jra.idHubKey
  SET @logStatus = @logStatus + 'Y' -- 18

  SET IDENTITY_INSERT dbo.[DKO_hrAsset] ON
  INSERT INTO [dbo].[DKO_hrAsset]([IdRecord],[_NumActive],[IdAsset],[D_From],[IdUpdate],[StateSED],[IsArchive],[CorrectionType],[AssetType],[AssetCode],[AssetIssue],[AssetNumber],[AssetDate]
			   ,[D_Repayment],[FaceValue],[FaceValueCurrency],[FvCurrencyRate],[PurchaseValue],[PurchaseCurrency],[PureMass],[NetMass],[Standard],[IsActive],[AssetQuality],[IdIssuer],[IdOrgToCheck]
			   ,[MainAccount],[PayOrgBIC],[TUHolder],[DebtorRegNum],[IdParentAsset],[OriginalsState],[isGuaranteedByCrimea],[isGuaranteedByRF],[isMinFinAgreed],[AlterComment],[ESDate],[ESNo]
			   ,[GKDNum],[GKDDate])
   SELECT      hrA.idHubKey,CASE isnull([hub_DKO_hrAsset].IdParentAsset,-1) WHEN -1 THEN  jrA.[idHubKey] ELSE jrt.idHubKey END,[IdAsset],[D_From],
               Case @IsHUB When 1 Then [IdUpdate] Else 0 End, [StateSED],[IsArchive],[CorrectionType],[AssetType],[AssetCode],[AssetIssue],[AssetNumber]               ,[AssetDate]
			   ,[D_Repayment],[FaceValue],[FaceValueCurrency],[FvCurrencyRate],[PurchaseValue],[PurchaseCurrency],[PureMass],[NetMass],[Standard],[IsActive],[AssetQuality],[IdIssuer],[IdOrgToCheck]
			   ,[MainAccount],[PayOrgBIC],[TUHolder],[DebtorRegNum],[IdParentAsset],[OriginalsState],[isGuaranteedByCrimea],[isGuaranteedByRF],[isMinFinAgreed],[AlterComment],[ESDate],[ESNo]
			   ,[GKDNum],[GKDDate]
  FROM       [dbo].[hub_DKO_hrAsset]
	         LEFT  JOIN (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrActive') jrA ON   jrA.idTUKey =_NumActive
	         INNER JOIN (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='DKO_hrAsset') hrA ON   hrA.idTUKey =[IdRecord]
	         LEFT  JOIN (SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='jrTransh') jrt ON   jrt.idTUKey =_NumActive --Aheburg 1763 загрузка по траншам
  SET IDENTITY_INSERT dbo.[DKO_hrAsset] OFF;
  SET @logStatus = @logStatus + 'Y' -- 19

  SET IDENTITY_INSERT DKO_hrAssetState ON
	INSERT INTO [DKO_hrAssetState](IDRECORD,[_IdRecord],[IdAssetState],[D_From],[IdUpdate],hub_DKO_hrAssetState.[StateSED],[IsArchive],[CorrectionType],[IdAsset],[PartitionTypeTo],[CrdNum],[ReqType],[ReqNum],[ReqDate],[ESDate],[ESNo],[ToolCode],[PartitionTypeFrom],[IS_IN],[idDKOPTandActiveState])
	SELECT    t.idHubKey, hub_DKO_hrAssetState._IdRecord, hub_DKO_hrAssetState.IdAssetState, hub_DKO_hrAssetState.D_From, 
	                      Case @IsHub When 1 Then hub_DKO_hrAssetState.IdUpdate Else 0 End,
	                      hub_DKO_hrAssetState.StateSED, 
						  hub_DKO_hrAssetState.IsArchive, hub_DKO_hrAssetState.CorrectionType, hub_DKO_hrAssetState.IdAsset, hub_DKO_hrAssetState.PartitionType, hub_DKO_hrAssetState.CrdNum, 
						  hub_DKO_hrAssetState.ReqType, hub_DKO_hrAssetState.ReqNum, hub_DKO_hrAssetState.ReqDate,  hub_DKO_hrAssetState.ESDate, 
						  hub_DKO_hrAssetState.ESNo, cast(CASE WHEN hub_hrStockSect.tpSect IN ('13N', '14N', '22N', '3N') THEN '1' WHEN hub_hrStockSect.tpSect IN ('12L', '13L', '14L', '21L', '3L') THEN '3' ELSE NULL 
						  END as varchar)AS TOOLCODE,
						 -- cast(CASE WHEN  hub_hrStockPawn.cdStock is NOT null 
						  --THEN CASE WHEN hub_hrStockPawn.tpSatisf = 'M' then 2
							--		WHEN hub_hrStockPawn.tpSatisf IN ('D', 'E', 'H', 'J') then 1
							--		WHEN hub_hrStockPawn.tpSatisf NOT IN ('D', 'E', 'H', 'J','M') then 3
							--	end
						  -- ELSE NULL  
						  --END as int) as OperType,
						  PartitionType,
						  CAST( (CASE WHEN hub_DKO_hrAssetState.StateSed in (-3,-2,-1,0,1) then 0
							   WHEN hub_DKO_hrAssetState.StateSed in (-4,2,3) then 1 
							   ELSE NULL end) as int
							   ) as IS_IN, NULL idDKOPTandActiveState
	                      
	FROM         hub_DKO_hrAssetState INNER JOIN
						  hub_DKO_hrAsset ON hub_DKO_hrAssetState.IdAsset = hub_DKO_hrAsset.IdAsset INNER JOIN
						  hub_hrStockSect ON hub_hrStockSect.idStock = hub_DKO_hrAsset._NumActive LEFT JOIN
						  hub_hrStockPawn ON  hub_hrStockSect.cdStock = hub_hrStockPawn.cdStock 
						  INNER JOIN hub_keys t on t.idTUKey=hub_DKO_hrAssetState.IdRecord AND TU=@TU and NameTable='DKO_hrAssetState'
 SET IDENTITY_INSERT DKO_hrAssetState OFF
  SET @logStatus = @logStatus + 'Y' -- 20

  SET IDENTITY_INSERT dbo.[DKO_hrRepayment] ON
  INSERT INTO [dbo].[DKO_hrRepayment]([IdRecord],[_IdRecord],[IdRepayment],[D_From],[IdUpdate],[StateSED],[IsArchive],[CorrectionType],[IdAsset],[D_Payment],[Amount],[AlterComment],[IdAssetIdRecord],[ESDate],[ESNo])
  
  -- 65vev3
  SELECT      
    DKO.[idHubKey], jrP.[idHubKey], r.[IdRepayment], r.[D_From], 
    Case @IsHUB When 1 Then r.[IdUpdate] Else 0 End, 
    r.[StateSED], r.[IsArchive], r.[CorrectionType],
	r.[IdAsset], r.[D_Payment], r.[Amount], r.[AlterComment], jrA.[idHubKey], r.[ESDate], r.[ESNo]
  FROM
    [dbo].[hub_DKO_hrRepayment] r 
          INNER JOIN [dbo].[hub_DKO_hrAsset] a On r.[IdAsset] = a.[IdAsset]
          INNER JOIN ( SELECT idTUKey, idHubKey FROM hub_Keys WHERE TU = @TU and NameTable = 'DKO_hrRepayment' ) DKO ON DKO.idTUKey = r.[IdRecord] 
          INNER JOIN ( SELECT idTUKey, idHubKey FROM hub_keys WHERE TU = @TU and NameTable = 'jrPaymentGrafic' ) jrP ON jrP.[idTuKey] = r.[_IdRecord] 
          INNER JOIN ( SELECT idTUKey, idHubKey FROM hub_Keys WHERE TU = @TU and NameTable = 'DKO_hrAsset' ) jrA ON jrA.idTUKey = a.[IdRecord]
  order by jrP.[idHubKey]
  --SELECT      DKO.[idHubKey],jrP.[idHubKey],[IdRepayment],[D_From],[IdUpdate],[StateSED],[IsArchive],[CorrectionType],
	--			[IdAsset],[D_Payment],[Amount],[AlterComment],jrA.[idHubKey],[ESDate],[ESNo]
  --FROM        [dbo].[hub_DKO_hrRepayment] INNER JOIN  
	--			(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='DKO_hrRepayment') DKO ON DKO.idTUKey =idRecord INNER JOIN  
	--			(SELECT idTUKey,idHubKey FROM hub_keys  WHERE TU=@TU and NameTable='jrPaymentGrafic') jrP ON jrP.[idTuKey]=_idRecord INNER JOIN 
	--			(SELECT idTUKey,idHubKey FROM hub_Keys WHERE TU=@TU and NameTable='DKO_hrAsset') jrA ON jrA.idTUKey =idAssetIdRecord
  
  SET IDENTITY_INSERT dbo.[DKO_hrRepayment] OFF;
  
  SET @logStatus = @logStatus + 'Y' -- 21
 
  RETURN 1
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
	INSERT INTO	 rfSign (tpSign,nmSign) VALUES('StartSolution',null) 
  UPDATE rfSign SET nmSign = '-1' WHERE tpSign = 'StartSolution' 

  SET @logStatus = @logStatus + 'N'

  RETURN 0
END CATCH
GO