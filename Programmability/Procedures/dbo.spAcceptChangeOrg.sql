﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spAcceptChangeOrg] 
/********************
   2020-03-18 65fev aheburg-3242 восстанавливаю аффилированность утеряную в aheburg-2873
   
-- AHEBURG-2437	Процедура spAcceptChangeOrg
-- =============================================
-- Author:<65eav2>
-- Create date: <12.12.2019>
-- Description:	<Измененный алгоритм приема изменений в форме "Принять решение по ЭС">
-- =============================================
*/
	@idES int,				-- ид ЭС с изменениями по организации(С101, С104)
	@UserName varchar(100)	-- Имя учетной записи пользователя, который принимает изменения по организации
AS
BEGIN

	DECLARE @EDName varchar(33) = (SELECT EDFileName FROM EDOKO_hrES WHERE idES = @idES);
	DECLARE @dtWork DateTime = (SELECT dtWork FROM rfStatus)		-- Текущий ОД
	DECLARE @id_isAffKO int = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedOrg') -- 65fev aheburg-3242
	DECLARE @id_isAffOrg int = (SELECT idField FROM EDOKO_ListChangeField WHERE NmFieldEDOKO = 'IsAffiliatedKO') -- 65fev aheburg-3242
	DECLARE @idKO int = (SELECT idko FROM EDOKO_hrES WHERE idES = @idES ) -- 65fev aheburg-3242
-- п.1   
	UPDATE 
		EDOKO_jrChangeORG 
	SET 
		ChangeStatus = 'T', 
		Annotation = 'Принято на основании рассмотрения ЭС <' + @EDName + '>', 
		idESProcess = @idES
	WHERE 
		idES = @idES
		AND idESProcess is NULL
		AND ChangeStatus = 'R'
	
-- п.2
	UPDATE 
		EDOKO_hrES 
	SET 
		Doer = @UserName, 
		EDStatus = 'T' 
	WHERE 
		EDType = 'C104'
		AND idES = @idES

-- п.3.2
-- Для всех Изменений_найденных(ChangeSearch), у которых Изменение_найденное.NewValue=Изменение_принимаемое(ChangeAccept).NewValue, тогда для Изменение_найденное проставляем "принято"
	UPDATE 
		EDOKO_jrChangeORG 
	SET 
		ChangeStatus = 'T', 
		Annotation = 'Принято на основании рассмотрения ЭС<' + @EDName + '>', 
		idESProcess = @idES
	FROM 
		EDOKO_jrChangeORG   --п.3.1 
	WHERE 
		idChangeOrg in (SELECT 
							ChangeSearch.idChangeOrg
						FROM 
							EDOKO_jrChangeORG as ChangeSearch
							join (SELECT 
									idField, 
									idORG, 
									ides, 
									NewValue 
								  FROM 
									EDOKO_jrChangeORG 
								  WHERE ides = @idES) as ChangeAccept on ChangeSearch.idField = ChangeAccept.idField and ChangeSearch.idORG = ChangeAccept.idORG
							join EDOKO_hrES as Change_hrES on ChangeSearch.idES = Change_hrES.idES  -- 65fev aheburg-3242 start
						WHERE 
						    (
							  (ChangeSearch.ChangeStatus = 'R') 
							  and (ChangeSearch.idES <> ChangeAccept.ides) 
							  and (dbo.fnNormalTextForFT (ChangeSearch.NewValue) = dbo.fnNormalTextForFT (ChangeAccept.NewValue))
							 )
							 AND
							 (
							   (ChangeSearch.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
							 )
						) -- 65fev aheburg-3242 finito
		AND idESProcess is NULL
		AND ChangeStatus = 'R'
								
-- Для всех Изменений_найденных(ChangeSearch), у которых Изменение_найденное.NewValue <> Изменение_принимаемое(ChangeAccept).NewValue, тогда для Изменение_найденное проставляем "отклонено"						
	UPDATE 
		EDOKO_jrChangeORG 
	SET 
		ChangeStatus = 'F', 
		Annotation = 'Отклонено на основании рассмотрения ЭС<' + @EDName + '>', 
		idESProcess = @idES
	FROM 
		EDOKO_jrChangeORG 
	WHERE 
		idChangeOrg in (SELECT 
							ChangeSearch.idChangeOrg
						FROM 
							EDOKO_jrChangeORG as ChangeSearch
							join (SELECT 
									idField, 
									idORG, 
									ides, 
									NewValue 
								  FROM 
									EDOKO_jrChangeORG 
								  WHERE 
									ides = @idES) as ChangeAccept on ChangeSearch.idField = ChangeAccept.idField and ChangeSearch.idORG = ChangeAccept.idORG
							join EDOKO_hrES as Change_hrES on ChangeSearch.idES = Change_hrES.idES  -- 65fev aheburg-3242 start
						WHERE 
						  (
							(ChangeSearch.ChangeStatus = 'R') 
							and (ChangeSearch.idES <> ChangeAccept.ides) 
							and (dbo.fnNormalTextForFT (ChangeSearch.NewValue) <> dbo.fnNormalTextForFT (ChangeAccept.NewValue))
						  )
						  AND
						  (
						    (ChangeSearch.idField not IN (@id_isAffKO,@id_isAffOrg)) or (Change_hrES.idKO = @idKO)  --условие на аффилированность
						  )
						) -- 65fev aheburg-3242 finito
		AND idESProcess is NULL
		AND ChangeStatus = 'R'
  
-- п.3.3
	/*UPDATE EDOKO_hrES 
	SET EDStatus = 'P', Doer = @UserName
	WHERE idES in (SELECT idES 
					FROM EDOKO_jrChangeORG 
					WHERE ChangeStatus = 'F' and idESProcess = @idES
				  )
		AND EDType = 'C104'*/--ah-2873
		
-- проставление рисков в карточку актива на рассмотрении
	EXECUTE spPutDownRisksNoteActive @idES, 'В С101 указана неверная информация об организации', @UserName
		
-- Если в рамках С101 или С104, у которых происходит отклонение изменений(в ходе принятий изменений по исходному ЭС), 
-- в таблице EDOKO_jrChangeORG зафиксированы изменения и других реквизитов - проставляем отклонение по всем.	
	--UPDATE EDOKO_jrChangeORG 
	--SET ChangeStatus = 'F'
-- Выбираем пары < [idES], [список полей, по которым был принят отказ в ходе принятия решения] > и записываем @RejectedChanges	
	DECLARE @SQL VARCHAR(MAX)
	DECLARE @es int
	DECLARE cr CURSOR 
	FOR SELECT distinct idES FROM EDOKO_jrChangeORG  WHERE (idES <> @idES) and (idESProcess = @idES) AND (changeStatus = 'F')
		OPEN cr
	FETCH NEXT FROM cr 
	INTO @es
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = N'';
		SELECT @SQL = coalesce(@SQL + NmFieldEDOKO + ',','' )
		FROM EDOKO_ListChangeField WHERE idField in (SELECT idField FROM EDOKO_jrChangeORG WHERE idES = @es and changeStatus = 'F')
		
		IF LEN(@SQL) > 0
			SET @SQL = SUBSTRING(@SQL, 1, LEN(@SQL)-1)
		
		UPDATE EDOKO_jrChangeORG 
		SET ChangeStatus = 'F'
			,Annotation = 'Отклонено без рассмотрения вместе с отклонением изменений реквизитов <' + @SQL + '> при принятии решения по изменениям ЭС <' + @EDName + '>'
			,idESProcess = NULL
		WHERE 
			idES = @es 
			AND ChangeStatus = 'R'	
			
		FETCH NEXT FROM cr
		INTO @es
	END
	CLOSE cr
	DEALLOCATE cr	
	
--	п.5
	EXECUTE spPutDownRisksNoteActiveOtherES @idES, @UserName
END
GO