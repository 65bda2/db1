﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[spChangeOperationDate]
  @dtOldOD DateTime,
  @dtNewOD DateTime,
  @UserName VarChar(100)
AS
BEGIN
  Begin Try
	Begin Transaction
    -- Смена операционного дня
    Update rfStatus Set dtWork = @dtNewOD
		
	/* ---- Исключаем нерыночные активы из пула обеспечения по сроку платежа ---- */
	Declare  @dtValid DateTime, @NumActive Int, @tpSect VarChar(4), @Razdel11D Int, @idAcc Int
    Select @dtValid = DATEADD( dd, 60, dbo.IncWorkDay( @dtNewOD, 1 )  )
    Select @Razdel11D = idActiveState From rfActiveState Where tpSect = '11D'
	
	--  запись в допинфо для договоров
   	Update jrActive Set
   	  DopInf = SubString( DopInf, 1, 2 ) + 'F' + SubString( DopInf, 4, 24 )
   	Where
   	  DatePayment <= @dtValid
   	  And Not jrActive.status In ( 'F', 'E' )
   	  And jrActive.TypeActive = 'D'
   	  And SubString( DopInf, 3, 1 ) <> 'F'
   	
   	-- Создаем курсор по исключаемым активам
   	Declare BadActive Cursor For
   	  Select NumActive From jrActive Where DatePayment <= @dtValid And status = 'T' And TypeActive = 'D'
   	
   	Open BadActive
   	Fetch Next From BadActive Into @NumActive
   	
   	While @@FETCH_STATUS = 0
   	Begin
   	  -- Определяем состояние актива (в залоге или нет), запоминаем счет
   	  Select Top 1
   	    @tpSect = r.tpSect, @idAcc = idAcc
   	  From
   	    hrActiveSect h Join rfActiveState r On h.idActiveState = r.idActiveState
   	  Where
   	    h.NumActive = @NumActive And
   	    h.DateAssign = @dtOldOD
   	  Order By
   	    idActiveSect Desc
   	  
   	  -- Запись в таблицу событий hrActiveEvent для АС ППА, для создания C202
      Insert Into hrActiveEvent ( tpEvent, dtEvent, NumActive )
           Values ( Case When @tpSect In ( '21L', '22N', '3N', '3L' ) Then '3' Else '5' End,  @dtNewOD, @NumActive ) -- AHEBURG-1748 изменение tpEvent c 4 на 5
      
      -- Запись в журнал, что актив перестал соответствовать критериям Банка России
      Insert Into jrJournalActive ( NumActive, DateIt, SysDate, Note, Kategory, Doer )
           Values ( @NumActive, @dtNewOD, GETDATE(), 'Актив перестал соответствовать критериям Банка России', 'N', @UserName )
      
      -- Если актив не в залоге
      If @tpSect Not In ( '21L', '22N', '3N', '3L' )
      Begin
        -- Запись в журнал, что актив исключен из пула обеспечения
        Insert Into jrJournalActive ( NumActive, DateIt, SysDate, Note, Kategory, Doer )
             Values ( @NumActive, @dtNewOD, GETDATE(), 'Актив выведен из пула обеспечения, так как до даты полного погашения осталось менее 61 дня.', 'Q', @UserName )
        -- Запись в hrActiveSect раздела 11D
        Delete From hrActiveSect Where NumActive = @NumActive And DateAssign >= @dtNewOD
        Insert Into hrActiveSect ( DateAssign, NumActive, idAcc, idActiveState )
             Values ( @dtNewOD, @NumActive, @idAcc, @Razdel11D )
      End
      
      -- Меняем статус актива
      Update jrActive Set
        status = Case When @tpSect In ( '21L', '22N', '3N', '3L' ) Then 'N' Else 'Q' End,
        Begindate = @dtNewOD
      Where
        NumActive = @NumActive
        
   	  Fetch Next From BadActive Into @NumActive
   	End
   	
   	Close BadActive
   	Deallocate BadActive
   	
	/* ---- Конец исключаем нерыночные активы из пула обеспечения по сроку платежа ---- */
    /* ----- Перенос данных в hrActiveSect на текущий операционный день ---- */
    Insert Into hrActiveSect ( DateAssign, NumActive, idAcc, idActiveState )
      Select
        @dtNewOD, NumActive, idAcc, idActiveState
      From
        hrActiveSect a
      Where
        a.idActiveSect = ( Select Top 1 a1.idActiveSect From hrActiveSect a1 Where
                             a.NumActive = a1.NumActive And a1.DateAssign >= @dtOldOD And a1.DateAssign < @dtNewOD
                           Order By a1.DateAssign Desc, a1.idActiveSect desc )
        And Not Exists ( Select * From hrActiveSect a2 Where a.NumActive = a2.NumActive And a2.DateAssign = @dtNewOD )

    -->>>AHEBURG-(4365)4505 65MakarovDA проверка БО за актуальный, обязательный период
    -->>>AHEBURG-4211 16.09.2020 65MakarovDA SET IsToExcludeRep = 0 только в ОД = обязательной БО
    --Следует также присваивать jrActive.dtExclude значение NULL при переходе в новый ОД, если он является днем, 
    --когда наступает срок предоставления очередной отчетности (новый период становится обязательным), вполнять операцию до обновления jrActive.DopInf[27] .
	if
    (SELECT Max(idPeriod) from dbo.fnGetActualBuhPeriod( DEFAULT, DEFAULT, @dtNewOD ) where obPeriod = 1)> 
    (SELECT Max(idPeriod) from dbo.fnGetActualBuhPeriod( DEFAULT, DEFAULT, @dtOldOD ) where obPeriod = 1) 
	UPDATE jrActive SET IsToExcludeRep = 0 where IsToExcludeRep=1 and [status]='T' and IsSimpleCheck<>1
    --<<<AHEBURG-4211 16.09.2020 65MakarovDA
    UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 27, 1,[dbo].[fnCheck_dopinfo27](a.numactive))
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('T','M')
    and isnull(a.IsSimpleCheck,0)<>1
    and a.TypeActive='D'
    and substring(a.DopInf,27,1)<>[dbo].[fnCheck_dopinfo27](a.numactive)
    
    UPDATE jrActive 
    set DopInf=STUFF(a.DopInf, 26, 1,[dbo].[fnCheck_dopinfo27](a.numactive))
    from jrActive a join jrLetter l on a.idLetter=l.idLetter and a.status in ('R','W','K')
    and isnull(a.IsSimpleCheck,0)<>1
    and a.TypeActive='D'
    and substring(a.DopInf,26,1)<>[dbo].[fnCheck_dopinfo27](a.numactive)

    UPDATE jrActive 
    set DopInf=STUFF(DopInf, 27, 1,'0')
    where status in ('T','M') and isnull(IsSimpleCheck,0)=1 and TypeActive='D'
    and substring(DopInf,27,1)<>'0'  
    UPDATE jrActive 
    set DopInf=STUFF(DopInf, 26, 1,'0')
    where status in ('R','W','K') and isnull(IsSimpleCheck,0)=1 and TypeActive='D'
	and substring(DopInf,26,1)<>'0'
	--<<<AHEBURG-(4365)4505 65MakarovDA проверка БО за актуальный, обязательный период
	--AHEBURG-4211 11.09.2020 65MakarovDA SET IsToExcludeRep = 0 только в ОД = обязательной БО
	--UPDATE jrActive SET IsToExcludeRep = 0 where IsToExcludeRep=1 and dtExclude<=@dtNewOD
    
    --При переходе в ОД , когда выполняется условие  ОД>dtExlude для активов в статусе "Принят в пул обеспечения" 
    --(jrActive.status = T), у которых dtExlude is not null, и isToExcludeRep=1 или есть бо за все обязательные актуальные периоды ,  
    --требуется обновить dtExlude  в null
    UPDATE jrActive SET dtExclude = NULL where dtExclude is not null and dtExclude<@dtNewOD and isnull(IsSimpleCheck,0)<>1 
    and substring(DopInf,27,1)='T'
    Commit Transaction
  End Try
  Begin Catch
    Rollback Transaction
    Declare @ErrMsg VarChar(255)
    Select @ErrMsg = ERROR_MESSAGE()
    Select @ErrMsg = 'При переходе на новый операционный день возникла ошибка. Переход не выполнен. ' + @ErrMsg
    RaisError( @ErrMsg, 16, 1  )
  End Catch
END
GO