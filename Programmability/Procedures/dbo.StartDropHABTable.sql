﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO


CREATE Procedure [dbo].[StartDropHABTable] 
@ErrorMSG varchar(max) output
as
BEGIN TRY
 --BEGIN TRANSACTION
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_hrTypeCredits]') AND type in (N'U'))
    DROP TABLE [dbo].[hub_hrTypeCredits]
 
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_rfStatus]') AND type in (N'U'))
    DROP TABLE [dbo].[hub_rfStatus]

    --IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Counters]') AND type in (N'U'))
    ---DROP TABLE [dbo].[Counters]

    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_Counters]') AND type in (N'U'))
    DROP TABLE [dbo].[hub_Counters]
 
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_LastAssetNumber]') AND type in (N'U'))
    DROP TABLE [dbo].[Hub_LastAssetNumber]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrActiveEvent]') AND type in (N'U'))
	DROP TABLE [hub_hrActiveEvent]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrPaymentGrafic]') AND type in (N'U'))
	DROP TABLE [hub_hrPaymentGrafic]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrStockSect]') AND type in (N'U'))
	DROP TABLE [hub_hrStockSect]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrJournalActive]') AND type in (N'U'))
	DROP TABLE [hub_jrJournalActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrNoteActive]') AND type in (N'U'))
	DROP TABLE [hub_jrNoteActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrTransh]') AND type in (N'U'))
	DROP TABLE [hub_jrTransh]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_lgArchActive]') AND type in (N'U'))
	DROP TABLE [hub_lgArchActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrPaymentGrafic]') AND type in (N'U'))
	DROP TABLE [hub_jrPaymentGrafic]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrRequestDocActive]') AND type in (N'U'))
	DROP TABLE [hub_jrRequestDocActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfDopLetActive]') AND type in (N'U'))
	DROP TABLE [hub_rfDopLetActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrActive]') AND type in (N'U'))
	DROP TABLE [hub_jrActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrBlock]') AND type in (N'U'))
	DROP TABLE [hub_jrBlock]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_JrJournalLetter]') AND type in (N'U'))
	DROP TABLE [hub_JrJournalLetter]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfGKD]') AND type in (N'U'))
	DROP TABLE [hub_rfGKD]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrAccount]') AND type in (N'U'))
	DROP TABLE [hub_jrAccount]

	--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfFaceKO]') AND type in (N'U'))
	--DROP TABLE [hub_rfFaceKO]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfAffPers]') AND type in (N'U'))
	DROP TABLE [hub_rfAffPers]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrLetter]') AND type in (N'U'))
	DROP TABLE [hub_jrLetter]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrOwnFunds]') AND type in (N'U'))
	DROP TABLE [hub_hrOwnFunds]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrDopLetter]') AND type in (N'U'))
	DROP TABLE [hub_jrDopLetter]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrGKD]') AND type in (N'U'))
	DROP TABLE [hub_jrGKD]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_JrBuhRecord]') AND type in (N'U'))
	DROP TABLE [hub_JrBuhRecord]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrRateExch]') AND type in (N'U'))
	DROP TABLE [hub_hrRateExch]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrKO]') AND type in (N'U'))
	DROP TABLE [hub_jrKO]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrOrg]') AND type in (N'U'))
	DROP TABLE [hub_jrOrg]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfASPPARestriction]') AND type in (N'U'))
	DROP TABLE [hub_rfASPPARestriction]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfBuhFactor]') AND type in (N'U'))
	DROP TABLE [hub_rfBuhFactor]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOrgList]') AND type in (N'U'))
	DROP TABLE [hub_rfOrgList]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfReportsASPPA]') AND type in (N'U'))
	DROP TABLE [hub_rfReportsASPPA]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfSign]') AND type in (N'U'))
	DROP TABLE [hub_rfSign]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfSignASPPA]') AND type in (N'U'))
	DROP TABLE [hub_rfSignASPPA]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfSignRekvizitASPPA]') AND type in (N'U'))
	DROP TABLE [hub_rfSignRekvizitASPPA]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfFactor]') AND type in (N'U'))
	DROP TABLE [hub_rfFactor]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfDays]') AND type in (N'U'))
	DROP TABLE [hub_rfDays]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrRKC]') AND type in (N'U'))
	DROP TABLE [hub_jrRKC]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrCurrency]') AND type in (N'U'))
	DROP TABLE [hub_jrCurrency]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrFactor]') AND type in (N'U'))
	DROP TABLE [hub_hrFactor]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrOrg]') AND type in (N'U'))
	DROP TABLE [hub_hrOrg]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrBuhRecord]') AND type in (N'U'))
	DROP TABLE [hub_hrBuhRecord]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrAsset]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrAsset]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrAssetState]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrAssetState]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrBankInTU]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrBankInTU]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrCompany]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrCompany]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrMutualParticipation]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrMutualParticipation]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_DKO_hrRepayment]') AND type in (N'U'))
	DROP TABLE [hub_DKO_hrRepayment]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrActive]') AND type in (N'U'))
	DROP TABLE [hub_hrActive]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_hrWaitCheck]') AND type in (N'U'))
	DROP TABLE [hub_hrWaitCheck]

--	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_jrSavePlace]') AND type in (N'U'))
--	DROP TABLE [hub_jrSavePlace]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfDKKP]') AND type in (N'U'))
	DROP TABLE [hub_rfDKKP]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOKATO]') AND type in (N'U'))
	DROP TABLE [hub_rfOKATO]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOKFS]') AND type in (N'U'))
	DROP TABLE [hub_rfOKFS]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOKOPF]') AND type in (N'U'))
	DROP TABLE [hub_rfOKOPF]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOKVED]') AND type in (N'U'))
	DROP TABLE [hub_rfOKVED]
/*
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfOKVED2_R]') AND type in (N'U'))
	DROP TABLE [hub_rfOKVED2_R]
*/
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfTpClassBuh]') AND type in (N'U'))
	DROP TABLE [hub_rfTpClassBuh]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfBuhFactorType]') AND type in (N'U'))
	DROP TABLE [hub_rfBuhFactorType]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfBuhPeriod]') AND type in (N'U'))
	DROP TABLE [hub_rfBuhPeriod]

	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[hub_rfBuhRecord]') AND type in (N'U'))
	DROP TABLE [hub_rfBuhRecord]
	
    IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[hub_hrStockPawn]') AND type in (N'U'))
    DROP TABLE [dbo].[hub_hrStockPawn]

    --SELECT EventName = CAST('Удаление временных таблиц' as varchar(100)),EventStatus=CAST(1 as bit),EventMSG=CAST('Успешно' as varchar(max)),EventDate=GETDATE()
  --COMMIT TRANSACTION
  RETURN 1
END TRY
BEGIN CATCH
--  ROLLBACK TRANSACTION    
  --SELECT EventName = CAST('Удаление временных таблиц' as varchar(100)),EventStatus=CAST(1 as bit),EventMSG=CAST(ERROR_MESSAGE() as varchar(max)),EventDate=GETDATE()
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'
  RETURN 0
END CATCH
GO