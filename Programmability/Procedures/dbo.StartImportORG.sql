﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[StartImportORG] 
@IsHUB BIT,
@TU varchar(2),
@ErrorMSG varchar(max) output,
@logStatus varchar(6) output AS -- каждый символ означает успешно или нет загружена каждая таблица. может имет значение (Y -успешно, N - ошибка, 0 - нет данных(только rfAffPers)). порядок символов 1-jrOrg,2-DKO_hrCompany,3-rfFactor,4-rfAffPers,5-DKO_hrMutualParticipation,6-hrOrg
BEGIN TRY;

IF @TU = '92' -- для Татарстана
BEGIN
	Delete From hub_jrOrg Where idOrg in ( 31, 137, 193, 245, 261, 267, 289, 297, 302, 544, 787, 791, 853, 856, 858, 861, 863, 864, 865 )
	Delete From hub_hrOrg Where idOrg in ( 31, 137, 193, 245, 261, 267, 289, 297, 302, 544, 787, 791, 853, 856, 858, 861, 863, 864, 865 )
	Delete From hub_DKO_hrCompany Where _IdOrg in ( 31, 137, 193, 245, 261, 267, 289, 297, 302, 544, 787, 791, 853, 856, 858, 861, 863, 864, 865 )
END

  SET @logStatus = ''
  delete from jrORG  where IdOrg in (select idHubKey from hub_Keys k where k.TU=@TU and k.NameTable = 'jrORG' );
  delete from hub_keys where nametable='jrORG' and TU=@TU;
	
  insert into hub_keys (idTuKey,idHubKey,NameTable,TU,isReplace) 
  select  idTUkey,isnull(idDubl,row_number() over (PARTITION BY isReplace order by idTUkey) + maxkey) as idhubkey,'jrORG' as NameTable,@TU  as tu,isReplace
  FROM (select t.idTUkey,t.idDubl ,(select isnull(MAX(idhubkey),0) FROM  hub_keys WHERE NameTable='jrOrg') as maxkey,case when t.idDubl is null then 0 else 1 end as isReplace
	FROM 
	  ( SELECT 
	      h.idOrg as idTUkey,
	      Case When ( IsNull( h.isResident, 0 ) = 1 ) Or ( h.IsSubject <> 0 ) 
	           Then ( Select idOrg From jrORG o Where o.INN = h.inn And o.EGRUL = h.egrul )
	           Else ( Select idOrg From jrORG o Where ( h.egrul Is Not Null and o.EGRUL = h.egrul ) or 
	                                                  ( h.egrul Is Null and o.INN = h.inn ) or 
	                                                  ( h.INN Is Null and ( ( h.NonResidentID Is Not Null And h.NonResidentID = o.NonResidentID ) or
	                                                                        ( h.NonResidentID  Is Null And h.CompanyName = o.CompanyName ) ) ) )
		   End as idDubl
	     FROM 
	       hub_jrOrg h
	  ) as t
  ) as tt;
	
  SET IDENTITY_INSERT dbo.jrOrg ON;
	
  INSERT INTO jrOrg(idOrg,nmAddr,businessQuality,OKPO,INN,EGRUL,CompanyName,CodeOKVED,CodeOKOPF,CodeOKFS,IsSubject,IsConforming,IsInList,IsResident,D_lastRegDate,IsBookKeepingOK,IsFederalLaw,AlterComment,isSootv/*nmOrg,*/,belonging,dtOgrn,CodeOKATO,ShortNmORG,TpDtReg,NmLaw,dtBookKeeping,AnotherInf,AnotherInfNote,Rating,NonResidentID,tpDopInfo)
	SELECT k.idhubkey,h.nmAddr,h.businessQuality,h.OKPO,h.INN,h.EGRUL,h.CompanyName,h.CodeOKVED,h.CodeOKOPF,h.CodeOKFS,h.IsSubject,h.IsConforming,ISNULL(h.IsInList,0),h.IsResident,h.D_lastRegDate,h.IsBookKeepingOK,h.IsFederalLaw,h.AlterComment,h.isSootv/*nmOrg,*/,h.belonging,h.dtOgrn,h.CodeOKATO,h.ShortNmORG,h.TpDtReg,h.NmLaw,h.dtBookKeeping,h.AnotherInf,h.AnotherInfNote,h.Rating,h.NonResidentID,h.tpDopInfo -- AHEBURG-1691
	FROM hub_jrOrg h
		left join hub_keys k on h.idOrg=k.idTuKey and k.tu=@TU and k.NameTable = 'jrOrg'
		left join jrOrg o on o.idOrg=k.idhubkey 
		where o.idOrg is null 

  SET IDENTITY_INSERT dbo.jrOrg OFF;
  SET @logStatus = @logStatus + 'Y' --1

  DELETE from DKO_hrCompany  where IdRecord in (select idHubKey from hub_Keys k where k.TU=@TU and k.NameTable = 'DKO_hrCompany' );
  DELETE from hub_keys where nametable='DKO_hrCompany' and TU=@TU;
	
  Declare @MaxKey Int
  SELECT @MaxKey = IDENT_CURRENT( 'DKO_hrCompany' )
  If @MaxKey = 1 And NOT EXISTS( Select * From DKO_hrCompany ) Select @MaxKey = 0
	
  INSERT INTO hub_keys (idTuKey,idHubKey ,NameTable,TU,isReplace) 
    SELECT idTUkey, row_number() over ( order by idTUkey ) + @MaxKey as idhubkey, 'DKO_hrCompany' as NameTable, @TU as tu, 0 
    FROM ( select h.IdRecord as idTUkey, h._IdOrg as IdOrg FROM  hub_DKO_hrCompany h ) as t
    ORDER BY t.IdOrg;
     
  --SELECT idTUkey,row_number() over (order by idTUkey)+maxkey as idhubkey,'DKO_hrCompany' as NameTable,@TU  as tu,0 
  --FROM (select h.IdRecord as idTUkey,(select isnull(MAX(idhubkey),0) FROM  hub_keys WHERE NameTable='DKO_hrCompany') as maxkey
	   ---FROM  hub_DKO_hrCompany h) as t;

  SET IDENTITY_INSERT dbo.DKO_hrCompany ON;

  INSERT INTO DKO_hrCompany(IdRecord,_IdOrg,IdCompany,D_From,IdUpdate,StateSED,IsArchive,CorrectionType,INN,EGRUL,CompanyName,CodeOKVED,CodeOKOPF,CodeOKFS,SubjectType,IsConforming,IsInList,IsInMFOList,IsResident,BusinessQuality,D_LastRegDate,IsBookKeepingOK,AlterComment,ESDate,ESNo,IsLOConforming,TU)
  SELECT k.idhubkey /*--IdRecord*/,(select kk.idHubKey from hub_Keys kk where kk.idTuKey=h._idOrg and kk.NameTable='jrOrg' and kk.TU=@TU) /*--_IdOrg,*/,h.IdCompany,h.D_From,
   Case @IsHub When 1 Then h.IdUpdate Else 0 End, h.StateSED,h.IsArchive,h.CorrectionType,h.INN,h.EGRUL,h.CompanyName,h.CodeOKVED,h.CodeOKOPF,h.CodeOKFS,h.SubjectType,h.IsConforming,h.IsInList,h.IsInMFOList,h.IsResident,h.BusinessQuality,h.D_LastRegDate,h.IsBookKeepingOK,h.AlterComment,h.ESDate,h.ESNo,h.IsLOConforming,@TU 
  FROM hub_DKO_hrCompany h	 
		left join hub_keys k on h.IdRecord=k.idTuKey and k.tu=@TU 
		and k.NameTable = 'DKO_hrCompany'
		left join DKO_hrCompany o on o.IdRecord=k.idhubkey 
		where o.IdRecord is null
		and not (select m.idHubKey  from hub_Keys m where m.idTuKey=h._idOrg and m.NameTable='jrOrg' and m.TU=@TU) is null
  ORDER BY h._IdOrg		
  
  -- 65vev МФО и лизинговые
  INSERT INTO DKO_hrCompany(IdRecord,_IdOrg,IdCompany,D_From,IdUpdate,StateSED,IsArchive,CorrectionType,INN,EGRUL,CompanyName,CodeOKVED,CodeOKOPF,CodeOKFS,SubjectType,IsConforming,IsInList,IsInMFOList,IsResident,BusinessQuality,D_LastRegDate,IsBookKeepingOK,AlterComment,ESDate,ESNo,IsLOConforming,TU)
  SELECT k.idhubkey /*--IdRecord*/,
    (select kk.idHubKey from hub_Keys kk where kk.idTuKey=h._idOrg - 200000000 and kk.NameTable='jrKO' and kk.TU=@TU) + 200000000 /*--_IdOrg,*/
    ,h.IdCompany,h.D_From,
    Case @IsHub When 1 Then h.IdUpdate Else 0 End,h.StateSED,h.IsArchive,h.CorrectionType,h.INN,h.EGRUL,h.CompanyName,h.CodeOKVED,h.CodeOKOPF,h.CodeOKFS,h.SubjectType,h.IsConforming,h.IsInList,h.IsInMFOList,h.IsResident,h.BusinessQuality,h.D_LastRegDate,h.IsBookKeepingOK,h.AlterComment,h.ESDate,h.ESNo,h.IsLOConforming,@TU 
  FROM hub_DKO_hrCompany h	 
		left join hub_keys k on h.IdRecord=k.idTuKey and k.tu=@TU 
		and k.NameTable = 'DKO_hrCompany'
		left join DKO_hrCompany o on o.IdRecord=k.idhubkey 
		where o.IdRecord is null
		and not (select m.idHubKey  from hub_Keys m where m.idTuKey=h._idOrg - 200000000 and m.NameTable='jrKO' and m.TU=@TU) is null
  ORDER BY h._IdOrg	
  -- 65vev МФО и лизинговые	  

  SET IDENTITY_INSERT dbo.DKO_hrCompany OFF;
  SET @logStatus = @logStatus + 'Y' -- 2

  INSERT INTO [rfFactor]([dtFactor],[tpFactor],[pcFactor],[idOrg],[dtEnd])
  SELECT [dtFactor],[tpFactor],[pcFactor],[idOrg],[dtEnd] FROM hub_rfFactor INNER JOIN hub_Keys hk on hk.idTUKey=hub_rfFactor.idOrg
  WHERE idOrg is NOT NULL and hk.NameTable='jrOrg' and isnull(hk.isReplace,0)=0  and hk.TU=@TU; 
  IF @@ROWCOUNT = 0 SET @logStatus = @logStatus + '0' -- 3
  IF @@ROWCOUNT > 0 SET @logStatus = @logStatus + 'Y' -- 3

  delete from rfAffPers  where IdRecord in (select idHubKey from hub_Keys k where k.TU=@TU and k.NameTable = 'rfAffPers');

  delete from hub_keys where nametable='rfAffPers' and TU=@TU;

	INSERT INTO hub_keys (idTuKey,idHubKey ,NameTable,TU,isReplace) 
	select idTUkey,isnull(idDubl,row_number() over (PARTITION BY isReplace order by idTUkey) + maxkey) as idhubkey,'rfAffPers' as NameTable,@TU  as tu,isReplace
	from (select t.idTUkey,t.idDubl,(select isnull(MAX(idhubkey),0) FROM  hub_keys WHERE NameTable='rfAffPers') as maxkey,case when t.idDubl is null then 0 else 1 end as isReplace
	      from (SELECT h.idRecord as idTUkey,(select idRecord from rfAffPers a where a.idOrg=h.idorg and a.idKO=h.idKO and a.IsCompanyOwner=h.IsCompanyOwner) as idDubl
	FROM hub_rfAffPers h) as t
	) as tt;
	
  SET IDENTITY_INSERT dbo.rfAffPers ON;

  INSERT INTO rfAffPers(idKO,idOrg,pcShare,IsCompanyOwner,dtBegin,dtEnd,idRecord,AlterComment)
  SELECT (select idHubKey from hub_Keys k where k.idTuKey=h.idko and NameTable='jrKO' and TU=@TU) --idKO
		,(select idHubKey from hub_Keys k where k.idTuKey=h.idOrg  and isReplace=0 and NameTable='jrOrg' and TU=@TU) --idOrg
		,h.pcShare,h.IsCompanyOwner,h.dtBegin,h.dtEnd,k.idHubKey ,h.AlterComment 
	FROM hub_rfAffPers h
		left join hub_keys k on h.idRecord=k.idTuKey and k.tu=@TU and k.NameTable = 'rfAffPers'
		left join rfAffPers o on o.idRecord=k.idhubkey 
		where o.idRecord is null and not (select idHubKey from hub_Keys where idTUkey=h.idOrg and isReplace=0 and tu=@TU and NameTable = 'jrOrg') is null
		; 

  SET IDENTITY_INSERT dbo.rfAffPers OFF;
  SET @logStatus = @logStatus + 'Y' -- 4


  delete from DKO_hrMutualParticipation  where IdRecord in (select idHubKey from hub_Keys k where k.TU=@TU and k.NameTable = 'DKO_hrMutualParticipation');
  delete from hub_keys where nametable='DKO_hrMutualParticipation' and TU=@TU;
  
  SELECT @MaxKey = IDENT_CURRENT( 'DKO_hrMutualParticipation' )
  If ( @MaxKey = 1 ) And NOT EXISTS( Select * From DKO_hrMutualParticipation )  Select @MaxKey = 0
  
  INSERT INTO hub_keys (idTuKey,idHubKey,NameTable,TU,isReplace) 
  SELECT idTUkey, row_number() over ( order by idTUkey ) + @MaxKey as idhubkey, 'DKO_hrMutualParticipation' as NameTable, @TU as tu, 0 as isReplace
  FROM ( Select h.IdRecord as idTUkey From hub_DKO_hrMutualParticipation h ) as t;	
    
  --SELECT idTUkey,row_number() over (order by idTUkey)+maxkey as idhubkey,'DKO_hrMutualParticipation' as NameTable,@TU  as tu,0 as isReplace
  --FROM (select h.IdRecord as idTUkey,(select isnull(MAX(idhubkey),0) FROM  hub_keys WHERE NameTable='DKO_hrMutualParticipation') as maxkey
	--    from  hub_DKO_hrMutualParticipation h) as t;	
	
  SET IDENTITY_INSERT dbo.DKO_hrMutualParticipation ON;

  INSERT INTO DKO_hrMutualParticipation(IdRecord,_IdRecord,IdMutualParticipation,D_From,IdUpdate,StateSED,IsArchive,CorrectionType,IdCompany,CORegNum,IsCompanyOwner,AlterComment,ESDate,ESNo)
  SELECT k.idhubkey ,(select idhubkey from hub_Keys where idTuKey=h._IdRecord and NameTable = 'rfAffPers' and TU=@TU),h.IdMutualParticipation,h.D_From,
    Case @IsHub When 1 Then h.IdUpdate Else 0 End,
    h.StateSED,h.IsArchive,h.CorrectionType,h.IdCompany,h.CORegNum,h.IsCompanyOwner,h.AlterComment,h.ESDate,h.ESNo 
  FROM hub_DKO_hrMutualParticipation h
		left join hub_keys k on h.idRecord=k.idTuKey and k.tu=@TU and k.NameTable = 'DKO_hrMutualParticipation'
		left join DKO_hrMutualParticipation o on o.idRecord=k.idhubkey 
  where o.idRecord is null; 

  SET IDENTITY_INSERT dbo.DKO_hrMutualParticipation OFF;
  SET @logStatus = @logStatus + 'Y' -- 5

  INSERT INTO hrOrg(OperationDate,HrStatus,DFROM,idOrg,INN,EGRUL,NonResidentID,CompanyName,CodeOKVED,KodOKVED,CodeOKOPF,KODOKOPF,CodeOKFS,KODOKFS,IsSubject,tpDopInfo,IsConforming,IsInList,IsResident,BusinessQuality,D_lastRegDate,IsBookKeepingOK,IsFederalLaw,AlterComment,isSootv,ShortNmORG,TpDtReg,dtBookKeeping,AnotherInf,AnotherInfNote,TpCheck)
  SELECT h.OperationDate,h.HrStatus,h.DFROM,(select idHubKey from hub_Keys where idTUkey=h.idOrg and isNULL(isReplace,0)=0 and tu=@TU and NameTable = 'jrOrg'),h.INN,h.EGRUL,h.NonResidentID,h.CompanyName,h.CodeOKVED,h.KodOKVED,h.CodeOKOPF,h.KODOKOPF,h.CodeOKFS,h.KODOKFS,h.IsSubject,h.tpDopInfo,h.IsConforming,h.IsInList,h.IsResident,h.BusinessQuality,h.D_lastRegDate,h.IsBookKeepingOK,h.IsFederalLaw,h.AlterComment,h.isSootv,h.ShortNmORG,h.TpDtReg,h.dtBookKeeping,h.AnotherInf,h.AnotherInfNote,h.TpCheck
  FROM hub_hrOrg h 
  WHERE not (select idHubKey from hub_Keys where idTUkey=h.idOrg and ISNULL(isReplace,0)=0 and tu=@TU and NameTable = 'jrOrg') is null
  SET @logStatus = @logStatus + 'Y' -- 6

  RETURN 1
END TRY
BEGIN CATCH
  SELECT @ErrorMSG = ERROR_MESSAGE() +'\n'

  IF NOT EXISTS (SELECT * FROM rfSign WHERE tpSign = 'StartSolution')
    INSERT INTO	 rfSign (tpSign,nmSign) 
    VALUES('StartSolution',null) 

  UPDATE rfSign SET nmSign = '-1'
  WHERE tpSign = 'StartSolution' 

  SET @logStatus = @logStatus + 'N'   

  RETURN 0
END CATCH
GO