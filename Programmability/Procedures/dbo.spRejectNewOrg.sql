﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

-- =============================================
-- Author:		65vev3
-- Create date: 23.03.2020
-- Description:	Отклонение новой организации
-- =============================================
CREATE PROCEDURE [dbo].[spRejectNewOrg]
  @IdOrg Int,
  @UserName VarChar(100)
AS
BEGIN
  DECLARE @NA Int, @dtWork DateTime
  SELECT @dtWork = dtWork FROM rfStatus
  
  BEGIN Try
	BEGIN Transaction
  
    UPDATE jrOrg SET OrgStatus = 'F' WHERE idOrg = @IdOrg
  
    DECLARE cr CURSOR FOR 
      SELECT NumActive FROM jrActive WHERE ( NmZaemshik = @IdOrg Or Company = @IdOrg ) And Status = 'R' 
                        
    OPEN cr 
    FETCH Next FROM cr INTO @NA 
                        
    WHILE @@FETCH_STATUS = 0
    BEGIN
      IF EXISTS( SELECT * FROM jrNoteActive WHERE IsCreateTag = 1 AND Tag = 368 And NumActive = @NA ) 
         And NOT EXISTS( SELECT * FROM jrNoteActive WHERE IsCreateTag = 0 And Tag = 368 And NumActive = @NA ) 
        UPDATE jrNoteActive SET 
          IsCreateTag = 0, Note = 'В C101 отказ по новой организации', DateNote = @dtWork 
        WHERE Tag = 368 And NumActive = @NA 
      ELSE 
        INSERT INTO jrNoteActive ( [Tag], [DateNote], [Note], [NumActive], [IsCreateTag] ) 
          VALUES ( 368, @dtWork, 'В C101 отказ по новой организации', @NA, 0 ) 

      UPDATE jrActive SET DopInf = STUFF( DopInf, 23, 1, 'F' ) WHERE NumActive = @NA 
                                  
      INSERT jrJournalActive ( DateIt, SysDate, Note, NumActive, Kategory, Doer )
        VALUES  ( @dtWork, @dtWork, 'В C101 отказ по новой организации', @NA, 'AE', @UserName )
                          
      FETCH Next FROM cr INTO @NA 
    END 
                        
    CLOSE cr 
    DEALLOCATE cr
                        
    COMMIT Transaction
  END Try
  BEGIN Catch
    ROLLBACK Transaction
    DECLARE @ErrMsg VarChar(255)
    SELECT @ErrMsg = ERROR_MESSAGE()
    SELECT @ErrMsg = 'Ошибка при отклонении новой организации - ' + @ErrMsg
    RaisError( @ErrMsg, 16, 1  )
  END Catch                        
END
GO