﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SetTempCost](@dtWork SMALLDATETIME, @NumActive INT) AS
BEGIN 
	DECLARE @NumActiveStr VARCHAR(20)
	IF @NumActive = -1 SET @NumActiveStr = '%' ELSE SET @NumActiveStr = CAST(@NumActive AS VARCHAR(20))
	UPDATE jrActive SET 
		tempcostrub = ISNULL(dbo.CalcActiveCost(jrActive.NumActive ,@dtWork ,@dtWork), 0), 
		tempcost = dbo.CalcCurrencyActiveCost(jrActive.NumActive,@dtWork),
		istrigger = jrActive.istrigger + 1
  	WHERE jrActive.status <> 'E' and jrActive.TypeActive in('V','D','M','E','I','YK','YO') AND jrActive.NumActive LIKE @NumActiveStr
	UPDATE jrActive SET istrigger = 0
  	WHERE jrActive.status <> 'E' and jrActive.TypeActive in('V','D','M','E','I','YK','YO') AND jrActive.NumActive LIKE @NumActiveStr
END
GO